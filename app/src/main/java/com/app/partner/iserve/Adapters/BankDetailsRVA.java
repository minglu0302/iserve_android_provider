package com.app.partner.iserve.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.Pojo.BankDetailsData;
import com.app.partner.iserve.R;
import com.app.partner.iserve.TabActivities.BankBottomSheetFragment;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 20-Mar-17.
 */

public class BankDetailsRVA extends RecyclerView.Adapter<BankDetailsRVA.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "BankDetailsRVA";
    private ArrayList<BankDetailsData> bankDetailsDatas;
    private Typeface fontRegular;
    private Context context;
    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;
    private RefreshBankDetails refreshBankDetails;
    private FragmentManager fragmentManager;

    public BankDetailsRVA(Context context, ArrayList<BankDetailsData> bankDetailsDatas, FragmentManager fragmentManager, RefreshBankDetails refreshBankDetails)
    {
        this.bankDetailsDatas = bankDetailsDatas;
        fontRegular = Utility.getFontRegular(context);
        this.context = context;
        mProgressDailog=Utility.getProcessDialog((Activity) context);
        sessionManager = new SessionManager(context);
        this.fragmentManager = fragmentManager;
        this.refreshBankDetails = refreshBankDetails;

    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView ivCheck;
        TextView tvAccountNoLabel,tvAccountNo,tvAccountHolderLabel,tvAccountHolder;
        LinearLayout llBankDetails;

        public ViewHolder(View itemView)
        {
            super(itemView);

            ivCheck = (ImageView) itemView.findViewById(R.id.ivCheck);
            tvAccountNoLabel = (TextView) itemView.findViewById(R.id.tvAccountNoLabel);
            tvAccountNo = (TextView) itemView.findViewById(R.id.tvAccountNo);
            tvAccountHolderLabel = (TextView) itemView.findViewById(R.id.tvAccountHolderLabel);
            tvAccountHolder = (TextView) itemView.findViewById(R.id.tvAccountHolder);
            llBankDetails = (LinearLayout) itemView.findViewById(R.id.llBankDetails);

            tvAccountNoLabel.setTypeface(fontRegular);
            tvAccountHolderLabel.setTypeface(fontRegular);

            tvAccountNo.setTypeface(fontRegular);
            tvAccountHolder.setTypeface(fontRegular);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BankDetailsRVA.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_bank_details, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.tvAccountNo.setText("xxxxxxxx"+bankDetailsDatas.get(position).getLast4());
        holder.tvAccountHolder.setText(bankDetailsDatas.get(position).getAccount_holder_name());

        if(bankDetailsDatas.get(position).getDefault_account().equals("1"))
        {
            holder.ivCheck.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.ivCheck.setVisibility(View.INVISIBLE);
        }

        holder.llBankDetails.setOnClickListener(this);
        holder.llBankDetails.setTag(holder);

    }


    @Override
    public int getItemCount() {
        return bankDetailsDatas.size();
    }

    @Override
    public void onClick(View v)
    {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId())
        {
            case R.id.llBankDetails:
                //showDialog(position);
                BottomSheetDialogFragment bottomSheetDialogFragment = BankBottomSheetFragment.newInstance(bankDetailsDatas.get(position),refreshBankDetails);
                bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());

                break;

        }
    }

    private void showDialog(final int position)
    {
        AlertDialog.Builder alertDialogBuilder = new  AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.alert_dialog_bank_details,null);
        alertDialogBuilder.setView(view);

        TextView tvAccoutDetails= (TextView) view.findViewById(R.id.tvAccoutDetails);
        TextView tvAccountHolderLabel= (TextView) view.findViewById(R.id.tvAccountHolderLabel);
        TextView tvAccountHolder= (TextView) view.findViewById(R.id.tvAccountHolder);
        TextView tvAccountNoLabel= (TextView) view.findViewById(R.id.tvAccountNoLabel);
        TextView tvAccountNo= (TextView) view.findViewById(R.id.tvAccountNo);
        TextView tvRoutingNoLabel= (TextView) view.findViewById(R.id.tvRoutingNoLabel);
        TextView tvRoutinNo= (TextView) view.findViewById(R.id.tvRoutinNo);
        TextView tvBankNameLabel= (TextView) view.findViewById(R.id.tvBankNameLabel);
        TextView tvBankName= (TextView) view.findViewById(R.id.tvBankName);
        TextView tvCurrencyLabel= (TextView) view.findViewById(R.id.tvCurrencyLabel);
        TextView tvCurrency= (TextView) view.findViewById(R.id.tvCurrency);
        TextView tvCountryLabel= (TextView) view.findViewById(R.id.tvCountryLabel);
        TextView tvCountry= (TextView) view.findViewById(R.id.tvCountry);
        TextView tvMakeDefault= (TextView) view.findViewById(R.id.tvMakeDefault);
        TextView tvDeleteAccount= (TextView) view.findViewById(R.id.tvDeleteAccount);
        ImageView ivCancel= (ImageView) view.findViewById(R.id.ivCancel);

        tvAccountHolderLabel.setTypeface(fontRegular);
        tvAccountNoLabel.setTypeface(fontRegular);
        tvRoutingNoLabel.setTypeface(fontRegular);
        tvBankNameLabel.setTypeface(fontRegular);
        tvCurrencyLabel.setTypeface(fontRegular);
        tvCountryLabel.setTypeface(fontRegular);

        tvMakeDefault.setTypeface(fontRegular);
        tvDeleteAccount.setTypeface(fontRegular);
        tvAccountHolder.setTypeface(fontRegular);
        tvAccountNo.setTypeface(fontRegular);
        tvRoutinNo.setTypeface(fontRegular);
        tvBankName.setTypeface(fontRegular);
        tvCurrency.setTypeface(fontRegular);
        tvCountry.setTypeface(fontRegular);
        tvAccoutDetails.setTypeface(fontRegular);

        tvAccountHolder.setText(bankDetailsDatas.get(position).getAccount_holder_name());
        tvAccountNo.setText("xxxxxxxx"+bankDetailsDatas.get(position).getLast4());
        tvRoutinNo.setText(bankDetailsDatas.get(position).getRouting_number());
        tvBankName.setText(bankDetailsDatas.get(position).getBank_name());
        tvCurrency.setText(bankDetailsDatas.get(position).getCurrency());
        tvCountry.setText(bankDetailsDatas.get(position).getCountry());

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.show();

        tvMakeDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultAndDeleteAccount(position,alertDialog,ServiceUrls.BANK_ACCOUNT_DEFAULT);
            }
        });

        tvDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultAndDeleteAccount(position,alertDialog,ServiceUrls.DELETE_MASTER_BANK);
            }
        });

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    private void defaultAndDeleteAccount(int position, final AlertDialog alertDialog, String url)
    {
        if(Utility.isNetworkAvailable(context))
        {
            mProgressDailog=Utility.getProcessDialog((Activity) context);
            mProgressDailog.setMessage("loading...");
            mProgressDailog.setCancelable(false);
            mProgressDailog.show();

            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("bank_token",bankDetailsDatas.get(position).getAccountId())
                    .build();

            Okhttp_connection.doOkhttpRequest(url, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    alertDialog.dismiss();
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                    Log.d(TAG, "onSuccess: "+result);

                    if(result !=null )
                    {
                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String errFlag = jsonObject.getString("errFlag");
                            String errNum = jsonObject.getString("errNum");
                            String errMsg = jsonObject.getString("errMsg");

                            if(errFlag.equals("0"))
                            {
                                Toast.makeText(context,errMsg,Toast.LENGTH_SHORT).show();
                                refreshBankDetails.onRefresh();
                            }
                            else
                            {
                                Toast.makeText(context,errMsg,Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "respondBankDetails: "+e);
                        }
                    }
                }

                @Override
                public void onError(String error) {
                    alertDialog.dismiss();
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Toast.makeText(context,R.string.network_check,Toast.LENGTH_SHORT).show();
        }
    }

    public interface RefreshBankDetails extends Serializable {
        void onRefresh();
    }

}
