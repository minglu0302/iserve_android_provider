package com.app.partner.iserve.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.app.partner.iserve.CancelationDialog;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by embed on 22/11/16.
 */
public class CancelAdapter extends RecyclerView.Adapter<CancelAdapter.MyViewHolder>
{
    private Activity context;
    private ArrayList<String> mData;
    private ArrayList<ImageView> buttons;
    private int reason=-1;
    private String bid,cid;

    public CancelAdapter(Activity context, ArrayList<String> mData, String bid, String cid)
    {
     this.context=context;
     this.mData=mData;
     buttons=new ArrayList<>();
     this.bid=bid;
     this.cid=cid;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewsd= LayoutInflater.from(context).inflate(R.layout.single_row_cancellation,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(viewsd);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        buttons.add(holder.ivReason);
        holder.tvReason.setText(mData.get(position).toString());
        holder.rl_reason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetAll();
                holder.ivReason.setVisibility(View.VISIBLE);
                reason=position;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvReason;
        private ImageView ivReason;
        private RelativeLayout rl_reason;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvReason= (TextView) itemView.findViewById(R.id.tvReason);
            ivReason= (ImageView) itemView.findViewById(R.id.ivReason);
            rl_reason= (RelativeLayout) itemView.findViewById(R.id.rl_reason);

        }
    }



    private void resetAll() {
        for (int i=0;i<buttons.size();i++)
        {
            buttons.get(i).setVisibility(View.GONE);
        }
    }

    public void submitOnclick(CancelationDialog dialog)
    {
        if(reason!=-1)
        {
            dialog.mAbortAppointment(mData.get(reason),bid,cid);
        }
        else
        {
            Utility.ShowAlert("Please select any reason to cancel the Job",context);
        }

    }

}
