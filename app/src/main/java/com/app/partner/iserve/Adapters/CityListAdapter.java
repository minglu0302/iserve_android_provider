package com.app.partner.iserve.Adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.app.partner.iserve.CityListActivity;
import com.app.partner.iserve.Pojo.City;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by embed on 11/11/16.
 */
public class CityListAdapter extends RecyclerView.Adapter implements View.OnClickListener
{
    private Activity mContext;
    private ArrayList<City> cityList;
    private ArrayList<CheckBox> checkBoxes=new ArrayList<>();
    private String selectedCity,selectedCityId;

    public CityListAdapter(Activity context,ArrayList<City>cityList)
    {
        mContext=context;
        this.cityList=cityList;

    }



    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView cityName;
        private CheckBox checkBox;
        private LinearLayout singleRow;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            Typeface font = Utility.getFontRegular(mContext);

            cityName= (TextView) itemView.findViewById(R.id.category);
            cityName.setTypeface(font);

            checkBox= (CheckBox) itemView.findViewById(R.id.checkbox);
            singleRow= (LinearLayout) itemView.findViewById(R.id.ll_single_row);
        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View viewsd= LayoutInflater.from(mContext).inflate(R.layout.pro_list_single_row,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(viewsd);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        if(holder instanceof MyViewHolder)
        {
            ((MyViewHolder) holder).cityName.setText(cityList.get(position).getCity_Name().toUpperCase());

            checkBoxes.add(((MyViewHolder) holder).checkBox);

            ((MyViewHolder) holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b)
                    {
                        resetAll();
                        ((MyViewHolder) holder).checkBox.setChecked(true);
                        selectedCity=cityList.get(position).getCity_Name();
                        selectedCityId=cityList.get(position).getCity_Id();

                    }else
                    {
                        selectedCity="";
                        selectedCityId="";
                    }
                }
            });

            ((MyViewHolder) holder).singleRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    selectedCity=cityList.get(position).getCity_Name();
                    selectedCityId=cityList.get(position).getCity_Id();

                    Utility.printLog("AAAAAA selected item "+selectedCity);
                    ((CityListActivity)mContext).setResult(selectedCityId,selectedCity);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        Utility.printLog("CITYLIST size "+cityList.size());
        return cityList.size();
    }

    private void resetAll() {
        for (int i=0;i<checkBoxes.size();i++)
        {
            checkBoxes.get(i).setChecked(false);
        }
    }


    @Override
    public void onClick(View v)
    {
        if(v.getId()==R.id.tvDone)
        {
            if(selectedCityId.isEmpty())
            {
                Snackbar.make(v, "Please select a city.", Snackbar.LENGTH_LONG)
                        .setAction("CLOSE", null).show();
            }
            else
            {
                ((CityListActivity)mContext).setResult(selectedCityId,selectedCity);
            }
        }
    }
}
