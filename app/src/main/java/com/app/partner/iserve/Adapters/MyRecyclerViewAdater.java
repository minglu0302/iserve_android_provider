package com.app.partner.iserve.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


import java.util.ArrayList;

import com.app.partner.iserve.R;
import com.app.partner.iserve.TabActivities.ProfileActivity;
import com.app.partner.iserve.Utility.Scaler;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by embed on 1/9/16.
 */
public class MyRecyclerViewAdater extends RecyclerView.Adapter {

    private final double[] dblArray;
    Activity context;
    ArrayList<Uri> imageFile;
    public static final int ITEM_ROW = 0;
    public static final int FOOTER_ROW = 1;
    private int hieght,width;
    private boolean isAddOptionEnabled=false;
    private Dialog dialog;

    //ShipmentMultipleImagesPojos imagesPojos;
    public MyRecyclerViewAdater(Activity context, ArrayList<Uri> imageFile)
    {
        this.context = context;
        this.imageFile = imageFile;

        dblArray = Scaler.getScalingFactor(context);
        hieght= (int) dblArray[0]*150;
        width=(int) dblArray[1]*150;
        //imagesPojos = new ShipmentMultipleImagesPojos();
    }

    public ArrayList<Uri> getImageFile() {
        return imageFile;
    }

    public void setImageFile(ArrayList<Uri> imageFile) {
        this.imageFile = imageFile;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_ROW) {
            View view = LayoutInflater.from(context).inflate(R.layout.scroll_images, parent, false);
            return new MyHolder(view);
        }
        else if (viewType == FOOTER_ROW)
        {
            View view = LayoutInflater.from(context).inflate(R.layout.scroll_images, parent, false);
            return new FooterHolder(view);
        }
        else
            return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyHolder)
        {
            if (imageFile.size() > 0)
            {
                Picasso.with(context).load(imageFile.get(position))
                        .resize(width, hieght)
                        .centerCrop()
                        .placeholder(R.drawable.current_booking_add_photo_btn_off)
                        .into(((MyHolder) holder).iv_add_image);
            }
            ((MyHolder) holder).iv_add_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  (context).chooseImageOperation(position, false);//open gallery view

                    showBigImage(imageFile.get(position));
                }
            });
            ((MyHolder) holder).iv_add_image.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Utility.printLog("this position for deletion: "+position);
                    //delete operation
                    removeAlert(position);
                    return true;
                }
            });
        }
        else if (holder instanceof FooterHolder)
        {
            ((FooterHolder) holder).iv_add_image.setImageDrawable(context.getResources().getDrawable(R.drawable.current_booking_add_photo_btn_off));

            ((FooterHolder) holder).iv_add_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context,"footer clicked: "+imageFile.size(),Toast.LENGTH_SHORT).show();
                   // (context).chooseImageOperation(position, true);//add operation
                    ((ProfileActivity)context).chooseImageOperation(position,((FooterHolder) holder).iv_add_image);//add operation
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == imageFile.size())
        {
            return FOOTER_ROW;
        }
            return ITEM_ROW;
    }

    @Override
    public int getItemCount() {


            if (imageFile == null) {
                return 0;
            }
            if (imageFile.size() == 4)      //It will show that, if user added 4 photos, then extra adding photo option will be hide.
            {
                return 4;
            }
            if(isAddOptionEnabled)
            {
                return imageFile.size()+1;
            }else
            {
                return imageFile.size();
            }

    }

    public static class MyHolder extends RecyclerView.ViewHolder
    {
        ImageView iv_add_image;
        public MyHolder(View itemView) {
            super(itemView);
            iv_add_image = (ImageView) itemView.findViewById(R.id.iv_add_image);
        }
    }

    public static class FooterHolder extends RecyclerView.ViewHolder
    {
        ImageView iv_add_image;
        public FooterHolder(View itemView) {
            super(itemView);
            iv_add_image = (ImageView) itemView.findViewById(R.id.iv_add_image);
        }
    }

    public void setAddOptionEnabled(boolean flag)
    {
        isAddOptionEnabled=flag;
    }

    public void showBigImage(Uri url)
    {
        dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_imageview);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView bigImageView= (ImageView) dialog.findViewById(R.id.ivBigImage);

        Picasso.with(context)
                .load(url)
                .into(bigImageView);

        dialog.show();

    }

    public void removeAlert(final int position)
    {
        AlertDialog.Builder builder= Utility.getAlertDialogBuilder(context);
        builder.setTitle("Alert");
        builder.setMessage("Are you sure you want to delete this image ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //imageFile.remove(position);
                //((ProfileActivity)context).updateMasterProfile(imageFile.size());
                ((ProfileActivity)context).removeImageUri(position);
                notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}
