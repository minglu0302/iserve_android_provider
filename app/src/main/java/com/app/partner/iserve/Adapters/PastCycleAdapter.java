package com.app.partner.iserve.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.Pojo.PastCycle;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Utility;

import java.util.ArrayList;

/**
 * Created by embed on 8/12/16.
 */
public class PastCycleAdapter extends RecyclerView.Adapter<PastCycleAdapter.MyViewHolder>
{

    private Context mContext;
    private ArrayList<PastCycle> mDataset;
    private String Value;

    public PastCycleAdapter(Context mContext, ArrayList<PastCycle> mDataset) {
        this.mContext = mContext;
        this.mDataset = mDataset;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewsd= LayoutInflater.from(mContext).inflate(R.layout.single_row_past_cycle,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(viewsd);
        return myViewHolder;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvStartTime.setText(mDataset.get(position).getStart_date());
        holder.tvEndTime.setText(mDataset.get(position).getPay_date());
        holder.tvCompletedBookings.setText(mDataset.get(position).getNo_of_bookings());
        if(!"".equals(mDataset.get(position).getBooking_earn()))
        {
            Value = String.format("%.2f", Double.parseDouble(mDataset.get(position).getBooking_earn()));
            holder.tvEarnings.setText(AppConstants.CURRENCY+" "+Value);
        }
        if(!"".equals(mDataset.get(position).getPay_amount()))
        {
            Value = String.format("%.2f", Double.parseDouble(mDataset.get(position).getPay_amount()));
            holder.tvPaidAmtValue.setText(AppConstants.CURRENCY+" "+Value);
        }

        holder.singleRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                mCyclepopUp(mDataset.get(position));
                Toast.makeText(mContext, "clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvStartTime,tvEndTime,tvEarnings,tvCompletedBookings,tvPaidAmtValue,tvStartTimeHeader,tvEndTimeHeader,tvCompltdBookingsHeader,tvEarningHeader,tvPaidAmtHeader;
        private LinearLayout singleRow;

        public MyViewHolder(View itemView) {
            super(itemView);

            Typeface font= Utility.getFontRegular(mContext);
            Typeface fontBold= Utility.getFontBold(mContext);


            tvStartTime= (TextView) itemView.findViewById(R.id.tvStartTimeValue);
            tvStartTime.setTypeface(font);

            tvEndTime= (TextView) itemView.findViewById(R.id.tvEndTimeValue);
            tvEndTime.setTypeface(font);

            tvEarnings= (TextView) itemView.findViewById(R.id.tvEarningValue);
            tvEarnings.setTypeface(font);

            tvCompletedBookings= (TextView) itemView.findViewById(R.id.tvCompltdBookingsValue);
            tvCompletedBookings.setTypeface(font);

            tvPaidAmtValue= (TextView) itemView.findViewById(R.id.tvPaidAmtValue);
            tvPaidAmtValue.setTypeface(font);

            tvStartTimeHeader= (TextView) itemView.findViewById(R.id.tvStartTimeHeader);
            tvStartTimeHeader.setTypeface(font);

            tvEndTimeHeader= (TextView) itemView.findViewById(R.id.tvEndTimeHeader);
            tvEndTimeHeader.setTypeface(font);

            tvCompltdBookingsHeader= (TextView) itemView.findViewById(R.id.tvCompltdBookingsHeader);
            tvCompltdBookingsHeader.setTypeface(font);

            tvEarningHeader= (TextView) itemView.findViewById(R.id.tvEarningHeader);
            tvEarningHeader.setTypeface(font);

            tvPaidAmtHeader= (TextView) itemView.findViewById(R.id.tvPaidAmtHeader);
            tvPaidAmtHeader.setTypeface(font);

            singleRow= (LinearLayout) itemView.findViewById(R.id.singleItem);
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mCyclepopUp(PastCycle pastCycle)
    {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.past_cycle_pop_up);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView tvStartTime = (TextView) dialog.findViewById(R.id.tvStartTimeValue);
        TextView tvEndTime = (TextView) dialog.findViewById(R.id.tvEndTimeValue);
        TextView tvAvgRating = (TextView) dialog.findViewById(R.id.tvAvgRatingValue);
        TextView tvAccetanceRate = (TextView) dialog.findViewById(R.id.tvAcceptanceRateValue);
        TextView tvTotalBookings = (TextView) dialog.findViewById(R.id.tvTotalBookingsValue);
        TextView tvEarnings = (TextView) dialog.findViewById(R.id.tvEarningValue);
        TextView tvOpenningBal = (TextView) dialog.findViewById(R.id.tvOpenningBalValue);
        TextView tvClosedBal = (TextView) dialog.findViewById(R.id.tvClosedBalValue);
        TextView tvCompletedBookings = (TextView) dialog.findViewById(R.id.tvCompltdBookingsValue);
        TextView tvRejectedBookings = (TextView) dialog.findViewById(R.id.tvRejectedBookingsValue);
        TextView tvIgnoredBookings = (TextView) dialog.findViewById(R.id.tvIgnoredBookingsValue);
        TextView tvCancelledBookings = (TextView) dialog.findViewById(R.id.tvCancelledBookingsValue);

        tvStartTime.setText(pastCycle.getStart_date());

        tvEndTime.setText(pastCycle.getPay_date());

        tvAvgRating.setText(pastCycle.getRev_rate());

        tvAccetanceRate.setText(pastCycle.getAcpt_rate()+"%");

        tvTotalBookings.setText(pastCycle.getTot_bookings());

        if(!"".equals(pastCycle.getBooking_earn()))
        {
            Value = String.format("%.2f", Double.parseDouble(pastCycle.getBooking_earn()));
            tvEarnings.setText( AppConstants.CURRENCY+" "+Value);
        }
        if(!"".equals(pastCycle.getOpening_balance()))
        {
            Value = String.format("%.2f", Double.parseDouble(pastCycle.getOpening_balance()));
            tvOpenningBal.setText( AppConstants.CURRENCY+" "+Value);
        }
        if(!"".equals(pastCycle.getClosing_balance()))
        {
            Value = String.format("%.2f", Double.parseDouble(pastCycle.getClosing_balance()));
            tvClosedBal.setText( AppConstants.CURRENCY+" "+Value);
        }

        tvCompletedBookings.setText(pastCycle.getTot_accepted());

        tvRejectedBookings.setText(pastCycle.getTotal_reject());

        tvIgnoredBookings.setText(pastCycle.getTotal_ignore());

        tvCancelledBookings.setText(pastCycle.getTot_cancel());

        dialog.show();
    }

}
