package com.app.partner.iserve.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.R;
import com.app.partner.iserve.TabActivities.IhaveArrivedActivity;
import com.app.partner.iserve.TabActivities.InvoiceActivity;
import com.app.partner.iserve.TabActivities.JobAcceptedActivity;
import com.app.partner.iserve.TabActivities.JobCompletedActivity;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Utility;


/**
 * Created by embed on 13/7/16.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>
{
    private ArrayList<Appointments> mDataset=new ArrayList<Appointments>();
    Activity context;
    private Dialog dialog;
    private LinearLayout ll_service_Container;
    public Typeface font,fontBold;

    public RecyclerAdapter(ArrayList<Appointments> mDataset,Activity context) {
        this.mDataset = mDataset;
        this.context=context;


        font=Utility.getFontRegular(context);
        fontBold=Utility.getFontBold(context);

        Utility.printLog("PK RecyclerAdapter called"+this.mDataset.size());

    }

    public class MyViewHolder extends  RecyclerView.ViewHolder
    {
        public TextView tvHeader,
                        tvJobType,
                        tvJobStatus,
                        tvName,
                        tvAddress,
                        tvDistance,
                        tvJobId,
                        tvDateForLater;
        public LinearLayout singleItem;



        public MyViewHolder(View itemView)
        {
            super(itemView);


            tvHeader= (TextView) itemView.findViewById(R.id.itemHeader);
            tvHeader.setTypeface(fontBold);

            tvJobType= (TextView) itemView.findViewById(R.id.tvJobtype);
            tvJobType.setTypeface(fontBold);

            tvJobStatus= (TextView) itemView.findViewById(R.id.tvJobStatus);
            tvJobStatus.setTypeface(font);

            tvName= (TextView) itemView.findViewById(R.id.tvName);
            tvName.setTypeface(font);

            tvAddress= (TextView) itemView.findViewById(R.id.tvAddress);
            tvAddress.setTypeface(font);

            tvDistance= (TextView) itemView.findViewById(R.id.tvDistance);
            tvDistance.setTypeface(font);

            tvDateForLater= (TextView) itemView.findViewById(R.id.tvDateForLater);
            tvDateForLater.setTypeface(font);

            singleItem= (LinearLayout) itemView.findViewById(R.id.singleRow);
            tvJobId= (TextView) itemView.findViewById(R.id.tvJobId);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View viewsd= LayoutInflater.from(context).inflate(R.layout.single_list_item_for_jobs,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(viewsd);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        if(mDataset.get(position).isHeader())
        {
            if(mDataset.get(position).getBookType().equals("2"))
            {
                holder.tvHeader.setText(context.getResources().getString(R.string.SCHEDULED_JOBS));
            }
            else
            {
                holder.tvHeader.setText(context.getResources().getString(R.string.CURRENT_JOBS));
            }

            holder.tvHeader.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.tvHeader.setVisibility(View.GONE);
        }

        if(mDataset.get(position).getBookType().equals("2"))
        {
            holder.tvDateForLater.setVisibility(View.VISIBLE);

            SimpleDateFormat format = new SimpleDateFormat("dd MMM hh:mm a");
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            try {
                Date date=format1.parse(mDataset.get(position).getApntDt());
                String dateString=format.format(date);
                holder.tvDateForLater.setText(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        else
        {


            holder.tvDateForLater.setVisibility(View.GONE);
        }

        holder.tvName.setText(mDataset.get(position).getFname());
        holder.tvAddress.setText(mDataset.get(position).getAddrLine1()+" "+mDataset.get(position).getAddrLine2());

        holder.tvJobStatus.setText(mDataset.get(position).getStatusMsg());
        holder.tvJobId.setText("Job ID: "+mDataset.get(position).getBid());

        if(mDataset.get(position).getStatus().equals("7") || mDataset.get(position).getStatus().equals("4"))
        {
            if(mDataset.get(position).getStatus().equals("4"))
            {
                holder.tvDistance.setText(AppConstants.CURRENCY+" "+mDataset.get(position).getCancelAmt());
            }else
            {
                holder.tvDistance.setText(AppConstants.CURRENCY+" "+mDataset.get(position).getTotal_pro());
            }

            holder.tvJobType.setText(mDataset.get(position).getApntTime()+"-"+mDataset.get(position).getCat_name());
        }
        else
        {
            float dist=0;
            if(mDataset.get(position).getDistance()!=null && !mDataset.get(position).getDistance().isEmpty())
            {
                dist=Float.valueOf(mDataset.get(position).getDistance());
                dist=dist/1000;
            }
            if(mDataset.get(position).getBookType().equals("2"))
            {
                holder.tvJobType.setText(mDataset.get(position).getCat_name());
            }
            else
            {
                holder.tvJobType.setText("ASAP-"+mDataset.get(position).getCat_name());
            }

            holder.tvDistance.setText(dist+" Kms Away");
        }

        holder.singleItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=new Bundle();
                bundle.putSerializable("APPOINTMENT",mDataset.get(position));
                Intent intent = null;

                switch (mDataset.get(position).getStatus())
                {
                    case "2":
                        intent=new Intent(context, JobAcceptedActivity.class);
                        break;

                    case "5":
                        intent=new Intent(context, IhaveArrivedActivity.class);
                        break;

                    case "21":
                        intent=new Intent(context, JobCompletedActivity.class);
                        break;

                    case "6":
                        intent=new Intent(context, JobCompletedActivity.class);
                        break;

                    case "22":
                        intent=new Intent(context, InvoiceActivity.class);
                        break;

                    case "7":
                        mShowInvoicePopUp(mDataset.get(position));
                        break;

                    default:
                       // intent=new Intent(context, HomeActivity.class);
                }
                if(intent!=null)
                {
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.finish();
                }


            }
        });
    }

    @Override
    public int getItemCount()
    {
        Utility.printLog("Pk mdata size "+mDataset.size());
        return mDataset.size();

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mShowInvoicePopUp(Appointments appointments)
    {
        dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.invoice_pop_up);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView custName= (TextView) dialog.findViewById(R.id.tvCustomerName);
        custName.setTypeface(fontBold);

        TextView custMob= (TextView) dialog.findViewById(R.id.tvCustomerMobile);
        custMob.setTypeface(fontBold);

        TextView visitFee= (TextView) dialog.findViewById(R.id.tvVisitFee);
        visitFee.setTypeface(font);

        TextView timeFee= (TextView) dialog.findViewById(R.id.tvTimeFee);
        timeFee.setTypeface(font);

        TextView matFee= (TextView) dialog.findViewById(R.id.tvMaterialFee);
        matFee.setTypeface(font);

        TextView miscFee= (TextView) dialog.findViewById(R.id.tvMisc);
        miscFee.setTypeface(font);

        TextView subTotal= (TextView) dialog.findViewById(R.id.tvSubTotal);
        subTotal.setTypeface(font);

        TextView proDiscount= (TextView) dialog.findViewById(R.id.tvProviderDiscount);
        proDiscount.setTypeface(font);

        TextView discountCoupon= (TextView) dialog.findViewById(R.id.tvDiscount);
        discountCoupon.setTypeface(font);

        TextView total= (TextView) dialog.findViewById(R.id.tvTotal);
        total.setTypeface(font);

        TextView paymentType= (TextView) dialog.findViewById(R.id.tvPaymentMethod);
        paymentType.setTypeface(font);

        TextView notes= (TextView) dialog.findViewById(R.id.tvNotes);
        notes.setTypeface(font);

        TextView tvCustAddress= (TextView) dialog.findViewById(R.id.tvCustAddress);
        tvCustAddress.setTypeface(font);

        TextView tvCustHeader= (TextView) dialog.findViewById(R.id.tvCustHeader);
        tvCustHeader.setTypeface(fontBold);

        TextView tvTitleVisitFee= (TextView) dialog.findViewById(R.id.tvTitleVisitFee);
        tvTitleVisitFee.setTypeface(font);

        TextView tvTitleTimeFee= (TextView) dialog.findViewById(R.id.tvTitleTimeFee);
        tvTitleTimeFee.setTypeface(font);

        TextView tvTitleMaterialFee= (TextView) dialog.findViewById(R.id.tvTitleMaterialFee);
        tvTitleMaterialFee.setTypeface(font);

        TextView tvTitleMisc= (TextView) dialog.findViewById(R.id.tvTitleMisc);
        tvTitleMisc.setTypeface(font);

        TextView tvTitleSubTotal= (TextView) dialog.findViewById(R.id.tvTitleSubTotal);
        tvTitleSubTotal.setTypeface(font);

        TextView tvTitleProviderDiscount= (TextView) dialog.findViewById(R.id.tvTitleProviderDiscount);
        tvTitleProviderDiscount.setTypeface(font);

        TextView tvTitleDiscount= (TextView) dialog.findViewById(R.id.tvTitleDiscount);
        tvTitleDiscount.setTypeface(font);

        TextView tvTitleTotal= (TextView) dialog.findViewById(R.id.tvTitleTotal);
        tvTitleTotal.setTypeface(fontBold);

        TextView tvTitlePaymentMethod= (TextView) dialog.findViewById(R.id.tvTitlePaymentMethod);
        tvTitlePaymentMethod.setTypeface(font);

        TextView tvSignatureHeader= (TextView) dialog.findViewById(R.id.tvSignatureHeader);
        tvSignatureHeader.setTypeface(fontBold);

        TextView tvNotesHeader= (TextView) dialog.findViewById(R.id.tvNotesHeader);
        tvNotesHeader.setTypeface(fontBold);

        ImageView sign= (ImageView) dialog.findViewById(R.id.signature_pad);

        Button button= (Button) dialog.findViewById(R.id.btnClose);
        button.setTypeface(fontBold);

        ll_service_Container=(LinearLayout)dialog.findViewById(R.id.ll_service_Container);

        mSetSelectedServices(appointments);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
            }
        });

        custName.setText(appointments.getFname());

        custMob.setText(appointments.getPhone());

        tvCustAddress.setText(appointments.getAddrLine1());

        float fvisitFee=0;
        if(!appointments.getVisit_amount().isEmpty())
        {
            fvisitFee=Float.parseFloat(appointments.getVisit_amount());
        }
        visitFee.setText(AppConstants.CURRENCY+" "+fvisitFee);

        float ftimeFee=0,appt_duration=0;
        if(!appointments.getPrice_per_min().isEmpty() && !appointments.getAppt_duration().isEmpty())
        {
            ftimeFee=(Float.parseFloat(appointments.getPrice_per_min()))*(Float.parseFloat(appointments.getAppt_duration()));
        }
        timeFee.setText(AppConstants.CURRENCY+" "+ftimeFee);

        float fmatFee=0;
        if(!appointments.getMat_fees().isEmpty())
        {
            fmatFee=Float.parseFloat(appointments.getMat_fees());
        }
        matFee.setText(AppConstants.CURRENCY+" "+fmatFee);

        float fmiscFee=0;
        if(!appointments.getMisc_fees().isEmpty())
        {
            fmiscFee=Float.parseFloat(appointments.getMisc_fees());
        }
        miscFee.setText(AppConstants.CURRENCY+" "+fmiscFee);

        float fproDiscount=0;
        if(!appointments.getPro_disc().isEmpty())
        {
            fproDiscount=Float.parseFloat(appointments.getPro_disc());
        }
        proDiscount.setText(AppConstants.CURRENCY+" "+fproDiscount);

        float fdiscountCoupon=0;
        if(!appointments.getCoupon_discount().isEmpty())
        {
            fdiscountCoupon=Float.parseFloat(appointments.getCoupon_discount());
        }
        discountCoupon.setText(AppConstants.CURRENCY+" "+fdiscountCoupon);

        float ftotal=0;
        if(!appointments.getTotal_pro().isEmpty())
        {
            ftotal=Float.parseFloat(appointments.getTotal_pro());
        }
        total.setText(AppConstants.CURRENCY+" "+ftotal);


        if(appointments.getPayment_type().equals("1"))
        {
            paymentType.setText(context.getResources().getString(R.string.CASH));
        }else
        {
            paymentType.setText(context.getResources().getString(R.string.CARD));
        }

        notes.setText(appointments.getPro_notes());

        float fsubTotal=fvisitFee + ftimeFee + fmatFee + fmiscFee;
        subTotal.setText(AppConstants.CURRENCY+" "+fsubTotal);

        String url=appointments.getSign_url();

        if(!url.isEmpty())
        {
            Picasso.with(context)
                    .load(url)
                    .into(sign);
        }
        dialog.show();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mSetSelectedServices(Appointments appointments)
    {
        int size=appointments.getServices().size();

        if(size>0)
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_inflate_services,null);

            TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
            TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
            TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
            LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);

            tvSelectedService.setTypeface(font);
            tvSelectedServicePrice.setTypeface(font);
            tvNojobSelected.setTypeface(font);

            for(int i=0;i<size;i++)
            {
                tvSelectedServicePrice.setText(AppConstants.CURRENCY+" "+appointments.getServices().get(i).getSprice());
                tvSelectedService.setText(appointments.getServices().get(i).getSname());
                tvNojobSelected.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);

                ll_service_Container.addView(view);
            }
        }else
        {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_inflate_services,null);

            TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
            TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
            TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
            LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);

            tvSelectedService.setTypeface(font);
            tvSelectedServicePrice.setTypeface(font);
            tvNojobSelected.setTypeface(font);

            tvNojobSelected.setVisibility(View.VISIBLE);
            layout.setVisibility(View.GONE);
            ll_service_Container.addView(view);
        }

    }
}
