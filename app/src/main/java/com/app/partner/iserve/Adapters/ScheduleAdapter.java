package com.app.partner.iserve.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.app.partner.iserve.Pojo.Slot;
import com.app.partner.iserve.R;
import com.app.partner.iserve.TabActivities.LaterBookingDetails;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by embed on 17/11/16.
 */
public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyViewHolder>
{

    private ArrayList<Slot> mDataset=new ArrayList<Slot>();
    Activity context;
    private SimpleDateFormat sdf,displayFormat;
    private String startTime,endTime;

    public ScheduleAdapter(ArrayList<Slot> mDataset, Activity context) {
        this.mDataset = mDataset;
        this.context = context;
        sdf = new SimpleDateFormat("H:mm");
        displayFormat = new SimpleDateFormat("h:mm a");
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder
    {
        public TextView tvTitleStatus,
                tvStatus,
                tvTitleStartTime,
                tvTitleEndTime,
                tvEndTime,
                tvStartTime
                ;
        public RelativeLayout singleItem;
        public LinearLayout ll_Title;
        public Typeface font,fontBold;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            font=Utility.getFontRegular(context);
            fontBold=Utility.getFontBold(context);

            tvTitleStatus= (TextView) itemView.findViewById(R.id.tvTitleStatus);
            tvTitleStatus.setTypeface(font);

            tvStatus= (TextView) itemView.findViewById(R.id.tvStatus);
            tvStatus.setTypeface(font);

            tvTitleStartTime= (TextView) itemView.findViewById(R.id.tvTitleStartTime);
            tvTitleStartTime.setTypeface(font);

            tvTitleEndTime= (TextView) itemView.findViewById(R.id.tvTitleEndTime);
            tvTitleEndTime.setTypeface(font);

            tvEndTime= (TextView) itemView.findViewById(R.id.tvEndTime);
            tvEndTime.setTypeface(font);

            tvStartTime= (TextView) itemView.findViewById(R.id.tvStartTime);
            tvStartTime.setTypeface(font);

            singleItem= (RelativeLayout) itemView.findViewById(R.id.singleRow);
            ll_Title= (LinearLayout) itemView.findViewById(R.id.ll_Title);

        }
    }

    @Override
    public ScheduleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewsd= LayoutInflater.from(context).inflate(R.layout.single_row_slot_calender,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(viewsd);
        return  myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        try
        {
            startTime=displayFormat.format(sdf.parse(mDataset.get(position).getStart_dt()));
            endTime=displayFormat.format(sdf.parse(mDataset.get(position).getEnd_dt()));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.tvStartTime.setText(startTime);
        holder.tvEndTime.setText(endTime);

        if(mDataset.get(position).getBooked().equals("1"))
        {
            holder.tvStatus.setText("FREE");
            holder.ll_Title.setBackground(context.getResources().getDrawable(R.drawable.top_cornered_rectangle_shape));
        }else
        {
            holder.tvStatus.setText("BOOKED");
            holder.ll_Title.setBackground(context.getResources().getDrawable(R.drawable.top_cornered_rectangle_shape_red));
        }

        holder.singleItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LaterBookingDetails.class);
                intent.putExtra("SlotDetails",mDataset.get(position));
                context.startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }


}
