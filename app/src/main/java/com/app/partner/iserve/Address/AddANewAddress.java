package com.app.partner.iserve.Address;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.Utility.Utility;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.SessionManager;

public class AddANewAddress extends AppCompatActivity implements OnMapReadyCallback,View.OnClickListener {

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 99;
    private APICallerForAddress apiCallerForAddress = new APICallerForAddress();
    private TextView address_tv,tvHelperTextBottom;
    private Resources resources;
    private TextView action_bar_title_tv;
    private RelativeLayout action_bar_back_rl;
    private RelativeLayout action_bar_next_rl;
    private Button confirm_location_btn;
    private GoogleMap googleMap;
    private boolean isItFromPlace = false;
    private AddressPojo addressPojo = new AddressPojo();
    private SessionManager sessionManager;
    private Typeface font,fontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_anew_address);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        intitializeActionBar();

        address_tv = (TextView) findViewById(R.id.address_tv);
        address_tv.setTypeface(font);

        tvHelperTextBottom = (TextView) findViewById(R.id.tvHelperTextBottom);
        tvHelperTextBottom.setTypeface(font);

        confirm_location_btn= (Button) findViewById(R.id.confirm_location_btn);
        confirm_location_btn.setTypeface(fontBold);

        confirm_location_btn.setOnClickListener(this);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



    }

    private void intitializeActionBar() {
        font= Utility.getFontRegular(AddANewAddress.this);
        fontBold=Utility.getFontBold(AddANewAddress.this);

        resources=getResources();
        action_bar_title_tv = (TextView) findViewById(R.id.action_bar_title_tv);
        action_bar_title_tv.setTypeface(fontBold);

        action_bar_title_tv.setText(resources.getString(R.string.add_new_address));

        action_bar_back_rl = (RelativeLayout) findViewById(R.id.action_bar_back_rl);
        action_bar_next_rl = (RelativeLayout) findViewById(R.id.action_bar_next_rl);
        action_bar_back_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        action_bar_next_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPlace();
            }
        });
    }

    private void findPlace() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if(requestCode==PLACE_AUTOCOMPLETE_REQUEST_CODE)
        {
//            isreturnfrom=true;
            if(resultCode == RESULT_OK)
            {

                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("TAG123", "Place: " + place.getName()
                    +"\n"+place.getAddress()
                );

                isItFromPlace = true;

                address_tv.setText(place.getAddress());

                LatLng MOUNTAIN_VIEW = new LatLng(place.getLatLng().latitude,place.getLatLng().longitude);

//                googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(MOUNTAIN_VIEW)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                       // .bearing(90)                // Sets the orientation of the camera to east
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if(place.getLatLng().latitude!=0.0 && place.getLatLng().longitude!=0.0)
                {
                    addressPojo.setPLACE_ID(place.getId());
                    addressPojo.setLATITUDE(place.getLatLng().latitude+"");
                    addressPojo.setLONGITUDE(place.getLatLng().longitude+"");
                    addressPojo.setFORMATED_ADDRESS((String) place.getAddress());
                }



            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("TAG123", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onMapReady(final GoogleMap gMap) {

        sessionManager=new SessionManager(AddANewAddress.this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap=gMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                return false;
            }
        });

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                Log.d("TAG123", googleMap.getCameraPosition().target.toString());
            }
        });

       /* double lat=0.0,lng=0.0;
        if(!sessionManager.getProviderCurrentlat().isEmpty())
        {
            lat=Double.parseDouble(sessionManager.getProviderCurrentlat());
        }
        if(!sessionManager.getProviderCurrentlong().isEmpty())
        {
            lat=Double.parseDouble(sessionManager.getProviderCurrentlong());
        }*/
        LatLng MOUNTAIN_VIEW = new LatLng(Double.parseDouble(sessionManager.getProviderCurrentlat()),
                Double.parseDouble(sessionManager.getProviderCurrentlong()));


        googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

// Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(MOUNTAIN_VIEW)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                //.bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                if (!isItFromPlace) {
                    address_tv.setText(resources.getString(R.string.fetching_location));
                }
            }
        });

        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {

            }
        });

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (!isItFromPlace) {
                    apiCallerForAddress.setForAddress(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude, new AddressProvider() {
                        @Override
                        public void onAddress(final LocationPlaces places) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("TAG123", googleMap.getCameraPosition().target.toString() + "\n" + places.getAddress());
                                    address_tv.setText(places.getAddress());
                                    addressPojo.setPLACE_ID(places.getPlace_id());
                                    addressPojo.setLATITUDE(places.getLatitude()+"");
                                    addressPojo.setLONGITUDE(places.getLongitude()+"");
                                    addressPojo.setFORMATED_ADDRESS(places.getAddress());
                                }
                            });
                        }

                        @Override
                        public void onError(final String error) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("TAG123", googleMap.getCameraPosition().target.toString() + "\n" + error);
                                    address_tv.setText(error);
                                }
                            });
                        }
                    });
                }else {
                    isItFromPlace=false;
                }
            }
        });


        if(getIntent().hasExtra("ADDRESS_INFO")) {
            addressPojo = (AddressPojo) getIntent().getSerializableExtra("ADDRESS_INFO");
            isItFromPlace = true;
            address_tv.setText(addressPojo.getFORMATED_ADDRESS());

            if(addressPojo.getLATITUDE()!=null && addressPojo.getLONGITUDE()!=null) {

                LatLng MOUNTAIN_VIEW1 = new LatLng(Double.parseDouble(addressPojo.getLATITUDE()), Double.parseDouble(addressPojo.getLONGITUDE()));

//                googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

                CameraPosition cameraPosition1 = new CameraPosition.Builder()
                        .target(MOUNTAIN_VIEW)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                        .bearing(0)                // Sets the orientation of the camera to east
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
                address_tv.setText(addressPojo.getFORMATED_ADDRESS());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_location_btn:
                if(addressPojo.getLATITUDE()!=null && addressPojo.getLONGITUDE()!=null)
                {
                    Intent intent = new Intent(this, SaveAddressActivity.class);
                    intent.putExtra("ADDRESS_INFO", addressPojo);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(AddANewAddress.this, resources.getString(R.string.plz_select_your_address), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
