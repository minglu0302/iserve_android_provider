package com.app.partner.iserve.Address;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.partner.iserve.Pojo.Addlist;
import com.app.partner.iserve.Pojo.GetProviderAddress;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class AddSlotsActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,AdapterView.OnItemSelectedListener{

    private static final int ADDRESS_REQUEST = 420;
    private LinearLayout ll_location,ll_repeate_mode_types,llEnd_date;
    private AddressPojo addressPojo;
    private TextView selectedAddress;
    private double[] latlng=new double[2];
    private ImageView ivBackBtn;
    private TextView mToolbar_title,tvLocHeader;
    private EditText etRadiusOfOperation,
            etSetRate,
            etSlots,
            etSlotTime;

    private TextView  tvStartDate,
            tvEndDate,
            tvStartTime,
            tvEverydays,
            tvWeekdays,
            tvWeekends,
            tvTitleRadiusOfOperation,
            tvTitleMiles,
            tvTitleSetRate,
            tvCurrencyTitle,
            tvTitleSlots,
            tvTitleSlotTime,
            tvConfigScheduleTitle,
            tvTitleStartDate,
            tvTitleEndDate,
            tvTitleStartTime,
            tvTitleSlotCreated;

    private Button btnRepeatMode,btnNonRepeatMode,btnConfirm;
    private int datePickerVarID=0;
    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;
    private int mStartHour,mStartminut;
    private String TAG="AddSlotsActivity";
    private Typeface fontBold,font;
    private ArrayList address_list=new ArrayList();
    private String mStartTime;
    private String[] slot_time={"30","60","90","150"};
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_slots);
        sessionManager=new SessionManager(AddSlotsActivity.this);
        initLayoutId();
        mGetProviderAddress();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void initLayoutId()
    {
        font= Utility.getFontRegular(AddSlotsActivity.this);
        fontBold=Utility.getFontBold(AddSlotsActivity.this);

        ll_location=(LinearLayout)findViewById(R.id.ll_location);
        ivBackBtn= (ImageView) findViewById(R.id.ivBackbtn);
        ivBackBtn.setVisibility(View.VISIBLE);

        selectedAddress=(TextView)findViewById(R.id.selectedAddress);
        selectedAddress.setTypeface(font);

        mToolbar_title = (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);
        mToolbar_title.setText(getResources().getString(R.string.ADD_SLOTS));
        mToolbar_title.setVisibility(View.VISIBLE);

        tvLocHeader = (TextView) findViewById(R.id.tvLocHeader);
        tvLocHeader.setTypeface(font);

        etRadiusOfOperation=(EditText)findViewById(R.id.etRadiusOfOperation);
        etRadiusOfOperation.setTypeface(font);

        etSetRate=(EditText)findViewById(R.id.etSetRate);
        etSetRate.setTypeface(font);

        etSlots=(EditText)findViewById(R.id.etSlots);
        etSlots.setTypeface(font);

        etSlotTime=(EditText)findViewById(R.id.etSlotTime);
        etSlots.setTypeface(font);

        tvStartDate=(TextView)findViewById(R.id.tvStartDate);
        tvStartDate.setTypeface(font);

        tvEndDate=(TextView)findViewById(R.id.tvEndDate);
        tvEndDate.setTypeface(font);

        tvStartTime=(TextView)findViewById(R.id.tvStartTime);
        tvStartTime.setTypeface(font);

        btnRepeatMode=(Button)findViewById(R.id.btnRepeatMode);
        btnRepeatMode.setTypeface(fontBold);

        btnNonRepeatMode=(Button)findViewById(R.id.btnNonRepeatMode);
        btnNonRepeatMode.setTypeface(fontBold);

        btnConfirm=(Button)findViewById(R.id.btnConfirm);
        btnConfirm.setTypeface(fontBold);

        tvEverydays=(TextView)findViewById(R.id.btnEverydays);
        tvEverydays.setTypeface(font);

        tvWeekdays=(TextView)findViewById(R.id.btnWeekdays);
        tvWeekdays.setTypeface(font);

        tvWeekends=(TextView)findViewById(R.id.btnWeekends);
        tvWeekends.setTypeface(font);

        tvTitleRadiusOfOperation=(TextView)findViewById(R.id.tvTitleRadiusOfOperation);
        tvTitleRadiusOfOperation.setTypeface(font);

        tvTitleMiles=(TextView)findViewById(R.id.tvTitleMiles);
        tvTitleMiles.setTypeface(font);

        tvTitleSetRate=(TextView)findViewById(R.id.tvTitleSetRate);
        tvTitleSetRate.setTypeface(font);

        tvCurrencyTitle=(TextView)findViewById(R.id.tvCurrencyTitle);
        tvCurrencyTitle.setTypeface(font);

        tvTitleSlots=(TextView)findViewById(R.id.tvTitleSlots);
        tvTitleSlots.setTypeface(font);

        tvTitleSlotTime=(TextView)findViewById(R.id.tvTitleSlotTime);
        tvTitleSlotTime.setTypeface(font);

        tvConfigScheduleTitle=(TextView)findViewById(R.id.tvConfigScheduleTitle);
        tvConfigScheduleTitle.setTypeface(font);

        tvTitleStartDate=(TextView)findViewById(R.id.tvTitleStartDate);
        tvTitleStartDate.setTypeface(font);

        tvTitleEndDate=(TextView)findViewById(R.id.tvTitleEndDate);
        tvTitleEndDate.setTypeface(font);

        tvTitleStartTime=(TextView)findViewById(R.id.tvTitleStartTime);
        tvTitleStartTime.setTypeface(font);

        tvTitleSlotCreated=(TextView)findViewById(R.id.tvTitleSlotCreated);
        tvTitleSlotCreated.setTypeface(font);

        ll_repeate_mode_types=(LinearLayout)findViewById(R.id.ll_repeate_mode_types);
        llEnd_date=(LinearLayout)findViewById(R.id.llEnd_date);

        ll_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddSlotsActivity.this,SelectAddressActivity.class);
                startActivityForResult(intent,ADDRESS_REQUEST);
            }
        });

        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnRepeatMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRepeatMode.setSelected(true);
                btnNonRepeatMode.setSelected(false);
                ll_repeate_mode_types.setVisibility(View.VISIBLE);
                llEnd_date.setVisibility(View.VISIBLE);
            }
        });

        btnNonRepeatMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRepeatMode.setSelected(false);
                btnNonRepeatMode.setSelected(true);
                ll_repeate_mode_types.setVisibility(View.GONE);
                llEnd_date.setVisibility(View.GONE);
            }
        });

        tvEverydays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvEverydays.setSelected(true);
                tvWeekdays.setSelected(false);
                tvWeekends.setSelected(false);
            }
        });

        tvWeekdays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvEverydays.setSelected(false);
                tvWeekdays.setSelected(true);
                tvWeekends.setSelected(false);
            }
        });

        tvWeekends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvEverydays.setSelected(false);
                tvWeekdays.setSelected(false);
                tvWeekends.setSelected(true);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields())
                {
                    mAddSlots();
                }
            }
        });

        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerVarID=1;
                selectDate();
            }
        });

        tvEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerVarID=2;
                selectDate();
            }
        });

        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTime();
            }
        });

        TextWatcher textWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCreateSlot();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        etSlots.addTextChangedListener(textWatcher);
        etSlotTime.addTextChangedListener(textWatcher);

        btnRepeatMode.setSelected(true);
        tvEverydays.setSelected(true);

        spinner=(Spinner)findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADDRESS_REQUEST && resultCode == Activity.RESULT_OK){
//            AddressPojo addressPojo = (AddressPojo) data.getSerializableExtra("ADDRESS_INFO");
            addressPojo = (AddressPojo) data.getSerializableExtra("ADDRESS_INFO");

            Utility.printLog(TAG+" selected address "+addressPojo.getFORMATED_ADDRESS()+", "+addressPojo.getFLAT_NO_OR_HOUSE_NO()+", "+addressPojo.getLANDMARK() );
            selectedAddress.setText(addressPojo.getFORMATED_ADDRESS()/*+", "+addressPojo.getFLAT_NO_OR_HOUSE_NO()+", "+addressPojo.getLANDMARK()*/ );
            latlng[0] = Double.parseDouble(addressPojo.getLATITUDE());
//                latlng[0] = addressPojo.getLATITUDE();
            latlng[1] = Double.parseDouble(addressPojo.getLONGITUDE());
//                latlng[1] = addressPojo.getLongitude();

        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public boolean validateFields()
    {

        if(selectedAddress.getText().toString().isEmpty())
        {
            Snackbar.make(getCurrentFocus(),"Please select location.", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if(etRadiusOfOperation.getText().toString().isEmpty())
        {
            Snackbar.make(getCurrentFocus(),"Please add radius of operation.", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if(etSetRate.getText().toString().isEmpty())
        {
            Snackbar.make(getCurrentFocus(),"Please set the rate.", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if(etSlots.getText().toString().isEmpty())
        {
            Snackbar.make(getCurrentFocus(),"Please add the no. of slots.", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if(etSlotTime.getText().toString().isEmpty())
        {
            Snackbar.make(getCurrentFocus(),"Please set the slot time", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if(tvStartDate.getText().toString().isEmpty())
        {
            Snackbar.make(getCurrentFocus(),"Please select the start date", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if(tvStartTime.getText().toString().isEmpty())
        {
            Snackbar.make(getCurrentFocus(),"Please select the start time", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if(btnRepeatMode.isSelected())
        {
            if(tvEndDate.getText().toString().isEmpty())
            {
                Snackbar.make(getCurrentFocus(),"Please select the end date.", Snackbar.LENGTH_SHORT).show();
                return false;
            }
        }



        return  true;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void selectDate() {
        Calendar calendar=Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AddSlotsActivity.this, (DatePickerDialog.OnDateSetListener) this, year, month, date);
        datePickerDialog.show();


    }
///////////////////////////////////////////////////s/////////////////////////////////////////////////
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Log.d("selected_date","year "+ year+" monthOfYear "+monthOfYear +" dayOfMonth "+ dayOfMonth);

        if(datePickerVarID==1)
        {
            tvStartDate.setText(String.format("%d-%d-%d",year ,monthOfYear + 1,dayOfMonth ));
            datePickerVarID=0;
        }
        else
        {
            tvEndDate.setText(String.format("%d-%d-%d", year, monthOfYear + 1,dayOfMonth ));
            datePickerVarID=0;
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void selectTime()
    {
        Calendar calendar=Calendar.getInstance();
        Date mDate=calendar.getTime();

        TimePickerDialog timePickerDialog=new TimePickerDialog(AddSlotsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Log.d("selected_time","hourOfDay "+ hourOfDay+" minute "+minute );

                String format="";

                int hour=hourOfDay;
                if (hour == 0) {
                    hour += 12;
                    format = "AM";
                } else if (hour == 12) {
                    format = "PM";
                } else if (hour > 12) {
                    hour -= 12;
                    format = "PM";
                } else {
                    format = "AM";
                }
                mStartTime=String.format("%02d",hourOfDay)+":"+String.format("%02d",minute);

                tvStartTime.setText(String.format("%02d",hour)+":"+String.format("%02d",minute)+" "+format);
                mStartHour=hourOfDay;
                mStartminut=minute;

                mCreateSlot();
            }
        }, mDate.getHours(), mDate.getMinutes(), false);

        timePickerDialog.show();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mAddSlots()
    {
        mProgressDailog= Utility.getProcessDialog(AddSlotsActivity.this);
        if(mProgressDailog!=null && !mProgressDailog.isShowing())
        {
            mProgressDailog.show();
        }

        int option=-1,repeatday=-1;
        if(btnRepeatMode.isSelected())
        {
            option=2;
        }
        else if(btnNonRepeatMode.isSelected())
        {
            option=1;
        }

        if(tvEverydays.isSelected())
        {
            repeatday=1;
        }
        else if(tvWeekdays.isSelected())
        {
            repeatday=2;
        }
        else if(tvWeekends.isSelected())
        {
            repeatday=3;
        }

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_pro_id",sessionManager.getProviderID())
                .add("ent_location",addressPojo.getPLACE_ID())
                .add("ent_start_time",tvStartDate.getText().toString()+" "+mStartTime+":00")
                .add("ent_radius",etRadiusOfOperation.getText().toString())
                .add("ent_price",etSetRate.getText().toString())
                .add("ent_duration",etSlotTime.getText().toString())
                .add("ent_num_slot",etSlots.getText().toString())
                .add("ent_option",option+"")
                .add("ent_repeatday",repeatday+"")
                .add("ent_end_date",tvEndDate.getText().toString())
                .build();


        Okhttp_connection.doOkhttpRequest(ServiceUrls.ADD_SLOT, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("AddSlot resonse "+result);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
                if(result!=null)
                {
                    try {
                        JSONObject jsonObject=new JSONObject(result);
                        if(jsonObject.get("errFlag").equals("0"))
                        {
                            Toast.makeText(AddSlotsActivity.this, jsonObject.get("errMsg").toString(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else if(jsonObject.get("errFlag").equals("1"))
                        {
                            Toast.makeText(AddSlotsActivity.this, jsonObject.get("errMsg").toString(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onError(String error) {
                Utility.printLog("AddSlot error "+error);
                mProgressDailog.dismiss();
            }
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public  void mCreateSlot()
    {
        LinearLayout ll_container= (LinearLayout) findViewById(R.id.ll_container_slots);

        int no_of_slots=0;
        long slotDurationInSec=0;

        long dur=((mStartHour*60)+mStartminut)*60;

        if(!etSlots.getText().toString().isEmpty())
        {
            no_of_slots=Integer.parseInt(etSlots.getText().toString());
        }
        if(!etSlotTime.getText().toString().isEmpty())
        {
            slotDurationInSec=(Integer.parseInt(etSlotTime.getText().toString()))*60;
        }
        ll_container.removeAllViews();
        if(slotDurationInSec>0)
        {
            for (int i=0;i<no_of_slots;i++)
            {
                LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.single_row_slot_schedule,null);

                TextView startTime= (TextView) view.findViewById(R.id.tvSlotStartTime);
                TextView endTime= (TextView) view.findViewById(R.id.tvSlotEndTime);

                startTime.setText(getDurationString((int) dur));
                endTime.setText(getDurationString((int) (dur+slotDurationInSec)));

                dur=dur+slotDurationInSec;

                ll_container.addView(view);
            }
        }


    }
////////////////////////////////////////////////////////////////////////////////////////////////////
        private String getDurationString(int seconds)
        {
            int day = (int) TimeUnit.SECONDS.toDays(seconds);
            int hour = (int) (TimeUnit.SECONDS.toHours(seconds) - (day *24));
            int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60));
            int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60));
            //Utility.printLog(TAG+"Duration "+hours+" : "+minute);

            String format="";

            if (hour == 0) {
                hour += 12;
                format = "AM";
            } else if (hour == 12) {
                format = "PM";
            } else if (hour > 12) {
                hour -= 12;
                format = "PM";
            } else {
                format = "AM";
            }

            String timer=String.format("%02d",hour)+" : "+String.format("%02d",minute)+/*" : "+String.format("%02d",second)*/" "+format;

            return timer;
        }
////////////////////////////////////////////////////////////////////////////////////////////////////


    public void mGetProviderAddress()
    {
        mProgressDailog= Utility.getProcessDialog(AddSlotsActivity.this);
        if(mProgressDailog!=null && !mProgressDailog.isShowing())
        {
            mProgressDailog.show();
        }

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_pro_id",sessionManager.getProviderID())
                .build();


        Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_PROVIDER_ADDRESS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("GET_PROVIDER_ADDRESS resonse "+result);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
                Gson gson=new Gson();
                GetProviderAddress getProviderAddress=gson.fromJson(result,GetProviderAddress.class);
                setAddressAdapter(getProviderAddress.getAddlist());
            }

            @Override
            public void onError(String error) {
                Utility.printLog("GET_PROVIDER_ADDRESS error "+error);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
            }
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setAddressAdapter(ArrayList<Addlist> list)
    {
        if(list.size()>0)
        {
            address_list.clear();
            for(int i=0;i<list.size();i++)
            {
                AddressPojo addressPojo=new AddressPojo();
                addressPojo.setADDRESS_TYPE(list.get(i).getLocName());
                addressPojo.setFLAT_NO_OR_HOUSE_NO(list.get(i).getLocName());
                addressPojo.setFORMATED_ADDRESS(list.get(i).getAddress1()+" "+list.get(i).getAddress2());
                addressPojo.setLATITUDE(list.get(i).getLat());
                addressPojo.setLONGITUDE(list.get(i).getLng());
                addressPojo.setPLACE_ID(list.get(i).getId());
                addressPojo.setUSER_ID(list.get(i).getUser());

                address_list.add(addressPojo);
            }

            addressPojo = (AddressPojo) address_list.get(0);

            selectedAddress.setText(addressPojo.getFORMATED_ADDRESS()/*+", "+addressPojo.getFLAT_NO_OR_HOUSE_NO()+", "+addressPojo.getLANDMARK()*/ );
            latlng[0] = Double.parseDouble(addressPojo.getLATITUDE());
            latlng[1] = Double.parseDouble(addressPojo.getLONGITUDE());

        }
    }
///////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        String spin = spinner.getSelectedItem().toString();
        mCreateSlot();
        etSlotTime.setText(spin);
        Utility.printLog("AAAAAAA" + spin);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
