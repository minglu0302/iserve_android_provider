package com.app.partner.iserve.Address;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by embed on 17/10/16.
 */
public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

   // private final Typeface regular;
    private ArrayList<AddressPojo> mDataset;
    private AddressDB addressDB;
    Activity context;
    Resources resources;
    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;
    private Typeface fontBold,font;

    // Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView address_tv;
        public TextView flat_no_or_house_no_tv;
        public TextView landmark_tv;
        public TextView address_type_tv;
        public ImageView address_type_iv;
        public ImageView delete_address_iv;
        public RelativeLayout delete_address_rl;
        public CardView address_info_item_cv;
        public ViewHolder(View v) {
            super(v);
            address_tv = (TextView) v.findViewById(R.id.address_tv);
            flat_no_or_house_no_tv = (TextView) v.findViewById(R.id.flat_no_or_house_no_tv);
            landmark_tv = (TextView) v.findViewById(R.id.landmark_tv);
            address_type_tv = (TextView) v.findViewById(R.id.address_type_tv);

            address_tv.setTypeface(font);
            flat_no_or_house_no_tv.setTypeface(font);
            landmark_tv.setTypeface(font);
            address_type_tv.setTypeface(fontBold);

            address_type_iv = (ImageView) v.findViewById(R.id.address_type_iv);
            delete_address_iv = (ImageView) v.findViewById(R.id.delete_address_iv);
            delete_address_rl = (RelativeLayout) v.findViewById(R.id.delete_address_rl);
            address_info_item_cv = (CardView) v.findViewById(R.id.address_info_item_cv);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AddressAdapter(Activity context, ArrayList<AddressPojo> myDataset, AddressDB addressDB) {
        this.mDataset = myDataset;
        Log.d("FoursquareApi_All"," con "+mDataset.size());
        this.context=context;
        this.addressDB=addressDB;
        sessionManager=new SessionManager(context);
        //regular= MyApplication.regular;
        resources = context.getResources();
        fontBold=Utility.getFontBold(context);
        font=Utility.getFontRegular(context);
        //reg = MyApplication.regular;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_view_for_address_data, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final AddressPojo addressPojo = mDataset.get(position);

        Log.d("Foursquare_pa"," yes "+addressPojo.getPLACE_ID()
                +"\n"+addressPojo.getADDRESS_TYPE()
                +"\n"+addressPojo.getFLAT_NO_OR_HOUSE_NO()
                +"\n"+addressPojo.getLANDMARK()
                +"\n"+addressPojo.getLATITUDE()
                +"\n"+addressPojo.getLONGITUDE()
                +"\n"+addressPojo.getUSER_ID()
                +"\n"+addressPojo.getFORMATED_ADDRESS()
        );
        holder.address_tv.setText(addressPojo.getFORMATED_ADDRESS());
        holder.flat_no_or_house_no_tv.setText(resources.getString(R.string.flat_no_house_no_appt_name)+" : "+addressPojo.getFLAT_NO_OR_HOUSE_NO());
        holder.landmark_tv.setText(resources.getString(R.string.land_mark)+" : "+addressPojo.getLANDMARK());
        holder.address_tv.setText(addressPojo.getFORMATED_ADDRESS());
        if(addressPojo.getADDRESS_TYPE().equals("0")){
            holder.address_type_iv.setBackgroundResource(R.drawable.address_home_icon_on);
            holder.address_type_tv.setText(resources.getString(R.string.home));
        } else if(addressPojo.getADDRESS_TYPE().equals("1")){
            holder.address_type_iv.setBackgroundResource(R.drawable.address_office_icon_on);
            holder.address_type_tv.setText(resources.getString(R.string.office));
        } else if(addressPojo.getADDRESS_TYPE().equals("2")){
            holder.address_type_iv.setBackgroundResource(R.drawable.address_other_icon_on);
            holder.address_type_tv.setText(resources.getString(R.string.other));
        }

        holder.address_info_item_cv.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

//                if(AppConstants.isAddressSelectable) {


                    Intent intent = new Intent();
                    intent.putExtra("ADDRESS_INFO", addressPojo);
                    context.setResult(Activity.RESULT_OK, intent);
                    context.finish();
                    AppConstants.isAddressSelectable=false;
//                }

                /*if(Constants.addressFlag)
                {
                    Constants.addressFlag = false;
                    Constants.addressId = adrListData.get(position).getAddressid();
                    Utility.printLog("Address",adrListData.get(position).getArea());
                    context.finish();
                }*/
//                Toast.makeText(context, ""+addressPojo.getFORMATED_ADDRESS(), Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(context, DeleteCardActivity.class);
//                ((HomePageActivity)context).startActivity(intent);
            }
        });

       /* if(AppConstants.isAddressEditable){
            holder.delete_address_rl.setVisibility(View.VISIBLE);
        }else {
            holder.delete_address_rl.setVisibility(View.GONE);      PK
        }*/
        holder.delete_address_rl.setVisibility(View.VISIBLE);
        holder.delete_address_iv.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

               // remove(addressPojo);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                // Setting Dialog Title
                alertDialog.setTitle(resources.getString(R.string.alert));
                // Setting Dialog Message
                alertDialog.setMessage(resources.getString(R.string.del_address_confirmation));
                alertDialog.setCancelable(false);
                // On pressing Settings button
                alertDialog.setPositiveButton(resources.getString(R.string.delete),
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog,int which)
                            {
                                remove(addressPojo);
                            }
                        });
                // on pressing cancel button
                alertDialog.setNegativeButton(resources.getString(R.string.cancel), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
                // Showing Alert Message
                alertDialog.show();
            }
        });
    }

    public void remove(AddressPojo item) {
        int position = mDataset.indexOf(item);
        mDeleteProviderAddress(position);
        /*if(addressDB.deleteAnAddress(item.getPLACE_ID())>0) {
            mDataset.remove(position);
            notifyItemRemoved(position);
            Toast.makeText(context, "Deleted sucessfully!", Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public int getItemCount() {
        Log.d("FoursquareApi_pa"," yes "+mDataset.size());
        return mDataset.size();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mDeleteProviderAddress(final int position)
    {
        mProgressDailog= Utility.getProcessDialog(context);
        if(mProgressDailog!=null && !mProgressDailog.isShowing())
        {
            mProgressDailog.show();
        }

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_pro_id",sessionManager.getProviderID())
                .add("ent_addressid",mDataset.get(position).getPLACE_ID())
                .build();


        Okhttp_connection.doOkhttpRequest(ServiceUrls.DELETE_PROVIDER_ADDRESS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("DELETE_PROVIDER_ADDRESS resonse "+result);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
                try {
                    JSONObject jsonObject=new JSONObject(result);
                    if(jsonObject.get("errFlag").equals("0"))
                    {
                        mDataset.remove(position);
                        notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String error) {
                Utility.printLog("DELETE_PROVIDER_ADDRESS error "+error);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
            }
        });
    }
}
