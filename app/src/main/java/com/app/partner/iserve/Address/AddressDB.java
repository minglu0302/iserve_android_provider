package com.app.partner.iserve.Address;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by embed on 17/10/16.
 */
public class AddressDB extends SQLiteOpenHelper {
    //all constants as they are static and final(Db=Database)
    Context context;
    private static final String DB_NAME = "address_db";
    //Db Version
    private static final int Db_Version = 1;
    //Db Name
//    private static final String Db_Name = "messagesdb";
    /**
     * Item Table fields
     */
    private static final String ADDRESS_TABLE = "address_table";
    private static final String PLACE_ID = "PLACE_ID";
    private static final String LATITUDE = "LATITUDE";
    private static final String LONGITUDE = "LONGITUDE";
    private static final String FORMATED_ADDRESS = "FORMATED_ADDRESS";
    private static final String ADDRESS_TYPE = "ADDRESS_TYPE";
    private static final String USER_ID = "USER_ID";
    private static final String FLAT_NO_OR_HOUSE_NO = "FLAT_NO_OR_HOUSE_NO";
    private static final String LANDMARK = "LANDMARK";

    String CREATE_ADDRESS_TABLE = "CREATE TABLE " + ADDRESS_TABLE + "( "
            + PLACE_ID + " TEXT," + LATITUDE + " TEXT," + LONGITUDE + " TEXT," + FORMATED_ADDRESS + " TEXT,"
            + ADDRESS_TYPE + " TEXT," +USER_ID + " TEXT," + FLAT_NO_OR_HOUSE_NO + " TEXT," + LANDMARK + " TEXT" + ")";

    //constructor here
    public AddressDB(Context context) {
        super(context, DB_NAME, null, Db_Version);
        this.context = context;
    }

    /**
     * Constructor to initialize DatabaseHandler
     * @param context
     * @return
     */
    public static AddressDB getInstance(Context context){
        AddressDB handler = new AddressDB(context);
        return handler;
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ADDRESS_TABLE);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " +CREATE_ADDRESS_TABLE);
//        db.execSQL("DROP TABLE IF EXISTS " + ITEM_TABLE);
        // Create tables again
        onCreate(db);
    }


    /**
     *
     * adding custom order in the database
     * @param
     */
    public void insertAddress(AddressPojo address_pojo){
        SQLiteDatabase db = AddressDB.this.getWritableDatabase();
        ContentValues values =  new ContentValues();
        values.put(PLACE_ID,address_pojo.getPLACE_ID());
        values.put(LATITUDE, address_pojo.getLATITUDE());
        values.put(LONGITUDE, address_pojo.getLONGITUDE());
        values.put(FORMATED_ADDRESS, address_pojo.getFORMATED_ADDRESS());
        values.put(ADDRESS_TYPE, address_pojo.getADDRESS_TYPE());
        values.put(FLAT_NO_OR_HOUSE_NO, address_pojo.getFLAT_NO_OR_HOUSE_NO());
        values.put(LANDMARK, address_pojo.getLANDMARK());
        values.put(USER_ID, address_pojo.getUSER_ID());
        Log.d("Inserting" ,
                "\n"+address_pojo.getPLACE_ID() + "\n"+address_pojo.getLATITUDE()
                        +"\n"+address_pojo.getLONGITUDE() + "\n"+address_pojo.getFORMATED_ADDRESS()
                        +"\n"+address_pojo.getADDRESS_TYPE() + "\n"+address_pojo.getLANDMARK()
                        +"\n"+address_pojo.getUSER_ID()
        );
        db.insert(ADDRESS_TABLE, null, values);
    }

    /**
     * fetch the custom order based on the store id
     * @param user_id
     * @return
     */
    public ArrayList<AddressPojo> getListOfOldAddrees(String user_id){
        ArrayList<AddressPojo> messageFormat_pojoArrayList = new ArrayList();
        SQLiteDatabase db = AddressDB.this.getReadableDatabase();
//        Cursor cursor = db.query(MESSAGING_TABLE, new String[]{"*"}, CUSTOM_ORDER_STORE + "=? ", new String[]{PLACE_ID}, null, null, null);
        Cursor cursor = db.query(ADDRESS_TABLE, new String[]{"*"}, USER_ID + "=? ", new String[]{user_id}, null, null, null);
//        Cursor cursor=db.("SELECT *" + "FROM Messaging_Table" + "WHERE LONGITUDE = " +LONGITUDE+ "ORDER BY FORMATED_ADDRESS;");
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            do {
                AddressPojo address_pojo = new AddressPojo();
                address_pojo.setPLACE_ID(cursor.getString(cursor.getColumnIndex(PLACE_ID)));
                address_pojo.setLATITUDE(cursor.getString(cursor.getColumnIndex(LATITUDE)));
                address_pojo.setLONGITUDE(cursor.getString(cursor.getColumnIndex(LONGITUDE)));
                address_pojo.setFORMATED_ADDRESS(cursor.getString(cursor.getColumnIndex(FORMATED_ADDRESS)));
                address_pojo.setADDRESS_TYPE(cursor.getString(cursor.getColumnIndex(ADDRESS_TYPE)));
                address_pojo.setFLAT_NO_OR_HOUSE_NO(cursor.getString(cursor.getColumnIndex(FLAT_NO_OR_HOUSE_NO)));
                address_pojo.setLANDMARK(cursor.getString(cursor.getColumnIndex(LANDMARK)));
                address_pojo.setUSER_ID(cursor.getString(cursor.getColumnIndex(USER_ID)));
                messageFormat_pojoArrayList.add(address_pojo);

                Log.d("fetching_data" ,
                        "\n"+address_pojo.getPLACE_ID() + "\n"+address_pojo.getLATITUDE()
                                +"\n"+address_pojo.getLONGITUDE() + "\n"+address_pojo.getFORMATED_ADDRESS()
                                +"\n"+address_pojo.getADDRESS_TYPE() + "\n"+address_pojo.getLANDMARK()
                                +"\n"+address_pojo.getUSER_ID()
                                +"\n"+user_id
                );

//                Utility.printLog("Store Id "+messageFormat_pojoArrayList.getStoreId()+" Custom Order Item "+customOrder.getItemName()+" Custom Order Id "+customOrder.getId());
            }while (cursor.moveToNext());
        }
        return messageFormat_pojoArrayList;
    }


    public int deleteAnAddress(String place_id){
        SQLiteDatabase db = AddressDB.this.getReadableDatabase();
        return db.delete(ADDRESS_TABLE,PLACE_ID + "=? ",new String[]{place_id});
    }

}

