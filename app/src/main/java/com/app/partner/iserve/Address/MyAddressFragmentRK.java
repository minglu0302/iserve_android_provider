package com.app.partner.iserve.Address;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;

import com.app.partner.iserve.Pojo.Addlist;
import com.app.partner.iserve.Pojo.GetProviderAddress;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class MyAddressFragmentRK extends Fragment implements View.OnClickListener{

    Button add_new_address_btn;
    Activity activity;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<AddressPojo> address_list = new ArrayList<AddressPojo>();
    public AddressDB addressDB;
    SessionManager sessionManager;
    TextView ActionbarTitile,ActionbarEditTv;
    ImageView ActionbarTitileIamge;
    private LinearLayout Editll;
    private ProgressDialog mProgressDailog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_address, container, false);
        initializeViewComponent(view);
        return view;
    }

    private void initializeViewComponent(View view) {

        Typeface fontBold = Utility.getFontBold(getActivity());
//        activity.pubnubProgressBar.setVisibility(View.GONE);

//        if (getActivity() instanceof HomeScreen){
//            activity = (HomeScreen) getActivity();
//            sessionManager = new SessionManager(activity);
//            addressDB = new AddressDB(activity);
////            ActionbarTitileIamge=  (ImageView)getActivity().findViewById(R.id.action_bar_title);
////            ActionbarTitileIamge.setVisibility(View.GONE);
////
////            ActionbarTitile= (TextView) getActivity().findViewById(R.id.action_bar_title_text);
////            ActionbarTitile.setText(getResources().getString(R.string.my_address));
////            ActionbarTitile.setVisibility(View.VISIBLE);
////            ActionbarEditTv= (TextView) getActivity().findViewById(R.id.action_bar_editTv);
////            ActionbarEditTv.setVisibility(View.GONE);
//
//            ActionbarTitileIamge=  (ImageView)getActivity().findViewById(R.id.action_bar_title);
//            ActionbarTitileIamge.setVisibility(View.GONE);
//
//            ActionbarTitile= (TextView) getActivity().findViewById(R.id.action_bar_title_text);
//            ActionbarTitile.setText(getString(R.string.my_address));
//            ActionbarTitile.setVisibility(View.VISIBLE);
//
//            ActionbarEditTv= (TextView) getActivity().findViewById(R.id.action_bar_editTv);
//            ActionbarEditTv.setVisibility(View.VISIBLE);
//            ActionbarEditTv.setText(getString(R.string.edit));
//
//            Editll= (LinearLayout) getActivity().findViewById(R.id.Editll);
//
//            Editll.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(ActionbarEditTv.getText().toString().equals(getActivity().getResources().getString(R.string.edit))){
//                        Constants.isAddressEditable=true;
//                        notifier();
//                        ActionbarEditTv.setText(getActivity().getResources().getString(R.string.save));
//                    } else {
//                        Constants.isAddressEditable=false;
//                        notifier();
//                        ActionbarEditTv.setText(getActivity().getResources().getString(R.string.edit));
//                    }
//                }
//            });
//
//        } else
        if(getActivity() instanceof SelectAddressActivity){
            activity = (Activity) getActivity();
            sessionManager = new SessionManager(activity);
            addressDB = new AddressDB(activity);
        }

        add_new_address_btn = (Button) view.findViewById(R.id.add_new_address_btn);
        add_new_address_btn.setTypeface(fontBold);
        add_new_address_btn.setOnClickListener(this);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.all_saved_address_rv);


        mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);
//        mAdapter= activity.setUpAdapter(mRecyclerView,0);
        mAdapter = new AddressAdapter(activity, address_list,addressDB);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void notifier(){
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        //ArrayList<AddressPojo> address_list1 = addressDB.getListOfOldAddrees(sessionManager.getProviderID());
      /*  if(address_list1.size()>0){
            address_list.clear();
            for(int i=address_list1.size()-1;i>=0;i--){
                address_list.add(address_list1.get(i));
            }
        }
        mAdapter.notifyDataSetChanged();*/

        mGetProviderAddress();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_new_address_btn:
                Intent intent = new Intent(getActivity(), AddANewAddress.class);
                startActivity(intent);
                break;
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mGetProviderAddress()
    {
        mProgressDailog= Utility.getProcessDialog(getActivity());
        if(mProgressDailog!=null && !mProgressDailog.isShowing())
        {
            mProgressDailog.show();
        }

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_pro_id",sessionManager.getProviderID())
                .build();


        Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_PROVIDER_ADDRESS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("GET_PROVIDER_ADDRESS resonse "+result);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
                Gson gson=new Gson();
                GetProviderAddress getProviderAddress=gson.fromJson(result,GetProviderAddress.class);
                setAddressAdapter(getProviderAddress.getAddlist());
            }

            @Override
            public void onError(String error) {
                Utility.printLog("GET_PROVIDER_ADDRESS error "+error);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setAddressAdapter(ArrayList<Addlist>list)
    {
        if(list.size()>0)
        {
            address_list.clear();
            for(int i=0;i<list.size();i++)
            {
               AddressPojo addressPojo=new AddressPojo();
                addressPojo.setADDRESS_TYPE(list.get(i).getLocName());
                addressPojo.setFLAT_NO_OR_HOUSE_NO(list.get(i).getLocName());
                addressPojo.setFORMATED_ADDRESS(list.get(i).getAddress1()+" "+list.get(i).getAddress2());
                addressPojo.setLATITUDE(list.get(i).getLat());
                addressPojo.setLONGITUDE(list.get(i).getLng());
                addressPojo.setPLACE_ID(list.get(i).getId());
                addressPojo.setUSER_ID(list.get(i).getUser());

                address_list.add(addressPojo);
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}
