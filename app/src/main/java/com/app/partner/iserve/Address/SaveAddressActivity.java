package com.app.partner.iserve.Address;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;


public class SaveAddressActivity extends AppCompatActivity implements View.OnClickListener,View.OnFocusChangeListener,TextWatcher {

    private Resources resources;
    private TextView action_bar_title_tv,action_bar_move_forward_tv;
    private TextView address_home_tv,address_office_tv,address_other_tv,formated_address_tv,tvTitleAddrsDtls;
    private EditText flat_no_or_house_no_et,landmark_et,generic_error_et;
    private RelativeLayout action_bar_back_rl,action_bar_next_rl;
    private ImageButton address_home_ibtn,address_office_ibtn,address_other_ibtn;
    private AddressPojo addressPojo;
   // private AddressDB addressDB;
    private SessionManager sessionManager;
    private String address_type = "2";
    private ProgressDialog mProgressDailog;
    private Typeface fontBold,font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_address);

        sessionManager = new SessionManager(this);
       // addressDB= new AddressDB(this);
        font=Utility.getFontRegular(SaveAddressActivity.this);
        fontBold=Utility.getFontBold(SaveAddressActivity.this);

        intitializeActionBar();
        intitializeViewComponent();
        if(getIntent().hasExtra("ADDRESS_INFO")) {
            addressPojo = (AddressPojo) getIntent().getSerializableExtra("ADDRESS_INFO");
            formated_address_tv.setText(addressPojo.getFORMATED_ADDRESS());
        }
    }

    private void intitializeViewComponent() {



        formated_address_tv = (TextView) findViewById(R.id.formated_address_tv);
        formated_address_tv.setTypeface(font);

        flat_no_or_house_no_et = (EditText) findViewById(R.id.flat_no_or_house_no_et);
        flat_no_or_house_no_et.setTypeface(font);

        landmark_et = (EditText) findViewById(R.id.landmark_et);
        landmark_et.setTypeface(font);

        flat_no_or_house_no_et.setOnFocusChangeListener(this);
        landmark_et.setOnFocusChangeListener(this);

        address_home_tv = (TextView) findViewById(R.id.address_home_tv);
        address_home_tv.setTypeface(font);

        address_office_tv = (TextView) findViewById(R.id.address_office_tv);
        address_office_tv.setTypeface(font);

        address_other_tv = (TextView) findViewById(R.id.address_other_tv);
        address_other_tv.setTypeface(font);

        tvTitleAddrsDtls = (TextView) findViewById(R.id.tvTitleAddrsDtls);
        tvTitleAddrsDtls.setTypeface(fontBold);

        address_home_ibtn = (ImageButton) findViewById(R.id.address_home_ibtn);
        address_office_ibtn = (ImageButton) findViewById(R.id.address_office_ibtn);
        address_other_ibtn = (ImageButton) findViewById(R.id.address_other_ibtn);
        address_home_ibtn.setOnClickListener(this);
        address_office_ibtn.setOnClickListener(this);
        address_other_ibtn.setOnClickListener(this);
        address_other_ibtn.setSelected(true);
        address_other_tv.setTextColor(resources.getColor(R.color.black));
    }

    private void intitializeActionBar() {
        resources=getResources();
        action_bar_title_tv = (TextView) findViewById(R.id.action_bar_title_tv);
        action_bar_title_tv.setTypeface(fontBold);

        action_bar_move_forward_tv = (TextView) findViewById(R.id.action_bar_move_forward_tv);
        action_bar_move_forward_tv.setTypeface(font);
//        action_bar_move_forward_tv.setText(resources.getString(R.string.save));
        action_bar_title_tv.setText(resources.getString(R.string.title_activity_save_address));
//        action_bar_move_forward_tv.setText(resources.getString(R.string.save));
        action_bar_back_rl = (RelativeLayout) findViewById(R.id.action_bar_back_rl);
        action_bar_back_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SaveAddressActivity.this, AddANewAddress.class);
                intent.putExtra("ADDRESS_INFO",addressPojo);
                startActivity(intent);
                finish();
            }
        });

        action_bar_next_rl = (RelativeLayout) findViewById(R.id.action_bar_next_rl);
        action_bar_next_rl.setVisibility(View.VISIBLE);

        action_bar_next_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // onFocusChange(flat_no_or_house_no_et,false);
                //onFocusChange(landmark_et,false);

                if(formated_address_tv.getText()==null || formated_address_tv.getText().toString().equals(""))
                {
                    Toast.makeText(SaveAddressActivity.this, resources.getString(R.string.input_address), Toast.LENGTH_SHORT).show();
                }
                else if(flat_no_or_house_no_et.getError()!=null)
                {
                    flat_no_or_house_no_et.requestFocus();
                }
                else if(landmark_et.getError()!=null)
                {
                    landmark_et.requestFocus();
                }else {
                    addressPojo.setADDRESS_TYPE(address_type);
                    addressPojo.setFLAT_NO_OR_HOUSE_NO(flat_no_or_house_no_et.getText().toString());
                    addressPojo.setLANDMARK(landmark_et.getText().toString());
                    addressPojo.setUSER_ID(sessionManager.getProviderID());
                    //addressDB.insertAddress(addressPojo);
                    mAddProviderAddress(addressPojo);
                }

                Toast.makeText(SaveAddressActivity.this, "Save is Clicked!", Toast.LENGTH_SHORT).show();
            }
        });
        action_bar_move_forward_tv.setText(resources.getString(R.string.save));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.address_home_ibtn:
                address_home_ibtn.setSelected(true);
                address_office_ibtn.setSelected(false);
                address_other_ibtn.setSelected(false);
                address_home_tv.setTextColor(resources.getColor(R.color.black));
                address_office_tv.setTextColor(resources.getColor(R.color.silver));
                address_other_tv.setTextColor(resources.getColor(R.color.silver));
                address_type="0";
                break;
            case R.id.address_office_ibtn:
                address_home_ibtn.setSelected(false);
                address_office_ibtn.setSelected(true);
                address_other_ibtn.setSelected(false);
                address_home_tv.setTextColor(resources.getColor(R.color.silver));
                address_office_tv.setTextColor(resources.getColor(R.color.black));
                address_other_tv.setTextColor(resources.getColor(R.color.silver));
                address_type="1";
                break;
            case R.id.address_other_ibtn:
                address_home_ibtn.setSelected(false);
                address_office_ibtn.setSelected(false);
                address_other_ibtn.setSelected(true);
                address_home_tv.setTextColor(resources.getColor(R.color.silver));
                address_office_tv.setTextColor(resources.getColor(R.color.silver));
                address_other_tv.setTextColor(resources.getColor(R.color.black));
                address_type="2";
                break;

        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        switch (v.getId()) {

            case R.id.flat_no_or_house_no_et:
                Log.d("has_focus", "user_first_name_et \t" + flat_no_or_house_no_et.getError() + "\t" + flat_no_or_house_no_et.hasFocus());
                if (!hasFocus) {
                    if (flat_no_or_house_no_et.getText().toString().trim().isEmpty()) {
                        flat_no_or_house_no_et.setError(getString(R.string.empty_field));
                    }
                } else {
                    generic_error_et = flat_no_or_house_no_et;
                }
                break;

            case R.id.landmark_et:
                Log.d("has_focus", "landmark_et " + hasFocus);
                if (!hasFocus) {
                    if (landmark_et.getText().toString().trim().isEmpty()) {
                        landmark_et.setError(getString(R.string.empty_field));
                    }
                } else {
                    generic_error_et = landmark_et;
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        generic_error_et.setError(null);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mAddProviderAddress(AddressPojo addressPojo)
    {
        String locName = "";
        if (address_type.equals("0"))
        {
            locName="HOME";
        }
        else if(address_type.equals("1"))
        {
            locName="OFFICE";
        }
        else if(address_type.equals("2"))
        {
            locName="OTHER";
        }
        mProgressDailog= Utility.getProcessDialog(SaveAddressActivity.this);
        if(mProgressDailog!=null && !mProgressDailog.isShowing())
        {
            mProgressDailog.show();
        }

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_pro_id",sessionManager.getProviderID())
                .add("ent_locName",locName)
                .add("ent_address1",addressPojo.getFORMATED_ADDRESS())
                .add("ent_address2","")
                .add("ent_lat",addressPojo.getLATITUDE())
                .add("ent_lng",addressPojo.getLONGITUDE())
                .build();


        Okhttp_connection.doOkhttpRequest(ServiceUrls.ADD_PROVIDER_ADDRESS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("AddProviderAddress resonse "+result);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }

                if(result!=null)
                {
                    try {

                        JSONObject jsonObject=new JSONObject(result);
                        if(jsonObject.get("errFlag").toString().equals("0"))
                        {
                            Toast.makeText(SaveAddressActivity.this, jsonObject.get("errMsg").toString(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else
                        {
                            Toast.makeText(SaveAddressActivity.this, jsonObject.get("errMsg").toString(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String error) {
                Utility.printLog("AddSlot error "+error);
                mProgressDailog.dismiss();
            }
        });
    }
}
