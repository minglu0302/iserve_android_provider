package com.app.partner.iserve.Address;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Utility;

public class SelectAddressActivity extends AppCompatActivity {

    RelativeLayout address_container;
    private Resources resources;
    private TextView action_bar_title_tv;
    private RelativeLayout action_bar_back_rl;
    private RelativeLayout action_bar_next_rl;
    private TextView action_bar_move_forward_tv;

    MyAddressFragmentRK addressFragmentRK;
    private Typeface font,fontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        intitializeActionBar();

        AppConstants.isAddressSelectable=true;

        addressFragmentRK = new MyAddressFragmentRK();
        address_container = (RelativeLayout) findViewById(R.id.address_container);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.address_container, addressFragmentRK, null);
        fragmentTransaction.commit();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

    }
    private void intitializeActionBar() {
        resources=getResources();
        font= Utility.getFontRegular(SelectAddressActivity.this);
        fontBold=Utility.getFontBold(SelectAddressActivity.this);

        action_bar_title_tv = (TextView) findViewById(R.id.action_bar_title_tv);
        action_bar_title_tv.setTypeface(fontBold);

        action_bar_move_forward_tv = (TextView) findViewById(R.id.action_bar_move_forward_tv);
        action_bar_move_forward_tv.setTypeface(font);

        action_bar_title_tv.setText(resources.getString(R.string.select_address));

        action_bar_move_forward_tv.setText(resources.getString(R.string.edit));
        action_bar_back_rl = (RelativeLayout) findViewById(R.id.action_bar_back_rl);
        action_bar_next_rl = (RelativeLayout) findViewById(R.id.action_bar_next_rl);
        action_bar_back_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        action_bar_next_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(action_bar_move_forward_tv.getText().toString().equals(resources.getString(R.string.edit))){
                    AppConstants.isAddressEditable=true;
                    addressFragmentRK.notifier();
                    action_bar_move_forward_tv.setText(resources.getString(R.string.save));
                } else {
                    AppConstants.isAddressEditable=false;
                    addressFragmentRK.notifier();
                    action_bar_move_forward_tv.setText(resources.getString(R.string.edit));
                }
            }
        });
    }
}
