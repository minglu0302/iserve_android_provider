package com.app.partner.iserve;

import android.app.Application;

import com.app.partner.iserve.Utility.ServiceUrls;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

/**
 * Created by embed on 6/7/16.
 * <P>This class will be first called when application loads
 * socket is initialized
 * and this same socket instance will be used everywhere in the project <p/>
 *
 */
public class ApplicationController extends Application
{
    private static Socket socket;
    {
        try
        {
            IO.Options opts = new IO.Options();
            opts.reconnection=true;
            socket= IO.socket(ServiceUrls.SOCKET_PATH,opts);
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
    }

    public static Socket getSocketInstance()
    {
        return socket;
    }


}
