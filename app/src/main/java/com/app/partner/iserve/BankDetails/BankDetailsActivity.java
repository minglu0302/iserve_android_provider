package com.app.partner.iserve.BankDetails;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Utility;

public class BankDetailsActivity extends AppCompatActivity {

    private Typeface fontBold;
    private TextView mToolbar_title,tvAdd;
    private ImageView ivBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);

        mInitLayoutId();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mInitLayoutId()
    {
        fontBold= Utility.getFontBold(BankDetailsActivity.this);

        mToolbar_title= (TextView) findViewById(R.id.mToolbar_title);
        ivBackBtn= (ImageView) findViewById(R.id.ivBackbtn);
        tvAdd= (TextView) findViewById(R.id.tvAdd);
        mToolbar_title.setText(getResources().getString(R.string.Bank_Details));
        mToolbar_title.setTypeface(fontBold);
        tvAdd.setTypeface(fontBold);

        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
