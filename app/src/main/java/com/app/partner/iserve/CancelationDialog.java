package com.app.partner.iserve;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.app.partner.iserve.Adapters.CancelAdapter;
import com.app.partner.iserve.Pojo.GetCancellationReasons;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by embed on 8/10/16.
 *
 * <p> This class is used to show the cancellation dialogue , which will be used to cancel the
 * job or appointment.
 * It loads the cancellation reasons dynamically by calling API
 *check <code> mGetCancellationReasons() <code/>
 *<code> mAbortAppointment() <code/> is used to cancel the appointment and the response will be
 * passed to the calling activity.
 * <p/>
 */
public class CancelationDialog
{
    private Activity activity;

    private Dialog dialog;

    private int reason=-1;

    private SessionManager sessionManager;

    private ProgressDialog mProgressDailog;

    private CancelationCallback callback;

    //private String[] reasons;

    private ArrayList<String> reasons;

    private CancelAdapter adapter;
    private RecyclerView recyclerView;

    public CancelationDialog(Activity activity,CancelationCallback callback)
    {
        this.activity=activity;
        this.callback=callback;
        sessionManager=new SessionManager(activity);

    }

    public void showCancellationDialog(final String bid,final String cid)
    {
        dialog=new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cancel_pop_up);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

       // reasons=activity.getResources().getStringArray(R.array.cancelation_reasons);

        recyclerView= (RecyclerView) dialog.findViewById(R.id.cancelListRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        reasons=new ArrayList<>();



        TextView tvSubmit= (TextView) dialog.findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.submitOnclick(CancelationDialog.this);
            }
        });

        dialog.show();
        mGetCancellationReasons(bid,cid);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
        public void mAbortAppointment(final String reason, final String bid, final String cid)
        {
            if(Utility.isNetworkAvailable(activity))
            {

                mProgressDailog=Utility.getProcessDialog(activity);

                if(mProgressDailog!=null && !mProgressDailog.isShowing())
                {
                    mProgressDailog.show();
                }
                RequestBody requestBody= new FormEncodingBuilder()
                        .add("ent_sess_token",sessionManager.getSessionToken())
                        .add("ent_dev_id",sessionManager.getDeviceId())
                        .add("ent_reason",reason)
                        .add("ent_bid",bid)
                        .add("ent_date_time",Utility.getCurrentGmtTime())
                        .build();

                Okhttp_connection.doOkhttpRequest(ServiceUrls.ABORT_APPOINTMENT, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if(mProgressDailog!=null && mProgressDailog.isShowing()){
                            mProgressDailog.dismiss();
                        }
                        Utility.printLog("PK response"+result);
                        if(dialog!=null && dialog.isShowing())
                        {
                            dialog.dismiss();
                        }
                        callback.onSuccess(result);

                        try {
                            if(result!=null && ! result.equals(""))
                            {
                                JSONObject jsonObject=new JSONObject(result);
                                if(jsonObject.get("errFlag").equals("0"))
                                {
                                    MainActivity.acknowledgeCustomer(bid,cid, AppConstants.STATUS_CANCEL,sessionManager.getProviderID(),reason+"");
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(String error)
                    {
                        if(mProgressDailog!=null && mProgressDailog.isShowing()){
                            mProgressDailog.dismiss();
                        }
                        Utility.printLog("PK error"+error);
                        callback.onError(error);
                    }
                });

            }
            else
            {
                Snackbar.make(activity.getCurrentFocus(), activity.getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                        .setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view)
                            {
                                mAbortAppointment(reason,bid,cid);
                            }
                        }).show();
            }

        }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public interface CancelationCallback
    {
         void onSuccess(String result);

         void onError(String error);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mGetCancellationReasons(final String bid, final String cid)
    {

        if(Utility.isNetworkAvailable(activity))
        {

            mProgressDailog=Utility.getProcessDialog(activity);

            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }
            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_lan","0")
                    .add("ent_user_type",1+"")
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_CANCELLATION_REASON, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK response"+result);
                    //callback.onSuccess(result);

                    try {
                        if(result!=null && ! result.equals(""))
                        {
                            Gson gson=new Gson();
                            GetCancellationReasons cancellationReasons=gson.fromJson(result,GetCancellationReasons.class);

                            if(cancellationReasons.getErrFlag().equals("0"))
                            {
                                reasons=cancellationReasons.getData();
                                //adapter.notifyDataSetChanged();
                                adapter=new CancelAdapter(activity,reasons,bid,cid);

                                recyclerView.setAdapter(adapter);

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK error"+error);
                    callback.onError(error);
                }
            });

        }
        else
        {
            Snackbar.make(activity.getCurrentFocus(), activity.getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mGetCancellationReasons(bid,cid);
                        }
                    }).show();
        }
    }
}
