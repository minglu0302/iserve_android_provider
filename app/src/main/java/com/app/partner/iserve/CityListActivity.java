package com.app.partner.iserve;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import com.app.partner.iserve.Adapters.CityListAdapter;
import com.app.partner.iserve.Pojo.City;
import com.app.partner.iserve.Utility.Utility;

public class CityListActivity extends AppCompatActivity {

    private RecyclerView cityList;
    private TextView mToolbar_title;
    private ArrayList<City> cityArrayList;
    private TextView tvDone;
    private CityListAdapter adapter;
    private ImageView ivBackbtn;
    private EditText search_et;
    private ArrayList<City> filteredCityList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list);

        initLayoutId();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initLayoutId()
    {

        Typeface fontBold = Utility.getFontBold(CityListActivity.this);

        cityList=(RecyclerView)findViewById(R.id.cityRecyclerView);

        mToolbar_title=(TextView)findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvDone=(TextView)findViewById(R.id.tvDone);
        ivBackbtn=(ImageView)findViewById(R.id.ivBackbtn);
        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        cityArrayList= (ArrayList<City>) getIntent().getSerializableExtra("CITY_LIST");

        if(cityArrayList!=null)
        {
            if(cityArrayList.size()>0)
            {
                filteredCityList.addAll(cityArrayList);

                adapter=new CityListAdapter(CityListActivity.this,filteredCityList);
                LinearLayoutManager llm = new LinearLayoutManager(this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                cityList.setLayoutManager(llm);
                cityList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }

        tvDone.setOnClickListener(adapter);
        search_et= (EditText) findViewById(R.id.search_et);
        search_et.setTypeface(fontBold);

        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                search(s.toString());
            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setResult(String cityId, String selectedCity)
    {
        Intent intent=new Intent(CityListActivity.this,RegisterActivity.class);
        intent.putExtra("SELECTED_CITY",cityId);
        intent.putExtra("SELECTED_CITY_NAME",selectedCity);
        setResult(RESULT_OK,intent);
        finish();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void search(String text)
    {
        Utility.printLog("Search() workplace size "+cityArrayList.size()+" filteredWorkPlace size "+filteredCityList.size());

        filteredCityList.clear();

        if(!text.isEmpty())
        {
            for (City city : cityArrayList)
            {
                if (city.getCity_Name().toLowerCase(Locale.ENGLISH)
                        .contains(text.toLowerCase()))
                {
                    filteredCityList.add(city);
                }
            }
        }
        else
        {
            filteredCityList.addAll(cityArrayList);
        }
        adapter.notifyDataSetChanged();
    }

}
