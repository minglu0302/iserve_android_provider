package com.app.partner.iserve.CountryPicker;

public interface CountryPickerListener {
	public void onSelectCountry(String name, String code, String dialCode);
}
