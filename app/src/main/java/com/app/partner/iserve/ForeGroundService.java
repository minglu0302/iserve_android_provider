package com.app.partner.iserve;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by embed on 19/1/17.
 */
public class ForeGroundService extends Service implements GoogleApiClient.ConnectionCallbacks
{

    private Socket socket;
    private String eventLiveBooking="LiveBooking";
    private Emitter.Listener listener;
    private static ForeGroundService instance;
    private SessionManager sessionManager;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private String TAG="ForeGroundService";
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location myLoc;
    private double currentLat,currentLng;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ForeGroundService getInstance()
    {
        return instance;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try
        {
            if (intent.getAction().equals(AppConstants.ACTION.STARTFOREGROUND_ACTION))
            {
                Intent notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.setAction(AppConstants.ACTION.MAIN_ACTION);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_RECEIVER_FOREGROUND);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                        notificationIntent, 0);

                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                        R.drawable.ic_launcher);

                Notification notification = new NotificationCompat.Builder(this)
                        .setContentTitle(getString(R.string.app_name))
                        .setTicker("")
                        .setContentText("Running...")
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                        //.setContentIntent(pendingIntent)
                        .setOngoing(true).build();

                /*.....................................................*/
                instance=ForeGroundService.this;

                sessionManager=new SessionManager(ForeGroundService.this);

                mListenToSocket();

                buildGoogleApiClient();

                mGoogleApiClient.connect();

                mUpdateHeartBeat();

                startPublishingWithTimer();

                startForeground(AppConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);


            }
            else if (intent.getAction().equals(
                    AppConstants.ACTION.STOPFOREGROUND_ACTION))
            {
                stopForeground(true);
                stopSelf();
            }

        }catch (NullPointerException e)
        {
            e.printStackTrace();
            Utility.printLog("Crashed in forground service");
        }

        return START_STICKY;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mListenToSocket()
    {
        socket=ApplicationController.getSocketInstance();

        listener=new Emitter.Listener() {
            @Override
            public void call(Object... args)
            {
                JSONObject jsonObject=(JSONObject) args[0];

                try
                {


                    /*SocketResponse socketResponse;
                    Gson gson=new Gson();
                    socketResponse=gson.fromJson(jsonObject.toString(),SocketResponse.class);*/
                    Utility.printLog("AAAAAAA",jsonObject.toString());
                    if(jsonObject.getString("btype").equals("1")||jsonObject.getString("btype").equals("3") || jsonObject.getString("btype").equals("2"))
                    {
                        if(!Utility.isApplicationSentToBackground(ForeGroundService.this))
                        {
                            Utility.printLog(TAG+" app is in foreground");
                            Intent homeIntent=new Intent("com.app.iserve.intent.action.MESSAGE_SOCKET");
                            homeIntent.putExtra("MESSAGE_SOCKET",jsonObject.toString());
                            sendBroadcast(homeIntent);

                        }
                        else
                        {
                            sessionManager.setBookingData(jsonObject.toString());
                            mSendNotification(jsonObject.toString());
                            Utility.printLog(TAG+" MainScreen is on app is in background");
                            if(!MainActivity.screenOff)
                            {
                                Utility.printLog(TAG+" MainScreen is off");
                                Intent homeIntent=new Intent("com.app.iserve.intent.action.MESSAGE_SOCKET");
                                homeIntent.putExtra("MESSAGE_SOCKET",jsonObject.toString());
                                sendBroadcast(homeIntent);
                            }

                        }
                    }

                }
                catch (Exception e)
                {
                    Utility.printLog("Service: Caught Exception",e.getMessage());
                    //Toast.makeText(ForeGroundService.this, "Service: Caught Exception", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        };

        if(socket!=null)
        {
            socket.disconnect();
            socket.connect();
            socket.off(eventLiveBooking);
            socket.on(eventLiveBooking,listener);
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mStopListening()
    {
        if(socket!=null)
        {
            socket.off(eventLiveBooking);
            socket.disconnect();
        }

    }

    public void mStopEmitting()
    {
        if(mTimer!=null){
            mTimerTask.cancel();
            mTimer.cancel();
        }
        if(mGoogleApiClient!=null)
        {
            mGoogleApiClient.disconnect();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mReconnectSocket()
    {
        Utility.printLog("Pkkkk.... Reconnecting socket..");
        if(socket!=null && listener!=null)
        {
            if(!socket.connected())
            {
                socket.connect();
                socket.on(eventLiveBooking,listener);

                mUpdateHeartBeat();
            }
            else
            {
                socket.off(eventLiveBooking);
                socket.on(eventLiveBooking,listener);
            }
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mSendNotification(String msg)
    {
            Intent intent=new Intent(ForeGroundService.this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onDestroy()
    {
        super.onDestroy();

        mStopListening();

        mStopEmitting();

        if(socket!=null && socket.connected())
        {
            socket.disconnect();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void startPublishingWithTimer()
    {
        mTimer=new Timer();
        mTimerTask= new TimerTask() {
            @Override
            public void run() {
                if(Utility.isNetworkAvailable(getApplicationContext()))
                {
                  /*  if(socket.connected())
                    {
                        JSONObject jsonObject=new JSONObject();
                        try {
                            jsonObject.put("email",sessionManager.getProviderEmail());
                            jsonObject.put("status","1");
                            socket.emit("ProviderStatus",jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }*/
                    mStartEmitting();
                }

                Utility.printLog(TAG+" Socket connected "+socket.connected());
                Utility.printLog(TAG+" Socket connected "+socket.id());
            }
        };
        mTimer.schedule(mTimerTask,000,4000);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mStartEmitting()
    {

        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.put("email",sessionManager.getProviderEmail());
            jsonObject.put("lat",currentLat);
            jsonObject.put("long",currentLng);
            jsonObject.put("pid",sessionManager.getProviderID());
            jsonObject.put("devId", Utility.getDeviceId(this));

            socket.emit("UpdateProvider",jsonObject);
            Log.v("Socket sendMessage",jsonObject.toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(2000);
        mLocationRequest.setSmallestDisplacement(20);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
            @Override
            public void onLocationChanged(Location currentLoc)
            {
                myLoc=currentLoc;

                currentLat = currentLoc.getLatitude();
                currentLng = currentLoc.getLongitude();
                sessionManager.setProviderCurrentlat(""+currentLat);
                sessionManager.setProviderCurrentlong(""+currentLng);
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mUpdateHeartBeat()
    {

        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.put("email",sessionManager.getProviderEmail());
            jsonObject.put("status","1");
            jsonObject.put("pid",sessionManager.getProviderID());


            socket.emit("ProviderStatus",jsonObject);
            Log.v("Socket sendMessage",jsonObject.toString());
            Log.d("UpdateHeartBeat called",jsonObject.toString());

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
}
