package com.app.partner.iserve;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by rahul on 15/10/16.
 */

public class ForgotPassOtpActivity extends Activity
{
    private String mobileNumber="";
    private EditText stup2text1,stup2text2,stup2text3,stup2text4,stup2text5;
    private ProgressDialog dialogL;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pass_otp_screen);
        intialize();
    }

    private void intialize()
    {
        mobileNumber= getIntent().getStringExtra("NUMBER");
        Utility.printLog("number in otp "+mobileNumber);

        /*Typeface nexaBold = Typeface.createFromAsset(this.getAssets(), "fonts/NexaBold.otf");
        Typeface nexaLight = Typeface.createFromAsset(this.getAssets(), "fonts/NexaLight.otf");*/

        TextView title= (TextView) findViewById(R.id.title);
        TextView tvDescription= (TextView) findViewById(R.id.tvDescription);
        TextView submit= (TextView) findViewById(R.id.submit);
        stup2text1= (EditText) findViewById(R.id.stup2text1);
        stup2text2= (EditText) findViewById(R.id.stup2text2);
        stup2text3= (EditText) findViewById(R.id.stup2text3);
        stup2text4= (EditText) findViewById(R.id.stup2text4);
        stup2text5= (EditText) findViewById(R.id.stup2text5);
        ImageView ivBackbtn= (ImageView) findViewById(R.id.ivBackbtn);

        stup2text1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                stup2text2.requestFocus();
            }
        });stup2text2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                stup2text3.requestFocus();
            }
        });stup2text3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                stup2text4.requestFocus();
            }
        });stup2text4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count==1)
                    mVerifyPhone();
            }

            @Override
            public void afterTextChanged(Editable s) {
                stup2text5.requestFocus();
            }
        });

        stup2text5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count==1)
                    mVerifyPhone();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvDescription.setText(getResources().getString(R.string.text_for_otp)+" "+mobileNumber);

        //title.setTypeface(nexaBold);
       /* tvDescription.setTypeface(nexaLight);
        submit.setTypeface(nexaBold);
        stup2text1.setTypeface(nexaLight);
        stup2text2.setTypeface(nexaLight);
        stup2text3.setTypeface(nexaLight);
        stup2text5.setTypeface(nexaLight);
        stup2text4.setTypeface(nexaLight);*/
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mVerifyPhone()
    {

        dialogL= Utility.getProcessDialog(ForgotPassOtpActivity.this);
        dialogL.setCancelable(true);
        dialogL.setMessage(getResources().getString(R.string.pleaseWait));
        if (dialogL!=null)
        {
            dialogL.show();
        }
        jsonObject=new JSONObject();
        try
        {
            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_phone",mobileNumber)
                    .add("ent_user_type",1+"")
                    .add("ent_service_type",2+"")
                    .add("ent_code",stup2text1.getText().toString()+stup2text2.getText().toString()+stup2text3.getText().toString()+stup2text4.getText().toString()/*+ stup2text5.getText().toString()*/)
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.SERVICE_URL + "verifyPhone", requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {

                    Utility.printLog( "success for updated the language   " + result);
                    if (dialogL!=null)
                    {
                        dialogL.dismiss();
                    }
                    try {
                        if (result != null) {
                            jsonObject = new JSONObject(result);
                            String errFlag = jsonObject.getString("errFlag");
                            String errMsg = jsonObject.getString("errMsg");

                            if(errFlag.equals("0"))
                            {
                                Intent intent=new Intent(ForgotPassOtpActivity.this,ForgotpassNewPassActivity.class);
                                intent.putExtra("MOBILE",mobileNumber);
                                intent.putExtra("FROM","1");
                                startActivity(intent);
                                finish();
                            }
                            else
                            {
                                Utility.ShowAlert(errMsg,ForgotPassOtpActivity.this);
                                stup2text1.setText(null);
                                stup2text2.setText(null);
                                stup2text3.setText(null);
                                stup2text4.setText(null);
                                stup2text5.setText(null);
                            }
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error) {
                    if (dialogL!=null)
                    {
                        dialogL.dismiss();
                    }
                    //Toast.makeText(ForgotPwdActivity.this, getResources().getString(R.string.servererror), Toast.LENGTH_LONG).show();
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
