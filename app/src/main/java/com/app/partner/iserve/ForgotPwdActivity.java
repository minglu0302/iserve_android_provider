package com.app.partner.iserve;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Locale;

import com.app.partner.iserve.CountryPicker.CountryPicker;
import com.app.partner.iserve.CountryPicker.CountryPickerListener;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.Utility;


public class ForgotPwdActivity extends FragmentActivity implements OnClickListener
{
	private EditText etPhoneNo;
	private JSONObject jsonObject;
	ProgressDialog dialogL;
	private Typeface nexaBold,nexaLight;
	private TextView countryCode;
	private ImageView flag;
	private String phoneNo="";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_password);
		intialize();
	}

	private void intialize() 
	{

		//Typeface font = Utility.getFontRegular(ForgotPwdActivity.this);
		//Typeface fontBold = Utility.getFontBold(ForgotPwdActivity.this);

		etPhoneNo =(EditText)findViewById(R.id.etPass);
		//etPhoneNo.setTypeface(font);

		ImageView back = (ImageView) findViewById(R.id.ivBackbtn);

		Button submit = (Button) findViewById(R.id.submit);
		//submit.setTypeface(font);

		TextView tvHeader1 = (TextView) findViewById(R.id.tvHeader1);
		//tvHeader1.setTypeface(fontBold);

		TextView tvHeader2 = (TextView) findViewById(R.id.tvHeader2);
		//tvHeader2.setTypeface(font);

		TextView title = (TextView) findViewById(R.id.title);
		//title.setTypeface(font);

		RelativeLayout countryPicker= (RelativeLayout) findViewById(R.id.countryPicker);
		countryCode= (TextView) findViewById(R.id.code);
		flag= (ImageView) findViewById(R.id.flag);

		/**
		 * to get the default country code
		 */
		getCountryDialCode();

		phoneNo = etPhoneNo.getText().toString();
		if(etPhoneNo.getText().toString().length()>0)
		{
			if(etPhoneNo.getText().toString().charAt(0)=='0')
			{
				phoneNo = etPhoneNo.getText().toString().substring(1);
			}
			else
			{
				phoneNo= etPhoneNo.getText().toString();
			}
		}

		back.setOnClickListener(this);
		submit.setOnClickListener(this);
		countryPicker.setOnClickListener(this);
	}

	/**
	 * to get default countrycode
	 * @return
	 */
	public String getCountryDialCode(){
		String contryId;
		String contryDialCode = null;
		TelephonyManager telephonyMngr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		contryId = telephonyMngr.getNetworkCountryIso().toUpperCase();
		String[] arrContryCode=ForgotPwdActivity.this.getResources().getStringArray(R.array.DialingCountryCode);
		for (String anArrContryCode : arrContryCode) {
			String[] arrDial = anArrContryCode.split(",");
			if (arrDial[1].trim().equals(contryId.trim())) {
				contryDialCode = arrDial[0];
				countryCode.setText(contryDialCode);
				String drawableName = "flag_"
						+ contryId.toLowerCase(Locale.ENGLISH);
				flag.setBackgroundResource(getResId(drawableName));
				break;
			}
		}
		return contryDialCode;
	}

	@Override
	public void onClick(View v)
	{
		if(v.getId()==R.id.ivBackbtn)
		{
			finish();
			//overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
			return;
		}
		
		if(v.getId()==R.id.submit)
		{
			if(!etPhoneNo.getText().toString().isEmpty())
			{
				if(etPhoneNo.getText().toString().charAt(0)=='0')
				{
					phoneNo = etPhoneNo.getText().toString().substring(1);
				}
				else
				{
					phoneNo = etPhoneNo.getText().toString();
				}

				if(Utility.isNetworkAvailable(ForgotPwdActivity.this)){
					BackgroundFrgtPwd();
				}

			}
			else
			{
				Utility.ShowAlert(getResources().getString(R.string.mobile_empty), ForgotPwdActivity.this);
			}
		}
		if(v.getId()==R.id.countryPicker)
		{
			showDialoagforcountrypicker();
		}
	}
	void showDialoagforcountrypicker(){
		final CountryPicker picker = CountryPicker.newInstance(getResources().getString(R.string.select_country));
		picker.show(getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
		picker.setListener(new CountryPickerListener()
		{
			@Override
			public void onSelectCountry(String name, String code, String dialCode) {
				countryCode.setText(dialCode);
				String drawableName = "flag_"
						+ code.toLowerCase(Locale.ENGLISH);
				flag.setBackgroundResource(getResId(drawableName));

				picker.dismiss();
			}
		});
	}


	private void BackgroundFrgtPwd()
	{
		dialogL= Utility.getProcessDialog(ForgotPwdActivity.this);
		dialogL.setCancelable(true);
		dialogL.setMessage(getResources().getString(R.string.pleaseWait));
		if (dialogL!=null)
		{
			dialogL.show();
		}
		jsonObject=new JSONObject();
		try
		{
			String curenttime=Utility.getCurrentGmtTime();
			RequestBody requestBody=new FormEncodingBuilder()
					.add("ent_date_time", curenttime)
					.add("ent_mobile", phoneNo)
					.add("ent_user_type", ""+1)
					.build();

			Okhttp_connection.doOkhttpRequest(ServiceUrls.SERVICE_URL + "ForgotPasswordWithOtp", requestBody, new Okhttp_connection.OkHttpRequestCallback() {
				@Override
				public void onSuccess(String result) {

					Utility.printLog( "success for updated the language   " + result);
					if (dialogL!=null)
					{
						dialogL.dismiss();
					}
					try {
						if (result != null) {
							jsonObject = new JSONObject(result);
							String errFlag = jsonObject.getString("errFlag");
							String errMsg = jsonObject.getString("errMsg");

							if (errFlag.equals("0")) {
								Intent intent = new Intent(ForgotPwdActivity.this, ForgotPassOtpActivity.class);
								intent.putExtra("NUMBER", phoneNo);
								startActivity(intent);
								finish();
							} else {
								Utility.ShowAlert(errMsg, ForgotPwdActivity.this);
							}
						}
					}catch (Exception e)
					{
						e.printStackTrace();
					}
				}

				@Override
				public void onError(String error) {
					if (dialogL!=null)
					{
						dialogL.dismiss();
					}
					//Toast.makeText(ForgotPwdActivity.this, getResources().getString(R.string.servererror), Toast.LENGTH_LONG).show();
				}
			});

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void onBackPressed() 
	{
		finish();
		//overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
	}

	public static int getResId(String drawableName)
	{
		try
		{
			Class<R.drawable> res = R.drawable.class;
			Field field = res.getField(drawableName);
			int drawableId = field.getInt(null);
			return drawableId;
		} catch (Exception e) {
			Log.e("CountryCodePicker", "Failure to get drawable id.", e);
		}
		return -1;
	}

}
