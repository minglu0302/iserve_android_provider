package com.app.partner.iserve;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by rahul on 15/10/16.
 */

public class ForgotpassNewPassActivity extends Activity
{
    private String mobileNumber="";
    private TextView etConfirmPass,etCurPass;
    private Button submit;
    private ProgressDialog mdialog;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_new_password);
        sessionManager=new SessionManager(ForgotpassNewPassActivity.this);
        intialize();
    }

    private void intialize()
    {
        mobileNumber= getIntent().getStringExtra("MOBILE");
        String from= getIntent().getStringExtra("FROM");

        TextView title= (TextView) findViewById(R.id.title);
        TextView tvHeader= (TextView) findViewById(R.id.tvHeader);
        final TextView etPass= (TextView) findViewById(R.id.etPass);
        etConfirmPass= (TextView) findViewById(R.id.etConfirmPass);
        etCurPass= (TextView) findViewById(R.id.etCurPass);
        submit= (Button) findViewById(R.id.submit);
        ImageView ivBackbtn= (ImageView) findViewById(R.id.ivBackbtn);
        TextInputLayout etCurPasswordWrapper= (TextInputLayout) findViewById(R.id.etCurPasswordWrapper);
        if(from.equals("1"))
        {
            tvHeader.setVisibility(View.VISIBLE);
            etCurPasswordWrapper.setVisibility(View.GONE);
        }


        mdialog = Utility.getProcessDialog(ForgotpassNewPassActivity.this);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etPass.getText().toString().isEmpty())
                {
                    if(etPass.getText().toString().equals(etConfirmPass.getText().toString()) && etCurPass.getText().toString().equals(sessionManager.getPassword()))
                    {
                        submitPass();
                    }
                    else
                    {
                        Utility.ShowAlert(getResources().getString(R.string.passDintMatch),ForgotpassNewPassActivity.this);
                    }
                }
                else
                {
                    Utility.ShowAlert(getResources().getString(R.string.enterPass),ForgotpassNewPassActivity.this);
                }

            }
        });

        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

       /* Typeface nexaBold = Typeface.createFromAsset(this.getAssets(), "fonts/NexaBold.otf");
        Typeface nexaLight = Typeface.createFromAsset(this.getAssets(), "fonts/NexaLight.otf");*/

        /*tvHeader.setTypeface(nexaLight);
        etPass.setTypeface(nexaLight);
        etConfirmPass.setTypeface(nexaLight);
        submit.setTypeface(nexaBold);*/
    }


    private void submitPass()
    {

        mdialog.setMessage(getResources().getString(R.string.pleaseWait));
        mdialog.show();
        mdialog.setCancelable(false);


        try
        {
            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_mobile", mobileNumber)
                    .add("ent_pass", etConfirmPass.getText().toString())
                    .add("ent_user_type","1")
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.SERVICE_URL +"updatePasswordForUser", requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {

                    Utility.printLog("updatePasswordForUser Result "+result);
                    String errFlag = null;
                    String errMsg = null;

                    if(mdialog!=null && mdialog.isShowing())
                    {
                        mdialog.dismiss();
                    }
                    try
                    {
                        JSONObject jsonObject1=new JSONObject(result);
                        errFlag = jsonObject1.getString("errFlag");
                        errMsg = jsonObject1.getString("errMsg");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(errFlag.equals("0"))
                    {
                        finish();
                    }
                    else
                    {
                        Utility.ShowAlert(errMsg,ForgotpassNewPassActivity.this);
                    }
                }

                @Override
                public void onError(String error) {
                    if(mdialog!=null && mdialog.isShowing())
                    {
                        mdialog.dismiss();
                    }
                    Toast.makeText(ForgotpassNewPassActivity.this, getResources().getString(R.string.servererror), Toast.LENGTH_LONG).show();
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
