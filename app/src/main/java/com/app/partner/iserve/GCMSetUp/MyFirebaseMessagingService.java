package com.app.partner.iserve.GCMSetUp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app.partner.iserve.MainActivity;
import com.app.partner.iserve.Messaging.MessagingActivityNew;
import com.app.partner.iserve.R;
import com.app.partner.iserve.SplashActivity;
import com.app.partner.iserve.Utility.Utility;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by embed on 18/9/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    private String action,message,st;
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0)
        {
            Log.d(TAG, "Messagedata: " + remoteMessage.getData());
            Map map=remoteMessage.getData();

            if(map.containsKey("action"))
            {
                action= (String) map.get("action");
            }
            if(map.containsKey("payload"))
            {
                message= (String) map.get("payload");
            }
            if(map.containsKey("st"))
            {
                st= (String) map.get("st");
            }

            if(action!=null && action.equals("7"))
            {
                if(Utility.isApplicationSentToBackground(MyFirebaseMessagingService.this))
                {
                    message="You got new request from the customer... ";
                    sendNotification(message,action);
                }
                else
                {
                    Intent homeIntent=new Intent("com.app.partner.firbase");
                    Bundle bundle=new Bundle();
                    bundle.putString("action",action);
                    bundle.putString("payload",message);
                    homeIntent.putExtras(bundle);
                    sendBroadcast(homeIntent);
                }
            }
            if(st!=null && st.equals("8"))
            {
                if(!MessagingActivityNew.isOpen)
                {

                    sendNotification(message,st);

                }
               /* else
                {
                    Intent homeIntent=new Intent("com.app.partner.firbase");
                    Bundle bundle=new Bundle();
                    bundle.putString("action",action);
                    bundle.putString("payload",message);
                    homeIntent.putExtras(bundle);
                    sendBroadcast(homeIntent);
                }*/
            }

            if(action != null && action.equals("3"))
            {
                if(message != null)
                {
                    sendNotification(message);
                }
                else
                {
                    sendNotification(getString(R.string.customerCancel));
                }

                Intent cancelIntent = new Intent("com.app.partner.firbase.cancel");
                Bundle bundle=new Bundle();
                bundle.putString("action",action);
                bundle.putString("payload",message);
                cancelIntent.putExtras(bundle);
                sendBroadcast(cancelIntent);
            }

            if(action != null && action.equals("2")) {
                if (message != null) {
                    sendNotification(message);
                } else {
                    sendNotification(getString(R.string.slotAlotted));
                }
                Intent cancelIntent = new Intent("com.app.partner.firbase.shedule");
                Bundle bundle=new Bundle();
                bundle.putString("action",action);
                bundle.putString("payload",message);
                cancelIntent.putExtras(bundle);
                sendBroadcast(cancelIntent);
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null)
        {
            Log.d(TAG, "MessageNotification Body: " + remoteMessage.getNotification());
        }

    }

    private void sendNotification(String messageBody,String status)
    {

        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();

        Notification notification = new Notification(icon, messageBody, when);
        String title = this.getString(R.string.app_name);
        PendingIntent intent =null;

        Intent notificationIntent;

        if(status.equals("8"))
        {
            notificationIntent = new Intent(this, MessagingActivityNew.class);
            try
            {
                JSONObject jsonObject=new JSONObject(messageBody);
                String msg=jsonObject.getString("msg");
                String pic=jsonObject.getString("pic");
                String bid=jsonObject.getString("bid");
                String name=jsonObject.getString("name");

                Bundle bundle=new Bundle();
                bundle.putString("msg",msg);
                bundle.putString("name",name);
                bundle.putString("pic",pic);
                bundle.putString("bid",bid);

                messageBody=msg;

                notificationIntent.putExtras(bundle);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }
        else
        {
            notificationIntent = new Intent(this, SplashActivity.class);
        }

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        notification.flags |= Notification.FLAG_AUTO_CANCEL;


        Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);

        //Assign inbox style notification
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(messageBody);
        bigText.setBigContentTitle(title);
        //bigText.setSummaryText("Alert");

        //build notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentIntent(intent)
                        //.setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission
                        .setPriority(NotificationCompat.PRIORITY_MAX) //must give priority to High, Max which will considered as heads-up notification)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setContentText(messageBody)
                        .setLargeIcon(icon1)
                        .setStyle(bigText);

        mBuilder.setDefaults(Notification.DEFAULT_SOUND);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    private void sendNotification(String messageBody)
    {

        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();

        Notification notification = new Notification(icon, messageBody, when);
        String title = this.getString(R.string.app_name);

        Intent notificationIntent = new Intent(this, MainActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent  intent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        notification.flags |= Notification.FLAG_AUTO_CANCEL;


        Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);

        //Assign inbox style notification
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(messageBody);
        bigText.setBigContentTitle(title);
        //bigText.setSummaryText("Alert"
        //build notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentIntent(intent)
                        .setPriority(NotificationCompat.PRIORITY_MAX) //must give priority to High, Max which will considered as heads-up notification)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setContentText(messageBody)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(Bitmap.createScaledBitmap(icon1, 128, 128, false))
                        .setStyle(bigText);

        mBuilder.setDefaults(Notification.DEFAULT_SOUND);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());


    }


}
