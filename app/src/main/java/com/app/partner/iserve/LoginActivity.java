package com.app.partner.iserve;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.Utility.AppConstants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import com.app.partner.iserve.Pojo.MasterLoginPojo;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

/**
 * Created by embed on 23/2/16.
 */
public class LoginActivity extends AppCompatActivity
{
    private TextInputLayout passwordWrapper,usernameWrapper;
    private ImageView ivBackBtn;
    private Button btnLogin;
    private TextView tvForgotPassword,tvCheckBoxTxt;
    private SessionManager sessionManager;
    private String TAG="LoginActivity";
    private MasterLoginPojo masterLoginResponse;
    private ProgressDialog mProgressDailog;
    private TextView mToolbar_title;
    private CheckBox ivCheckbox;
    private String currentVersion;
    private JSONObject jsonObject;
    private double[] location;
    private String latitude,longitude;
    private Typeface font,fontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager=new SessionManager(LoginActivity.this);

        initLayoutId();

        if(sessionManager.getPushToken()==null)
        {
            String token = FirebaseInstanceId.getInstance().getToken();
            sessionManager.setPushToken(token);
        }
    }

    private void initLayoutId()
    {
        font=Utility.getFontRegular(LoginActivity.this);
        fontBold=Utility.getFontBold(LoginActivity.this);

        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        tvForgotPassword= (TextView) findViewById(R.id.tvForgotPass);
        tvCheckBoxTxt= (TextView) findViewById(R.id.tvCheckBoxTxt);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        ivBackBtn= (ImageView) findViewById(R.id.ivBackbtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        mToolbar_title = (TextView) findViewById(R.id.mToolbar_title);

        ivCheckbox = (CheckBox) findViewById(R.id.ivCheckbox);

        mToolbar_title.setText(getResources().getString(R.string.LOGIN));


        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mForgotPassword();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateFields()) {
                    mLoginService();
                   /* Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);*/
                }
            }
        });

       String name= Utility.getPreference(LoginActivity.this,"USER_EMAIL");
       String pass= Utility.getPreference(LoginActivity.this,"USER_PASSWORD");

        usernameWrapper.getEditText().setText(name);
        passwordWrapper.getEditText().setText(pass);

        if(!name.isEmpty() || !pass.isEmpty())
        {
            ivCheckbox.setChecked(true);
        }

        try
        {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        usernameWrapper.setTypeface(font);
        passwordWrapper.setTypeface(font);
        tvForgotPassword.setTypeface(font);
        tvCheckBoxTxt.setTypeface(font);
        mToolbar_title.setTypeface(fontBold);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private boolean validateFields()
    {
        if (usernameWrapper.getEditText().getText().toString().isEmpty())
        {
            usernameWrapper.setErrorEnabled(true);
            usernameWrapper.setError(getResources().getString(R.string.enterEmail));
            return false;
        }
        else if (passwordWrapper.getEditText().getText().toString().isEmpty())
        {
            usernameWrapper.setError(null);
            usernameWrapper.setErrorEnabled(false);
            passwordWrapper.setError(getResources().getString(R.string.NotValidPass));
            return false;
        }
        else
        {
            passwordWrapper.setError(null);
            passwordWrapper.setErrorEnabled(false);
            return true;
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mForgotPassword()
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.forgotpassdialogue);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        TextView email= (TextView) dialog.findViewById(R.id.forgotemail);
        email.setTypeface(font);

        TextView forgotmobile= (TextView) dialog.findViewById(R.id.forgotmobile);
        forgotmobile.setTypeface(font);

        TextView text= (TextView) dialog.findViewById(R.id.text);
        text.setTypeface(fontBold);

        TextView tvOr= (TextView) dialog.findViewById(R.id.tvOr);
        tvOr.setTypeface(fontBold);

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mForgotPassPopUp();
                dialog.dismiss();
            }
        });

        forgotmobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,ForgotPwdActivity .class);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        dialog.show();

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mLoginService()
    {
        String email=usernameWrapper.getEditText().getText().toString();
        String password=passwordWrapper.getEditText().getText().toString();

        if(sessionManager.getPushToken()==(null))
        {
            String token = FirebaseInstanceId.getInstance().getToken();
            sessionManager.setPushToken(token);
        }
        if(Utility.isNetworkAvailable(LoginActivity.this))
        {
            mProgressDailog=Utility.getProcessDialog(LoginActivity.this);
            mProgressDailog.setMessage("Logging in...");
            mProgressDailog.setCancelable(false);
            mProgressDailog.show();

            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_email",email)
                    .add("ent_password",password)
                    .add("ent_dev_id", Utility.getDeviceId(LoginActivity.this))
                    .add("ent_push_token", sessionManager.getPushToken())
                    .add("ent_device_type", ""+2)
                    .add("ent_date_time", Utility.getCurrentGmtTime())
                    .add("ent_dev_model", Build.MODEL)
                    .add("ent_dev_os",Build.VERSION.RELEASE)
                    .add("ent_app_version",currentVersion)
                    .add("ent_manf",Build.MANUFACTURER)
                    .add("ent_latitude",latitude)
                    .add("ent_longitude",longitude)
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.MASTER_LOGIN, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {

                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }

                    Utility.printLog(TAG+" masterLogin Response "+result);
                    Gson gson=new Gson();
                    masterLoginResponse=gson.fromJson(result, MasterLoginPojo.class);
                    mLoginResponseHandler(masterLoginResponse);
                }

                @Override
                public void onError(String error) {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mLoginService();
                        }
                    }).show();
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mLoginResponseHandler(MasterLoginPojo masterLoginResponse)
    {
        try{
            if("0".equals(masterLoginResponse.getErrFlag()) && "9".equals(masterLoginResponse.getErrNum()))
            {
                if(ivCheckbox.isChecked())
                {
                    Utility.setPreference(LoginActivity.this,usernameWrapper.getEditText().getText().toString(),"USER_EMAIL");
                    Utility.setPreference(LoginActivity.this,passwordWrapper.getEditText().getText().toString(),"USER_PASSWORD");
                }
                else
                {
                    Utility.setPreference(LoginActivity.this,"","USER_EMAIL");
                    Utility.setPreference(LoginActivity.this,"","USER_PASSWORD");
                }

                sessionManager.createSession();
                sessionManager.setSessionToken(masterLoginResponse.getToken());
                sessionManager.setServerChannel(masterLoginResponse.getServerChn());
                sessionManager.setMyChannel(masterLoginResponse.getChn());
                sessionManager.setMyListnerChannel(masterLoginResponse.getSubsChn());
                sessionManager.setProviderEmail(masterLoginResponse.getEmail());
                sessionManager.setProviderID(masterLoginResponse.getProid());
                sessionManager.setPassword(passwordWrapper.getEditText().getText().toString());

                AppConstants.isFromLogin = true;
                Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
            else if("1".equals(masterLoginResponse.getErrFlag()))
            {
               switch (masterLoginResponse.getErrNum())
               {
                   case "8":
                       mShowErrorMessage(masterLoginResponse.getErrMsg());
                       break;

                   default:
                       mShowErrorMessage(masterLoginResponse.getErrMsg());
               }
            }
        }catch (Exception e )
        {
            e.printStackTrace();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(LoginActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });
        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mForgotPassPopUp()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this,5);
        alertDialog.setTitle(getResources().getString(R.string.forgotPassAlert));
        alertDialog.setMessage(getResources().getString(R.string.enterEmailAlert));


        final EditText input = new EditText(LoginActivity.this);
        input.setTypeface(font);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        input.setEms(1);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton(getResources().getString(R.string.send),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       // Toast.makeText(LoginActivity.this, input.getText().toString(), Toast.LENGTH_SHORT).show();
                        if(!input.getText().toString().equals(""))
                        {
                            if(Utility.validateEmail(input.getText().toString()))
                            {
                                mForgotPasswordService(input.getText().toString());
                            }
                            else
                            {
                                Toast.makeText(LoginActivity.this,getString(R.string.enterValidEmail),Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this,getString(R.string.plsEnterEmail),Toast.LENGTH_SHORT).show();
                        }

                    }
                });

        alertDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }
        );

        alertDialog.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mForgotPasswordService(String email)
    {

        if(Utility.isNetworkAvailable(LoginActivity.this))
        {
            mProgressDailog=Utility.getProcessDialog(LoginActivity.this);
            mProgressDailog.setMessage("Sending...");
            mProgressDailog.setCancelable(false);
            mProgressDailog.show();

            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_email",email)
                    .add("ent_user_type",""+1)
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.FORGOT_PASSWORD, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {

                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }

                    Utility.printLog(TAG+" forgotPassword Response "+result);

                    try {
                        if (result != null) {
                            jsonObject = new JSONObject(result);
                            String errFlag = jsonObject.getString("errFlag");
                            String errMsg = jsonObject.getString("errMsg");

                            if (errFlag.equals("0")) {
                                Utility.ShowAlert(errMsg, LoginActivity.this);
                            } else {
                                Utility.ShowAlert(errMsg, LoginActivity.this);
                            }
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(String error) {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mLoginService();
                        }
                    }).show();
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume()
    {
        super.onResume();
        location = Utility.getLocation(this);
        latitude = String.valueOf(location[0]);
        longitude = String.valueOf(location[1]);
        Log.i("location ", latitude + ", " + longitude);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onBackPressed()
    {
        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
        finish();

    }
}
