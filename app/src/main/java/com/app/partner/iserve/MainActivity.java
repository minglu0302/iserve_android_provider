package com.app.partner.iserve;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.TabActivities.ScheduleActivity;
import com.app.partner.iserve.Utility.MyNetworkChangeListner;
import com.app.partner.iserve.Utility.NetworkChangeReceiver;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import com.app.partner.iserve.Pojo.RespondToApptResponsePojo;
import com.app.partner.iserve.TabActivities.EarningsActivity;
import com.app.partner.iserve.TabActivities.HistoryActivity;
import com.app.partner.iserve.TabActivities.HomeActivity;
import com.app.partner.iserve.TabActivities.ProfileActivity;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.LocationUtil;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.RnuTimePermissionRequest;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.socket.client.Socket;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends TabActivity implements LocationUtil.GetLocationListener,TabHost.OnTabChangeListener {

    private int count=-1;
    public static TabHost thMain;
    private static Socket socket;
    private static String TAG="MainActivity";
    private CircularProgressBar circularProgressBar;
    private CountDownTimer mCounter;
    private int mm,ss;
    private String resp;
    private static MediaPlayer mediaPlayer;
    private Dialog alertDialog;
    private Button accept_btn,reject_btn;
    private  SessionManager sessionManager;
    private TextView mTVBid,mTvPickup,mTvReqHeader;
    private ProgressDialog mProgressDailog;
    private RespondToApptResponsePojo mRespondToApptResponsePojo;
    private String bid=null;
    private String cid=null;
    private LocationUtil locationUtil;
    private RnuTimePermissionRequest rnuTimePermissionRequest;
    private String[] permissions;
    private static final int PERMISSION_REQUEST_CODE=143;
    private boolean reqPopUpIsShowing=false;
    private NetworkChangeReceiver networkChangeReceiver;
    private Typeface fontBold,font;
    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private PowerManager.WakeLock wl;
    public static boolean screenOff;

    NotificationManager notificationManager;

    android.support.v7.app.AlertDialog alertDialogPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sessionManager=new SessionManager(this);
        sessionManager.setDeviceId(Utility.getDeviceId(this));

        thMain=getTabHost();

        fontBold=Utility.getFontBold(MainActivity.this);
        font=Utility.getFontRegular(MainActivity.this);

        setTabs();

        notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        rnuTimePermissionRequest =new RnuTimePermissionRequest(this);
        permissions= new String[]{ACCESS_FINE_LOCATION,CAMERA,WRITE_EXTERNAL_STORAGE};

        if(!rnuTimePermissionRequest.mCheckPermission(permissions))
        {
            rnuTimePermissionRequest.mRequestPermissions(permissions,PERMISSION_REQUEST_CODE);
        }
        socket=ApplicationController.getSocketInstance();
       // startService(new Intent(MainActivity.this, MyService.class));

        networkChangeReceiver=new NetworkChangeReceiver();
        networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
            @Override
            public void onNetworkStateChanges(boolean nwStatus) {
                Utility.printLog("PK Network Status MainActvty "+nwStatus);
                if(nwStatus)
                {
                    /*socket.off();
                    socket.disconnect();
                    socket.connect();
                    socket.on("LiveBooking",HandleIncomingMsg);*/
                    try
                    {
                        ForeGroundService.getInstance().mReconnectSocket();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }

            }
        });
        filter=new IntentFilter();
        filter.addAction("com.app.iserve.intent.action.MESSAGE_SOCKET");
        filter.addAction("android.intent.action.SCREEN_OFF");
        filter.addAction("android.intent.action.SCREEN_ON");
        filter.addAction("com.app.partner.firbase.cancel");
        filter.addAction("com.app.partner.firbase.shedule");
        receiver= new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                try
                {
                    if(intent.getAction().equals("com.app.partner.firbase.cancel") || intent.getAction().equals("com.app.partner.firbase.shedule"))
                    {
                        Bundle bundle = intent.getExtras();
                        String action = bundle.getString("action");
                        String payload = bundle.getString("payload");
                        if(action!=null)
                        {
                            if(action.equals("3") || action.equals("2"))
                            {
                                mShowErrorMessage(payload);
                            }
                        }
                        return;
                    }
                    String message=intent.getStringExtra("MESSAGE_SOCKET");
                    if(message!=null && ! message.isEmpty())
                    {
                        JSONObject jsonObject=new JSONObject(message);
                        if(!reqPopUpIsShowing)
                        {
                            try
                            {
                                thMain.setCurrentTab(2);
                                if(jsonObject.getString("btype").equals("1")||jsonObject.getString("btype").equals("3"))
                                {
                                    mRequestPopUp(30,jsonObject);
                                    sessionManager.setBookingData("");
                                }
                                else if(jsonObject.getString("btype").equals("2"))
                                {
                                    long timeInMil=jsonObject.getLong("stime");
                                    Date date=new Date(timeInMil*1000);
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

                                    Utility.printLog(TAG+" Later booking date "+format.format(date));
                                    String msg="You confirmed a booking request from "+jsonObject.getString("cname")/*+" at "+format.format(date)*/;
                                    Utility.mShowMessage(getResources().getString(R.string.messagetitle),msg,MainActivity.this);
                                }
                                else if(jsonObject.has("can_reason"))
                                {
                                    String msg=jsonObject.getString("can_reason");
                                    Utility.mShowMessage(getResources().getString(R.string.booking_cancelled),msg,MainActivity.this);
                                    thMain.setCurrentTab(0);
                                }

                            }
                            catch (Exception e) {
                                e.printStackTrace();
                                Log.d(TAG, "onReceive: "+e);
                            }
                        }
                    }

                    if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF))
                    {
                        screenOff = true;
                    } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                        screenOff = false;
                    }
                }
                catch (Exception e)
                {
                    Utility.printLog(TAG+":Caught "+e.getMessage());
                    Toast.makeText(MainActivity.this, "Main: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        };

        registerReceiver(receiver,filter);

        if(!sessionManager.getBookingData().equals(""))
        {
            try
            {
                JSONObject jsonObject=new JSONObject(sessionManager.getBookingData());
                if(jsonObject.toString().contains("bid"))
                {
                    int bid=jsonObject.getInt("bid");


                    if(bid>=sessionManager.getLastBookingRequested())
                    {
                        if(jsonObject.getString("btype").equals("1")||jsonObject.getString("btype").equals("3"))
                        {
                            thMain.setCurrentTab(2);
                            if(!reqPopUpIsShowing)
                            {
                                mRequestPopUp(30,jsonObject);
                                sessionManager.setBookingData("");
                            }

                        }
                        else if(jsonObject.getString("btype").equals("2"))
                        {
                            long timeInMil=jsonObject.getLong("stime");
                            Date date=new Date(timeInMil*1000);
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

                            Utility.printLog(TAG+" Later booking date "+format.format(date));
                            String msg="You confirmed a booking request from "+jsonObject.getString("cname")/*+" at "+format.format(date)*/;
                            Utility.mShowMessage(getResources().getString(R.string.messagetitle),msg,MainActivity.this);
                        }

                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setTabs()
    {
        addTab(R.drawable.home_page_home_btn,getResources().getString(R.string.HOME), HomeActivity.class);
        addTab(R.drawable.home_page_history_btn,getResources().getString(R.string.HISTORY), HistoryActivity.class);
        addTab(R.drawable.home_page_schedule_selector,getResources().getString(R.string.SCHEDULE), ScheduleActivity.class);
        addTab(R.drawable.home_page_earning_btn, getResources().getString(R.string.EARNINGS),EarningsActivity.class);
        addTab(R.drawable.home_page_profile_btn,getResources().getString(R.string.PROFILE), ProfileActivity.class);

    }
////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    protected void onRestart() {
        super.onRestart();

       /* if(receiver!=null)
        {
            registerReceiver(receiver,filter);
        }*/
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void addTab(int drawableId, String title, Class<?> c)
    {
        count++;
        TabHost.TabSpec spec = thMain.newTabSpec("tab"+String.valueOf(count));
        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tabbuttons, getTabWidget(), false);

        tabIndicator.setTag("tab"+String.valueOf(count));

        ImageView icon = (ImageView) tabIndicator.findViewById(R.id.ivTab_icon);
        icon.setImageResource(drawableId);
        //icon.setBackgroundResource(drawableId);

        TextView textView= (TextView) tabIndicator.findViewById(R.id.titleText);
        textView.setText(title);
        textView.setTypeface(font);

        if(count==0)
        {
            textView.setTextColor(getResources().getColor(R.color.white));
        }

        spec.setIndicator(tabIndicator);

        Intent tabIntent = new Intent(this, c);

        spec.setContent(tabIntent);

        thMain.addTab(spec);
        thMain.setOnTabChangedListener(MainActivity.this);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume()
    {
        super.onResume();


        mUpdateHeartBeat();
        Utility.printLog(TAG +" PKKK Socket status "+socket.connected());
       /* if(!socket.connected())
        {
            socket.connect();
            socket.on("LiveBooking",HandleIncomingMsg);
        }*/
/*
        KeyguardManager kgm = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
        boolean isKeyguardUp = kgm.inKeyguardRestrictedInputMode();
        KeyguardManager.KeyguardLock kgl = kgm.newKeyguardLock("ISERVE_SERVICE");

        if(isKeyguardUp){
            kgl.disableKeyguard();
            isKeyguardUp = false;
        }

        wl.acquire();*/
        getCurrentLocation();

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mUpdateHeartBeat()
    {

        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.put("email",sessionManager.getProviderEmail());
            jsonObject.put("status","1");
            jsonObject.put("pid",sessionManager.getProviderID());


            socket.emit("ProviderStatus",jsonObject);
            Log.v("Socket sendMessage",jsonObject.toString());
            Log.d("UpdateHeartBeat called",jsonObject.toString());

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////
/*    Emitter.Listener HandleIncomingMsg=new Emitter.Listener() {
    @Override
    public void call(final Object... args) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                JSONObject jsonObject=(JSONObject) args[0];
                Utility.printLog("Socket Booking Request", String.valueOf(jsonObject));

                if(!reqPopUpIsShowing)
                {
                    try
                    {

                        if(jsonObject.getString("btype").equals("1")||jsonObject.getString("btype").equals("3"))
                        {
                            thMain.setCurrentTab(2);
                            mRequestPopUp(30,jsonObject);
                        }
                        else
                        {
                            long timeInMil=jsonObject.getLong("stime");
                            Date date=new Date(timeInMil*1000);
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

                            Utility.printLog(TAG+" Later booking date "+format.format(date));
                            String msg="You confirmed a booking request from "+jsonObject.getString("cname")*//*+" at "+format.format(date)*//*;
                            Utility.mShowMessage(getResources().getString(R.string.messagetitle),msg,MainActivity.this);
                        }

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
         }
        };*/

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onStop() {
        super.onStop();


        if(alertDialogPermission != null && alertDialogPermission.isShowing())
        {
            alertDialogPermission.dismiss();
        }

        /*if(receiver!=null)
        {
            unregisterReceiver(receiver);
        }*/
    }


////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

       /* if(socket!=null)
        {
            socket.off();
            socket.disconnect();

            Utility.printLog(TAG+" onDestroy called");
        }*/
        if(receiver!=null)
        {
            unregisterReceiver(receiver);
        }
        Utility.printLog(TAG+" On Destroy");
       // stopService(new Intent(MainActivity.this,MyService.class));
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mRequestPopUp(int i, final JSONObject jsonObject)
    {
        Utility.printLog("Request pop up called");
        String jobLocation = null,requestHeader = null;
        int bid = -1;

        try {
            jobLocation=jsonObject.getString("add");
            requestHeader=jsonObject.getString("catname");
            bid=jsonObject.getInt("bid");

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        mediaPlayer = MediaPlayer.create(this, R.raw.taxina);
        mediaPlayer.setLooping(true);
        reqPopUpIsShowing=true;

        alertDialog = new Dialog(this);
        alertDialog.setCancelable(false);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

       /* KeyguardManager kgm = (KeyguardManager)getActivity().getSystemService(Context.KEYGUARD_SERVICE);
        boolean isKeyguardUp = kgm.inKeyguardRestrictedInputMode();
        KeyguardManager.KeyguardLock kgl = kgm.newKeyguardLock("PQ_SERVICE");

        if(isKeyguardUp){
            kgl.disableKeyguard();
            isKeyguardUp = false;
        }

        wl.acquire();
*/
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        alertDialog.setContentView(R.layout.booking_request_pop_up);

        accept_btn= (Button) alertDialog.findViewById(R.id.accept_btn);
        reject_btn= (Button) alertDialog.findViewById(R.id.reject_btn);

        mTVBid= (TextView) alertDialog.findViewById(R.id.tvBid);
        //mTvDate= (TextView) alertDialog.findViewById(R.id.tvDate);
        mTvPickup= (TextView) alertDialog.findViewById(R.id.tvPick_up_address);
        mTvReqHeader= (TextView) alertDialog.findViewById(R.id.mTvReqHeader);
        //mTvApptTime= (TextView) alertDialog.findViewById(R.id.tvApptTime);
        //mTvApptDate= (TextView) alertDialog.findViewById(R.id.tvApptDate);

        //String apptDate[]=appointmentDetailData.getApptDt().toString().split(" ");
        mTVBid.setText("BID: "+bid);

        sessionManager.setLastBookingRequested(bid);

        mTvReqHeader.setText("NEW "+requestHeader.toUpperCase()+" REQUEST");
        //mTvDate.setText(" "+apptDate[0]);
        mTvPickup.setText(jobLocation);
        //mTvApptTime.setText(" "+apptDate[1]);

        startTimer(i,alertDialog);
            /*
             * For custom color only using layerdrawable to fill the star colors
             */
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                reqPopUpIsShowing=false;
                /*if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.stop();
                }*/
                if(mediaPlayer !=null)
                {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }

                if(Utility.isNetworkAvailable(getApplicationContext()))
                {
                    mRespondToAppointment(AppConstants.STATUS_ACCEPTED,jsonObject);
                }
                alertDialog.dismiss();
            }
        });
        reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                reqPopUpIsShowing=false;
               /* if(mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                }*/
                if(mediaPlayer !=null)
                {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }

                if(Utility.isNetworkAvailable(getApplicationContext()))
                {
                    mRespondToAppointment(AppConstants.STATUS_REJECTED,jsonObject);
                }
                alertDialog.dismiss();
            }
        });
        //isRequested=true;
        if(mediaPlayer != null &&!mediaPlayer.isPlaying())
        {
            mediaPlayer.start();
        }

        alertDialog.show();
    }


///////////////////////////////////////////////////////////////////////////////////////////////////

    private void startTimer(int j, final Dialog alertDialog) {

        circularProgressBar = (CircularProgressBar)alertDialog.findViewById(R.id.circular_progress_bar);
        circularProgressBar.setProgress(100);

        //textView= (TextView) alertDialog.findViewById(R.id.timer);
        //Toast.makeText(MainActivity.this, "Timer Started.!", Toast.LENGTH_SHORT).show();

        final int finalTime = j;
        mCounter=new CountDownTimer(finalTime *1000, 1000) {

            public void onTick(long millisUntilFinished)
            {

//                textView.setText("seconds remaining: " + millisUntilFinished / 1000);
                float i=((millisUntilFinished / 1000));
                float res=(i/ finalTime)*100;
                Log.d("timer_hai", " " + i + "\t" + res);
                circularProgressBar.setProgress(res);
                mm=(int)(i/60);
                ss=(int)(i%60);

//                textView.setText("seconds remaining: " +((int)(i/60))+":"+((int)(i%60)));

                if(mm<10 && ss<10 ){
                    resp="0"+mm+":0"+ss;
                }else if(mm>10 && ss<10){
                    resp=mm+":0"+ss;
                }else if(mm<10 && ss>10){
                    resp="0"+mm+":"+ss;
                }else if(mm>10 && ss>10){
                    resp=""+mm+ss;
                }
                //textView.setText("" + resp );

            }

            public void onFinish() {
                try {
                    circularProgressBar.setProgress(0);
                    //textView.setText("00:00");
                   // isRequested=false;
                    /*if(mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                    }*/
                    if(mediaPlayer !=null)
                    {
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                        mediaPlayer.release();
                        mediaPlayer = null;
                    }

                    reqPopUpIsShowing=false;
                    alertDialog.dismiss();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }.start();

    }
///////////////////////////////////////////////////////////////////////////////////////////////////

    public void mRespondToAppointment(final int responseCode, JSONObject jsonObject)
    {
        mProgressDailog=Utility.getProcessDialog(MainActivity.this);
        mProgressDailog.show();

        String cust_email=null,appt_date=null;
        try {
            cust_email=jsonObject.getString("email");
            appt_date=jsonObject.getString("dt");
            bid=jsonObject.getString("bid");
            cid=jsonObject.getString("cid");
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        RequestBody requestBody= new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_pat_email",cust_email)
                .add("ent_appnt_dt",appt_date)
                .add("ent_response",""+responseCode)
                .add("ent_book_type","1")
                .add("ent_bid",bid)
                .add("ent_proid",sessionManager.getProviderID())
                .add("ent_date_time",Utility.getCurrentGmtTime())
                .build();

        Okhttp_connection.doOkhttpRequest(ServiceUrls.RESPOND_TO_APPOINTMENT, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(mProgressDailog!=null&&mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
                Gson gson=new Gson();
                mRespondToApptResponsePojo=gson.fromJson(result, RespondToApptResponsePojo.class);
                if(mRespondToApptResponsePojo!=null){
                    if("0".equals(mRespondToApptResponsePojo.getErrFlag())&&"40".equals(mRespondToApptResponsePojo.getErrNum()))
                    {
                        acknowledgeCustomer(bid,cid,responseCode,sessionManager.getProviderID(),"");
                        thMain.setCurrentTab(0);
                    }
                    else if("1".equals(mRespondToApptResponsePojo.getErrFlag()))
                    {
                        mShowErrorMessage(mRespondToApptResponsePojo.getErrMsg());
                    }
                }

                Utility.printLog(TAG+" respondToAppointment response :"+result);
            }

            @Override
            public void onError(String error)
            {
                if(mProgressDailog!=null&&mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
                Utility.printLog(TAG+" respondToAppointment error :"+error);
            }
        });

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void acknowledgeCustomer(String bid, String cid,int responseCode,String proId,String reason)
    {
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("bid", bid);
            jsonObject.put("bstatus", ""+responseCode);
            jsonObject.put("cid",cid);
            jsonObject.put("proid",proId);
            jsonObject.put("dt",Utility.getCurrentGmtTime());

            if(!reason.equals(""))
            {
                jsonObject.put("ent_pro_reason",reason);
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        socket.emit("LiveBookingAck",jsonObject);

        Utility.printLog(TAG+" Socket Acknowledge"+jsonObject.toString());
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(MainActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();

                notificationManager.cancelAll();
                Intent intent = new Intent("com.phix.provider.refreshBooking");
                intent.putExtra("action","1");
                sendBroadcast(intent);

            }
        });
        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void getCurrentLocation()
    {
        if (locationUtil == null)
        {
            locationUtil = new LocationUtil(this, this);
        }
        else
        {
            locationUtil.checkLocationSettings();
        }
    }

    @Override
    public void updateLocation(Location location)
    {

    }

    @Override
    public void location_Error(String error)
    {

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        switch (requestCode){
            case PERMISSION_REQUEST_CODE :
                boolean isDeninedRTPs = false, showRationaleRTPs = true;
                StringBuilder permissionStringBuilder = new StringBuilder("");
                String prefix = "",permissionString = "";
                if(grantResults.length>0){

                    for (int i =0; i < permissions.length; i++)
                    {
                        String permission = permissions[i];

                        if(grantResults[i]!= PackageManager.PERMISSION_GRANTED)
                        {
                            isDeninedRTPs = true;

                            if(permission.equals(ACCESS_FINE_LOCATION))
                            {
                                permissionStringBuilder.append(prefix);
                                permissionStringBuilder.append(getString(R.string.location));
                            }
                            if(permission.equals(CAMERA))
                            {
                                permissionStringBuilder.append(prefix);
                                permissionStringBuilder.append(getString(R.string.camera));
                            }
                            if(permission.equals(WRITE_EXTERNAL_STORAGE))
                            {
                                permissionStringBuilder.append(prefix);
                                permissionStringBuilder.append(getString(R.string.storage));
                            }
                            prefix = ",";

                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            showRationaleRTPs = shouldShowRequestPermissionRationale(permission);
                        }
                    }
                    if(!permissionStringBuilder.toString().equals(""))
                    {
                        permissionString = permissionStringBuilder.toString();
                        if(permissionString.substring(permissionString.length()-1).equals(","))
                        {
                            permissionString =  permissionStringBuilder.substring(0,permissionStringBuilder.length()-1);
                        }
                    }

                    if(!showRationaleRTPs)
                    {
                        showMessage(permissionStringBuilder.toString(),2,"SETTINGS","EXIT APP","You need to allow access for " +permissionString+" permission.");
                    }
                    else if(isDeninedRTPs)
                    {
                        rnuTimePermissionRequest.mRequestPermissions(permissions,PERMISSION_REQUEST_CODE);
                    }
                }
                break;
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShoudHavePermission(String permission)
    {
        Log.d("showrational","called "+permission);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if(shouldShowRequestPermissionRationale(permission))
            {
                //showMessage(permission,1,"OK","CANCEL","You need to allow access for "+permission+" permission.");

                rnuTimePermissionRequest.mRequestPermissions(new String[]{permission},8080);
            }
            else
            {
                showMessage(permission,2,"SETTINGS","EXIT APP","You need to allow access for "+permission+" permission.");
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void showMessage(final String permission,final int dialog,String positiveButton,String negativeButton,String message)
    {
        final android.support.v7.app.AlertDialog.Builder permissionDialog =  new android.support.v7.app.AlertDialog.Builder(this);
        permissionDialog.setMessage(message);
        permissionDialog.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (MainActivity.this == null) {
                    return;
                }

                dialogInterface.dismiss();
                final Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + MainActivity.this.getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(intent);
                finish();

            }
        });
        permissionDialog.setCancelable(false);

        alertDialogPermission = permissionDialog.create();
        alertDialogPermission.show();

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onTabChanged(String tabId) {
        for (int i = 0; i < thMain.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) thMain.getTabWidget().getChildAt(i).findViewById(R.id.titleText); //Unselected Tabs
            tv.setTextColor(Color.parseColor("#ffffff"));
        }

        TextView tv = (TextView) thMain.getCurrentTabView().findViewById(R.id.titleText); //for Selected Tab
        tv.setTextColor(Color.parseColor("#ffffff"));
    }
}
