package com.app.partner.iserve.Messaging;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 3/1/17.
 */
public class GetAllMsgs implements Serializable
{
    private String errNum;

    private String errMsg;

    private String errFlag;

    private ArrayList<Messages> messages;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public ArrayList<Messages> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Messages> messages) {
        this.messages = messages;
    }
}
