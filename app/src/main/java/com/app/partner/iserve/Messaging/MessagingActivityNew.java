package com.app.partner.iserve.Messaging;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.ApplicationController;
import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.R;
import com.app.partner.iserve.SplashActivity;
import com.app.partner.iserve.Utility.CircleTransform;
import com.app.partner.iserve.Utility.MyNetworkChangeListner;
import com.app.partner.iserve.Utility.NetworkChangeReceiver;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MessagingActivityNew extends Activity {

    private String bid="",pic,cname;
    private ProgressDialog pDialog;
    private SessionManager sessionManager;
    private EditText message_text_field_et;
    private ImageView message_send_btn;
    private RelativeLayout message_back_rl;

    private RecyclerView mRecyclerView;
    private MessagingAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Messages> msg_list=new ArrayList();
    private Runnable runnable;
    private static Socket socket;
    private String TAG="MessagingActivityNew";
    private Appointments appointment;
    private TextView tvCustomerName,tvBid;
    public static boolean isOpen=false;


    private Typeface fontBold,font;
    private ImageView ivProfilePic;
    private Emitter.Listener HandleIncomingMsg;
    private NetworkChangeReceiver networkChangeReceiver;

    Gson gson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging_activity_new);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            appointment =(Appointments) bundle.getSerializable("APPOINTMENT");
        }
        pDialog = Utility.getProcessDialog(MessagingActivityNew.this);
        pDialog.setCancelable(false);
        sessionManager = new SessionManager(MessagingActivityNew.this);
        socket= ApplicationController.getSocketInstance();

        font=Utility.getFontRegular(MessagingActivityNew.this);
        fontBold=Utility.getFontBold(MessagingActivityNew.this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_for_messaging);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MessagingAdapter(this, msg_list);
        mRecyclerView.setAdapter(mAdapter);

        gson = new Gson();
        runnable = new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
                mRecyclerView.scrollToPosition(mAdapter.getItemCount()-1);
            }
        };

        message_text_field_et=(EditText) findViewById(R.id.message_text_field_et);

        message_send_btn = (ImageView) findViewById(R.id.message_send_btn);
        message_send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!message_text_field_et.getText().toString().isEmpty()) {
                    if (Utility.isNetworkAvailable(MessagingActivityNew.this)) {
                        sendMessage(message_text_field_et.getText().toString());
                        message_text_field_et.setText("");
                    } else
                    {
                        Toast.makeText(MessagingActivityNew.this, "Uh-ho! No Internet Connection!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MessagingActivityNew.this, "Please enter the message!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        message_text_field_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerView.scrollToPosition(mAdapter.getItemCount()-1);
                    }
                },200);
            }
        });

        message_back_rl = (RelativeLayout) findViewById(R.id.message_back_rl);
        message_back_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        tvCustomerName= (TextView)findViewById(R.id.tvCustomerName);
        tvCustomerName.setTypeface(fontBold);

        tvBid= (TextView)findViewById(R.id.tvBid);
        tvBid.setTypeface(font);

        ivProfilePic=(ImageView)findViewById(R.id.ivProfilePic);


        if(appointment!=null)
        {
            bid=appointment.getBid();
            pic=appointment.getpPic();
            cname=appointment.getFname();
        }
        else
        {
            if(bundle!=null)
            {
                if(bundle.containsKey("bid"))
                {
                    bid=bundle.getString("bid");
                }
                if(bundle.containsKey("pic"))
                {
                    pic=bundle.getString("pic");
                }
                if(bundle.containsKey("name"))
                {
                    cname=bundle.getString("name");
                }
            }


        }

        tvBid.setText(getResources().getString(R.string.jobId)+" "+bid);

        tvCustomerName.setText(cname);

        if(pic!=null && !pic.isEmpty())
        {
            Picasso.with(MessagingActivityNew.this)
                    .load(pic)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.pro_photo)
                    //.resize(width,hieght)
                    .into(ivProfilePic);
        }


        HandleIncomingMsg = new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject jsonObject = (JSONObject) args[0];
                        Utility.printLog(TAG+" Socket Messaging message", String.valueOf(jsonObject));

                        try {
                            if (jsonObject.toString().contains("deliver"))
                            {
                                String status=jsonObject.getString("deliver");

                                if(status.equals("2"))
                                {
                                    String key=jsonObject.getString("msgid");


                                    mAdapter.notifyDataSetChanged();
                                    mRecyclerView.scrollToPosition(mAdapter.getItemCount()-1);
                                }

                            }
                            else if (jsonObject.toString().contains("bid")) {
                                Messages message = new Messages();
                                message.setStatus("2");
                                message.setPayload(jsonObject.getString("payload"));
                                message.setMsgid(jsonObject.getString("msgid"));
                                message.setTimestamp(jsonObject.getString("timestamp"));
                                message.setUsertype(jsonObject.getString("usertype"));
                                message.setMsgtype("0");
                                message.setDt(jsonObject.getString("dt"));
                                msg_list.add(message);

                                mAdapter.notifyDataSetChanged();
                                mRecyclerView.scrollToPosition(mAdapter.getItemCount()-1);
                            }
                        } catch (Exception e) {
                            Utility.printLog(TAG + " Caught : " + e.getMessage());
                            e.printStackTrace();
                        }


                    }
                });
            }
        };

        networkChangeReceiver=new NetworkChangeReceiver();
        networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
            @Override
            public void onNetworkStateChanges(boolean nwStatus) {
                Utility.printLog("PK Network Status MainActvty "+nwStatus);
                if(nwStatus)
                {
                    getAllMessages();
                    onResume();
                }


            }
        });
    }



    @Override
    protected void onResume()
    {
        super.onResume();

        isOpen=true;

        if(!socket.connected())
        {
            socket.connect();
            socket.off("Message");
            socket.on("Message",HandleIncomingMsg);
            Utility.printLog(TAG+"pkkkkkk Socket "+socket+" connected 1  "+socket.id());
        }
        else
        {
            if(HandleIncomingMsg!=null)
            {
                socket.off("Message");
                socket.on("Message",HandleIncomingMsg);
                Utility.printLog(TAG+"pkkkkkk Socket connected 2"+socket.id());
            }
        }
        mUpdateHeartBeat();

    }

    @Override
    protected void onStart() {
        super.onStart();

        getAllMessages();
    }

    @Override
    protected void onStop() {
        super.onStop();

        isOpen=false;

    }

    public void getAllMessages() {

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_bid",bid)
                .add("ent_user_type","1")
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .build();

        Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_ALL_MESSAGES, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("getAllMessages Response "+result);
                Gson gson=new Gson();
                GetAllMsgs getAllMsgs=gson.fromJson(result,GetAllMsgs.class);

                if(getAllMsgs.getErrFlag().equals("0"))
                {
                    /*;*/

                    int size=getAllMsgs.getMessages().size();
                    if(size>0)
                    {
                        msg_list.clear();
                        msg_list.addAll(getAllMsgs.getMessages());
                    }

                    mAdapter.notifyDataSetChanged();
                    mRecyclerView.scrollToPosition(mAdapter.getItemCount()-1);


                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void sendMessage(String msg)
    {
        JSONObject jsonObject=new JSONObject();
        try{
            long msgid = System.currentTimeMillis();

            jsonObject.put("msgtype","0");
            jsonObject.put("payload",msg);
            jsonObject.put("bid",bid);
            jsonObject.put("msgid", msgid);
            jsonObject.put("usertype","1");
            jsonObject.put("currdt",Utility.getGmtTime());

            socket.emit("Message",jsonObject);
            Log.v("Socket sendMessage",jsonObject.toString());

            Messages message=new Messages();
            message.setStatus("1");
            message.setPayload(msg);
            message.setMsgid(""+msgid);
            message.setUsertype("1");
            message.setMsgtype("0");
            message.setTimestamp(""+msgid);

            msg_list.add(message);

            mAdapter.notifyDataSetChanged();
            mRecyclerView.scrollToPosition(mAdapter.getItemCount()-1);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(MessagingActivityNew.this,SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(socket!=null)
        {
            socket.off("Message");
            socket.disconnect();
            Utility.printLog(TAG+"pkkkkkk Socket disconnected ");

        }
    }

    private void mUpdateHeartBeat()
    {

        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.put("email",sessionManager.getProviderEmail());
            jsonObject.put("status","1");
            jsonObject.put("pid",sessionManager.getProviderID());


            socket.emit("ProviderStatus",jsonObject);
            Log.v("Socket sendMessage",jsonObject.toString());
            Log.d("UpdateHeartBeat called",jsonObject.toString());

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

}
