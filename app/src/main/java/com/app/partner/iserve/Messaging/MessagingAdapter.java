package com.app.partner.iserve.Messaging;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.partner.iserve.R;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


public class MessagingAdapter extends RecyclerView.Adapter<MessagingAdapter.ViewHolder> {

    private final ArrayList<Messages> mDataset;
    MessagingActivityNew context;
    private Set<String> mKeys;
    private ArrayList<String> mKeyList;
    private HashMap<String,Messages>mData;


    public class ViewHolder extends RecyclerView.ViewHolder {


        private final ImageView ivDeliverStatus;
        LinearLayout reciver_l_out;
        LinearLayout sender_l_out;
        TextView sender,receiver,reciver_time_stamp_tv,sender_time_stamp_tv;

        public ViewHolder(View v) {
            super(v);

            reciver_l_out = (LinearLayout) v.findViewById(R.id.reciver_msg_l_out);
            sender_l_out = (LinearLayout) v.findViewById(R.id.sender_msg_l_out);
            sender = (TextView) v.findViewById(R.id.sender_msg_tv);
            receiver = (TextView) v.findViewById(R.id.reciver_msg_tv);
            reciver_time_stamp_tv = (TextView) v.findViewById(R.id.reciver_time_stamp_tv);
            sender_time_stamp_tv = (TextView) v.findViewById(R.id.sender_time_stamp_tv);
            ivDeliverStatus = (ImageView) v.findViewById(R.id.ivDeliver);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MessagingAdapter(MessagingActivityNew context, ArrayList<Messages> myDataset) {
        this.mDataset = myDataset;

        // getAllKey(myDataset);


        this.context=context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_list_item_for_message, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        Messages messageFormat_pojo =mDataset.get(position) ;

        if (messageFormat_pojo.getUsertype().equals("1")) {
            holder.reciver_l_out.setVisibility(View.GONE);
            holder.sender_l_out.setVisibility(View.VISIBLE);
            holder.sender.setText(messageFormat_pojo.getPayload());
            holder.sender_time_stamp_tv.setText(getStampToTimeConverter(messageFormat_pojo.getTimestamp()));

        } else if (messageFormat_pojo.getUsertype().equals("2")) {

            holder.sender_l_out.setVisibility(View.GONE);
            holder.reciver_l_out.setVisibility(View.VISIBLE);
            holder.receiver.setText(messageFormat_pojo.getPayload());
            holder.reciver_time_stamp_tv.setText(getStampToTimeConverter(messageFormat_pojo.getTimestamp()));

        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }



    //GETTING CURRENT DATE
    public String getStampToTimeConverter(String stamp){
        Timestamp timestamp = new Timestamp(Long.parseLong(stamp));

        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        String text = dateFormat.format(timestamp);

        System.out.println(text);
        Log.d("StampToTimeConv",text);
        return text;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////




}
