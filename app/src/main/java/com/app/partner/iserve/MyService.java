/*
package com.app.provider.iserve;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import com.app.provider.iserve.Utility.SessionManager;
import com.app.provider.iserve.Utility.Utility;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

*/
/**
 * Created by embed on 6/7/16.
 *//*

public class MyService extends Service implements GoogleApiClient.ConnectionCallbacks {
    Socket socket;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location myLoc;
    private double currentLat,currentLng;
    private SessionManager sessionManager;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private String TAG="Myservice";
    private Emitter.Listener HandleIncomingMsg;
   // public static MessageCallback messageCallback;

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onCreate() {
        super.onCreate();
        buildGoogleApiClient();
        sessionManager=new SessionManager(this);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onStart(Intent intent, int startId)
    {
        Utility.printLog(TAG+" service started");
        socket = ApplicationController.getSocketInstance();
        mGoogleApiClient.connect();
        startPublishingWithTimer();

       */
/* HandleIncomingMsg=new Emitter.Listener() {
            @Override
            public void call(final Object... args) {

                JSONObject jsonObject = (JSONObject) args[0];
                Log.v(TAG+"Message Socket", String.valueOf(jsonObject));

                if(messageCallback!=null)
                {
                    messageCallback.onSuccess(jsonObject.toString());
                }
                else
                {
                    Log.v(TAG,"Pkkkkk" +messageCallback);
                }
            }
        };
        startListning();*//*

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    private void startPublishingWithTimer()
    {
        mTimer=new Timer();
        mTimerTask= new TimerTask() {
            @Override
            public void run() {
                if(Utility.isNetworkAvailable(getApplicationContext()))
                {
                    if(!socket.connected())
                    {
                        socket.connect();
                        JSONObject jsonObject=new JSONObject();
                        try {
                            jsonObject.put("email",sessionManager.getProviderEmail());
                            jsonObject.put("status","1");
                            socket.emit("ProviderStatus",jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    mStartEmitting();
                }
                else
                {
                    Utility.printLog(TAG+" PK No internet");
                    if(socket.connected())
                    {
                        socket.disconnect();
                    }
                }
                Utility.printLog(TAG+" Socket connected "+socket.connected());
            }
        };
        mTimer.schedule(mTimerTask,000,4000);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mStartEmitting()
    {

        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.put("email",sessionManager.getProviderEmail());
            jsonObject.put("lat",currentLat);
            jsonObject.put("long",currentLng);
            jsonObject.put("pid",sessionManager.getProviderID());
            jsonObject.put("devId", Utility.getDeviceId(this));

            socket.emit("UpdateProvider",jsonObject);
            Log.v("Socket sendMessage",jsonObject.toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(2000);
        mLocationRequest.setSmallestDisplacement(20);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
            @Override
            public void onLocationChanged(Location currentLoc)
            {
                myLoc=currentLoc;

                currentLat = currentLoc.getLatitude();
                currentLng = currentLoc.getLongitude();
                sessionManager.setProviderCurrentlat(""+currentLat);
                sessionManager.setProviderCurrentlong(""+currentLng);
            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onConnectionSuspended(int i) {

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onDestroy()
    {
        if(mTimer!=null){
            mTimerTask.cancel();
            mTimer.cancel();
        }
        socket.disconnect();
        Utility.printLog(TAG+" service destroyed");
        mGoogleApiClient.disconnect();

    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
  */
/*  public void startListning()
    {
        socket.on("Message",HandleIncomingMsg);
    }


    public interface MessageCallback
    {
        void onSuccess(String result);

    }

    public static void setMessageCallback(MessageCallback callback)
    {
        messageCallback=callback;
    }*//*

}
*/
