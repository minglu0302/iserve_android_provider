package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 10/10/16.
 */
public class AbortAppointmentResponse implements Serializable
{
  /*  {
        "errNum":"42",
            "errFlag":"0",
            "errMsg":"Appointment cancelled with out any charge.",
            "test":"update appointment set status = 10,last_modified_dt = '2016-10-10 15:15:41',cancel_status = '2' where doc_id = '174' and appointment_id = '2356'"
    }*/

    private String errNum;

    private String test;

    private String errMsg;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
