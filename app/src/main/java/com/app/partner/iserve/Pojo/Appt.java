package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 25/10/16.
 */
public class Appt implements Serializable
{
    private String total_pro;

    private String phone;

    private String sign_url;

    private String visit_amount;

    private String apptLong;

    private String bookType;

    private String cat_name;

    private String amount;

    private String distance;

    private String timer_status;

    private String mat_fees;

    private String apntDt;

    private String timer;

    private String job_start_time;

    private String apptLat;

    private String pro_notes;

    private String price_per_min;

    private String bid;

    private String fname;

    private String customer_notes;

    private String pPic;

    private String job_timer;

    private String status;

    private String statusMsg;

    private String pro_disc;

    private String misc_fees;

    private String addrLine1;

    private String addrLine2;

    private String email;

    private String coupon_discount;

    private String cancelAmt;

    private String appt_duration;

    private String payment_type;

    private String apntTime;

    private String apntDate;

    private ArrayList<Services> services;

    public ArrayList<Services> getServices() {
        return services;
    }

    public void setServices(ArrayList<Services> services) {
        this.services = services;
    }

    public String getTotal_pro() {
        return total_pro;
    }

    public void setTotal_pro(String total_pro) {
        this.total_pro = total_pro;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSign_url() {
        return sign_url;
    }

    public void setSign_url(String sign_url) {
        this.sign_url = sign_url;
    }

    public String getVisit_amount() {
        return visit_amount;
    }

    public void setVisit_amount(String visit_amount) {
        this.visit_amount = visit_amount;
    }

    public String getApptLong() {
        return apptLong;
    }

    public void setApptLong(String apptLong) {
        this.apptLong = apptLong;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTimer_status() {
        return timer_status;
    }

    public void setTimer_status(String timer_status) {
        this.timer_status = timer_status;
    }

    public String getMat_fees() {
        return mat_fees;
    }

    public void setMat_fees(String mat_fees) {
        this.mat_fees = mat_fees;
    }

    public String getApntDt() {
        return apntDt;
    }

    public void setApntDt(String apntDt) {
        this.apntDt = apntDt;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getJob_start_time() {
        return job_start_time;
    }

    public void setJob_start_time(String job_start_time) {
        this.job_start_time = job_start_time;
    }

    public String getApptLat() {
        return apptLat;
    }

    public void setApptLat(String apptLat) {
        this.apptLat = apptLat;
    }

    public String getPro_notes() {
        return pro_notes;
    }

    public void setPro_notes(String pro_notes) {
        this.pro_notes = pro_notes;
    }

    public String getPrice_per_min() {
        return price_per_min;
    }

    public void setPrice_per_min(String price_per_min) {
        this.price_per_min = price_per_min;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getCustomer_notes() {
        return customer_notes;
    }

    public void setCustomer_notes(String customer_notes) {
        this.customer_notes = customer_notes;
    }

    public String getpPic() {
        return pPic;
    }

    public void setpPic(String pPic) {
        this.pPic = pPic;
    }

    public String getJob_timer() {
        return job_timer;
    }

    public void setJob_timer(String job_timer) {
        this.job_timer = job_timer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getPro_disc() {
        return pro_disc;
    }

    public void setPro_disc(String pro_disc) {
        this.pro_disc = pro_disc;
    }

    public String getMisc_fees() {
        return misc_fees;
    }

    public void setMisc_fees(String misc_fees) {
        this.misc_fees = misc_fees;
    }

    public String getAddrLine1() {
        return addrLine1;
    }

    public void setAddrLine1(String addrLine1) {
        this.addrLine1 = addrLine1;
    }

    public String getAddrLine2() {
        return addrLine2;
    }

    public void setAddrLine2(String addrLine2) {
        this.addrLine2 = addrLine2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public String getCancelAmt() {
        return cancelAmt;
    }

    public void setCancelAmt(String cancelAmt) {
        this.cancelAmt = cancelAmt;
    }

    public String getAppt_duration() {
        return appt_duration;
    }

    public void setAppt_duration(String appt_duration) {
        this.appt_duration = appt_duration;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getApntTime() {
        return apntTime;
    }

    public void setApntTime(String apntTime) {
        this.apntTime = apntTime;
    }

    public String getApntDate() {
        return apntDate;
    }

    public void setApntDate(String apntDate) {
        this.apntDate = apntDate;
    }
}
