package com.app.partner.iserve.Pojo;

import java.util.ArrayList;

/**
 * Created by murashid on 18-Mar-17.
 */

public class BankDetailsPojo {

    private String errNum;
    private String errFlag;
    private String errMsg;
    private ArrayList<BankDetailsData> data;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public ArrayList<BankDetailsData> getData() {
        return data;
    }

    public void setData(ArrayList<BankDetailsData> data) {
        this.data = data;
    }
}
