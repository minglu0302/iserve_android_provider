package com.app.partner.iserve.Pojo;

import java.util.ArrayList;

/**
 * Created by embed on 25/10/16.
 */
public class Bookings
{
    private ArrayList<Appt> appt;

    private String date;

    public ArrayList<Appt> getAppt() {
        return appt;
    }

    public void setAppt(ArrayList<Appt> appt) {
        this.appt = appt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
