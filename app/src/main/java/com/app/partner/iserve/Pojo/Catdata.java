package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 22/12/16.
 */
public class Catdata implements Serializable
{
    private String visit_fees;

    private String price_min;

    private String radius;

    private String fee_type;

    private String cat_desc;

    private String price_set_by;

    private String cat_name;

    private String priceMin;

    private String priceMax;

    public String getVisit_fees() {
        return visit_fees;
    }

    public void setVisit_fees(String visit_fees) {
        this.visit_fees = visit_fees;
    }

    public String getPrice_min() {
        return price_min;
    }

    public void setPrice_min(String price_min) {
        this.price_min = price_min;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getFee_type() {
        return fee_type;
    }

    public void setFee_type(String fee_type) {
        this.fee_type = fee_type;
    }

    public String getCat_desc() {
        return cat_desc;
    }

    public void setCat_desc(String cat_desc) {
        this.cat_desc = cat_desc;
    }

    public String getPrice_set_by() {
        return price_set_by;
    }

    public void setPrice_set_by(String price_set_by) {
        this.price_set_by = price_set_by;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(String priceMin) {
        this.priceMin = priceMin;
    }

    public String getPriceMax() {

        return priceMax;
    }

    public void setPriceMax(String priceMax) {
        this.priceMax = priceMax;
    }
}
