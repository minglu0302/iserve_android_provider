package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 23/6/16.
 */
public class City implements Serializable
{
    private String City_Long;

    private String City_Lat;

    private String City_Id;

    private String City_Name;

    public String getCity_Long() {
        return City_Long;
    }

    public void setCity_Long(String city_Long) {
        City_Long = city_Long;
    }

    public String getCity_Lat() {
        return City_Lat;
    }

    public void setCity_Lat(String city_Lat) {
        City_Lat = city_Lat;
    }

    public String getCity_Id() {
        return City_Id;
    }

    public void setCity_Id(String city_Id) {
        City_Id = city_Id;
    }

    public String getCity_Name() {
        return City_Name;
    }

    public void setCity_Name(String city_Name) {
        City_Name = city_Name;
    }

   /* {
        "errNum":"106",
            "errFlag":"0",
            "errMsg":"Got City List !",
            "city":[
        {
            "City_Id":"18762",
                "City_Name":"chennai",
                "City_Lat":"89.243432",
                "City_Long":"43.542544"
        },
        {
            "City_Id":"19085",
                "City_Name":"Bangalore",
                "City_Lat":"12.9716",
                "City_Long":"77.594644"
        }
        ]
    }*/
}
