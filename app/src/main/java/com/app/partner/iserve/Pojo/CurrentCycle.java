package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 1/12/16.
 */
public class CurrentCycle implements Serializable
{
    private String opening_balance;

    private String rev_rate;

    private String booking_cycle;

    private String total_reject;

    private String closing_balance;

    private String total_bookings;

    private String total_ignore;

    private String booking_earn;

    private String tot_cancel;

    private String acpt_rate;

    private String pay_date;

    private String start_bid;

    private String total_accepted;

    private String start_date;

    public String getOpening_balance() {
        return opening_balance;
    }

    public void setOpening_balance(String opening_balance) {
        this.opening_balance = opening_balance;
    }

    public String getRev_rate() {
        return rev_rate;
    }

    public void setRev_rate(String rev_rate) {
        this.rev_rate = rev_rate;
    }

    public String getBooking_cycle() {
        return booking_cycle;
    }

    public void setBooking_cycle(String booking_cycle) {
        this.booking_cycle = booking_cycle;
    }

    public String getTotal_reject() {
        return total_reject;
    }

    public void setTotal_reject(String total_reject) {
        this.total_reject = total_reject;
    }

    public String getClosing_balance() {
        return closing_balance;
    }

    public void setClosing_balance(String closing_balance) {
        this.closing_balance = closing_balance;
    }

    public String getTotal_bookings() {
        return total_bookings;
    }

    public void setTotal_bookings(String total_bookings) {
        this.total_bookings = total_bookings;
    }

    public String getTotal_ignore() {
        return total_ignore;
    }

    public void setTotal_ignore(String total_ignore) {
        this.total_ignore = total_ignore;
    }

    public String getBooking_earn() {
        return booking_earn;
    }

    public void setBooking_earn(String booking_earn) {
        this.booking_earn = booking_earn;
    }

    public String getTot_cancel() {
        return tot_cancel;
    }

    public void setTot_cancel(String tot_cancel) {
        this.tot_cancel = tot_cancel;
    }

    public String getAcpt_rate() {
        return acpt_rate;
    }

    public void setAcpt_rate(String acpt_rate) {
        this.acpt_rate = acpt_rate;
    }

    public String getPay_date() {
        return pay_date;
    }

    public void setPay_date(String pay_date) {
        this.pay_date = pay_date;
    }

    public String getStart_bid() {
        return start_bid;
    }

    public void setStart_bid(String start_bid) {
        this.start_bid = start_bid;
    }

    public String getTotal_accepted() {
        return total_accepted;
    }

    public void setTotal_accepted(String total_accepted) {
        this.total_accepted = total_accepted;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }
}
