package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by embed on 15/11/16.
 */
public class GetAllSlotResponse implements Serializable
{
    private String errNum;

    private ArrayList<Slots> slots;

    private String errMsg;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public ArrayList<Slots> getSlots() {
        return slots;
    }

    public void setSlots(ArrayList<Slots> slots) {
        this.slots = slots;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
