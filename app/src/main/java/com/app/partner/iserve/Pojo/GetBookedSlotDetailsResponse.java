package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 26/12/16.
 */
public class GetBookedSlotDetailsResponse implements Serializable
{
    private String errNum;

    private String errMsg;

    private SlotDetails data;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public SlotDetails getData() {
        return data;
    }

    public void setData(SlotDetails data) {
        this.data = data;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
