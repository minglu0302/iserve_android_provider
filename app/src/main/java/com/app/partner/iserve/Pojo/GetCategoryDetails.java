package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 22/12/16.
 */
public class GetCategoryDetails implements Serializable
{
    private String errNum;

    private String errMsg;

    private Catdata catdata;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Catdata getData() {
        return catdata;
    }

    public void setData(Catdata data) {
        this.catdata = data;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
