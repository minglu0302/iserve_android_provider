package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 23/6/16.
 */
public class GetCityListPojo implements Serializable
{
    private String errNum;

    private String errMsg;

    private String errFlag;

    private ArrayList<City> city;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public ArrayList<City> getCity() {
        return city;
    }

    public void setCity(ArrayList<City> city) {
        this.city = city;
    }
}
