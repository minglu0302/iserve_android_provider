package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 1/12/16.
 */
public class GetFinancialDataResponse implements Serializable
{
    private String errNum;

    private CurrentCycle currentCycle;

    private ArrayList<PastCycle> pastCycle;

    private String errMsg;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public CurrentCycle getCurrentCycle() {
        return currentCycle;
    }

    public void setCurrentCycle(CurrentCycle currentCycle) {
        this.currentCycle = currentCycle;
    }

    public ArrayList<PastCycle> getPastCycle() {
        return pastCycle;
    }

    public void setPastCycle(ArrayList<PastCycle> pastCycle) {
        this.pastCycle = pastCycle;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
