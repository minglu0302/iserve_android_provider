package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 25/10/16.
 */
public class GetMasterAppointmentsHistory implements Serializable
{
    private String errNum;

    private String errMsg;

    private ArrayList<Bookings> Bookings;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public ArrayList<Bookings> getBookings() {
        return Bookings;
    }

    public void setBookings(ArrayList<Bookings> bookings) {
        Bookings = bookings;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
