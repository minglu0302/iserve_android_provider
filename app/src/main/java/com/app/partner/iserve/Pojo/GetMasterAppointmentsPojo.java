package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 15/7/16.
 */
public class GetMasterAppointmentsPojo implements Serializable
{
    private String errNum;

    private ArrayList newReqs;

    private String t;

    private ArrayList<Nowbooking> nowbooking;

    private ArrayList<LaterBookings> laterBookings;

    private String status;

    private String errMsg;

    private String penCount;

    private ArrayList refIndex;

    private String errFlag;


    public ArrayList<Nowbooking> getNowbooking() {
        return nowbooking;
    }

    public void setNowbooking(ArrayList<Nowbooking> nowbooking) {
        this.nowbooking = nowbooking;
    }

    public ArrayList<LaterBookings> getLaterBookings() {
        return laterBookings;
    }

    public void setLaterBookings(ArrayList<LaterBookings> laterBookings) {
        this.laterBookings = laterBookings;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public ArrayList getNewReqs() {
        return newReqs;
    }

    public void setNewReqs(ArrayList newReqs) {
        this.newReqs = newReqs;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getPenCount() {
        return penCount;
    }

    public void setPenCount(String penCount) {
        this.penCount = penCount;
    }

    public ArrayList getRefIndex() {
        return refIndex;
    }

    public void setRefIndex(ArrayList refIndex) {
        this.refIndex = refIndex;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
