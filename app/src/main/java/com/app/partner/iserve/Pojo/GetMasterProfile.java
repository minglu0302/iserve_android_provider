package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 5/9/16.
 */
public class GetMasterProfile implements Serializable
{
    private String selCat;

    private String todayAmt;

    private String about;

    private String rejApts;

    private String licExp;

    private String totalAmt;

    private String type;

    private String fName;

    private String amount;

    private String languages;

    private String avgRate;

    private ArrayList<ReviewsArr> reviewsArr;

    private String zip;

    private String totRats;

    private String monthAmt;

    private String cmpltApts;

    private String pPic;

    private String status;

    private String qry;

    private String lName;

    private String lastBilledAmt;

    private String weekAmt;

    private String errNum;

    private String expertise;

    private String email;

    private String errMsg;

    private String errFlag;

    private String typeId;

    private String licNo;

    private String mobile;

    private String num_of_job_images;

    private ArrayList<CatArray> catArray;

    private ArrayList<String> job_images;

    public ArrayList<String> getJob_images() {
        return job_images;
    }

    public void setJob_images(ArrayList<String> job_images) {
        this.job_images = job_images;
    }

    public ArrayList<CatArray> getCatArray() {
        return catArray;
    }

    public void setCatArray(ArrayList<CatArray> catArray) {
        this.catArray = catArray;
    }

    public String getNum_of_job_images() {
        return num_of_job_images;
    }

    public void setNum_of_job_images(String num_of_job_images) {
        this.num_of_job_images = num_of_job_images;
    }

    public String getSelCat() {
        return selCat;
    }

    public void setSelCat(String selCat) {
        this.selCat = selCat;
    }

    public String getTodayAmt() {
        return todayAmt;
    }

    public void setTodayAmt(String todayAmt) {
        this.todayAmt = todayAmt;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getRejApts() {
        return rejApts;
    }

    public void setRejApts(String rejApts) {
        this.rejApts = rejApts;
    }

    public String getLicExp() {
        return licExp;
    }

    public void setLicExp(String licExp) {
        this.licExp = licExp;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(String avgRate) {
        this.avgRate = avgRate;
    }

    public ArrayList<ReviewsArr> getReviewsArr() {
        return reviewsArr;
    }

    public void setReviewsArr(ArrayList<ReviewsArr> reviewsArr) {
        this.reviewsArr = reviewsArr;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getTotRats() {
        return totRats;
    }

    public void setTotRats(String totRats) {
        this.totRats = totRats;
    }

    public String getMonthAmt() {
        return monthAmt;
    }

    public void setMonthAmt(String monthAmt) {
        this.monthAmt = monthAmt;
    }

    public String getCmpltApts() {
        return cmpltApts;
    }

    public void setCmpltApts(String cmpltApts) {
        this.cmpltApts = cmpltApts;
    }

    public String getpPic() {
        return pPic;
    }

    public void setpPic(String pPic) {
        this.pPic = pPic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQry() {
        return qry;
    }

    public void setQry(String qry) {
        this.qry = qry;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getLastBilledAmt() {
        return lastBilledAmt;
    }

    public void setLastBilledAmt(String lastBilledAmt) {
        this.lastBilledAmt = lastBilledAmt;
    }

    public String getWeekAmt() {
        return weekAmt;
    }

    public void setWeekAmt(String weekAmt) {
        this.weekAmt = weekAmt;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getLicNo() {
        return licNo;
    }

    public void setLicNo(String licNo) {
        this.licNo = licNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
