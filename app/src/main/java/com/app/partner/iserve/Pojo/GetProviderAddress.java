package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 16/11/16.
 */
public class GetProviderAddress implements Serializable
{
    private String errNum;

    private String errMsg;

    private ArrayList<Addlist> addlist;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public ArrayList<Addlist> getAddlist() {
        return addlist;
    }

    public void setAddlist(ArrayList<Addlist> addlist) {
        this.addlist = addlist;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }
}
