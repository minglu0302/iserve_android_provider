package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 23/6/16.
 */
public class GetTypeByCityIdPojo implements Serializable
{
    private String errNum;

    private ArrayList<Hourly> Hourly;

    private String errMsg;

    private ArrayList<Mileage> Mileage;

    private String errFlag;

    private ArrayList<Fixed> Fixed;

    private int selectedType=0;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public ArrayList<com.app.partner.iserve.Pojo.Hourly> getHourly() {
        return Hourly;
    }

    public void setHourly(ArrayList<com.app.partner.iserve.Pojo.Hourly> hourly) {
        Hourly = hourly;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public ArrayList<com.app.partner.iserve.Pojo.Mileage> getMileage() {
        return Mileage;
    }

    public void setMileage(ArrayList<com.app.partner.iserve.Pojo.Mileage> mileage) {
        Mileage = mileage;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public ArrayList<com.app.partner.iserve.Pojo.Fixed> getFixed() {
        return Fixed;
    }

    public void setFixed(ArrayList<com.app.partner.iserve.Pojo.Fixed> fixed) {
        Fixed = fixed;
    }

    public int getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(int selectedType) {
        this.selectedType = selectedType;
    }

   /*  {
    "errNum":"107",
    "errFlag":"0",
    "errMsg":"Got Provider List !",
    "Mileage":[
    {
    "cat_id":"579222bee127c96634ef25b3",
    "cat_name":"Delivery"
    }
    ],
    "Hourly":[
    {
    "cat_id":"57866015e127c9f305517fdb",
    "cat_name":"tutor"
    },
    {
    "cat_id":"57906528e127c99d79ef25b3",
    "cat_name":"PLUMBER"
    },
    {
    "cat_id":"57919aa7e127c96d31ef25b2",
    "cat_name":"Painter"
    },
    {
    "cat_id":"5791a678e127c9df31ef25b2",
    "cat_name":"DOCTOR"
    },
    {
    "cat_id":"578cd31ae127c90a3fef25b5",
    "cat_name":"Cleaner"
    }
    ],
    "Fixed":[
    {
    "cat_id":"578c8889e127c9462aef25b2",
    "cat_name":"Babysitter"
    },
    {
    "cat_id":"57866073e127c93b12517fdb",
    "cat_name":"hair stylist"
    }
    ]
    }*/
}
