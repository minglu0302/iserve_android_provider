package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 24/10/16.
 */
public class LaterBookings implements Serializable
{
    private String expireTime;

    private String phone;

    private String end_slot;

    private String start_slot;

    private String bookType;

    private String apptLong;

    private String visit_amount;

    private String job_imgs;

    private String cat_name;

    private String amt_per_min;

    private String distance;

    private String timer_status;

    private String apntDt;

    private String timer;

    private String job_start_time;

    private String apptLat;

    private String pro_notes;

    private String price_per_min;

    private String bid;

    private String customer_notes;

    private String fname;

    private String pPic;

    private String job_timer;

    private String status;

    private String statusMsg;

    private String cid;

    private String discount;

    private String addrLine1;

    private String addrLine2;

    private String email;

    private String coupon_discount;

    private String cancelAmt;

    private String payment_type;

    private String appt_duration;

    private String apntTime;

    private String apntDate;

    private ArrayList<Services> services;

    private boolean isHeader=false;

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEnd_slot() {
        return end_slot;
    }

    public void setEnd_slot(String end_slot) {
        this.end_slot = end_slot;
    }

    public String getStart_slot() {
        return start_slot;
    }

    public void setStart_slot(String start_slot) {
        this.start_slot = start_slot;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public String getApptLong() {
        return apptLong;
    }

    public void setApptLong(String apptLong) {
        this.apptLong = apptLong;
    }

    public String getVisit_amount() {
        return visit_amount;
    }

    public void setVisit_amount(String visit_amount) {
        this.visit_amount = visit_amount;
    }

    public String getJob_imgs() {
        return job_imgs;
    }

    public void setJob_imgs(String job_imgs) {
        this.job_imgs = job_imgs;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getAmt_per_min() {
        return amt_per_min;
    }

    public void setAmt_per_min(String amt_per_min) {
        this.amt_per_min = amt_per_min;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTimer_status() {
        return timer_status;
    }

    public void setTimer_status(String timer_status) {
        this.timer_status = timer_status;
    }

    public String getApntDt() {
        return apntDt;
    }

    public void setApntDt(String apntDt) {
        this.apntDt = apntDt;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getJob_start_time() {
        return job_start_time;
    }

    public void setJob_start_time(String job_start_time) {
        this.job_start_time = job_start_time;
    }

    public String getApptLat() {
        return apptLat;
    }

    public void setApptLat(String apptLat) {
        this.apptLat = apptLat;
    }

    public String getPro_notes() {
        return pro_notes;
    }

    public void setPro_notes(String pro_notes) {
        this.pro_notes = pro_notes;
    }

    public String getPrice_per_min() {
        return price_per_min;
    }

    public void setPrice_per_min(String price_per_min) {
        this.price_per_min = price_per_min;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getCustomer_notes() {
        return customer_notes;
    }

    public void setCustomer_notes(String customer_notes) {
        this.customer_notes = customer_notes;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getpPic() {
        return pPic;
    }

    public void setpPic(String pPic) {
        this.pPic = pPic;
    }

    public String getJob_timer() {
        return job_timer;
    }

    public void setJob_timer(String job_timer) {
        this.job_timer = job_timer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAddrLine1() {
        return addrLine1;
    }

    public void setAddrLine1(String addrLine1) {
        this.addrLine1 = addrLine1;
    }

    public String getAddrLine2() {
        return addrLine2;
    }

    public void setAddrLine2(String addrLine2) {
        this.addrLine2 = addrLine2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public String getCancelAmt() {
        return cancelAmt;
    }

    public void setCancelAmt(String cancelAmt) {
        this.cancelAmt = cancelAmt;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getAppt_duration() {
        return appt_duration;
    }

    public void setAppt_duration(String appt_duration) {
        this.appt_duration = appt_duration;
    }

    public String getApntTime() {
        return apntTime;
    }

    public void setApntTime(String apntTime) {
        this.apntTime = apntTime;
    }

    public String getApntDate() {
        return apntDate;
    }

    public void setApntDate(String apntDate) {
        this.apntDate = apntDate;
    }

    public ArrayList<Services> getServices() {
        return services;
    }

    public void setServices(ArrayList<Services> services) {
        this.services = services;
    }
}
