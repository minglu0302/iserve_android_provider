package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 28/6/16.
 */
public class MasterSignUpPojo implements Serializable
{
    private String mail;

    private String phone;

    private String serverChn;

    private String lName;

    private String fName;

    private String errNum;

    private String flag;

    private String joined;

    private String email;

    private String token;

    private String subsChn;

    private String name;

    private String errMsg;

    private String expiryLocal;

    private String expiryGMT;

    private String proid;

    private String chn;

    private String errFlag;

    private String typeId;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getServerChn() {
        return serverChn;
    }

    public void setServerChn(String serverChn) {
        this.serverChn = serverChn;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getJoined() {
        return joined;
    }

    public void setJoined(String joined) {
        this.joined = joined;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSubsChn() {
        return subsChn;
    }

    public void setSubsChn(String subsChn) {
        this.subsChn = subsChn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getExpiryLocal() {
        return expiryLocal;
    }

    public void setExpiryLocal(String expiryLocal) {
        this.expiryLocal = expiryLocal;
    }

    public String getExpiryGMT() {
        return expiryGMT;
    }

    public void setExpiryGMT(String expiryGMT) {
        this.expiryGMT = expiryGMT;
    }

    public String getProid() {
        return proid;
    }

    public void setProid(String proid) {
        this.proid = proid;
    }

    public String getChn() {
        return chn;
    }

    public void setChn(String chn) {
        this.chn = chn;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}


