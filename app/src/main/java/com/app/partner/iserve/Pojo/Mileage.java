package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 28/7/16.
 */
public class Mileage implements Serializable
{
    private String cat_id;

    private String cat_name;

    private boolean isSelected=false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }
}
