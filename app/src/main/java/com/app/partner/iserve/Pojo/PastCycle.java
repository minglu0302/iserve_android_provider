package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 1/12/16.
 */
public class PastCycle implements Serializable
{
    private String opening_balance;

    private String pay_amount;

    private String no_of_bookings;

    private String rev_rate;

    private String total_reject;

    private String last_bid;

    private String closing_balance;

    private String tot_accepted;

    private String total_ignore;

    private String booking_earn;

    private String tot_cancel;

    private String acpt_rate;

    private String tot_bookings;

    private String payroll_id;

    private String pay_date;

    private String start_date;

    public String getOpening_balance() {
        return opening_balance;
    }

    public void setOpening_balance(String opening_balance) {
        this.opening_balance = opening_balance;
    }

    public String getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(String pay_amount) {
        this.pay_amount = pay_amount;
    }

    public String getNo_of_bookings() {
        return no_of_bookings;
    }

    public void setNo_of_bookings(String no_of_bookings) {
        this.no_of_bookings = no_of_bookings;
    }

    public String getRev_rate() {
        return rev_rate;
    }

    public void setRev_rate(String rev_rate) {
        this.rev_rate = rev_rate;
    }

    public String getTotal_reject() {
        return total_reject;
    }

    public void setTotal_reject(String total_reject) {
        this.total_reject = total_reject;
    }

    public String getLast_bid() {
        return last_bid;
    }

    public void setLast_bid(String last_bid) {
        this.last_bid = last_bid;
    }

    public String getClosing_balance() {
        return closing_balance;
    }

    public void setClosing_balance(String closing_balance) {
        this.closing_balance = closing_balance;
    }

    public String getTot_accepted() {
        return tot_accepted;
    }

    public void setTot_accepted(String tot_accepted) {
        this.tot_accepted = tot_accepted;
    }

    public String getTotal_ignore() {
        return total_ignore;
    }

    public void setTotal_ignore(String total_ignore) {
        this.total_ignore = total_ignore;
    }

    public String getBooking_earn() {
        return booking_earn;
    }

    public void setBooking_earn(String booking_earn) {
        this.booking_earn = booking_earn;
    }

    public String getTot_cancel() {
        return tot_cancel;
    }

    public void setTot_cancel(String tot_cancel) {
        this.tot_cancel = tot_cancel;
    }

    public String getAcpt_rate() {
        return acpt_rate;
    }

    public void setAcpt_rate(String acpt_rate) {
        this.acpt_rate = acpt_rate;
    }

    public String getTot_bookings() {
        return tot_bookings;
    }

    public void setTot_bookings(String tot_bookings) {
        this.tot_bookings = tot_bookings;
    }

    public String getPayroll_id() {
        return payroll_id;
    }

    public void setPayroll_id(String payroll_id) {
        this.payroll_id = payroll_id;
    }

    public String getPay_date() {
        return pay_date;
    }

    public void setPay_date(String pay_date) {
        this.pay_date = pay_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }
}
