package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 23/6/16.
 */
public class Pro implements Serializable
{
    private String cat_name;

    private String cat_id;

    private boolean checked = false;

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
