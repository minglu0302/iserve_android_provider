package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 5/9/16.
 */
public class ReviewsArr implements Serializable
{
    private String total;

    private String review_dt;

    private String rating;

    private String review;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getReview_dt() {
        return review_dt;
    }

    public void setReview_dt(String review_dt) {
        this.review_dt = review_dt;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
