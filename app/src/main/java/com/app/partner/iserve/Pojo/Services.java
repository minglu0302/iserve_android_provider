package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 2/12/16.
 */
public class Services implements Serializable
{
    private String sid;

    private String sprice;

    private String sname;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSprice() {
        return sprice;
    }

    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
}
