package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 26/12/16.
 */
public class SlotDetails implements Serializable
{
    private String lname;

    private String appt_date;

    private String address;

    private String email;

    private String job_images;

    private String pic;

    private String bid;

    private String cid;

    private String customer_notes;

    private String fname;

    private String cat_name;

    private String mobile;

    private String appt_lat;

    private String appt_long;

    public String getAppt_lat() {
        return appt_lat;
    }

    public void setAppt_lat(String appt_lat) {
        this.appt_lat = appt_lat;
    }

    public String getAppt_long() {
        return appt_long;
    }

    public void setAppt_long(String appt_long) {
        this.appt_long = appt_long;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getAppt_date() {
        return appt_date;
    }

    public void setAppt_date(String appt_date) {
        this.appt_date = appt_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJob_images() {
        return job_images;
    }

    public void setJob_images(String job_images) {
        this.job_images = job_images;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCustomer_notes() {
        return customer_notes;
    }

    public void setCustomer_notes(String customer_notes) {
        this.customer_notes = customer_notes;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }
}
