package com.app.partner.iserve.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 15/11/16.
 */
public class Slots implements Serializable
{
    private ArrayList<Slot> slot;

    private String date;

    public ArrayList<Slot> getSlot() {
        return slot;
    }

    public void setSlot(ArrayList<Slot> slot) {
        this.slot = slot;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
