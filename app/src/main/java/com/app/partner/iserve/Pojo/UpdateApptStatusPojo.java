package com.app.partner.iserve.Pojo;

import java.io.Serializable;

/**
 * Created by embed on 27/7/16.
 */
public class UpdateApptStatusPojo implements Serializable
{
    private String errNum;

    private String test;

    private String errMsg;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }


  /*
   SAMPLE RESPONSE:
   {
        "errNum":"57",
            "errFlag":"0",
            "errMsg":"Status updated as on the way.",
            "test":""
    }*/
}
