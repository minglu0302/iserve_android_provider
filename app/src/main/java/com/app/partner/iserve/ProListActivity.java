package com.app.partner.iserve;

import android.app.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import com.app.partner.iserve.Pojo.Fixed;
import com.app.partner.iserve.Pojo.GetTypeByCityIdPojo;
import com.app.partner.iserve.Pojo.Hourly;
import com.app.partner.iserve.Pojo.Mileage;
import com.app.partner.iserve.Utility.Utility;

public class ProListActivity extends Activity {



    private GetTypeByCityIdPojo mGetTypeByCityIdPojo;
    TextView header,tvDone,mToolbar_title;
    private ArrayList<Hourly> mHourlyPriceCategories;
    private ArrayList<Mileage> mMilegePriceCategories;
    private ArrayList<Fixed> mFixedPriceCategories;
    LinearLayout llMain,llSub;
    private ImageView ivBackbtn;
    private ArrayList<CompoundButton> selectedTypes=new ArrayList<>();
    private ArrayList<CheckBox> lHourlyPriceCategories=new ArrayList<>();
    private ArrayList<CheckBox> lFixedPriceCategories=new ArrayList<>();
    private ArrayList<CheckBox> lMilegePriceCategories=new ArrayList<>();
    private Typeface font,fontBold;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Intent intent=getIntent();
        mGetTypeByCityIdPojo= (GetTypeByCityIdPojo) intent.getSerializableExtra("CITY_CATEGORY");
        setContentView(R.layout.activity_pro_list);

        fontBold = Utility.getFontBold(ProListActivity.this);
        font=Utility.getFontRegular(ProListActivity.this);

        llMain= (LinearLayout) findViewById(R.id.mainContainer);
        llSub= (LinearLayout) findViewById(R.id.subContainer);
        header= (TextView) findViewById(R.id.header);
        ivBackbtn= (ImageView) findViewById(R.id.ivBackbtn);
        ivBackbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvDone= (TextView) findViewById(R.id.tvDone);
        tvDone.setTypeface(font);

        mToolbar_title= (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvDone.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(selectedTypes.size()>0)
                {
                    sendResult();
                }else
                {
                    Snackbar.make(view, "Please select the types first.", Snackbar.LENGTH_LONG)
                            .setAction("Close", null).show();
                }

            }
        });
        if(mGetTypeByCityIdPojo!=null)
        {
            mHourlyPriceCategories=mGetTypeByCityIdPojo.getHourly();
            mFixedPriceCategories=mGetTypeByCityIdPojo.getFixed();
            mMilegePriceCategories=mGetTypeByCityIdPojo.getMileage();
        }

        mSetViews();

    }

    public void mSetViews()
    {

        for(int index=0;index<3;index++)
        {
            LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row1_pro_cat,null);
            final   LinearLayout linearLayout= (LinearLayout) view.findViewById(R.id.subContainer);
            header= (TextView) view.findViewById(R.id.header);
            header.setTypeface(fontBold);
//            AppCompatCheckBox fakeCheckbox[]=new AppCompatCheckBox[1];
//            fakeCheckbox[0]=null;
            switch (index) 
            {
                case 0:


                    if(mHourlyPriceCategories.size()>0)
                    {
                        header.setText("HOURLY");

                        header.setVisibility(View.VISIBLE);
                        for(int i=0;i<mHourlyPriceCategories.size();i++)
                        {
                            LayoutInflater inflater2= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                            View subView=inflater2.inflate(R.layout.pro_list_single_row,null);

                            TextView textView= (TextView) subView.findViewById(R.id.category);
                            textView.setText(mHourlyPriceCategories.get(i).getCat_name().toUpperCase());
                            textView.setTypeface(font);

                            final    AppCompatCheckBox checkBox= (AppCompatCheckBox) subView.findViewById(R.id.checkbox);
                            checkBox.setVisibility(View.VISIBLE);
                            checkBox.setTag(mHourlyPriceCategories.get(i).getCat_name().toUpperCase());
                            lHourlyPriceCategories.add(checkBox);
                            final int finalI = i;
                            if(mGetTypeByCityIdPojo.getHourly().get(finalI).isSelected())
                            {
                                checkBox.setChecked(true);
                                mGetTypeByCityIdPojo.setSelectedType(2);
                                selectedTypes.add(checkBox);
                            }

                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b)
                                {
                                    if(b && mGetTypeByCityIdPojo.getSelectedType()==0)
                                    {
                                        mGetTypeByCityIdPojo.setSelectedType(2);
                                        selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.getHourly().get(finalI).setSelected(true);

                                    }else if(b && mGetTypeByCityIdPojo.getSelectedType()==2)
                                    {
                                        selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.getHourly().get(finalI).setSelected(true);
                                    }
                                    else if(!b && mGetTypeByCityIdPojo.getSelectedType()==2)
                                    {
                                        selectedTypes.remove(checkBox);
                                        mGetTypeByCityIdPojo.getHourly().get(finalI).setSelected(false);
                                        if(selectedTypes.size()==0){
                                            mGetTypeByCityIdPojo.setSelectedType(0);
                                        }

                                    }else
                                    {

                                        removeViews(mGetTypeByCityIdPojo.getSelectedType());
                                        /*selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.setSelectedType(1);*/
                                    }
                                    Utility.printLog(" item added size "+selectedTypes.size()+"type "+mGetTypeByCityIdPojo.getSelectedType());
                                    //manageClick(checkBox,linearLayout);
                                }
                            });


                            linearLayout.addView(subView);

                        }
                    }
                    break;
                case 1:

                    if(mFixedPriceCategories.size()>0) {
                        header.setText("FIXED");

                        header.setVisibility(View.VISIBLE);
                        for (int i = 0; i < mFixedPriceCategories.size(); i++) {
                            LayoutInflater inflater2 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                            View subView = inflater2.inflate(R.layout.pro_list_single_row, null);

                            TextView textView = (TextView) subView.findViewById(R.id.category);
                            textView.setText(mFixedPriceCategories.get(i).getCat_name().toUpperCase());
                            textView.setTypeface(font);

                            final AppCompatCheckBox checkBox = (AppCompatCheckBox) subView.findViewById(R.id.checkbox);
                            checkBox.setVisibility(View.VISIBLE);
                            final int finalI = i;
                            checkBox.setTag(mFixedPriceCategories.get(i).getCat_name().toUpperCase());
                            if(mGetTypeByCityIdPojo.getFixed().get(finalI).isSelected())
                            {
                                checkBox.setChecked(true);
                                mGetTypeByCityIdPojo.setSelectedType(3);
                                selectedTypes.add(checkBox);
                            }

                            lFixedPriceCategories.add(checkBox);
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && mGetTypeByCityIdPojo.getSelectedType() == 0) {
                                        mGetTypeByCityIdPojo.setSelectedType(3);
                                        selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.getFixed().get(finalI).setSelected(true);

                                    } else if (b && mGetTypeByCityIdPojo.getSelectedType() == 3)
                                    {
                                        selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.getFixed().get(finalI).setSelected(true);
                                    }
                                    else if (!b && mGetTypeByCityIdPojo.getSelectedType() == 3)
                                    {
                                        selectedTypes.remove(checkBox);
                                        mGetTypeByCityIdPojo.getFixed().get(finalI).setSelected(false);
                                        if (selectedTypes.size() == 0) {
                                            mGetTypeByCityIdPojo.setSelectedType(0);
                                        }
                                    } else {
                                        removeViews(mGetTypeByCityIdPojo.getSelectedType());
                                        /*selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.setSelectedType(2);*/
                                    }

                                    Utility.printLog(" item added size " + selectedTypes.size() + "type " + mGetTypeByCityIdPojo.getSelectedType());
                                    // manageClick(checkBox,linearLayout);
                                }
                            });


                            linearLayout.addView(subView);

                        }
                    }
                    break;
                case 2:


                    if(mMilegePriceCategories.size()>0) {
                        header.setText("MILEAGE");

                        header.setVisibility(View.VISIBLE);
                        for (int i = 0; i < mMilegePriceCategories.size(); i++) {
                            LayoutInflater inflater2 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                            View subView = inflater2.inflate(R.layout.pro_list_single_row, null);

                            TextView textView = (TextView) subView.findViewById(R.id.category);
                            textView.setText(mMilegePriceCategories.get(i).getCat_name().toUpperCase());
                            textView.setTypeface(font);

                            final AppCompatCheckBox checkBox = (AppCompatCheckBox) subView.findViewById(R.id.checkbox);
                            checkBox.setVisibility(View.VISIBLE);
                            checkBox.setTag(mMilegePriceCategories.get(i).getCat_name().toUpperCase());
                            lMilegePriceCategories.add(checkBox);
                            final int finalI=i;
                            if(mGetTypeByCityIdPojo.getMileage().get(finalI).isSelected())
                            {
                                checkBox.setChecked(true);
                                mGetTypeByCityIdPojo.setSelectedType(1);
                                selectedTypes.add(checkBox);
                            }

                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && mGetTypeByCityIdPojo.getSelectedType() == 0) {
                                        mGetTypeByCityIdPojo.setSelectedType(1);
                                        selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.getMileage().get(finalI).setSelected(true);


                                    } else if (b && mGetTypeByCityIdPojo.getSelectedType() == 1)
                                    {
                                        selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.getMileage().get(finalI).setSelected(true);
                                    }
                                    else if (!b && mGetTypeByCityIdPojo.getSelectedType() == 1)
                                    {
                                        selectedTypes.remove(checkBox);
                                        mGetTypeByCityIdPojo.getMileage().get(finalI).setSelected(false);
                                        if (selectedTypes.size() == 0) {
                                            mGetTypeByCityIdPojo.setSelectedType(0);
                                        }
                                    } else {

                                        removeViews(mGetTypeByCityIdPojo.getSelectedType());
                                       /* selectedTypes.add(checkBox);
                                        mGetTypeByCityIdPojo.setSelectedType(3);*/
                                    }

                                    //manageClick(checkBox,linearLayout);
                                }
                            });


                            linearLayout.addView(subView);

                        }
                    }
                    break;
            }

            llMain.addView(view);

        }
    }



    public void removeViews(int selectedType)
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(ProListActivity.this,5);
        builder.setTitle("Alert:");
        builder.setMessage("You cannot select types from different categories");
        builder.setCancelable(false);
        builder.setNegativeButton("ok",
        null);
        AlertDialog alertDialog=builder.create();
        if(!alertDialog.isShowing())
        {
            alertDialog.show();
        }


        /*switch (selectedType)
        {
            case 2:*/
                for(int i=0;i<lHourlyPriceCategories.size();i++)
                {
                    if(lHourlyPriceCategories.get(i).isChecked()){
                        lHourlyPriceCategories.get(i).setChecked(false);
                        mGetTypeByCityIdPojo.getHourly().get(i).setSelected(false);
                    }
                }
                selectedTypes.clear();
               // break;
            //case 3:
                for(int i=0;i<lFixedPriceCategories.size();i++)
                {
                    if(lFixedPriceCategories.get(i).isChecked()){
                        lFixedPriceCategories.get(i).setChecked(false);
                        mGetTypeByCityIdPojo.getFixed().get(i).setSelected(false);
                    }
                }
                //selectedTypes.clear();
              //  break;
            //case 1:
                for(int i=0;i<lMilegePriceCategories.size();i++)
                {
                    if(lMilegePriceCategories.get(i).isChecked()){
                        lMilegePriceCategories.get(i).setChecked(false);
                        mGetTypeByCityIdPojo.getMileage().get(i).setSelected(false);
                    }
                }
               // selectedTypes.clear();
               // break;
     //   }
        mGetTypeByCityIdPojo.setSelectedType(0);

    }

    public void sendResult()
    {
        if(selectedTypes!=null && selectedTypes.size()>0)
        {
            String selected="",typeID="";
            for (int index = 0; index<selectedTypes.size();index++)
            {
                selected = selected + selectedTypes.get(index).getTag()+",";
            }
            switch (mGetTypeByCityIdPojo.getSelectedType())
            {
                case 2:
                    for (int index = 0; index<selectedTypes.size();index++)
                    {
                        for(int i=0;i<mHourlyPriceCategories.size();i++)
                        {
                            if(selectedTypes.get(index).getTag().toString().equalsIgnoreCase(mHourlyPriceCategories.get(i).getCat_name().toString()))
                            {
                                typeID = typeID + mHourlyPriceCategories.get(i).getCat_id()+",";
                            }
                        }
                    }

                    break;
                case 3:
                    for (int index = 0; index<selectedTypes.size();index++)
                    {
                        for(int i=0;i<mFixedPriceCategories.size();i++)
                        {
                            if(selectedTypes.get(index).getTag().toString().equalsIgnoreCase(mFixedPriceCategories.get(i).getCat_name().toString()))
                            {
                                typeID = typeID + mFixedPriceCategories.get(i).getCat_id()+",";
                            }
                        }
                    }

                    break;
                case 1:
                    for (int index = 0; index<selectedTypes.size();index++)
                    {
                        for(int i=0;i<mMilegePriceCategories.size();i++)
                        {
                            if(selectedTypes.get(index).getTag().toString().equalsIgnoreCase(mMilegePriceCategories.get(i).getCat_name().toString()))
                            {
                                typeID = typeID + mMilegePriceCategories.get(i).getCat_id()+",";
                            }
                        }
                    }
                    break;
            }

            Utility.printLog("Pk result "+selected+"  "+typeID);
            if(!selected.isEmpty())
            {
                selected=selected.substring(0,selected.length()-1);
            }
            if(!typeID.isEmpty())
            {
                typeID=typeID.substring(0,typeID.length()-1);
            }

            /*selected=selected.substring(0,selected.length());
            typeID=typeID.substring(0,typeID.length());*/
            Toast.makeText(ProListActivity.this,selected, Toast.LENGTH_LONG).show();
            Intent intent1=new Intent(ProListActivity.this,RegisterActivity.class);
            intent1.putExtra("SELECTED_CATEGORY",selected);
            intent1.putExtra("FEES_GROUP",""+mGetTypeByCityIdPojo.getSelectedType());
            intent1.putExtra("TYPE_ID",""+typeID);
            intent1.putExtra("PRO_LIST",mGetTypeByCityIdPojo);
            setResult(RESULT_OK,intent1);
            finish();
        }
    }

}
