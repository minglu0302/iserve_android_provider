package com.app.partner.iserve;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import com.app.partner.iserve.CountryPicker.CountryPicker;
import com.app.partner.iserve.CountryPicker.CountryPickerListener;
import com.app.partner.iserve.Pojo.City;
import com.app.partner.iserve.Pojo.GetCityListPojo;
import com.app.partner.iserve.Pojo.GetTypeByCityIdPojo;
import com.app.partner.iserve.Pojo.MasterSignUpPojo;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.InternalStorageContentProvider;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.RnuTimePermissionRequest;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Upload_file_AmazonS3;
import com.app.partner.iserve.Utility.Utility;
import eu.janmuller.android.simplecropimage.CropImage;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


/**
 * Created by embed on 23/2/16.
 */
public class RegisterActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_PRO_LIST_ACTIVITY = 107;
    private String TAG = "RegisterActivity";
    private TextInputLayout mFirstNameWrapper;
    private RelativeLayout mSelectCityWrapper;
    private TextInputLayout mLastNameWrapper;
    private TextInputLayout mEmailWrapper;
    private TextInputLayout mPhoneNoWrapper;
    private RelativeLayout mProviderTypeWrapper;
    private TextInputLayout mPasswordWrapper;
    private TextInputLayout mConfPasswordWrapper;
    private TextInputLayout mLicenseNoWrapper;
    private TextView mCheckBoxtext,tvSelectCity;
    private ImageView ivCheckbox, ivBackbtn, ivNextbtn,ivProfilePic;
    private Button mSignupbtn,btnCamera,btnGallery,btnRemove,btnCancel;
    CheckBox ivChk;
    private Toolbar mToolbar;
    private TextView mToolbar_title;
    private final int REQUEST_CODE_GALLERY = 0x1;
    private final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    private final int REQUEST_CODE_CROP_IMAGE = 0x3;
    private Bitmap bitmap;
    private boolean isPicturetaken;
    private File mFileTemp;
    private ProgressDialog mProgressDailog;
    private GetCityListPojo mGetCityList;
    private AlertDialog cityDialog;
    private String mCityListItems[];
    private TextView tvProviderType;
    private int mPosition;
    private GetTypeByCityIdPojo mGetTypeByIdResponse;
    private String selected;
    private double[] location;
    private String latitude;
    private String longitude;
    private SessionManager sessionManager;
    private MasterSignUpPojo masterSignUpPojo;
    private EditText etFirstName,etLastName,etEmail,etPhoneNo,etPassword,etZipCode,etLicenseNumber,etConfirmPassword;
    private String fees_group;
    private String mTypeId;
    private String mAmazonImgUrl="";
    private ImageView flag;
    private TextView countryCode;
    private RelativeLayout countryPickerLayout;
    private RnuTimePermissionRequest rnuTimePermissionRequest;
    private String[] permissions;
    private static final int PERMISSION_REQUEST_CODE=102;
    private ProgressBar pbEmail,pbMobile;
    private String currentVersion;
    private HashMap<String,GetTypeByCityIdPojo> ProListHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        sessionManager=new SessionManager(this);
        ProListHashMap=new HashMap<>();
        initLayoutId();


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void initLayoutId()
    {

        rnuTimePermissionRequest =new RnuTimePermissionRequest(this);
        permissions= new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};


        Typeface font=Utility.getFontRegular(RegisterActivity.this);
        Typeface fontBold=Utility.getFontBold(RegisterActivity.this);

        String state = Environment.getExternalStorageState();

        String filename=AppConstants.TEMP_PHOTO_FILE_NAME+System.currentTimeMillis()+".png";

        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), filename);
        }
        else
        {
            mFileTemp = new File(getFilesDir(),filename);
        }
        Utility.printLog("PK file path "+mFileTemp.getPath());
        //mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        mToolbar_title = (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setText(getResources().getString(R.string.REGISTER));
        mToolbar_title.setTypeface(fontBold);
        // mToolbar_title.setVisibility(View.VISIBLE);

        //  setSupportActionBar(mToolbar);
        //  getSupportActionBar().setTitle("");


        mFirstNameWrapper = (TextInputLayout) findViewById(R.id.etFirstNameWrapper);
        mFirstNameWrapper.setTypeface(font);

        mSelectCityWrapper = (RelativeLayout) findViewById(R.id.etCityWrapper);

        mLastNameWrapper = (TextInputLayout) findViewById(R.id.etLastNameWrapper);
        mLastNameWrapper.setTypeface(font);

        mEmailWrapper = (TextInputLayout) findViewById(R.id.etEmailNameWrapper);
        mEmailWrapper.setTypeface(font);

        mPhoneNoWrapper = (TextInputLayout) findViewById(R.id.etPhoneNoWrapper);
        mPhoneNoWrapper.setTypeface(font);

        mProviderTypeWrapper = (RelativeLayout) findViewById(R.id.etProviderTypeWrapper);

        mPasswordWrapper = (TextInputLayout) findViewById(R.id.etPasswordWrapper);
        mPasswordWrapper.setTypeface(font);

        mConfPasswordWrapper = (TextInputLayout) findViewById(R.id.etConfirmPasswordWrapper);
        mConfPasswordWrapper.setTypeface(font);

        mLicenseNoWrapper = (TextInputLayout) findViewById(R.id.etLicenseNumberWrapper);
        mLicenseNoWrapper.setTypeface(font);

        mCheckBoxtext = (TextView) findViewById(R.id.tvCheckBoxTxt);
        mCheckBoxtext.setTypeface(font);

        tvSelectCity = (TextView) findViewById(R.id.etSelectCity);
        tvSelectCity.setTypeface(font);

        tvProviderType = (TextView) findViewById(R.id.etProviderType);
        tvProviderType.setTypeface(font);

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etFirstName.setTypeface(font);

        etLicenseNumber = (EditText) findViewById(R.id.etLicenseNumber);
        etLicenseNumber.setTypeface(font);

        countryCode = (TextView) findViewById(R.id.countryCode);
        countryCode.setTypeface(font);

        etLastName = (EditText) findViewById(R.id.etLastName);
        etLastName.setTypeface(font);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.setTypeface(font);

        etPhoneNo = (EditText) findViewById(R.id.etPhoneNo);
        etPhoneNo.setTypeface(font);

        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword.setTypeface(font);

        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        etConfirmPassword.setTypeface(font);
        //etZipCode = (EditText) findViewById(R.id.etZipCode);
        ivChk = (CheckBox) findViewById(R.id.ivCheckbox);
        ivBackbtn = (ImageView) findViewById(R.id.ivBackbtn);
        ivNextbtn = (ImageView) findViewById(R.id.ivNextbtn);
        ivProfilePic = (ImageView) findViewById(R.id.ivProfilePic);
        flag = (ImageView) findViewById(R.id.flag);
        mSignupbtn = (Button) findViewById(R.id.btnSignUp);
        countryPickerLayout = (RelativeLayout) findViewById(R.id.countryPickerLayout);
        pbEmail = (ProgressBar) findViewById(R.id.pbEmail);
        pbMobile = (ProgressBar) findViewById(R.id.pbMobile);

        ivBackbtn.setVisibility(View.VISIBLE);

        SpannableString content = new SpannableString(getResources().getString(R.string.termsNcondtns));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        mCheckBoxtext.setText(content);
        mCheckBoxtext.setTypeface(font);

        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ivNextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignupbtn.callOnClick();
            }
        });

        ivChk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mSelectCityWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvProviderType.setText("");
                mSelectCityList();
            }
        });

        mProviderTypeWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tvSelectCity.getText().toString().isEmpty())
                {
                    Snackbar.make(view, "First select the city.", Snackbar.LENGTH_LONG)
                            .setAction("Close", null).show();
                }
                else
                {
                    if(!ProListHashMap.containsKey(tvSelectCity.getTag().toString()))
                    {
                        mGetTypeByCityID();
                    }
                    else
                    {
                        mGetTypeByCityIDHandler(ProListHashMap.get(tvSelectCity.getTag().toString()));
                    }

                }
            }
        });

        mSignupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                if (validateFields()) {
                    // Toast.makeText(RegisterActivity.this, "Registered", Toast.LENGTH_SHORT).show();
                    mUploadImageToBucket();

                }

            }
        });

        ivProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!rnuTimePermissionRequest.mCheckPermission(permissions))
                {
                    rnuTimePermissionRequest.mRequestPermissions(permissions,PERMISSION_REQUEST_CODE);
                }
                else
                {
                    mProfileOnclick();
                }

            }
        });

        countryPickerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialoagforcountrypicker();
            }
        });

        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    if(! etEmail.getText().toString().isEmpty() && Utility.isNetworkAvailable(RegisterActivity.this))
                    {
                        mVerifyEmail(etEmail.getText().toString());
                    }
                }
            }
        });

        etPhoneNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    if(! etPhoneNo.getText().toString().isEmpty() && Utility.isNetworkAvailable(RegisterActivity.this))
                    {
                        mVerifyMobile(etPhoneNo.getText().toString());
                    }
                }
            }
        });

        mCheckBoxtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegisterActivity.this,TermsAndConditionActivity.class);
                startActivity(intent);
            }
        });

        try
        {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////
        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mFirstNameWrapper.setError(null);
                mFirstNameWrapper.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mFirstNameWrapper.setError(null);
                mFirstNameWrapper.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mEmailWrapper.setError(null);
                mEmailWrapper.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPhoneNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPhoneNoWrapper.setError(null);
                mPhoneNoWrapper.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etLicenseNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mLicenseNoWrapper.setError(null);
                mLicenseNoWrapper.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPasswordWrapper.setError(null);
                mPasswordWrapper.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPasswordWrapper.setError(null);
                mPasswordWrapper.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private boolean validateFields()
    {
        if (mFirstNameWrapper.getEditText().getText().toString().isEmpty())
        {
            mFirstNameWrapper.setErrorEnabled(true);
            mFirstNameWrapper.setError(getResources().getString(R.string.enterFName));
            return false;
        }
        else if (mEmailWrapper.getEditText().getText().toString().isEmpty())
        {
            mFirstNameWrapper.setError(null);
            mFirstNameWrapper.setErrorEnabled(false);
            mEmailWrapper.setErrorEnabled(true);
            mEmailWrapper.setError(getResources().getString(R.string.enterEmail));
            return false;
        }
        else if(!Utility.validateEmail(mEmailWrapper.getEditText().getText().toString()))
        {
            mEmailWrapper.setErrorEnabled(true);
            mEmailWrapper.setError(getResources().getString(R.string.enterValidEmail));
            return false;
        }
        else if (mPhoneNoWrapper.getEditText().getText().toString().isEmpty())
        {
            mEmailWrapper.setError(null);
            mEmailWrapper.setErrorEnabled(false);
            mPhoneNoWrapper.setErrorEnabled(true);
            mPhoneNoWrapper.setError(getResources().getString(R.string.enterPhone));
            return false;
        }
        else if(tvSelectCity.getText().toString().isEmpty())
        {
            mPhoneNoWrapper.setError(null);
            mPhoneNoWrapper.setErrorEnabled(false);
            mShowErrorMessage(getResources().getString(R.string.constraint_select_city),false);
            return false;
        }

        else if(tvProviderType.getText().toString().isEmpty())
        {
            mShowErrorMessage(getResources().getString(R.string.constraint_select_provder_type),false);
            return false;
        }
        else if (mLicenseNoWrapper.getEditText().getText().toString().isEmpty())
        {
            mPhoneNoWrapper.setError(null);
            mPhoneNoWrapper.setErrorEnabled(false);
            mLicenseNoWrapper.setErrorEnabled(true);
            mLicenseNoWrapper.setError(getResources().getString(R.string.enterLicense));
            return false;
        }

        else if (mPasswordWrapper.getEditText().getText().toString().isEmpty())
        {
            mLicenseNoWrapper.setError(null);
            mLicenseNoWrapper.setErrorEnabled(false);
            mPasswordWrapper.setErrorEnabled(true);
            mPasswordWrapper.setError(getResources().getString(R.string.enterPass));
            return false;
        }
        else if (mConfPasswordWrapper.getEditText().getText().toString().isEmpty())
        {
            mPasswordWrapper.setError(null);
            mPasswordWrapper.setErrorEnabled(false);
            mConfPasswordWrapper.setErrorEnabled(true);
            mConfPasswordWrapper.setError(getResources().getString(R.string.confirmPass));
            return false;
        }
        else if(!mConfPasswordWrapper.getEditText().getText().toString().equals(mPasswordWrapper.getEditText().getText().toString()))
        {
            mConfPasswordWrapper.setErrorEnabled(true);
            mConfPasswordWrapper.setError(getResources().getString(R.string.passDintMatch));
            return false;
        }
        else if(!ivChk.isChecked())
        {
            mConfPasswordWrapper.setError(null);
            mConfPasswordWrapper.setErrorEnabled(false);
            mShowErrorMessage(getResources().getString(R.string.acceptTC),false);
            return false;
        }



        mFirstNameWrapper.setError(null);
        mEmailWrapper.setError(null);
        mPhoneNoWrapper.setError(null);
        mPasswordWrapper.setError(null);
        mConfPasswordWrapper.setError(null);

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onBackPressed()
    {
        finish();
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mProfileOnclick()
    {
        final Dialog mDialog = new Dialog(RegisterActivity.this);
        // mDialog.setTitle("Select photo from :");
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        mDialog.setContentView(R.layout.profile_pic_options);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));



        btnCamera= (Button) mDialog.findViewById(R.id.camera);
        btnCancel= (Button) mDialog.findViewById(R.id.cancel);
        btnGallery= (Button) mDialog.findViewById(R.id.gallery);
        btnRemove= (Button) mDialog.findViewById(R.id.removephoto);
        View line=  mDialog.findViewById(R.id.line3);

        if(!isPicturetaken)
        {
            btnRemove.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
        }
        else
        {
            btnRemove.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
        }
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                takePicture();
                mDialog.dismiss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                openGallery();
                mDialog.dismiss();
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ivProfilePic.setImageResource(R.drawable.pro_photo);
                isPicturetaken=false;
                mDialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mDialog.dismiss();
            }
        });

        mDialog.show();

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void takePicture()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider",mFileTemp);
            }
            else
            {
                if (Environment.MEDIA_MOUNTED.equals(state))
                {
                    mImageCaptureUri = Uri.fromFile(mFileTemp);
                }
                else
                {
                    mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
                }
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

            Log.d("error", "cannot take picture", e);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {

            return;
        }

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:

                try {
                    InputStream inputStream = getContentResolver().openInputStream(
                            data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(
                            mFileTemp);
                    Log.d("", "inputStream" + inputStream);
                    Log.d("", "fileOutputStream" + fileOutputStream);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                    Log.d("", "Error while creating temp file", e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;

            case REQUEST_CODE_CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                Log.d("", "path fileOutputStream " + path);

                if (path == null) {

                    return;
                }

                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                bitmap = Bitmap.createScaledBitmap(bitmap, getResources().getDrawable(R.drawable.pro_photo).getMinimumWidth(), getResources().getDrawable(R.drawable.pro_photo).getMinimumHeight(), true);

                Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

                BitmapShader shader = new BitmapShader(bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                Paint paint = new Paint();
                paint.setShader(shader);
                paint.setAntiAlias(true);
                Canvas c = new Canvas(circleBitmap);
                c.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth()/2, paint);
                ivProfilePic.setImageBitmap(circleBitmap);
                ivProfilePic.setBackgroundDrawable(null);
                //Picasso.with(SignUpUser.this).load(Uri.fromFile(mFileTemp)).transform(new CircleTransform()).into(display_pic);
                isPicturetaken=true;
                break;

            case REQUEST_CODE_PRO_LIST_ACTIVITY:
                selected=data.getStringExtra("SELECTED_CATEGORY");
                fees_group=data.getStringExtra("FEES_GROUP");
                mTypeId=data.getStringExtra("TYPE_ID");
                GetTypeByCityIdPojo getTypeByCityIdPojo= (GetTypeByCityIdPojo) data.getSerializableExtra("PRO_LIST");
                if(getTypeByCityIdPojo!=null)
                {
                    ProListHashMap.put(tvSelectCity.getTag().toString(),getTypeByCityIdPojo);
                    Utility.printLog(TAG+ "OnActivityResult prolist hashmap"+ProListHashMap.toString());
                }
                Log.v("SELECTED_CATEGORY",selected);
                tvProviderType.setText(selected);
                break;

            case  106 :
                String selectedCity = data.getStringExtra("SELECTED_CITY");
                String selectedCityName = data.getStringExtra("SELECTED_CITY_NAME");
                tvSelectCity.setText(selectedCityName);
                tvSelectCity.setTag(selectedCity);

                if(!ProListHashMap.containsKey(tvSelectCity.getTag().toString()))
                {
                    mGetTypeByCityID();
                }
                else
                {
                    mGetTypeByCityIDHandler(ProListHashMap.get(tvSelectCity.getTag().toString()));
                }
        }

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void startCropImage()
    {
        Utility.printLog("Strat crop");
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mSignUpRequest()
    {

        String fname=mFirstNameWrapper.getEditText().getText().toString();
        String lname=mLastNameWrapper.getEditText().getText().toString();
        String email=mEmailWrapper.getEditText().getText().toString();
        String password=mPasswordWrapper.getEditText().getText().toString();
        String mobile=mPhoneNoWrapper.getEditText().getText().toString();
        String city=tvSelectCity.getText().toString();
        String ccode=countryCode.getText().toString();


        if(sessionManager.getPushToken()==null)
        {
            String token = FirebaseInstanceId.getInstance().getToken();
            sessionManager.setPushToken(token);
        }
        String mCityId="";
        for(int i=0;i<mGetCityList.getCity().size();i++){
            if(city.equals(mGetCityList.getCity().get(i).getCity_Name())){
                mCityId=mGetCityList.getCity().get(i).getCity_Id();
            }
        }

        //mTypeId=mTypeId.substring(0,mTypeId.length()-1);

        if(Utility.isNetworkAvailable(RegisterActivity.this))
        {
            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_first_name",fname)
                    .add("ent_last_name",lname)
                    .add("ent_email",email)
                    .add("ent_password",password)
                    .add("ent_mobile",mobile)
                    .add("ent_zipcode","123")
                    .add("ent_latitude",""+latitude)
                    .add("ent_longitude",""+longitude)
                    .add("ent_city_id",mCityId)
                    .add("ent_fees_group",fees_group)
                    .add("ent_device_type_arr",mTypeId)
                    .add("ent_pro_image",mAmazonImgUrl)
                    .add("ent_dev_id",Utility.getDeviceId(RegisterActivity.this))
                    .add("ent_push_token",sessionManager.getPushToken())
                    .add("ent_device_type","2")
                    .add("ent_country_code",ccode)
                    .add("ent_date_time",Utility.getCurrentGmtTime())
                    .add("ent_dev_model", Build.MODEL)
                    .add("ent_dev_os",Build.VERSION.RELEASE)
                    .add("ent_app_version",currentVersion)
                    .add("ent_manf",Build.MANUFACTURER)
                    .build();

            Utility.printReq(requestBody,"Signup Request");
            Okhttp_connection.doOkhttpRequest(ServiceUrls.MASTER_SIGNUP1, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    Utility.printLog(TAG+" SignUp Response"+result);
                    if(mProgressDailog!=null&&mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Gson gson=new Gson();
                    masterSignUpPojo=gson.fromJson(result,MasterSignUpPojo.class);

                    mShowErrorMessage(masterSignUpPojo.getErrMsg(),true);

                }


                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null&&mProgressDailog.isShowing())
                    {
                        mProgressDailog.dismiss();
                    }
                }
            });

        }
        else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mSignUpRequest();
                        }
                    }).show();
        }

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mSelectCityList()
    {
        try
        {
            mProgressDailog=Utility.getProcessDialog(RegisterActivity.this);
            mProgressDailog.show();
            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_date_time",Utility.getCurrentGmtTime())
                    .build();

            Utility.printLog(TAG+" getCityList Request: "+requestBody.toString());

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_CITY_LIST, requestBody, new Okhttp_connection.OkHttpRequestCallback()
            {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null)
                    {
                        mProgressDailog.dismiss();
                    }

                    Utility.printLog(TAG +" getCityList:"+result);
                    Gson gson=new Gson();
                    mGetCityList=gson.fromJson(result, GetCityListPojo.class);
                    if(mGetCityList!=null)
                    {
                        mGetCityListHandler(mGetCityList);
                    }

                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                    Toast.makeText(RegisterActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e) {
            if(mProgressDailog!=null){
                mProgressDailog.dismiss();
            }
            e.printStackTrace();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mGetCityListHandler(final GetCityListPojo mGetCityList)
    {
        if(("1".equals(mGetCityList.getErrNum()) && ( "1".equals(mGetCityList.getErrFlag()))))
        {
            mShowErrorMessage(mGetCityList.getErrMsg(),false);
        }
        else if(("106".equals(mGetCityList.getErrNum()) && ( "0".equals(mGetCityList.getErrFlag()))))
        {

            ArrayList<City> list=mGetCityList.getCity();

            Intent intent=new Intent(RegisterActivity.this,CityListActivity.class);
            intent.putExtra("CITY_LIST",list);
            startActivityForResult(intent,106);

        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void  mGetTypeByCityID()
    {
        String cityValue=tvSelectCity.getText().toString();
        for(int position=0;position<mGetCityList.getCity().size();position++)
        {
            if(cityValue.equals(mGetCityList.getCity().get(position).getCity_Name()))
            {
                mPosition=position;
            }
        }
        try {

            mProgressDailog=Utility.getProcessDialog(RegisterActivity.this);
            mProgressDailog.show();

            RequestBody requestBody = new FormEncodingBuilder()
                    //.add("ent_city_id", mGetCityList.getCity().get(mPosition).getCity_Id())
                    .add("ent_city_id",tvSelectCity.getTag().toString() )
                    .add("ent_date_time", Utility.getCurrentGmtTime())
                    .build();

            Utility.printReq(requestBody,TAG + " getTypeById Request: ");

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_TYPE_BY_CITY_ID, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null)
                    {
                        mProgressDailog.dismiss();
                    }

                    Utility.printLog(TAG + " getTypeById response: " + result);
                    Gson gson = new Gson();
                    mGetTypeByIdResponse = gson.fromJson(result, GetTypeByCityIdPojo.class);

                    ProListHashMap.put(tvSelectCity.getTag().toString(),mGetTypeByIdResponse);
                    Utility.printLog(TAG + " getTypeById ProListHashMap: " + ProListHashMap.toString());

                    mGetTypeByCityIDHandler(mGetTypeByIdResponse);
                }

                @Override
                public void onError(String error) {
                    if(mProgressDailog!=null)
                    {
                        mProgressDailog.dismiss();
                    }
                    Toast.makeText(RegisterActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            if(mProgressDailog!=null)
            {
                mProgressDailog.dismiss();
            }
            e.printStackTrace();
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mGetTypeByCityIDHandler(GetTypeByCityIdPojo mGetTypeByIdResponse)
    {
        if(("1".equals(mGetTypeByIdResponse.getErrNum()) && ( "1".equals(mGetTypeByIdResponse.getErrFlag()))))
        {
            mShowErrorMessage(mGetTypeByIdResponse.getErrMsg(),false);
        }
        else if(("107".equals(mGetTypeByIdResponse.getErrNum()) && ( "0".equals(mGetTypeByIdResponse.getErrFlag()))))
        {

            if(mGetTypeByIdResponse!=null)
            {
                tvProviderType.setText("");
                Intent intent=new Intent(RegisterActivity.this,ProListActivity.class);
                intent.putExtra("CITY_CATEGORY",mGetTypeByIdResponse);
                startActivityForResult(intent,107);
            }
        }
        else if(("108".equals(mGetTypeByIdResponse.getErrNum()) && ( "0".equals(mGetTypeByIdResponse.getErrFlag()))))
        {
            mShowErrorMessage(mGetTypeByIdResponse.getErrMsg(),false);
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume()
    {
        super.onResume();
        location = Utility.getLocation(this);
        latitude = String.valueOf(location[0]);
        longitude = String.valueOf(location[1]);
        Log.i("location ", latitude + ", " + longitude);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg, final boolean flagToSwitch)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(RegisterActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {

                if(flagToSwitch)
                {
                    finish();
                }
                dialogInterface.dismiss();
            }
        });
        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mUploadImageToBucket()
    {
        mProgressDailog=Utility.getProcessDialog(RegisterActivity.this);
        mProgressDailog.setMessage("Signing up...");
        mProgressDailog.setCancelable(false);
        mProgressDailog.show();

        if(isPicturetaken)
        {
            Upload_file_AmazonS3 amazonS3 = Upload_file_AmazonS3.getInstance(RegisterActivity.this,AppConstants.COGNITO_POOL_ID);

            mAmazonImgUrl=AppConstants.AMAZON_BASE_URL+AppConstants.BUCKET+"/"+AppConstants.BUCKET_PROFILE_PICTURE+mFileTemp.getName();
            Log.d(TAG, "mUploadImageToBucket: "+mAmazonImgUrl);

            amazonS3.UploadFile(AppConstants.BUCKET,AppConstants.BUCKET_PROFILE_PICTURE+mFileTemp.getName(), mFileTemp, new Upload_file_AmazonS3.Upload_CallBack() {
                @Override
                public void sucess(String url)
                {
                    Log.d("Suri",url);
                    mSignUpRequest();
                }

                @Override
                public void error(String errormsg) {
                    Log.d("Suri","error");
                }
            });
        }else {
            mSignUpRequest();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * showing dialog for the country picker
     */
    public void showDialoagforcountrypicker() {
        final CountryPicker picker = CountryPicker.newInstance(getResources().getString(R.string.select_country));
        picker.show(getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {

                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                flag.setBackgroundResource(getResId(drawableName));


                countryCode.setText(dialCode);

                picker.dismiss();
            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * to get the resource id from the drawable name
     * @param drawableName
     * @return
     */
    public static int getResId(String drawableName)
    {
        try
        {
            Class<R.drawable> res = R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode){
            case PERMISSION_REQUEST_CODE :

                if(grantResults.length>0){

                    for(int i=0;i<grantResults.length;i++){
                        Log.d("ORPresult grantResults"," "+grantResults[i]);
                        if(grantResults[i]!= PackageManager.PERMISSION_GRANTED)
                        {
                            mShoudHavePermission(permissions[i]);
                        }
                    }
                }
                break;
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShoudHavePermission(String permission)
    {
        Log.d("showrational","called "+permission);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if(shouldShowRequestPermissionRationale(permission))
            {
                showMessage(permission,1,"OK","CANCEL","You need to allow access for "+permission+" permission.");
            }
            else
            {
                showMessage(permission,2,"SETTINGS","EXIT APP","You need to allow access for "+permission+" permission.");
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void showMessage(final String permission,final int dialog,String positiveButton,String negativeButton,String message)
    {
        new android.support.v7.app.AlertDialog.Builder((this))
                .setMessage(message)
                .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(dialog==1)
                        {
                            rnuTimePermissionRequest.mRequestPermissions(new String[]{permission},8080);
                        }
                        else if(dialog==2)
                        {
                            if (RegisterActivity.this == null) {
                                return;
                            }
                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + RegisterActivity.this.getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                            finish();
                        }

                    }
                })
                .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create()
                .show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mVerifyMobile(String mob)
    {

        pbMobile.setVisibility(View.VISIBLE);
        RequestBody requestBody=new FormEncodingBuilder().add("ent_phone",mob).build();

        Okhttp_connection.doOkhttpRequest(ServiceUrls.CHECK_MOBILE, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                if(result!=null)
                {
                    try
                    {
                        pbMobile.setVisibility(View.GONE);
                        JSONObject jsonObject=new JSONObject(result);
                        if(jsonObject.has("errFlag"))
                        {
                            if(jsonObject.get("errFlag").equals("1"))
                            {
                                Snackbar.make(getCurrentFocus(),jsonObject.get("errMsg").toString(),Snackbar.LENGTH_LONG).show();
                                mPhoneNoWrapper.getEditText().setText("");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String error) {
                pbMobile.setVisibility(View.GONE);
            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mVerifyEmail(String email)
    {
        pbEmail.setVisibility(View.VISIBLE);

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_email",email)
                .add("ent_user_type","1")
                .add("ent_date_time",Utility.getCurrentGmtTime())
                .build()
                ;

        Okhttp_connection.doOkhttpRequest(ServiceUrls.VALIDATE_EMAIL_ZIP, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog(TAG+" validateEmailzip response "+result);
                if(result!=null)
                {
                    pbEmail.setVisibility(View.GONE);
                    try
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        if(jsonObject.has("errFlag"))
                        {
                            if(jsonObject.get("errFlag").equals("1"))
                            {
                                Snackbar.make(getCurrentFocus(),jsonObject.get("errMsg").toString(),Snackbar.LENGTH_LONG).show();
                                mEmailWrapper.getEditText().setText("");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String error) {
                pbEmail.setVisibility(View.GONE);
            }
        });
    }
}
