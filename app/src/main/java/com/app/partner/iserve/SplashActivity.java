package com.app.partner.iserve;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.app.partner.iserve.Utility.RnuTimePermissionRequest;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.google.firebase.iid.FirebaseInstanceId;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WAKE_LOCK;

public class SplashActivity extends Activity {

    private static final int PERMISSION_REQUEST_CODE =106;
    String latitude = "", longitude = "";
    private double[] location;
    private String TAG="SplashActivity";
    private int PLAY_SERVICES_RESOLUTION_REQUEST=9000;
    private TextView tvSignIn,tvRegister,tvForgotPass;
    private SessionManager sessionManager;
    private RnuTimePermissionRequest rnuTimePermissionRequest;
    private String[] permissions;

    private Animation left_to_center,right_to_center;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionManager=new SessionManager(this);
        intiLayoutId();


        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            String token = FirebaseInstanceId.getInstance().getToken();
            sessionManager.setPushToken(token);
            Utility.printLog(TAG+" Push token "+token);
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    private void intiLayoutId()
    {
        tvSignIn= (TextView) findViewById(R.id.tvSignInSplash);
        tvRegister= (TextView) findViewById(R.id.tvRegisterSplash);

        Typeface font=Utility.getFontRegular(SplashActivity.this);
        tvSignIn.setTypeface(font);
        tvRegister.setTypeface(font);

        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent=new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                //finish();
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent=new Intent(SplashActivity.this,RegisterActivity.class);
                startActivity(intent);
                //finish();
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        rnuTimePermissionRequest =new RnuTimePermissionRequest(this);
        permissions= new String[]{ACCESS_FINE_LOCATION, READ_PHONE_STATE,/*CAMERA,WRITE_EXTERNAL_STORAGE,*/WAKE_LOCK};

        if(!rnuTimePermissionRequest.mCheckPermission(permissions))
        {
            rnuTimePermissionRequest.mRequestPermissions(permissions,PERMISSION_REQUEST_CODE);
        }

        right_to_center= AnimationUtils.loadAnimation(SplashActivity.this,R.anim.slide_right_to_center);
        left_to_center= AnimationUtils.loadAnimation(SplashActivity.this,R.anim.slide_left_to_center);
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onResume() {
        super.onResume();

        Utility.printLog(TAG+"isUserLoggedIn "+sessionManager.isUserLoggedIn());

        if(sessionManager.isUserLoggedIn() ){
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            tvRegister.startAnimation(right_to_center);
            tvSignIn.startAnimation(left_to_center);
        }


    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
       switch (requestCode){
           case PERMISSION_REQUEST_CODE:

               if(grantResults.length>0){

                   for(int i=0;i<grantResults.length;i++){
                       Log.d("ORPresult grantResults"," "+grantResults[i]);
                       if(grantResults[i]!= PackageManager.PERMISSION_GRANTED)
                       {
                           mShoudHavePermission(permissions[i]);
                       }
                   }
               }
               break;
       }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShoudHavePermission(String permission)
    {
        Log.d("showrational","called "+permission);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if(shouldShowRequestPermissionRationale(permission))
            {
                showMessage(permission,1,"OK","CANCEL","You need to allow access for "+permission+" permission.");
            }
            else
            {
                showMessage(permission,2,"SETTINGS","EXIT APP","You need to allow access for "+permission+" permission.");
            }
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void showMessage(final String permission,final int dialog,String positiveButton,String negativeButton,String message)
    {
        new AlertDialog.Builder((this))
                .setMessage(message)
                .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(dialog==1)
                        {
                            rnuTimePermissionRequest.mRequestPermissions(new String[]{permission},8080);
                        }
                        else if(dialog==2)
                        {
                            if (SplashActivity.this == null) {
                                return;
                            }
                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + SplashActivity.this.getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                            finish();
                        }

                    }
                })
                .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create()
                .show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

}
