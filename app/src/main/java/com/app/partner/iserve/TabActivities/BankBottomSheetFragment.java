package com.app.partner.iserve.TabActivities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.Adapters.BankDetailsRVA;
import com.app.partner.iserve.Pojo.BankDetailsData;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import okio.Buffer;

public class BankBottomSheetFragment extends BottomSheetDialogFragment {

    private static final String PARAM1 = "param1";
    private static final String PARAM2 = "param2";
    BankDetailsData bankDetailsData;
    BankDetailsRVA.RefreshBankDetails refreshBankDetails;
    private Typeface fontRegular ;
    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;

    private static final String TAG = "BankBottomSheetFragment";
    LayoutInflater inflater;

    public BankBottomSheetFragment()
    {

    }

    public static BankBottomSheetFragment newInstance(BankDetailsData bankDetailsData, BankDetailsRVA.RefreshBankDetails refreshBankDetails) {
        BankBottomSheetFragment fragment = new BankBottomSheetFragment();
        Bundle args = new Bundle();
        args.putSerializable(PARAM1, bankDetailsData);
        args.putSerializable(PARAM2, refreshBankDetails);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bankDetailsData = (BankDetailsData) getArguments().getSerializable(PARAM1);
            refreshBankDetails = (BankDetailsRVA.RefreshBankDetails) getArguments().getSerializable(PARAM2);
        }

        fontRegular = Utility.getFontRegular(getActivity());
        sessionManager = new SessionManager(getActivity());
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
           /*  Log.d(TAG, "onSlide: "+slideOffset);

           if(rl!= null)
            {
                if(slideOffset > 0.9)
                {
                    rl.setVisibility(View.VISIBLE);
                }
                else
                {
                    rl.setVisibility(View.GONE);
                }
            }*/

        }
    };

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);


        inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.alert_dialog_bank_details,null);
        dialog.setContentView(view);

        TextView tvAccoutDetails= (TextView) view.findViewById(R.id.tvAccoutDetails);
        TextView tvAccountHolderLabel= (TextView) view.findViewById(R.id.tvAccountHolderLabel);
        TextView tvAccountHolder= (TextView) view.findViewById(R.id.tvAccountHolder);
        TextView tvAccountNoLabel= (TextView) view.findViewById(R.id.tvAccountNoLabel);
        TextView tvAccountNo= (TextView) view.findViewById(R.id.tvAccountNo);
        TextView tvRoutingNoLabel= (TextView) view.findViewById(R.id.tvRoutingNoLabel);
        TextView tvRoutinNo= (TextView) view.findViewById(R.id.tvRoutinNo);
        TextView tvBankNameLabel= (TextView) view.findViewById(R.id.tvBankNameLabel);
        TextView tvBankName= (TextView) view.findViewById(R.id.tvBankName);
        TextView tvCurrencyLabel= (TextView) view.findViewById(R.id.tvCurrencyLabel);
        TextView tvCurrency= (TextView) view.findViewById(R.id.tvCurrency);
        TextView tvCountryLabel= (TextView) view.findViewById(R.id.tvCountryLabel);
        TextView tvCountry= (TextView) view.findViewById(R.id.tvCountry);
        TextView tvMakeDefault= (TextView) view.findViewById(R.id.tvMakeDefault);
        TextView tvDeleteAccount= (TextView) view.findViewById(R.id.tvDeleteAccount);
        ImageView ivCancel= (ImageView) view.findViewById(R.id.ivCancel);


        tvAccountHolderLabel.setTypeface(fontRegular);
        tvAccountNoLabel.setTypeface(fontRegular);
        tvRoutingNoLabel.setTypeface(fontRegular);
        tvBankNameLabel.setTypeface(fontRegular);
        tvCurrencyLabel.setTypeface(fontRegular);
        tvCountryLabel.setTypeface(fontRegular);

        tvMakeDefault.setTypeface(fontRegular);
        tvDeleteAccount.setTypeface(fontRegular);
        tvAccountHolder.setTypeface(fontRegular);
        tvAccountNo.setTypeface(fontRegular);
        tvRoutinNo.setTypeface(fontRegular);
        tvBankName.setTypeface(fontRegular);
        tvCurrency.setTypeface(fontRegular);
        tvCountry.setTypeface(fontRegular);
        tvAccoutDetails.setTypeface(fontRegular);

        if(bankDetailsData!=null)
        {
            tvAccountHolder.setText(bankDetailsData.getAccount_holder_name());
            tvAccountNo.setText("xxxxxxxx"+bankDetailsData.getLast4());
            tvRoutinNo.setText(bankDetailsData.getRouting_number());
            tvBankName.setText(bankDetailsData.getBank_name());
            tvCurrency.setText(bankDetailsData.getCurrency());
            tvCountry.setText(bankDetailsData.getCountry());

        }
        else
        {
            return;
        }

        tvMakeDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultAndDeleteAccount(ServiceUrls.BANK_ACCOUNT_DEFAULT);
            }
        });

        tvDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultAndDeleteAccount(ServiceUrls.DELETE_MASTER_BANK);
            }
        });

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

    }

    private void defaultAndDeleteAccount(String url)
    {
        if(Utility.isNetworkAvailable(getActivity()))
        {
            mProgressDailog=Utility.getProcessDialog(getActivity());
            mProgressDailog.setMessage("loading...");
            mProgressDailog.setCancelable(false);
            mProgressDailog.show();

            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("bank_token",bankDetailsData.getBank_id())
                    .build();


            Buffer sink = new Buffer();
            try {

                requestBody.writeTo(sink);
                byte[] bArr=sink.readByteArray();
                String req = new String(bArr, "UTF-8");
                Log.d(TAG, " requestBody: " + req);

            } catch (Exception e) {
                e.printStackTrace();
            }

            Okhttp_connection.doOkhttpRequest(url, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    dismiss();
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                    Log.d(TAG, "onSuccess: "+result);

                    if(result !=null )
                    {
                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String errFlag = jsonObject.getString("errFlag");
                            String errNum = jsonObject.getString("errNum");
                            String errMsg = jsonObject.getString("errMsg");

                            if(errFlag.equals("0"))
                            {
                                Toast.makeText(getActivity(),errMsg,Toast.LENGTH_SHORT).show();
                                refreshBankDetails.onRefresh();
                            }
                            else
                            {
                                Toast.makeText(getActivity(),errMsg,Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "respondBankDetails: "+e);
                        }
                    }
                }

                @Override
                public void onError(String error) {
                    dismiss();
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Toast.makeText(getActivity(),R.string.network_check,Toast.LENGTH_SHORT).show();
        }

    }
}