package com.app.partner.iserve.TabActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.app.partner.iserve.Adapters.BankDetailsRVA;
import com.app.partner.iserve.Pojo.BankDetailsData;
import com.app.partner.iserve.Pojo.BankDetailsPojo;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;

public class BankDetailActivity extends AppCompatActivity implements BankDetailsRVA.RefreshBankDetails {

    private static final String TAG = "BankDetailActivity";
    SessionManager sessionManager;

    private TextView tvBankDetails ;
    private Typeface fontRegular,fontBold;

    private ProgressDialog mProgressDailog;

    private RecyclerView rvBankDetails;
    private BankDetailsRVA bankDetailsRVA;
    private ArrayList<BankDetailsData> bankDetailsDatas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_detail);

        sessionManager = new SessionManager(this);
        initView();
    }

    private void initView()
    {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon_off);
        }


        fontBold = Utility.getFontBold(this);
        fontRegular = Utility.getFontRegular(this);

        tvBankDetails = (TextView) findViewById(R.id.tvBankDetails);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BankDetailActivity.this,BankNewAccountActivity.class));
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
            }
        });

        bankDetailsDatas = new ArrayList();
        bankDetailsRVA = new BankDetailsRVA(this,bankDetailsDatas,getSupportFragmentManager(),this);

        rvBankDetails = (RecyclerView) findViewById(R.id.rvBankDetails);
        rvBankDetails.setLayoutManager(new LinearLayoutManager(this));
        rvBankDetails.setAdapter(bankDetailsRVA);

    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh: called" );
        getBankDetails();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
                finish();
                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }


    @Override
    protected void onStart() {
        super.onStart();

        getBankDetails();
    }

    private void getBankDetails()
    {
        if(Utility.isNetworkAvailable(this))
        {
            mProgressDailog=Utility.getProcessDialog(this);
            mProgressDailog.setMessage("loading...");
            mProgressDailog.setCancelable(false);
            mProgressDailog.show();

            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_MASTER_BANK_DATA, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {

                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                    Log.d(TAG, "onSuccess: "+result);
                    bankDetailsResponse(result);
                }

                @Override
                public void onError(String error) {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Toast.makeText(this,R.string.network_check,Toast.LENGTH_SHORT).show();
        }

    }

    private void bankDetailsResponse(String result)
    {
        if(result!=null)
        {
            try {

                Gson gson=new Gson();
                BankDetailsPojo bankDetailsPojo=gson.fromJson(result, BankDetailsPojo.class);

                if ("0".equals(bankDetailsPojo.getErrFlag()))
                {
                    bankDetailsDatas.clear();
                    bankDetailsDatas.addAll(bankDetailsPojo.getData());
                    bankDetailsRVA.notifyDataSetChanged();
                }
                else if ("1".equals(bankDetailsPojo.getErrFlag()))
                {
                    Toast.makeText(this,bankDetailsPojo.getErrMsg(),Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e)
            {
                Log.d(TAG, "bankDetailsResponse: "+e);
            }
        }
    }
}
