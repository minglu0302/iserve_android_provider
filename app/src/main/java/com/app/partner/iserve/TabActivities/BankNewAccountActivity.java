package com.app.partner.iserve.TabActivities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.InternalStorageContentProvider;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.RnuTimePermissionRequest;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Upload_file_AmazonS3;
import com.app.partner.iserve.Utility.Utility;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class BankNewAccountActivity extends AppCompatActivity {

    private static final String TAG = "BankNewAccountActivity";
    private TextView tvBankDetails,tvSave,tvIdProof;
    private Typeface fontRegular,fontBold;
    private EditText etAccountNo,etRoutingNo,etName,etLName,etPersonalId,etState,etPostalCode,etCity,etAddress;
    private static EditText etDob;
    private static String sentingCaimDate;
    private TextInputLayout tilAccountNo,tilRoutingNo,tilName,tilLastName,tilDob,tilPersonalId,tilState,tilPostalCode,tilCity,tilAddress;
    private LinearLayout llAddFile;
    private ImageView ivAddFile;

    private SessionManager sessionManager;
    private ProgressDialog mProgressDailog;

    private RnuTimePermissionRequest rnuTimePermissionRequest;
    private String[] permissions;

    private static final int PERMISSION_REQUEST_CODE=102;
    private final int REQUEST_CODE_GALLERY = 0x1;
    private final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    private final int REQUEST_CODE_CROP_IMAGE = 0x3;
    private String mAmazonImgUrl = "" ;
    private Bitmap bitmap;
    private boolean isPicturetaken;
    private File mFileTemp;

    private DialogFragment datePickerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    private void initView()
    {
        sessionManager = new SessionManager(this);
        mProgressDailog=Utility.getProcessDialog(this);
        datePickerFragment = new DatePickerFragment();

        rnuTimePermissionRequest =new RnuTimePermissionRequest(this);
        permissions= new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};

        String state = Environment.getExternalStorageState();

        String filename= sessionManager.getProviderEmail()+System.currentTimeMillis()+".png";

        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), filename);
        }
        else
        {
            mFileTemp = new File(getFilesDir(),filename);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_cancel);
        }

        fontRegular = Utility.getFontRegular(this);
        fontBold = Utility.getFontBold(this);

        tvBankDetails = (TextView) findViewById(R.id.tvBankDetails);
        tvSave = (TextView) findViewById(R.id.tvSave);
        tvIdProof = (TextView) findViewById(R.id.tvIdProof);
        etAccountNo = (EditText) findViewById(R.id.etAccountNo);
        etRoutingNo = (EditText)findViewById(R.id.etRoutingNo);
        etName = (EditText)findViewById(R.id.etName);
        etLName = (EditText)findViewById(R.id.etLName);
        etDob =(EditText) findViewById(R.id.etDob);
        etPersonalId = (EditText)findViewById(R.id.etPersonalId);
        etState = (EditText)findViewById(R.id.etState);
        etPostalCode = (EditText) findViewById(R.id.etPostalCode);
        etCity = (EditText)findViewById(R.id.etCity);
        etAddress = (EditText)findViewById(R.id.etAddress);
        tilAccountNo = (TextInputLayout) findViewById(R.id.tilAccountNo);
        tilRoutingNo = (TextInputLayout)findViewById(R.id.tilRoutingNo);
        tilName = (TextInputLayout)findViewById(R.id.tilName);
        tilLastName = (TextInputLayout)findViewById(R.id.tilLastName);
        tilDob =(TextInputLayout) findViewById(R.id.tilDob);
        tilPersonalId = (TextInputLayout)findViewById(R.id.tilPersonalId);
        tilState = (TextInputLayout)findViewById(R.id.tilState);
        tilPostalCode = (TextInputLayout) findViewById(R.id.tilPostalCode);
        tilCity = (TextInputLayout)findViewById(R.id.tilCity);
        tilAddress = (TextInputLayout)findViewById(R.id.tilAddress);
        llAddFile = (LinearLayout) findViewById(R.id.llAddFile);
        ivAddFile = (ImageView) findViewById(R.id.ivAddFile);

        tvBankDetails.setTypeface(fontBold);
        tvSave.setTypeface(fontRegular);
        tvIdProof.setTypeface(fontRegular);
        etAccountNo.setTypeface(fontRegular);
        etRoutingNo.setTypeface(fontRegular);
        etName.setTypeface(fontRegular);
        etLName.setTypeface(fontRegular);
        etDob.setTypeface(fontRegular);
        etPersonalId.setTypeface(fontRegular);
        etState.setTypeface(fontRegular);
        etPostalCode.setTypeface(fontRegular);
        etCity.setTypeface(fontRegular);
        etAddress.setTypeface(fontRegular);


        /*etDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {
                    datePickerFragment.show(getFragmentManager(), "dataPicker");
                }
            }
        });

        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!datePickerFragment.isResumed())
                {
                    datePickerFragment.show(getFragmentManager(), "dataPicker");
                }
            }
        });*/

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadAmazon();
            }
        });

        llAddFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!rnuTimePermissionRequest.mCheckPermission(permissions))
                {
                    rnuTimePermissionRequest.mRequestPermissions(permissions,PERMISSION_REQUEST_CODE);
                }
                else
                {
                    mProfileOnclick();
                }

            }
        });


        etDob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (before - count == 1) {
                    etDob.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 2 || s.length() == 5)
                {
                    etDob.setText(s+"/");
                    etDob.setSelection(etDob.getText().length());
                }

            }
        });


        etPersonalId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (before - count == 1) {
                    etPersonalId.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 3 || s.length() == 6)
                {
                    etPersonalId.setText(s+"-");
                    etPersonalId.setSelection(etPersonalId.getText().length());
                }
            }
        });



    }
    public String removeLastLetter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length()-1)=='x') {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return datePickerDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            sentingCaimDate = Utility.sentingDateFormat(year, monthOfYear, dayOfMonth);
            etDob.setText(Utility.displayDateFormat(year, monthOfYear, dayOfMonth));
        }

    }


    private void uploadAmazon()
    {
        if(isValid())
        {
            if(Utility.isNetworkAvailable(this))
            {

                mProgressDailog.show();

                Upload_file_AmazonS3 amazonS3 = Upload_file_AmazonS3.getInstance(this,AppConstants.COGNITO_POOL_ID);

                mAmazonImgUrl=AppConstants.AMAZON_BASE_URL+AppConstants.BUCKET+"/"+AppConstants.BUCKET_ID_PROOF_PICTURE+mFileTemp.getName();
                Log.d(TAG, "mUploadImageToBucket: "+mAmazonImgUrl);

                amazonS3.UploadFile(AppConstants.BUCKET,AppConstants.BUCKET_ID_PROOF_PICTURE +mFileTemp.getName(), mFileTemp, new Upload_file_AmazonS3.Upload_CallBack() {
                    @Override
                    public void sucess(String url)
                    {
                        addBankDetails();
                    }

                    @Override
                    public void error(String errormsg) {
                        Log.d(TAG, "error: "+errormsg);

                        if(mProgressDailog!=null && mProgressDailog.isShowing()){
                            mProgressDailog.dismiss();
                        }

                    }
                });
            }
            else
            {
                Toast.makeText(this,getString(R.string.network_check),Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void addBankDetails()
    {
        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("routing_number",etRoutingNo.getText().toString())
                .add("account_number",etAccountNo.getText().toString())
                .add("account_holder_name",etName.getText().toString() )
                .add("ent_last_name",etLName.getText().toString() )
                .add("dob",etDob.getText().toString())
                .add("personal_id",etPersonalId.getText().toString())
                .add("state",etState.getText().toString())
                .add("postal_code",etPostalCode.getText().toString())
                .add("city",etCity.getText().toString())
                .add("address",etAddress.getText().toString())
                .add("IdProof",mAmazonImgUrl)
                .build();

        Log.d(TAG, "addBankDetails: "+etDob.getText().toString());
        Utility.printReq(requestBody,"AddBank Request");
        Okhttp_connection.doOkhttpRequest(ServiceUrls.ADD_BANK_DETAILS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(mProgressDailog!=null && mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
                Log.d(TAG, "onSuccess: "+result);
                respondBankDetails(result);
            }


            @Override
            public void onError(String error)
            {
                if(mProgressDailog!=null&&mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
            }
        });
    }


    private void respondBankDetails(String result)
    {
        if(result != null)
        {
            try {
                JSONObject jsonObject = new JSONObject(result);

                String errFlag = jsonObject.getString("errFlag");
                String errNum = jsonObject.getString("errNum");
                String errMsg = jsonObject.getString("errMsg");

                if(errFlag.equals("0"))
                {
                    Toast.makeText(this,errMsg,Toast.LENGTH_SHORT).show();
                    finish();
                    overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                }
                else
                {
                    Toast.makeText(this,errMsg,Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.d(TAG, "respondBankDetails: "+e);
            }
        }

    }

    private boolean isValid()
    {

        if(etName.getText().toString().equals(""))
        {
            tilName.setErrorEnabled(true);
            tilName.setError(getString(R.string.enterAccountHoldername));
            return false;
        }
        if(etLName.getText().toString().equals(""))
        {
            tilName.setErrorEnabled(false);

            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getString(R.string.enterLastName));
            return false;
        }
        if(etAccountNo.getText().toString().equals(""))
        {
            tilName.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);

            tilAccountNo.setErrorEnabled(true);
            tilAccountNo.setError(getString(R.string.enterAccountNo));
            return false;
        }
        else if(etRoutingNo.getText().toString().length() != 9 )
        {
            tilName.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilAccountNo.setErrorEnabled(false);

            tilRoutingNo.setErrorEnabled(true);
            tilRoutingNo.setError(getString(R.string.enterRoutinNo));
            return false;
        }
        else if(etDob.getText().toString().equals(""))
        {
            tilName.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilAccountNo.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);

            tilDob.setErrorEnabled(true);
            tilDob.setError(getString(R.string.enterDob));
            return false;
        }
        else if(etDob.getText().toString().length() != 10)
        {
            tilName.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilAccountNo.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);

            tilDob.setErrorEnabled(true);
            tilDob.setError(getString(R.string.enterValidDob));
            return false;
        }
        else if(etPersonalId.getText().toString().equals(""))
        {
            tilAccountNo.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);

            tilPersonalId.setErrorEnabled(true);
            tilPersonalId.setError(getString(R.string.enterPersonalid));
            return false;
        }
         else if(etPersonalId.getText().toString().length() != 11)
        {
            tilAccountNo.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);

            tilPersonalId.setErrorEnabled(true);
            tilPersonalId.setError(getString(R.string.enterValidPersonalid));
            return false;
        }
        else if(etAddress.getText().toString().equals(""))
        {
            tilAccountNo.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);
            tilPersonalId.setErrorEnabled(false);

            tilAddress.setErrorEnabled(true);
            tilAddress.setError(getString(R.string.enterAddress));
            return false;
        }
        else if(etCity.getText().toString().equals(""))
        {
            tilAccountNo.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);
            tilPersonalId.setErrorEnabled(false);
            tilAddress.setErrorEnabled(false);

            tilCity.setErrorEnabled(true);
            tilCity.setError(getString(R.string.enterCity));
            return false;
        }
        else if(etState.getText().toString().equals(""))
        {
            tilAccountNo.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);
            tilPersonalId.setErrorEnabled(false);
            tilCity.setErrorEnabled(false);
            tilAddress.setErrorEnabled(false);

            tilState.setErrorEnabled(true);
            tilState.setError(getString(R.string.enterState));
            return false;
        }
        else if(etPostalCode.getText().toString().equals(""))
        {
            tilAccountNo.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);
            tilPersonalId.setErrorEnabled(false);
            tilState.setErrorEnabled(false);
            tilCity.setErrorEnabled(false);
            tilAddress.setErrorEnabled(false);

            tilPostalCode.setErrorEnabled(true);
            tilPostalCode.setError(getString(R.string.enterPostalCode));

            return false;
        }
        else if(!isPicturetaken)
        {
            tilAccountNo.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);
            tilPersonalId.setErrorEnabled(false);
            tilState.setErrorEnabled(false);
            tilPostalCode.setErrorEnabled(false);
            tilCity.setErrorEnabled(false);
            tilAddress.setErrorEnabled(false);

            Toast.makeText(this,getString(R.string.plsUploadIdProf),Toast.LENGTH_SHORT).show();
            return false;
        }
        else
        {
            tilAccountNo.setErrorEnabled(false);
            tilRoutingNo.setErrorEnabled(false);
            tilLastName.setErrorEnabled(false);
            tilName.setErrorEnabled(false);
            tilDob.setErrorEnabled(false);
            tilPersonalId.setErrorEnabled(false);
            tilState.setErrorEnabled(false);
            tilPostalCode.setErrorEnabled(false);
            tilCity.setErrorEnabled(false);
            tilAddress.setErrorEnabled(false);
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode){
            case PERMISSION_REQUEST_CODE :
                if(grantResults.length>0)
                {
                    for(int i=0;i<grantResults.length;i++){
                        Log.d("ORPresult grantResults"," "+grantResults[i]);
                        if(grantResults[i]!= PackageManager.PERMISSION_GRANTED)
                        {
                            mShoudHavePermission(permissions[i]);
                        }
                    }
                }
                break;
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShoudHavePermission(String permission)
    {
        Log.d("showrational","called "+permission);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if(shouldShowRequestPermissionRationale(permission))
            {
                showMessage(permission,1,"OK","CANCEL","You need to allow access for "+permission+" permission.");
            }
            else
            {
                showMessage(permission,2,"SETTINGS","EXIT APP","You need to allow access for "+permission+" permission.");
            }
        }
    }

    private void showMessage(final String permission,final int dialog,String positiveButton,String negativeButton,String message)
    {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(dialog==1)
                        {
                            rnuTimePermissionRequest.mRequestPermissions(new String[]{permission},8080);
                        }
                        else if(dialog==2)
                        {

                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + BankNewAccountActivity.this.getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                            BankNewAccountActivity.this.finish();
                        }

                    }
                })
                .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        BankNewAccountActivity.this.finish();
                    }
                })
                .create()
                .show();
    }


    private void mProfileOnclick()
    {

        AlertDialog.Builder alertDialogBuilder = new  AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.profile_pic_options,null);
        alertDialogBuilder.setView(view);

        final AlertDialog mDialog = alertDialogBuilder.create();
        //final Dialog mDialog = new Dialog(this);
        // mDialog.setTitle("Select photo from :");


        /*mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        mDialog.setContentView(R.layout.profile_pic_options);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));*/

        mDialog.setCancelable(false);
        Button btnCamera= (Button) view.findViewById(R.id.camera);
        Button btnCancel= (Button) view.findViewById(R.id.cancel);
        Button btnGallery= (Button) view.findViewById(R.id.gallery);
        Button btnRemove= (Button) view.findViewById(R.id.removephoto);
        TextView tvHeader= (TextView) view.findViewById(R.id.tvHeader);

        btnCamera.setTypeface(fontRegular);
        btnCancel.setTypeface(fontRegular);
        btnGallery.setTypeface(fontRegular);
        btnRemove.setTypeface(fontRegular);
        tvHeader.setTypeface(fontBold);

        if(isPicturetaken)
        {
            btnRemove.setVisibility(View.VISIBLE);
        }
        else
        {
            btnRemove.setVisibility(View.GONE);
        }

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                takePicture();
                mDialog.dismiss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                openGallery();
                mDialog.dismiss();
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ivAddFile.setImageResource(R.drawable.vector_add_file);
                isPicturetaken=false;
                mDialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void takePicture()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state))
            {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else
            {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            Log.d("error", "cannot take picture", e);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != this.RESULT_OK) {

                return;
            }

            switch (requestCode) {

                case REQUEST_CODE_GALLERY:


                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(
                                data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(
                                mFileTemp);
                        Log.d("", "inputStream" + inputStream);
                        Log.d("", "fileOutputStream" + fileOutputStream);
                        copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();


                        bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                        bitmap = Bitmap.createScaledBitmap(bitmap, ivAddFile.getWidth(), ivAddFile.getHeight(), true);
                        ivAddFile.setImageBitmap(bitmap);

                        isPicturetaken=true;
                        //startCropImage();

                    } catch (Exception e) {

                        Log.d("", "Error while creating temp file", e);
                    }

                    break;
                case REQUEST_CODE_TAKE_PICTURE:
                    isPicturetaken=true;
                    bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                    bitmap = Bitmap.createScaledBitmap(bitmap, ivAddFile.getWidth(), ivAddFile.getHeight(), true);
                    ivAddFile.setImageBitmap(bitmap);

                    //startCropImage();
                    break;

                case REQUEST_CODE_CROP_IMAGE:

                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    Log.d("", "path fileOutputStream " + path);

                    if (path == null) {

                        return;
                    }

                    bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                    bitmap = Bitmap.createScaledBitmap(bitmap, ivAddFile.getWidth(), ivAddFile.getHeight(), true);
                    ivAddFile.setImageBitmap(bitmap);
                    ivAddFile.setBackgroundDrawable(null);


                    Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

                    BitmapShader shader = new BitmapShader(bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                    Paint paint = new Paint();
                    paint.setShader(shader);
                    paint.setAntiAlias(true);
                    Canvas c = new Canvas(circleBitmap);
                    c.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth()/2, paint);


                    isPicturetaken=true;
                    break;
            }
        }
        catch (Exception e)
        {
            Log.d(TAG, "onActivityResult: "+e);
        }


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void startCropImage()
    {
        Utility.printLog("Strat crop");
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

}
