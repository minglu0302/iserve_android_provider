package com.app.partner.iserve.TabActivities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.Pojo.GetCategoryDetails;
import com.app.partner.iserve.Pojo.SampleResponse;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;

import okio.Buffer;

public class CategoryDetails extends AppCompatActivity {

    private TextView mToolbar_title,tvDone,tvCategoryTitle,tvCategory,tvPriceSetByTitle,tvPriceSetBy,tvPricePerMinTitle,tvVisitFeeTitle,tvTitleRadiusOfOperation;
    private TextView etVisitFee;
    private EditText etRadiusOfOperation, etPricePerMin;
    private ProgressDialog mProgressDailog;
    private String TAG="CategoryDetails";
    private SessionManager sessionManager;
    private ImageView ivBackbtn;
    private String catid;
    private Typeface font,fontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_details);
        sessionManager=new SessionManager(this);

        Intent intent=getIntent();
        catid=intent.getStringExtra("CAT_ID");

        initLayoutId();
        mGetCategoryDetail(catid);

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    public void initLayoutId()
    {
        font=Utility.getFontRegular(CategoryDetails.this);
        fontBold=Utility.getFontBold(CategoryDetails.this);

        ivBackbtn=(ImageView)findViewById(R.id.ivBackbtn);

        mToolbar_title=(TextView)findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvDone=(TextView)findViewById(R.id.tvDone);
        tvDone.setTypeface(font);

        tvCategoryTitle=(TextView)findViewById(R.id.tvCategoryTitle);
        tvCategoryTitle.setTypeface(font);

        tvCategory=(TextView)findViewById(R.id.tvCategory);
        tvCategory.setTypeface(font);

        tvPriceSetByTitle=(TextView)findViewById(R.id.tvPriceSetByTitle);
        tvPriceSetByTitle.setTypeface(font);

        tvPriceSetBy=(TextView)findViewById(R.id.tvPriceSetBy);
        tvPriceSetBy.setTypeface(font);

        tvPricePerMinTitle=(TextView)findViewById(R.id.tvPricePerMinTitle);
        tvPricePerMinTitle.setTypeface(font);

        etPricePerMin =(EditText)findViewById(R.id.etPricePerMin);
        etPricePerMin.setTypeface(font);

        tvVisitFeeTitle=(TextView)findViewById(R.id.tvVisitFeeTitle);
        tvVisitFeeTitle.setTypeface(font);

        etVisitFee=(TextView)findViewById(R.id.etVisitFee);
        etVisitFee.setTypeface(font);

        tvTitleRadiusOfOperation=(TextView)findViewById(R.id.tvTitleRadiusOfOperation);
        tvTitleRadiusOfOperation.setTypeface(font);

        etRadiusOfOperation=(EditText)findViewById(R.id.etRadiusOfOperation);
        etRadiusOfOperation.setTypeface(font);

        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });

        mToolbar_title.setText(getResources().getString(R.string.cat_details));
        tvDone.setText(getResources().getString(R.string.edit));
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tvDone.getText().toString().equalsIgnoreCase(getResources().getString(R.string.edit)))
                {
                    setFocusForEditTexts(true);
                    tvDone.setText(getResources().getString(R.string.Done));
                }
                else if(tvDone.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Done)))
                {
                    if(isValidValue())
                    {
                        mUpdateCategoryDetail();
                    }
                }
            }
        });

        tvDone.setVisibility(View.GONE);

        tvPricePerMinTitle.setText(getResources().getString(R.string.Price_Per_Hour)+" ("+getResources().getString(R.string.CURRENCY_SYMBOL)+" )");
        tvVisitFeeTitle.setText(getResources().getString(R.string.Visit_Fee)+" ("+getResources().getString(R.string.CURRENCY_SYMBOL)+" )");
        tvTitleRadiusOfOperation.setText(getResources().getString(R.string.Radius_of_operation)+" ("+getResources().getString(R.string.DISTANCE_TYPE)+" )");
        setFocusForEditTexts(false);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private boolean isValidValue()
    {
        if(etPricePerMin.getText().toString().equals(""))
        {
            showWaringMessage(getString(R.string.plsEnterPrice));
            return false;
        }

        double price= Double.parseDouble(etPricePerMin.getText().toString());
        return true;
    }

    public void mGetCategoryDetail(String catid)
    {
        try
        {
            mProgressDailog= Utility.getProcessDialog(CategoryDetails.this);
            mProgressDailog.show();
            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_cat_id",catid)
                    .add("ent_pro_id",sessionManager.getProviderID())
                    .build();

            Utility.printLog(TAG+" mGetCategoryDetail Request: "+requestBody.toString());

            Buffer sink = new Buffer();
            try {
                requestBody.writeTo(sink);
                byte[] bArr=sink.readByteArray();
                String req = new String(bArr, "UTF-8");
                Log.i("HomeFrag", " mGetCategoryDetail requestBody: " + req);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GetCategoryDetail, requestBody, new Okhttp_connection.OkHttpRequestCallback()
            {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null)
                    {
                        mProgressDailog.dismiss();
                    }

                    Utility.printLog(TAG +" mGetCategoryDetail:"+result);
                    Gson gson=new Gson();
                    GetCategoryDetails details=gson.fromJson(result,GetCategoryDetails.class);
                    mSetCategoryValues(details);

                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                    Toast.makeText(CategoryDetails.this, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e) {
            if(mProgressDailog!=null){
                mProgressDailog.dismiss();
            }
            e.printStackTrace();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mSetCategoryValues(GetCategoryDetails details)
    {
        tvCategory.setText(details.getData().getCat_name());
        tvPriceSetBy.setText(details.getData().getPrice_set_by());
        etPricePerMin.setText(details.getData().getPrice_min());
        etVisitFee.setText(details.getData().getVisit_fees());
        etRadiusOfOperation.setText(details.getData().getRadius());

        if(tvPriceSetBy.getText().toString().equalsIgnoreCase("provider"))
        {
            tvDone.setVisibility(View.VISIBLE);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mUpdateCategoryDetail()
    {
        try
        {
            mProgressDailog= Utility.getProcessDialog(CategoryDetails.this);
            mProgressDailog.show();
            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_cat_id",catid)
                    .add("ent_pro_id",sessionManager.getProviderID())
                    .add("ent_price_min", etPricePerMin.getText().toString())
                    .add("ent_radius",etRadiusOfOperation.getText().toString())
                    .build();

            Utility.printLog(TAG+"mUpdateCategoryDetail Request: "+requestBody.toString());

            Buffer sink = new Buffer();
            try {
                requestBody.writeTo(sink);
                byte[] bArr=sink.readByteArray();
                String req = new String(bArr, "UTF-8");
                Log.i("HomeFrag", " updateAptStatus requestBody: " + req);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Okhttp_connection.doOkhttpRequest(ServiceUrls.UpdateCategoryDetail, requestBody, new Okhttp_connection.OkHttpRequestCallback()
            {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null)
                    {
                        mProgressDailog.dismiss();
                    }

                    Utility.printLog(TAG +"mUpdateCategoryDetail:"+result);
                    Gson gson=new Gson();
                    SampleResponse response=gson.fromJson(result,SampleResponse.class);
                    if(response.getErrFlag().equals("0"))
                    {
                        tvDone.setText(getResources().getString(R.string.edit));
                        setFocusForEditTexts(false);
                    }

                    showWaringMessage(response.getErrMsg());
                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                    Toast.makeText(CategoryDetails.this, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e) {
            if(mProgressDailog!=null){
                mProgressDailog.dismiss();
            }
            e.printStackTrace();
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setFocusForEditTexts(boolean focus)
    {
        etPricePerMin.setFocusableInTouchMode(focus);
        etPricePerMin.setFocusable(focus);

        etRadiusOfOperation.setFocusableInTouchMode(focus);
        etRadiusOfOperation.setFocusable(focus);

    }

    public void showWaringMessage(String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alert));
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
