package com.app.partner.iserve.TabActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.app.partner.iserve.Adapters.ViewPagerAdapter;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Utility;
import com.app.partner.iserve.ViewPagerFrag.CurrentCycleFrag;
import com.app.partner.iserve.ViewPagerFrag.PastCycleFrag;

public class EarningsActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView mToolbar_title,tvBankDetails;
    public ViewGroup viewGroup;
    private Typeface fontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earnings);

       initLayoutId();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void initLayoutId() {

        fontBold= Utility.getFontBold(EarningsActivity.this);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.VISIBLE);



        mToolbar_title= (TextView) findViewById(R.id.mToolbar_title);
        tvBankDetails= (TextView) findViewById(R.id.tvBankDetails);
        mToolbar_title.setText(getResources().getString(R.string.EARNINGS));
        mToolbar_title.setTypeface(fontBold);
        tvBankDetails.setTypeface(fontBold);

        tvBankDetails.setText(getResources().getString(R.string.bankDetailsline));
        tvBankDetails.setVisibility(View.VISIBLE);

        tvBankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EarningsActivity.this,BankDetailActivity.class));
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
            }
        });

        viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Adding fragments to ViewPager
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new CurrentCycleFrag(), "CURRENT CYCLE");
        adapter.addFrag(new PastCycleFrag(), "PAST CYCLE");
        viewPager.setAdapter(adapter);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume()
    {
        super.onResume();
        AppConstants.isTabService = true;
        InputMethodManager imm = (InputMethodManager)EarningsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }
}
