package com.app.partner.iserve.TabActivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.partner.iserve.Utility.AppConstants;
import com.example.calenderpk.compactcalendarview.CompactCalendarView;
import com.example.calenderpk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.app.partner.iserve.Adapters.RecyclerAdapter;
import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.Pojo.Appt;
import com.app.partner.iserve.Pojo.GetMasterAppointmentsHistory;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

public class HistoryActivity extends AppCompatActivity {

    private RecyclerView bookingsListView;
    private CompactCalendarView compactCalendarView;
    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;
    private String TAG="HistoryActivity";
    private GetMasterAppointmentsHistory mGetMasterAppointmentsHistory;
    private ArrayList<Appointments> mDataset=new ArrayList<>();
    private RecyclerAdapter recyclerAdapter;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private Calendar calendar;
    SimpleDateFormat format,sdfDateMonth;
    private ArrayList<Appointments> mutableBookings;
    List<Event> bookingsFromMap;
    private ImageView ivBackbtn,ivnextBtn,ivPrevBtn;
    private TextView mToolbar_title,mMonthTitle;
    private ViewGroup viewGroup;
    private CompactCalendarView.CompactCalendarViewListener myCompactCalendarViewListener;
    private Typeface font,fontBold;
    private GestureDetector gestureDetector;
    private boolean isCalenderHidden=false;

    private String date = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        sessionManager=new SessionManager(this);
        calendar=Calendar.getInstance();
        format = new SimpleDateFormat("yyyy-MM-dd");
        sdfDateMonth = new SimpleDateFormat("yyyy-MM");

        mProgressDailog = Utility.getProcessDialog(this);


        date = sdfDateMonth.format(Calendar.getInstance().getTime());
        initLayoutId();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initLayoutId()
    {
        fontBold= Utility.getFontBold(HistoryActivity.this);
        font=Utility.getFontRegular(HistoryActivity.this);

        bookingsListView = (RecyclerView)findViewById(R.id.bookings_listview);
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        ivBackbtn=(ImageView)findViewById(R.id.ivBackbtn);
        ivPrevBtn=(ImageView)findViewById(R.id.ivPrevBtn);
        ivnextBtn=(ImageView)findViewById(R.id.ivnextBtn);
        ivBackbtn.setVisibility(View.GONE);

        mToolbar_title=(TextView)findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);
        mToolbar_title.setText(getResources().getString(R.string.HISTORY));

        mMonthTitle=(TextView)findViewById(R.id.mMonthTitle);
        mMonthTitle.setTypeface(font);

        mutableBookings = new ArrayList<>();

        recyclerAdapter=new RecyclerAdapter(mutableBookings,HistoryActivity.this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        bookingsListView.setLayoutManager(llm);
        bookingsListView.setAdapter( recyclerAdapter );

        mMonthTitle.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        myCompactCalendarViewListener=new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked)
            {
               // Toast.makeText(HistoryActivity.this, ""+dateClicked, Toast.LENGTH_SHORT).show();
                mMonthTitle.setText(dateFormatForMonth.format(dateClicked));
                List<Event> eventList = compactCalendarView.getEvents(dateClicked);
                mutableBookings.clear();
                for(Event booking : eventList){
                    mutableBookings.add((Appointments) booking.getData());
                }
                recyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth)
            {
                mMonthTitle.setText(dateFormatForMonth.format(firstDayOfNewMonth));

                date=sdfDateMonth.format(firstDayOfNewMonth);
                Utility.printLog(TAG+" Getting slots for "+date);
                mGetMasterAppointments();
            }
        };

        compactCalendarView.setListener(myCompactCalendarViewListener);

        ivPrevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compactCalendarView.showPreviousMonth();
            }
        });
        ivnextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compactCalendarView.showNextMonth();
            }
        });

        viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);


        GestureDetector.SimpleOnGestureListener simpleOnGestureListener
                = new GestureDetector.SimpleOnGestureListener(){


            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                   float velocityY) {
                try
                {
                    String swipe = "";
                    float sensitvity = 50;

                    // TODO Auto-generated method stub
                    if((e1.getX() - e2.getX()) > sensitvity){
                        swipe += "Swipe Left\n";
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();

                    }else if((e2.getX() - e1.getX()) > sensitvity){
                        swipe += "Swipe Right\n";
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }else{
                        swipe += "\n";
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }

                    if((e1.getY() - e2.getY()) > sensitvity){
                        swipe += "Swipe Up\n";
                        if(!isCalenderHidden)
                        {
                            compactCalendarView.hideCalendarWithAnimation();
                            isCalenderHidden=true;
                        }


                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }else if((e2.getY() - e1.getY()) > sensitvity){
                        swipe += "Swipe Down\n";
                        if(isCalenderHidden)
                        {
                            compactCalendarView.showCalendarWithAnimation();
                            isCalenderHidden=false;
                        }
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }else{
                        swipe += "\n";
                    }

                    //gestureEvent.setText(swipe);


                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return super.onFling(e1, e2, velocityX, velocityY);
            }
        };

        gestureDetector = new GestureDetector(simpleOnGestureListener);

        bookingsListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    protected void onResume() {
        super.onResume();

        AppConstants.isTabService = true;
        InputMethodManager imm = (InputMethodManager)HistoryActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);

        mGetMasterAppointments();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mGetMasterAppointments()
    {

        if(Utility.isNetworkAvailable(HistoryActivity.this))
        {
            String mUrl= ServiceUrls.GET_MASTER_APPOINTMENTS_HISTORY;
            if(mProgressDailog!=null){
                mProgressDailog.show();
            }
            String currentDateTime= Utility.getCurrentGmtTime();

            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",Utility.getDeviceId(this))
                    .add("ent_appnt_dt",date)
                    .add("ent_date_time",currentDateTime)
                    .build();

            Okhttp_connection.doOkhttpRequest(mUrl, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog(TAG+" getMasterAppointmentsHistory response "+result);
                    Gson gson=new Gson();
                    mGetMasterAppointmentsHistory=gson.fromJson(result,GetMasterAppointmentsHistory.class);

                    mGetMasterApptsHandler(mGetMasterAppointmentsHistory);
                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Snackbar.make(viewGroup, getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            HistoryActivity.this.mGetMasterAppointments();
                        }
                    }).show();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mGetMasterApptsHandler(GetMasterAppointmentsHistory mGetMasterAppointmentsPojo)
    {
        if(mGetMasterAppointmentsPojo!=null){
            if("0".equals(mGetMasterAppointmentsPojo.getErrFlag()) && "31".equals(mGetMasterAppointmentsPojo.getErrNum()))
            {
                Log.d("getMasterAppointments","AppointmentsSize "+mGetMasterAppointmentsPojo.getBookings().size());

               setAppointments(mGetMasterAppointmentsPojo.getBookings().size(),mGetMasterAppointmentsPojo);
                //mDataset=mGetMasterAppointmentsPojo.getAppointments();

                //setCalenderView(mDataset);

            }
            else if("1".equals(mGetMasterAppointmentsPojo.getErrFlag()) && "30".equals(mGetMasterAppointmentsPojo.getErrNum()))
            {
                Log.d("getMasterAppointments","NoAppointments");
                /*mDataset.clear();
                recyclerAdapter=new RecyclerAdapter(mDataset,HistoryActivity.this);
                recyclerAdapter.notifyDataSetChanged();*/

            }
            else if("1".equals(mGetMasterAppointmentsPojo.getErrFlag()))
            {
                switch (mGetMasterAppointmentsPojo.getErrNum())
                {
                    case "83":
                        //logged out of this mobile as you recently logged in from another mobile
                       // mShowErrorMessage(mGetMasterAppointmentsPojo.getErrMsg(),true);
                        break;

                    case "7":
                        //Invalid token
                        //mShowErrorMessage(mGetMasterAppointmentsPojo.getErrMsg(),true);
                        break;
                    default:
                }
            }

        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setCalenderView(ArrayList<Appointments> mDataset)
    {
        compactCalendarView.removeAllEvents();

        if(mDataset.size()>0)
        {
            bookingsFromMap=new ArrayList<>();
            for(int i=0;i<mDataset.size();i++)
            {
                Date date;
                long timeInMills = 0;

                try
                {
                    date=format.parse(mDataset.get(i).getApntDt());
                    timeInMills=date.getTime();
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }

                bookingsFromMap.add(new Event(Color.argb(255, 169, 68, 65), timeInMills,mDataset.get(i)));

                Utility.printLog("PK EVENT "+timeInMills);


            }

            compactCalendarView.addEvents(bookingsFromMap);
            myCompactCalendarViewListener.onDayClick(new Date());
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setAppointments(int size, GetMasterAppointmentsHistory mGetMasterAppointmentsPojo)
    {
        mDataset.clear();
        int count = 0;
        for(int i=0;i<size;i++)
        {
            int apptSize=mGetMasterAppointmentsPojo.getBookings().get(i).getAppt().size();

            for(int j=0;j<apptSize;j++)
            {
                count = 1;
                Appt appt=mGetMasterAppointmentsPojo.getBookings().get(i).getAppt().get(j);
                Appointments appointments=new Appointments("",appt.getPhone(),"","",appt.getBookType(),appt.getApptLong(),appt.getVisit_amount(),
                        "",appt.getCat_name(),"",appt.getDistance(),appt.getTimer_status(),appt.getApntDt(),appt.getTimer(),
                        appt.getJob_start_time(),appt.getApptLat(),appt.getPro_notes(),appt.getPrice_per_min(),appt.getBid(),appt.getCustomer_notes(),
                        appt.getFname(),appt.getpPic(),appt.getJob_timer(),appt.getStatus(),appt.getStatusMsg(),"","",appt.getAddrLine1(),
                        appt.getAddrLine2(),appt.getEmail(),appt.getCoupon_discount(),appt.getCancelAmt(),appt.getPayment_type(),appt.getAppt_duration(),
                        appt.getApntTime(),appt.getApntDate(),appt.getAmount(),appt.getTotal_pro(),appt.getMisc_fees(),appt.getPro_disc(),appt.getMat_fees(),
                        appt.getSign_url());

                appointments.setServices(appt.getServices());
                mDataset.add(appointments);
            }
        }

        if(count == 0)
        {
            Toast.makeText(this,getString(R.string.noJobHistory),Toast.LENGTH_SHORT).show();
        }
        setCalenderView(mDataset);
    }

}
