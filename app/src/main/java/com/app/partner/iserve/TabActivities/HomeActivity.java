package com.app.partner.iserve.TabActivities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.app.partner.iserve.ForeGroundService;
import com.app.partner.iserve.Utility.AppConstants;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.app.partner.iserve.Adapters.RecyclerAdapter;
import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.Pojo.GetMasterAppointmentsPojo;
import com.app.partner.iserve.Pojo.LaterBookings;
import com.app.partner.iserve.Pojo.Nowbooking;
import com.app.partner.iserve.R;
import com.app.partner.iserve.SplashActivity;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

public class HomeActivity extends AppCompatActivity {



    private RecyclerView listView;
    private ArrayList<Appointments> mDataset;
    private LinearLayoutManager mLayoutManager;
    private SessionManager sessionManager;
    private String TAG="HomeActivity";
    private ProgressDialog mProgressDailog;
    private GetMasterAppointmentsPojo mGetMasterAppointmentsPojo;
    private Button btnOntheJob;
    private TextView tripTodayHeader,tvTripsToday,tvEarningsTodayHeader,tvEarningsToday,tvWalletBalHeader,tvWallet,tvNoAppts;
    private boolean fOnTheJob=false;
    private RecyclerAdapter recyclerAdapter;
    private ViewGroup viewGroup;
    private Typeface font;

    private BroadcastReceiver receiver;
    private IntentFilter filter;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sessionManager=new SessionManager(this);

        initLayoutId();

        mProgressDailog=Utility.getProcessDialog(this);
        Utility.printLog("OnCreate Called");
        mDataset=new ArrayList();

        filter = new IntentFilter();
        filter.addAction("com.phix.provider.refreshBooking");

        receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                if(intent.getStringExtra("action").equals("1"))
                {
                    Log.d("HomeFragment", "onReceive: called");
                    mGetMasterAppointments();
                }
            }
        };

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void initLayoutId()
    {
        font=Utility.getFontBold(HomeActivity.this);

        listView= (RecyclerView) findViewById(R.id.listView);
        listView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(mLayoutManager);

        tripTodayHeader= (TextView) findViewById(R.id.tripTodayHeader);
        tvTripsToday= (TextView) findViewById(R.id.tvTripsToday);
        tvEarningsTodayHeader= (TextView) findViewById(R.id.tvEarningsTodayHeader);
        tvEarningsToday= (TextView) findViewById(R.id.tvEarningsToday);
        tvWalletBalHeader= (TextView) findViewById(R.id.tvWalletBalHeader);
        tvWallet= (TextView) findViewById(R.id.tvWallet);
        tvNoAppts= (TextView) findViewById(R.id.tvNoAppts);

        btnOntheJob= (Button) findViewById(R.id.btnOnTheJob);
        btnOntheJob.setTypeface(font);

        btnOntheJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(fOnTheJob)
                {
                    mUpdateProviderStatus(4);

                }
                else
                {
                    mUpdateProviderStatus(3);

                }

            }
        });

         viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onResume()
    {
        super.onResume();
        AppConstants.isTabService = true;
        InputMethodManager imm = (InputMethodManager)HomeActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);

        Utility.printLog("OnResume Called");
        if(Utility.isNetworkAvailable(HomeActivity.this))
        {
            mGetMasterAppointments();
        }
        else
        {
            Snackbar.make(viewGroup, getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            HomeActivity.this.onResume();
                        }
                    }).show();
        }

        /*if(Utility.isMyServiceRunning(HomeActivity.this,MyService.class))
        {
            MyService.setMessageCallback(new MyService.MessageCallback() {
                @Override
                public void onSuccess(String result) {
                    Utility.printLog(TAG+" PKKKKKKK "+result);
                }
            });
        }*/

        if(receiver != null)
        {
            registerReceiver(receiver,filter);
        }

        if(AppConstants.isFromLogin)
        {
            AppConstants.isFromLogin = false;
            mUpdateProviderStatus(3);

        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onPause()
    {
        super.onPause();

        if(receiver!=null)
        {
            unregisterReceiver(receiver);
        }

        Utility.printLog("OnPause Called");
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Utility.printLog("OnDestroy Called");
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onStart()
    {
        super.onStart();
        Utility.printLog("OnStart Called");
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onStop()
    {
        super.onStop();
        //stopService(new Intent(HomeActivity.this, MyService.class));
        Utility.printLog("OnStop Called");
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mGetMasterAppointments()
    {
        String mUrl= ServiceUrls.GET_MASTER_APPOINTMENTS_HOME;
        if(mProgressDailog!=null){
            mProgressDailog.show();
        }
        String currentDateTime= Utility.getCurrentGmtTime();
        String dateAndTime[]=currentDateTime.split(" ");
        RequestBody requestBody= new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",Utility.getDeviceId(this))
                .add("ent_appnt_dt",dateAndTime[0])
                .add("ent_date_time",currentDateTime)
                .add("ent_offset","")
                .build();

        Okhttp_connection.doOkhttpRequest(mUrl, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(mProgressDailog!=null){
                    mProgressDailog.dismiss();
                }
                Utility.printLog(TAG+" getMasterAppointments response "+result);
                Gson gson=new Gson();
                mGetMasterAppointmentsPojo=gson.fromJson(result,GetMasterAppointmentsPojo.class);

                mGetMasterApptsHandler(mGetMasterAppointmentsPojo);
            }

            @Override
            public void onError(String error)
            {
                if(mProgressDailog!=null){
                    mProgressDailog.dismiss();
                }
            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mGetMasterApptsHandler(GetMasterAppointmentsPojo mGetMasterAppointmentsPojo)
    {
        if(mGetMasterAppointmentsPojo!=null){
            if("0".equals(mGetMasterAppointmentsPojo.getErrFlag()) && "31".equals(mGetMasterAppointmentsPojo.getErrNum()))
            {
                Log.d("getMasterAppointments","AppointmentsSize "+mGetMasterAppointmentsPojo.getNowbooking().size()+"  "+mGetMasterAppointmentsPojo.getLaterBookings().size());

                //mDataset=mGetMasterAppointmentsPojo.getAppointments();
                    mSetValuesForAdapter(mGetMasterAppointmentsPojo);


                    recyclerAdapter=new RecyclerAdapter(mDataset,HomeActivity.this);
                    listView.setAdapter(recyclerAdapter);
                    if(mGetMasterAppointmentsPojo.getNowbooking().size()>0 || mGetMasterAppointmentsPojo.getLaterBookings().size()>0)
                    {
                        tvNoAppts.setVisibility(View.GONE);
                    }
                    else
                    {
                        tvNoAppts.setVisibility(View.VISIBLE);
                    }

            }
            else if("1".equals(mGetMasterAppointmentsPojo.getErrFlag()) && "30".equals(mGetMasterAppointmentsPojo.getErrNum()))
            {
                Log.d("getMasterAppointments","NoAppointments");
               /* mDataset.clear();
                recyclerAdapter=new RecyclerAdapter(mDataset,HomeActivity.this);
                recyclerAdapter.notifyDataSetChanged();

                tvNoAppts.setVisibility(View.VISIBLE);*/
            }
            else if("1".equals(mGetMasterAppointmentsPojo.getErrFlag()))
            {
                switch (mGetMasterAppointmentsPojo.getErrNum())
                {
                    case "83":
                                //logged out of this mobile as you recently logged in from another mobile
                        mShowErrorMessage(mGetMasterAppointmentsPojo.getErrMsg(),true);
                        break;

                    case "7":
                                //Invalid token
                        mShowErrorMessage(mGetMasterAppointmentsPojo.getErrMsg(),true);
                        break;
                    default:
                }
            }
            if("3".equals(mGetMasterAppointmentsPojo.getStatus()))
            {
                fOnTheJob=true;
                btnOntheJob.setText(getResources().getString(R.string.GO_OFF_THE_JOB));
                btnOntheJob.setBackgroundResource(R.drawable.rectangle_for_job_status_green);
                if(!Utility.isMyServiceRunning(HomeActivity.this,ForeGroundService.class))
                {
                    //startService(new Intent(HomeActivity.this,MyService.class));
                    Intent startIntent = new Intent(HomeActivity.this, ForeGroundService.class);
                    startIntent.setAction(AppConstants.ACTION.STARTFOREGROUND_ACTION);
                    startService(startIntent);
                }
            }
            else if("4".equals(mGetMasterAppointmentsPojo.getStatus()))
            {
                fOnTheJob=false;
                btnOntheJob.setText(getResources().getString(R.string.GO_ON_THE_JOB));
                btnOntheJob.setBackgroundResource(R.drawable.rectangle_for_job_status_red);
                if(Utility.isMyServiceRunning(HomeActivity.this,ForeGroundService.class))
                {
                    //stopService(new Intent(HomeActivity.this,MyService.class));
                    Intent stopIntent = new Intent(HomeActivity.this, ForeGroundService.class);
                    stopIntent.setAction(AppConstants.ACTION.STOPFOREGROUND_ACTION);
                    startService(stopIntent);
                }

            }
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mUpdateProviderStatus(final int status)
    {
        String mUrl=ServiceUrls.UPDATE_MASTER_STATUS;
        if(Utility.isNetworkAvailable(HomeActivity.this))
        {
            if(mProgressDailog!=null){
                mProgressDailog.show();
            }
            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",Utility.getDeviceId(this))
                    .add("ent_status",""+status)
                    .add("ent_date_time",Utility.getCurrentGmtTime())
                    .build();

            Okhttp_connection.doOkhttpRequest(mUrl, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    try
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        String errFlag=jsonObject.getString("errFlag");

                        if(errFlag.equals("0"))
                        {
                            if(fOnTheJob)
                            {
                                fOnTheJob=false;
                                btnOntheJob.setText(getResources().getString(R.string.GO_ON_THE_JOB));
                                btnOntheJob.setBackgroundResource(R.drawable.rectangle_for_job_status_red);
                                //stopService(new Intent(HomeActivity.this,MyService.class));

                                Intent stopIntent = new Intent(HomeActivity.this, ForeGroundService.class);
                                stopIntent.setAction(AppConstants.ACTION.STOPFOREGROUND_ACTION);
                                startService(stopIntent);

                            }else {
                                fOnTheJob=true;
                                btnOntheJob.setText(getResources().getString(R.string.GO_OFF_THE_JOB));
                                btnOntheJob.setBackgroundResource(R.drawable.rectangle_for_job_status_green);
                                //startService(new Intent(HomeActivity.this,MyService.class));

                                Intent startIntent = new Intent(HomeActivity.this, ForeGroundService.class);
                                startIntent.setAction(AppConstants.ACTION.STARTFOREGROUND_ACTION);
                                startService(startIntent);


                            }
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    Utility.printLog(TAG+" updateMasterStatus Response "+result);
                }

                @Override
                public void onError(String error)
                {
                    Utility.printLog(TAG+" updateMasterStatus ErrorResponse "+error);
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mUpdateProviderStatus(status);
                        }
                    }).show();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg, final boolean logout)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(HomeActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                if(logout)
                {
                    sessionManager.clearSession();
                    Intent intent=new Intent(HomeActivity.this,SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                dialogInterface.dismiss();

            }
        });

        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mSetValuesForAdapter(GetMasterAppointmentsPojo pojo)
    {
        ArrayList<Appointments> appointmentsList=new ArrayList<>();
        mDataset.clear();

        if(pojo.getNowbooking().size()>0) {
            for (int i = 0; i < pojo.getNowbooking().size(); i++) {
                Nowbooking nowbooking = pojo.getNowbooking().get(i);

                Appointments appointment=new Appointments(
                        nowbooking.getExpireTime(),
                        nowbooking.getPhone(),
                        "",
                        "",
                        nowbooking.getBookType(),
                        nowbooking.getApptLong(),
                        nowbooking.getVisit_amount(),
                        nowbooking.getJob_imgs(),
                        nowbooking.getCat_name(),
                        nowbooking.getAmt_per_min(),
                        nowbooking.getDistance_met(),
                        nowbooking.getTimer_status(),
                        nowbooking.getApntDt(),
                        nowbooking.getTimer(),
                        nowbooking.getJob_start_time(),
                        nowbooking.getApptLat(),
                        nowbooking.getPro_notes(),
                        nowbooking.getPrice_per_min(),
                        nowbooking.getBid(),
                        nowbooking.getCustomer_notes(),
                        nowbooking.getFname(),
                        nowbooking.getpPic(),
                        nowbooking.getJob_timer(),
                        nowbooking.getStatus(),
                        nowbooking.getStatusMsg(),
                        nowbooking.getCid(),
                        nowbooking.getDiscount(),
                        nowbooking.getAddrLine1(),
                        nowbooking.getAddrLine2(),
                        nowbooking.getEmail(),
                        nowbooking.getCoupon_discount(),
                        nowbooking.getCancelAmt(),
                        nowbooking.getPayment_type(),
                        nowbooking.getAppt_duration(),
                        nowbooking.getApntTime(),
                        nowbooking.getApntDate(),
                        nowbooking.getServices()
                );


                if(i==0)
                {
                    appointment.setHeader(true);
                }
                appointmentsList.add(appointment);

            }
        }
        if(pojo.getLaterBookings().size()>0)
        {
            for(int i=0;i<pojo.getLaterBookings().size();i++)
            {
                LaterBookings laterBookings=pojo.getLaterBookings().get(i);

                Appointments appointment=new Appointments(
                        laterBookings.getExpireTime(),
                        laterBookings.getPhone(),
                        laterBookings.getEnd_slot(),
                        laterBookings.getStart_slot(),
                        laterBookings.getBookType(),
                        laterBookings.getApptLong(),
                        laterBookings.getVisit_amount(),
                        laterBookings.getJob_imgs(),
                        laterBookings.getCat_name(),
                        laterBookings.getAmt_per_min(),
                        laterBookings.getDistance(),
                        laterBookings.getTimer_status(),
                        laterBookings.getApntDt(),
                        laterBookings.getTimer(),
                        laterBookings.getJob_start_time(),
                        laterBookings.getApptLat(),
                        laterBookings.getPro_notes(),
                        laterBookings.getPrice_per_min(),
                        laterBookings.getBid(),
                        laterBookings.getCustomer_notes(),
                        laterBookings.getFname(),
                        laterBookings.getpPic(),
                        laterBookings.getJob_timer(),
                        laterBookings.getStatus(),
                        laterBookings.getStatusMsg(),
                        laterBookings.getCid(),
                        laterBookings.getDiscount(),
                        laterBookings.getAddrLine1(),
                        laterBookings.getAddrLine2(),
                        laterBookings.getEmail(),
                        laterBookings.getCoupon_discount(),
                        laterBookings.getCancelAmt(),
                        laterBookings.getPayment_type(),
                        laterBookings.getAppt_duration(),
                        laterBookings.getApntTime(),
                        laterBookings.getApntDate(),
                        laterBookings.getServices()
                );
                if(i==0)
                {
                    appointment.setHeader(true);
                }
                appointmentsList.add(appointment);

            }
        }
        mDataset.addAll(appointmentsList);
    }
}
