package com.app.partner.iserve.TabActivities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.partner.iserve.Messaging.MessagingActivityNew;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import com.app.partner.iserve.CancelationDialog;
import com.app.partner.iserve.MainActivity;
import com.app.partner.iserve.Pojo.AbortAppointmentResponse;
import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.Pojo.UpdateMasterStatusResponsePojo;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.LocationUtil;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

public class IhaveArrivedActivity extends AppCompatActivity implements LocationUtil.GetLocationListener, OnMapReadyCallback {

    private SessionManager sessionManager;
    private Appointments mAppointmentDetail;
    private String TAG = "IHaveArrivedActivity";
    private ImageView ivBackbtn, ivCall, ivMessage;
    private TextView mToolbar_title,
            tvCancel,
            tvTitleJobaccepted,
            tvTitleInvoice,
            tvTitleJobStarted,
            tvTitleOnTheWay,
            tvCustomerName,
            tvCustomerAddrs1,
            tvCustomerAddrs2,
            tvArrivedTitle,tvNavigateMaps,tvNavWaze;
    private RelativeLayout navWaze, navGMap;
    private SeekBar myseek;
    private ProgressDialog mProgressDailog;
    private LocationUtil locationUtil;
    private GoogleMap googleMap;
    private Typeface fontBold,font;
    private IntentFilter filter;
    private BroadcastReceiver receiver;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ihave_arrived);

        sessionManager = new SessionManager(this);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mAppointmentDetail = (Appointments) bundle.getSerializable("APPOINTMENT");
        Utility.printLog(TAG + " Appointments " + mAppointmentDetail.getFname());

        initLayoutId();
        setValues();

        filter = new IntentFilter("com.app.partner.firbase.cancel");
        receiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bucket=intent.getExtras();
                try
                {
                    String action = bucket.getString("action");
                    String payload = bucket.getString("payload");

                    if(action!=null && action.equals("3"))
                    {
                        mShowErrorMessage(payload,true);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e("PK","Crash "+e.getMessage());
                }

            }
        };

        registerReceiver(receiver,filter);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initLayoutId() {

        font=Utility.getFontRegular(IhaveArrivedActivity.this);
        fontBold=Utility.getFontBold(IhaveArrivedActivity.this);

        ivBackbtn = (ImageView) findViewById(R.id.ivBackbtn);
        ivCall = (ImageView) findViewById(R.id.ivCall);
        ivMessage = (ImageView) findViewById(R.id.ivMessage);

        mToolbar_title = (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvCancel.setTypeface(font);

        tvTitleInvoice = (TextView) findViewById(R.id.tvTitleInvoice);
        tvTitleInvoice.setTypeface(font);

        tvTitleJobaccepted = (TextView) findViewById(R.id.tvTitleJobaccepted);
        tvTitleJobaccepted.setTypeface(font);

        tvTitleJobStarted = (TextView) findViewById(R.id.tvTitleJobStarted);
        tvTitleJobStarted.setTypeface(font);

        tvTitleOnTheWay = (TextView) findViewById(R.id.tvTitleOnTheWay);
        tvTitleOnTheWay.setTypeface(font);

        tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
        tvCustomerName.setTypeface(fontBold);

        tvArrivedTitle = (TextView) findViewById(R.id.tvArrivedTitle);
        tvArrivedTitle.setTypeface(fontBold);

        tvCustomerAddrs1 = (TextView) findViewById(R.id.tvCustomerAddrs1);
        tvCustomerAddrs1.setTypeface(font);

        tvCustomerAddrs2 = (TextView) findViewById(R.id.tvCustomerAddrs2);
        tvCustomerAddrs2.setTypeface(font);

        tvNavigateMaps = (TextView) findViewById(R.id.tvNavigateMaps);
        tvNavigateMaps.setTypeface(fontBold);

        tvNavWaze = (TextView) findViewById(R.id.tvNavWaze);
        tvNavWaze.setTypeface(fontBold);

        navWaze = (RelativeLayout) findViewById(R.id.navWaze);
        navGMap = (RelativeLayout) findViewById(R.id.navGMap);

        myseek = (SeekBar) findViewById(R.id.myseek);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void setValues() {
        mProgressDailog = Utility.getProcessDialog(this);

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(this);


        if (mAppointmentDetail != null) {


            mToolbar_title.setText(getResources().getString(R.string.jobId) + " " + mAppointmentDetail.getBid());

            tvCustomerName.setText(mAppointmentDetail.getFname());

            tvCustomerAddrs1.setText(mAppointmentDetail.getAddrLine1());

            if (!"".equals(mAppointmentDetail.getAddrLine2())) {
                tvCustomerAddrs2.setVisibility(View.VISIBLE);
                tvCustomerAddrs2.setText(mAppointmentDetail.getAddrLine2());
            }

            ivCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String uri = "tel:" + mAppointmentDetail.getPhone();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                }
            });

            ivMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("sms:" + mAppointmentDetail.getPhone()));
                    startActivity(sendIntent);*/
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("APPOINTMENT",mAppointmentDetail);
                    Intent intent=new Intent(IhaveArrivedActivity.this, MessagingActivityNew.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            });
        }
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAbortAppointment();
            }
        });
        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        navWaze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String apntLat = mAppointmentDetail.getApptLat();
                    String apntLong = mAppointmentDetail.getApptLong();
                    ;

                    String url = "waze://?ll=" + apntLat + "," + apntLong + "&navigate=yes";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    Intent intent =
                            new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
                    startActivity(intent);
                }
            }
        });
        navGMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String apntLat = mAppointmentDetail.getApptLat();
                String apntLong = mAppointmentDetail.getApptLong();
                String muri = "google.navigation:q=" + apntLat + "," + apntLong;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(muri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });
        myseek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75) {
                    seekBar.setProgress(100);
                    mUpdateApptStatus();
                } else {
                    seekBar.setProgress(0);
                }
            }
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.googleMap.setMyLocationEnabled(true);


        //LatLng latLng=new LatLng(13.0286336,77.589478);
        double lat=Double.valueOf(mAppointmentDetail.getApptLat());
        double lng=Double.valueOf(mAppointmentDetail.getApptLong());
        LatLng latLng=new LatLng(lat,lng);
        this.googleMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.fev_address_pin_icon))
                .rotation(0)
                .flat(true));
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mUpdateApptStatus()
    {

        if(Utility.isNetworkAvailable(IhaveArrivedActivity.this))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }
            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_pat_email",mAppointmentDetail.getEmail())
                    .add("ent_appnt_dt",mAppointmentDetail.getApntDt())
                    .add("ent_response",""+ AppConstants.STATUS_I_HAVE_ARRIVED)
                    .add("ent_doc_remarks","")
                    .add("ent_timer","")
                    .add("ent_bid",mAppointmentDetail.getBid())
                    .add("ent_date_time",Utility.getCurrentGmtTime())
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.UPDATE_APPT_STATUS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK response"+result);
                    Gson gson=new Gson();
                    UpdateMasterStatusResponsePojo updateMasterStatusResponsePojo=gson.fromJson(result,UpdateMasterStatusResponsePojo.class);

                    if("0".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        if("58".equals(updateMasterStatusResponsePojo.getErrNum()))
                        {
                            MainActivity.acknowledgeCustomer(mAppointmentDetail.getBid(),mAppointmentDetail.getCid(),AppConstants.STATUS_I_HAVE_ARRIVED,sessionManager.getProviderID(),"");

                            mAppointmentDetail.setStatus(""+AppConstants.STATUS_I_HAVE_ARRIVED);

                            Intent intent=new Intent(IhaveArrivedActivity.this,JobCompletedActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("APPOINTMENT",mAppointmentDetail);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                    }
                    else if("1".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        mShowErrorMessage(updateMasterStatusResponsePojo.getErrMsg(),false);
                    }
                    else
                    {
                        finish();
                    }
                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK error"+error);
                }
            });
        }else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mUpdateApptStatus();
                        }
                    }).show();
        }


    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void updateLocation(Location location)
    {
        Utility.printLog(TAG+" LocationUpdates "+location.getLatitude()+" "+location.getLongitude());
        LatLng latLng=new LatLng(location.getLatitude(),location.getLongitude());
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));

    }

    @Override
    public void location_Error(String error)
    {

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void getCurrentLocation()
    {
        //checking the locationUtil.
        if (locationUtil == null)
        {
            locationUtil = new LocationUtil(IhaveArrivedActivity.this, this);
        }
        else
        {
            //checking location services.
            locationUtil.checkLocationSettings();

        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume()
    {
        super.onResume();
        getCurrentLocation();

    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg, final boolean finish)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(IhaveArrivedActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                if(finish)
                {
                    //sessionManager.clearSession();
                    finish();
                }
                dialogInterface.dismiss();

            }
        });

        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mAbortAppointment()
    {
        CancelationDialog dialog=new CancelationDialog(IhaveArrivedActivity.this, new CancelationDialog.CancelationCallback() {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog(TAG+"PK response"+result);
                if(result!=null && !result.isEmpty())
                {
                    Gson gson=new Gson();
                    AbortAppointmentResponse response=gson.fromJson(result,AbortAppointmentResponse.class);

                    if(response.getErrFlag().equals("0"))
                    {
                        mShowErrorMessage(response.getErrMsg(),true);
                       // finish();
                    }
                    else
                    {
                        mShowErrorMessage(response.getErrMsg(),false);
                    }
                }

            }

            @Override
            public void onError(String error) {
                Utility.printLog(TAG+"PK error response"+error);
            }
        });

        dialog.showCancellationDialog(mAppointmentDetail.getBid(),mAppointmentDetail.getCid());
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unregisterReceiver(receiver);
    }


}
