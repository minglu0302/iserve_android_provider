package com.app.partner.iserve.TabActivities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import com.app.partner.iserve.MainActivity;
import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.Pojo.UpdateMasterStatusResponsePojo;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Upload_file_AmazonS3;
import com.app.partner.iserve.Utility.Utility;

public class InvoiceActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    private Appointments mAppointmentDetail;
    private String TAG="InvoiceActivity";
    private TextView mToolbar_title,
            tvTitleInvoice,
            tvTitleJobaccepted,
            tvTitleJobStarted,
            tvTitleOnTheWay,
            tvFeeBreakDownHeader,
            tvTitleVisitFee,
            tvVisitFee,
            tvTitleTimeFee,
            tvTimeFee,
            tvAdditionalChargesHeader,
            tvTitleMaterialFee,
            tvTitleMisc,
            tvTitleProviderDiscount,
            tvTitleSubTotal,
            tvSubTotal,
            tvTitleDiscount,
            tvDiscount,
            tvTitleTotal,
            tvTotal,
            tvRetake;

    private EditText tvProviderDiscount,tvMaterialFee,tvMisc;
    private Button btnChargeCustomer;
    private LinearLayout llRetakeSignature;
    private SignaturePad mSignaturePad;
    private ProgressDialog mProgressDailog;
    private Timer my_timer;
    private TimerTask my_timer_task;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private File folder, photo;
    private String mAmazonImgUrl;
    private InputFilter filter;
    private int digits=2;
    private ImageView ivBackbtn;
    private SeekBar sb;
    private LinearLayout ll_service_Container;
    private EditText etProNotes;
    private double Servicetotal=0;
    private Typeface font,fontBold;

    double misc= 0,discount= 0,material =0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        verifyStoragePermissions(this);
        sessionManager=new SessionManager(this);
        sessionManager.setTimeWhilePaused(0);

        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        mAppointmentDetail= (Appointments) bundle.getSerializable("APPOINTMENT");

        InvoiceActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initLayoutId();
        setValues();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void initLayoutId()
    {
        font=Utility.getFontRegular(InvoiceActivity.this);
        fontBold=Utility.getFontBold(InvoiceActivity.this);

        mToolbar_title= (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvTitleInvoice= (TextView) findViewById(R.id.tvTitleInvoice);
        tvTitleInvoice.setTypeface(font);

        tvTitleJobaccepted= (TextView) findViewById(R.id.tvTitleJobaccepted);
        tvTitleJobaccepted.setTypeface(font);

        tvTitleJobStarted= (TextView) findViewById(R.id.tvTitleJobStarted);
        tvTitleJobStarted.setTypeface(font);

        tvTitleOnTheWay= (TextView) findViewById(R.id.tvTitleOnTheWay);
        tvTitleOnTheWay.setTypeface(font);

        tvFeeBreakDownHeader= (TextView) findViewById(R.id.tvFeeBreakDownHeader);
        tvFeeBreakDownHeader.setTypeface(font);

        tvTitleVisitFee= (TextView) findViewById(R.id.tvTitleVisitFee);
        tvTitleVisitFee.setTypeface(font);

        tvVisitFee= (TextView) findViewById(R.id.tvVisitFee);
        tvVisitFee.setTypeface(font);

        tvTitleTimeFee= (TextView) findViewById(R.id.tvTitleTimeFee);
        tvTitleTimeFee.setTypeface(font);

        tvTimeFee= (TextView) findViewById(R.id.tvTimeFee);
        tvTimeFee.setTypeface(font);

        tvAdditionalChargesHeader= (TextView) findViewById(R.id.tvAdditionalChargesHeader);
        tvAdditionalChargesHeader.setTypeface(font);

        tvTitleMaterialFee= (TextView) findViewById(R.id.tvTitleMaterialFee);
        tvTitleMaterialFee.setTypeface(font);

        tvMaterialFee= (EditText) findViewById(R.id.tvMaterialFee);
        tvMaterialFee.setTypeface(font);

        tvTitleMisc= (TextView) findViewById(R.id.tvTitleMisc);
        tvTitleMisc.setTypeface(font);

        tvMisc= (EditText) findViewById(R.id.tvMisc);
        tvMisc.setTypeface(font);

        tvTitleProviderDiscount= (TextView) findViewById(R.id.tvTitleProviderDiscount);
        tvTitleProviderDiscount.setTypeface(font);

        tvProviderDiscount= (EditText) findViewById(R.id.tvProviderDiscount);
        tvProviderDiscount.setTypeface(font);

        tvTitleSubTotal= (TextView) findViewById(R.id.tvTitleSubTotal);
        tvTitleSubTotal.setTypeface(font);

        tvSubTotal= (TextView) findViewById(R.id.tvSubTotal);
        tvSubTotal.setTypeface(font);

        tvTitleDiscount= (TextView) findViewById(R.id.tvTitleDiscount);
        tvTitleDiscount.setTypeface(font);

        tvDiscount= (TextView) findViewById(R.id.tvDiscount);
        tvDiscount.setTypeface(font);

        tvTitleTotal= (TextView) findViewById(R.id.tvTitleTotal);
        tvTitleTotal.setTypeface(font);

        tvTotal= (TextView) findViewById(R.id.tvTotal);
        tvTotal.setTypeface(font);

        tvRetake= (TextView) findViewById(R.id.tvRetake);
        tvRetake.setTypeface(font);

        etProNotes= (EditText) findViewById(R.id.etProNotes);
        etProNotes.setTypeface(font);

        ivBackbtn= (ImageView) findViewById(R.id.ivBackbtn);
        ll_service_Container= (LinearLayout) findViewById(R.id.ll_service_Container);

        sb= (SeekBar) findViewById(R.id.myseek);

        llRetakeSignature= (LinearLayout) findViewById(R.id.llRetakeSignature);

        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);


    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void setValues()
    {
        mProgressDailog=Utility.getProcessDialog(this);
        if(mAppointmentDetail!=null)
        {
            mToolbar_title.setText(getResources().getString(R.string.jobId)+" "+mAppointmentDetail.getBid());

            if(!mAppointmentDetail.getVisit_amount().isEmpty())
            {
                Float val=Float.parseFloat(mAppointmentDetail.getVisit_amount());
                tvVisitFee.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",val));
            }
            else
            {
                tvVisitFee.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",0.00));
            }
            /***************************************************************/
            if(!mAppointmentDetail.getCoupon_discount().isEmpty())
            {
                Float val=Float.parseFloat(mAppointmentDetail.getCoupon_discount());
                tvDiscount.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",val));
            }
            else
            {
                tvDiscount.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",0.00));
            }

            float ftimeFee=0,appt_duration=0;

            if(!mAppointmentDetail.getPrice_per_min().isEmpty())
            {
                if(!mAppointmentDetail.getAppt_duration().isEmpty())
                {
                    ftimeFee=(Float.parseFloat(mAppointmentDetail.getVisit_amount()))*(Float.parseFloat(mAppointmentDetail.getAppt_duration()));
                }
                if(sessionManager.getDuration()!=0)
                {
                    ftimeFee=(Float.parseFloat(mAppointmentDetail.getPrice_per_min()))*(sessionManager.getDuration()/60);
                }
                tvTitleTimeFee.setText("Time Fee( "+getDurationString(sessionManager.getDuration(),"H")+" Hr and "+getDurationString(sessionManager.getDuration(),"M")+" Mins)");

            }
            tvTimeFee.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",ftimeFee));

        }

        if(mAppointmentDetail.getServices()!=null)
        {
            mSetSelectedServices();
        }

        llRetakeSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
                photo=null;
            }
        });


        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75)
                {
                    seekBar.setProgress(100);
                    if(photo!=null)
                    {
                        if(isValidDiscount())
                        {
                            mUploadSignature();
                        }
                    }
                    else {
                        seekBar.setProgress(0);
                        Toast.makeText(InvoiceActivity.this, "Please take Signature from customer", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    seekBar.setProgress(0);
                }
            }
        });

        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onSigned() {
                // Toast.makeText(InvoiceActivity.this, "Signed", Toast.LENGTH_SHORT).show();

                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                if (addJpgSignatureToGallery(signatureBitmap))
                {
                    //Toast.makeText(InvoiceActivity.this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                } else
                {
                    //    Toast.makeText(InvoiceActivity.this, "Unable to store the signature", Toast.LENGTH_SHORT).show();
                }
               /* if (addSvgSignatureToGallery(mSignaturePad.getSignatureSvg())) {
                    Toast.makeText(InvoiceActivity.this, "SVG Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(InvoiceActivity.this, "Unable to store the SVG signature", Toast.LENGTH_SHORT).show();
                }*/


            }

            @Override
            public void onClear()
            {
                Toast.makeText(InvoiceActivity.this, "Cleared", Toast.LENGTH_SHORT).show();
            }
        });

        filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend)
            {
                int len = end - start;
                if (len == 0) {
                    return source;
                }

                int dlen = dest.length();

                // Find the position of the decimal .
                for (int i = 0; i < dstart; i++) {
                    if (dest.charAt(i) == '.') {

                        return (dlen-(i+1) + len > digits) ?
                                "" :
                                new SpannableStringBuilder(source, start, end);
                    }
                }

                for (int i = start; i < end; ++i) {
                    if (source.charAt(i) == '.') {

                        if ((dlen-dend) + (end-(i + 1)) > digits)
                            return "";
                        else
                            break;
                    }
                }

                return new SpannableStringBuilder(source, start, end);
            }
        };

        TextWatcher textWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0)
                {
                    String str = s.toString();

                    int indexOFdec =  str.indexOf(".");

                    if(indexOFdec >=0) {
                        if(str.substring(indexOFdec).length() >2)
                        {
                            if(tvMaterialFee.getText().hashCode()==s.hashCode())
                            {
                                tvMaterialFee.setFilters(new InputFilter[]{filter});
                            }
                            else if(tvMisc.getText().hashCode()==s.hashCode())
                            {
                                tvMisc.setFilters(new InputFilter[]{filter});
                            }
                            else if(tvProviderDiscount.getText().hashCode()==s.hashCode())
                            {
                                tvProviderDiscount.setFilters(new InputFilter[]{filter});
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        tvProviderDiscount.addTextChangedListener(textWatcher);
        tvMisc.addTextChangedListener(textWatcher);
        tvMaterialFee.addTextChangedListener(textWatcher);
        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        etProNotes.setMovementMethod(new ScrollingMovementMethod());
    }

    private boolean isValidDiscount()
    {
        if(discount > misc+material)
        {
            mShowErrorMessage(getString(R.string.discountErr),false);
            sb.setProgress(0);
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mStartTimerForCalculatingFares()
    {
        my_timer=new Timer();
        my_timer_task= new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        double total=0.0;
                        if(!tvVisitFee.getText().toString().trim().equals("") && !tvVisitFee.getText().toString().trim().endsWith("."))
                        {
                            String value = tvVisitFee.getText().toString().trim();
                            if (value.contains(","))
                            {
                                value = value.replace(",",".");
                            }
                            if (value.contains(AppConstants.CURRENCY))
                            {
                                value = value.replace(AppConstants.CURRENCY,"");
                            }
                            total = total + Double.parseDouble(value);
                        }
                        if(!tvTimeFee.getText().toString().trim().equals("") && !tvTimeFee.getText().toString().trim().endsWith("."))
                        {
                            String value = tvTimeFee.getText().toString().trim();
                            if (value.contains(AppConstants.CURRENCY))
                            {
                                value = value.replace(AppConstants.CURRENCY,"");
                            }
                            if (value.contains(","))
                            {
                                value = value.replace(",",".");
                            }
                            total = total + Double.parseDouble(value);
                        }

                        if(!tvMaterialFee.getText().toString().trim().equals("") && !tvMaterialFee.getText().toString().trim().endsWith("."))
                        {
                            String value = tvMaterialFee.getText().toString().trim();
                            if (value.contains(AppConstants.CURRENCY))
                            {
                                value = value.replace(AppConstants.CURRENCY,"");
                            }
                            if (value.contains(","))
                            {
                                value = value.replace(",",".");
                            }
                            material = Double.parseDouble(value);
                            total = total + Double.parseDouble(value);
                        }
                        else
                        {
                            material = 0;
                        }
                        if(!tvMisc.getText().toString().trim().equals("") && !tvMisc.getText().toString().trim().endsWith("."))
                        {
                            String value = tvMisc.getText().toString().trim();
                            if (value.contains(AppConstants.CURRENCY))
                            {
                                value = value.replace(AppConstants.CURRENCY,"");
                            }
                            if (value.contains(","))
                            {
                                value = value.replace(",",".");
                            }

                            misc = Double.parseDouble(value);
                            total = total + Double.parseDouble(value);
                        }
                        else
                        {
                            misc =0;
                        }
                        total=total+Servicetotal;

                        if(!tvProviderDiscount.getText().toString().trim().equals("") && !tvProviderDiscount.getText().toString().trim().endsWith("."))
                        {
                            String value = tvProviderDiscount.getText().toString().trim();

                            if (value.contains(","))
                            {
                                value = value.replace(",",".");
                            }

                            discount =Double.parseDouble(value);
                            total = total - Double.parseDouble(value);
                        }
                        else
                        {
                            discount = 0 ;
                        }

                        if(total>=0)
                        {
                            tvSubTotal.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",total));
                        }else
                        {
                            tvSubTotal.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",0.00));
                        }


                        if(!tvDiscount.getText().toString().trim().equals("") && !tvDiscount.getText().toString().trim().endsWith("."))
                        {
                            String value = tvDiscount.getText().toString().trim();
                            if (value.contains(AppConstants.CURRENCY))
                            {
                                value = value.replace(AppConstants.CURRENCY,"");
                            }
                            if (value.contains(","))
                            {
                                value = value.replace(",",".");
                            }
                            total = total - Double.parseDouble(value);

                            if(total>=0)
                            {
                                tvTotal.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",total));
                            }else
                            {
                                tvTotal.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",0.00));
                            }
                        }
                    }
                });

            }
        };
        my_timer.schedule(my_timer_task,000,2000);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume() {
        super.onResume();
        mStartTimerForCalculatingFares();
    }


////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onPause()
    {
        super.onPause();
        my_timer_task.cancel();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mUploadSignature()
    {

        mProgressDailog=Utility.getProcessDialog(InvoiceActivity.this);
        mProgressDailog.show();

        Upload_file_AmazonS3 amazonS3 = Upload_file_AmazonS3.getInstance(InvoiceActivity.this,AppConstants.COGNITO_POOL_ID);

        // final String folderFileName = AppConstants.AMZAON_S3_PROFILE_PICTURE+fileName;
        mAmazonImgUrl=AppConstants.AMAZON_BASE_URL+AppConstants.BUCKET+"/"+AppConstants.BUCKET_INVOICE_SIGNATURE+photo.getName();
        Log.d(TAG, "mUploadImageToBucket: "+mAmazonImgUrl);

        amazonS3.UploadFile(AppConstants.BUCKET,AppConstants.BUCKET_INVOICE_SIGNATURE+photo.getName(), photo, new Upload_file_AmazonS3.Upload_CallBack()
        {
            @Override
            public void sucess(String url)
            {
                Log.d("Suri",url);
                if(photo != null)
                {
                    photo.deleteOnExit();
                }
                mUpdateApptStatus();
            }

            @Override
            public void error(String errormsg) {
                Log.d("Suri","error");
                if(mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(InvoiceActivity.this, "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
            Utility.printLog("File path"+photo.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        InvoiceActivity.this.sendBroadcast(mediaScanIntent);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /*public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile = new File(getAlbumStorageDir("SignaturePad"), AppConstants.TEMP_SIGNATURE_FILE_NAME+mAppointmentDetail.getBid());
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }*/
////////////////////////////////////////////////////////////////////////////////////////////////////
    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        folder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!folder.mkdirs())
        {
            Log.e("SignaturePad", "Directory not created");
        }
        return folder;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mUpdateApptStatus()
    {
        String total=tvTotal.getText().toString();
        if(total.contains(AppConstants.CURRENCY)){
            total=total.replace(AppConstants.CURRENCY,"");
        }
        String timeFee=tvTimeFee.getText().toString();
        if(timeFee.contains(AppConstants.CURRENCY)){
            timeFee=timeFee.replace(AppConstants.CURRENCY,"");
        }
        if(mProgressDailog!=null && !mProgressDailog.isShowing())
        {
            mProgressDailog.show();
        }
        RequestBody requestBody= new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_pat_email",mAppointmentDetail.getEmail())
                .add("ent_appnt_dt",mAppointmentDetail.getApntDt())
                .add("ent_response",""+ AppConstants.STATUS_JOB_COMPLETED)
                .add("ent_notes",etProNotes.getText().toString())
                .add("ent_timer","")
                .add("ent_pymt","2")
                .add("ent_signature_url",mAmazonImgUrl)
                .add("ent_bid",mAppointmentDetail.getBid())
                .add("ent_total_pro",total)
                .add("ent_date_time",Utility.getCurrentGmtTime())
                .add("ent_misc_fees",tvMisc.getText().toString())
                .add("ent_pro_disc",tvProviderDiscount.getText().toString())
                .add("ent_mat_fees",tvMaterialFee.getText().toString())
                .add("ent_time_fees",timeFee)
                .build();

        Utility.printReq(requestBody," updateapptstatus req");
        Okhttp_connection.doOkhttpRequest(ServiceUrls.UPDATE_APPT_STATUS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result) {
                if(mProgressDailog!=null && mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
                Utility.printLog("PK response"+result);
                Gson gson=new Gson();
                UpdateMasterStatusResponsePojo updateMasterStatusResponsePojo=gson.fromJson(result,UpdateMasterStatusResponsePojo.class);

                if("0".equals(updateMasterStatusResponsePojo.getErrFlag()))
                {
                    if("61".equals(updateMasterStatusResponsePojo.getErrNum()))
                    {
                        MainActivity.acknowledgeCustomer(mAppointmentDetail.getBid(),mAppointmentDetail.getCid(),AppConstants.STATUS_JOB_COMPLETED,sessionManager.getProviderID(),"");
                        if(folder.exists())
                        {
                            File[] files= folder.listFiles();
                            if(files.length > 0)
                            {
                                for(int i = 0; i< files.length; i++)
                                {
                                    files[i].delete();
                                }
                            }
                        }
                        finish();
                    }
                }
                else if("1".equals(updateMasterStatusResponsePojo.getErrFlag()))
                {
                    mShowErrorMessage(updateMasterStatusResponsePojo.getErrMsg(),false);
                }
                else
                {
                    finish();
                }

            }

            @Override
            public void onError(String error) {
                if(mProgressDailog!=null && mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
                Utility.printLog("PK error"+error);
            }
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg, final boolean logout)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(InvoiceActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                if(logout)
                {
                    sessionManager.clearSession();
                    finish();
                }
                dialogInterface.dismiss();

            }
        });

        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mSetSelectedServices()
    {
        int size=mAppointmentDetail.getServices().size();

        if(size>0)
        {
            for(int i=0;i<size;i++)
            {
                LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.single_row_inflate_services,null);

                TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
                tvNojobSelected.setTypeface(font);

                TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
                tvSelectedServicePrice.setTypeface(font);

                TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
                tvSelectedService.setTypeface(font);

                LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);

                tvSelectedServicePrice.setText(mAppointmentDetail.getServices().get(i).getSprice());
                tvSelectedService.setText(mAppointmentDetail.getServices().get(i).getSname());
                tvNojobSelected.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);

                if(!tvSelectedServicePrice.getText().toString().trim().equals("") && !tvSelectedServicePrice.getText().toString().trim().endsWith("."))
                {
                    String value = tvSelectedServicePrice.getText().toString().trim();
                    if (value.contains(AppConstants.CURRENCY))
                    {
                        value = value.replace(AppConstants.CURRENCY,"");
                    }
                    if (value.contains(","))
                    {
                        value = value.replace(",",".");
                    }
                    Servicetotal = Servicetotal + Double.parseDouble(value);

                    tvSelectedServicePrice.setText(AppConstants.CURRENCY+"  "+String.format("%.2f",Servicetotal));
                }

                ll_service_Container.addView(view);
            }
        }else
        {
            LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_inflate_services,null);

            TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
            tvNojobSelected.setTypeface(font);

            TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
            TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
            LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);

            tvNojobSelected.setVisibility(View.VISIBLE);
            layout.setVisibility(View.GONE);
            ll_service_Container.addView(view);
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    private String getDurationString(long seconds, String format)
    {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        int hours = (int) (TimeUnit.SECONDS.toHours(seconds) - (day *24));
        int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60));
        int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60));

        if(format.equals("H"))
        {
            return ""+hours;
        }
        else if(format.equals("M"))
        {
            return ""+minute;
        }

        return ""+seconds;
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////
}
