package com.app.partner.iserve.TabActivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.partner.iserve.Messaging.MessagingActivityNew;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.app.partner.iserve.CancelationDialog;
import com.app.partner.iserve.MainActivity;
import com.app.partner.iserve.Pojo.AbortAppointmentResponse;
import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.Pojo.UpdateMasterStatusResponsePojo;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.LocationUtil;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.Scaler;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

public class JobAcceptedActivity extends AppCompatActivity implements LocationUtil.GetLocationListener {

    private Appointments mAppointmentDetail;
    private ImageView ivBackbtn;
    private TextView
                    mToolbar_title,
                    tvCancel,
                    tvTitleInvoice,
                    tvTitleJobaccepted,
                    tvTitleJobStarted,
                    tvTitleOnTheWay,
                    tvCustomerName,
                    tvCustomerAddrs1,
                    tvCustomerAddrs2,
                    tvJobDetailsHeader,
                    tvJobDetailsDescription,
                    tvHeaderJobPhotos,tvSelectedServiceHeader
            ;
    private LinearLayout llCall,llMessage,llLocate;
    private SeekBar sb;
    private String TAG="JObAcceptedActivity";
    private SessionManager sessionManager;
    private ProgressDialog mProgressDailog;
    private LinearLayout ll_inflater,ll_service_Container;
    private LocationUtil locationUtil;
    private Typeface font,fontBold;
    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_accepted);

        sessionManager=new SessionManager(this);
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        mAppointmentDetail= (Appointments) bundle.getSerializable("APPOINTMENT");

        initLayoutId();
        setValues();

        filter = new IntentFilter("com.app.partner.firbase.cancel");
        receiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bucket=intent.getExtras();
                try
                {
                    String action = bucket.getString("action");
                    String payload = bucket.getString("payload");

                    if(action!=null && action.equals("3"))
                    {
                        mShowErrorMessage(payload,true);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e("PK","Crash "+e.getMessage());
                }

            }
        };

        registerReceiver(receiver,filter);

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void initLayoutId()
    {
        font=Utility.getFontRegular(JobAcceptedActivity.this);
        fontBold=Utility.getFontBold(JobAcceptedActivity.this);

        mProgressDailog=Utility.getProcessDialog(this);

        ivBackbtn= (ImageView) findViewById(R.id.ivBackbtn);

        mToolbar_title= (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvCancel= (TextView) findViewById(R.id.tvCancel);
        tvCancel.setTypeface(font);

        tvTitleInvoice= (TextView) findViewById(R.id.tvTitleInvoice);
        tvTitleInvoice.setTypeface(font);

        tvTitleJobaccepted= (TextView) findViewById(R.id.tvTitleJobaccepted);
        tvTitleJobaccepted.setTypeface(font);

        tvTitleJobStarted= (TextView) findViewById(R.id.tvTitleJobStarted);
        tvTitleJobStarted.setTypeface(font);

        tvTitleOnTheWay= (TextView) findViewById(R.id.tvTitleOnTheWay);
        tvTitleOnTheWay.setTypeface(font);

        tvCustomerName= (TextView) findViewById(R.id.tvCustomerName);
        tvCustomerName.setTypeface(fontBold);

        tvCustomerAddrs1= (TextView) findViewById(R.id.tvCustomerAddrs1);
        tvCustomerAddrs1.setTypeface(font);

        tvCustomerAddrs2= (TextView) findViewById(R.id.tvCustomerAddrs2);
        tvCustomerAddrs2.setTypeface(font);

        tvJobDetailsHeader= (TextView) findViewById(R.id.tvJobDetailsHeader);
        tvJobDetailsHeader.setTypeface(font);

        tvJobDetailsDescription= (TextView) findViewById(R.id.tvJobDetailsDescription);
        tvJobDetailsDescription.setTypeface(font);

        tvHeaderJobPhotos= (TextView) findViewById(R.id.tvHeaderJobPhotos);
        tvHeaderJobPhotos.setTypeface(font);

        tvSelectedServiceHeader= (TextView) findViewById(R.id.tvSelectedServiceHeader);
        tvSelectedServiceHeader.setTypeface(font);

        llCall= (LinearLayout) findViewById(R.id.llCall);
        llMessage= (LinearLayout) findViewById(R.id.llMessage);
        llLocate= (LinearLayout) findViewById(R.id.llLocate);
        ll_inflater= (LinearLayout) findViewById(R.id.ll_inflater);
        ll_service_Container= (LinearLayout) findViewById(R.id.ll_service_Container);


        sb= (SeekBar) findViewById(R.id.myseek);

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void setValues()
    {

        if(mAppointmentDetail!=null)
        {
            mToolbar_title.setText(getResources().getString(R.string.jobId)+" "+mAppointmentDetail.getBid());

            if(mAppointmentDetail.getCustomer_notes().isEmpty())
            {
                tvJobDetailsDescription.setText("No details");
            }
            else
            {
                tvJobDetailsDescription.setText(mAppointmentDetail.getCustomer_notes());
            }

            tvCustomerName.setText(mAppointmentDetail.getFname());
            tvCustomerAddrs1.setText(mAppointmentDetail.getAddrLine1());
            if(!"".equals(mAppointmentDetail.getAddrLine2()))
            {
                tvCustomerAddrs2.setVisibility(View.VISIBLE);
                tvCustomerAddrs2.setText(mAppointmentDetail.getAddrLine2());
            }

            llCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String uri = "tel:"+mAppointmentDetail.getPhone() ;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                }
            });
            llMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("sms:"+mAppointmentDetail.getPhone()));
                    startActivity(sendIntent);*/
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("APPOINTMENT",mAppointmentDetail);
                    Intent intent=new Intent(JobAcceptedActivity.this, MessagingActivityNew.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            });
            llLocate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(JobAcceptedActivity.this, "Locate", Toast.LENGTH_SHORT).show();
                    String apntLat = mAppointmentDetail.getApptLat();
                    String apntLong = mAppointmentDetail.getApptLong();
                    String muri = "google.navigation:q=" + apntLat + "," + apntLong;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(muri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);
                }
            });
            sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (seekBar.getProgress() > 75)
                    {
                        seekBar.setProgress(100);
                        //startActivity(new Intent(JobAcceptedActivity.this,IhaveArrivedActivity.class));
                        mUpdateApptStatus();
                    }
                    else
                    {
                        seekBar.setProgress(0);
                    }
                }
            });
            if(mAppointmentDetail.getServices()!=null)
            {
                mSetSelectedServices();
            }

            mAddJobPhotos();

        }
        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAbortAppointment();
            }
        });

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mUpdateApptStatus()
    {

        if(Utility.isNetworkAvailable(JobAcceptedActivity.this))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }
            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_pat_email",mAppointmentDetail.getEmail())
                    .add("ent_appnt_dt",mAppointmentDetail.getApntDt())
                    .add("ent_response",""+ AppConstants.STATUS_ON_THE_WAY)
                    .add("ent_doc_remarks","")
                    .add("ent_timer","")
                    .add("ent_bid",mAppointmentDetail.getBid())
                    .add("ent_date_time",Utility.getCurrentGmtTime())
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.UPDATE_APPT_STATUS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK response"+result);
                    Gson gson=new Gson();
                    UpdateMasterStatusResponsePojo updateMasterStatusResponsePojo=gson.fromJson(result,UpdateMasterStatusResponsePojo.class);

                    if("0".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        if("57".equals(updateMasterStatusResponsePojo.getErrNum()))
                        {
                            MainActivity.acknowledgeCustomer(mAppointmentDetail.getBid(),mAppointmentDetail.getCid(),AppConstants.STATUS_ON_THE_WAY,sessionManager.getProviderID(),"");

                            mAppointmentDetail.setStatus(""+AppConstants.STATUS_ON_THE_WAY);

                            Intent intent=new Intent(JobAcceptedActivity.this,IhaveArrivedActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("APPOINTMENT",mAppointmentDetail);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                    }
                    else if("1".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        mShowErrorMessage(updateMasterStatusResponsePojo.getErrMsg(),false);
                    }
                    else
                    {
                        finish();
                    }

                }

                @Override
                public void onError(String error) {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK error"+error);
                }
            });
        }
        else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mUpdateApptStatus();
                        }
                    }).show();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mAddJobPhotos(){



        double size[]= Scaler.getScalingFactor(JobAcceptedActivity.this);
        int no_of_images=0;
        if(!"".equals(mAppointmentDetail.getJob_imgs()))
        {
            no_of_images=Integer.parseInt(mAppointmentDetail.getJob_imgs());

            for(int i=0;i<no_of_images;i++)
            {
                final String url=AppConstants.APPOINTMENT_JOB_IMAGES+mAppointmentDetail.getBid()+"_"+i+".png";

                LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.single_row_job_photos,null);

                ImageView imageView= (ImageView) view.findViewById(R.id.jobPhoto);
                imageView.setBackgroundResource(R.drawable.noimage);

                ProgressBar progressBar= (ProgressBar) view.findViewById(R.id.ProgressBar);



                // Picasso.with(JobAcceptedActivity.this).load().resize(size[])
           /* LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(5,5,5,5);
            imageView.setLayoutParams(layoutParams);*/

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showBigImage(Uri.parse(url));
                    }
                });

                ll_inflater.addView(view);


                Utility.printLog();
                int height=(int)size[0]*50;
                int width=(int)size[1]*50;
                try
                {

                    Picasso.with(this)
                            .load(url)
                            .resize(height,width)
                            .placeholder(R.drawable.noimage)
                            .into(imageView,new ImageLoadedCallback(progressBar));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }else
        {
            LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_job_photos,null);

            ImageView imageView= (ImageView) view.findViewById(R.id.jobPhoto);

            ProgressBar progressBar= (ProgressBar) view.findViewById(R.id.ProgressBar);

            TextView textView= (TextView) view.findViewById(R.id.tvNoImages);

            textView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

            ll_inflater.addView(view);
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * <p>Image loading Callback</p>
     * callback class for progress bar for
     * image loading
     * after image load setting progress bar visibility gone
     * @author embed
     */
    private class ImageLoadedCallback implements Callback
    {
        ProgressBar progressBar;

        public  ImageLoadedCallback(ProgressBar progBar)
        {
            progressBar = progBar;
        }

        @Override
        public void onSuccess()
        {
            if(progressBar != null)
            {
                progressBar.setVisibility(View.GONE);
            }

        }

        @Override
        public void onError()
        {

        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg, final boolean finish)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(JobAcceptedActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                if(finish)
                {
                    //sessionManager.clearSession();
                    finish();
                }
                dialogInterface.dismiss();

            }
        });

        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    protected void onResume() {
        super.onResume();

        getCurrentLocation();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mAbortAppointment()
    {
        CancelationDialog dialog=new CancelationDialog(JobAcceptedActivity.this, new CancelationDialog.CancelationCallback() {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog(TAG+"PK response"+result);
                if(result!=null && !result.isEmpty())
                {
                    Gson gson=new Gson();
                    AbortAppointmentResponse response=gson.fromJson(result,AbortAppointmentResponse.class);

                    if(response.getErrFlag().equals("0"))
                    {
                        mShowErrorMessage(response.getErrMsg(),true);
                    }
                    else
                    {
                        mShowErrorMessage(response.getErrMsg(),false);
                    }
                }
            }

            @Override
            public void onError(String error) {
                Utility.printLog(TAG+"PK error response"+error);
            }
        });

        dialog.showCancellationDialog(mAppointmentDetail.getBid(),mAppointmentDetail.getCid());
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    private void getCurrentLocation()
    {
        if (locationUtil == null)
        {
            locationUtil = new LocationUtil(this, this);
        }
        else
        {
            locationUtil.checkLocationSettings();
        }
    }
    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void location_Error(String error) {

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mSetSelectedServices()
    {
        int size=mAppointmentDetail.getServices().size();

        if(size>0)
        {
            for(int i=0;i<size;i++)
            {
                LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.single_row_inflate_services,null);

                TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
                TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
                tvSelectedServicePrice.setTypeface(font);

                TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
                tvSelectedService.setTypeface(font);

                LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);

                tvSelectedServicePrice.setText(AppConstants.CURRENCY+" "+mAppointmentDetail.getServices().get(i).getSprice());
                tvSelectedService.setText(mAppointmentDetail.getServices().get(i).getSname());
                tvNojobSelected.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);

                ll_service_Container.addView(view);
            }
        }else
        {
            LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_inflate_services,null);

            TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
            TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
            TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
            LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);

            tvNojobSelected.setVisibility(View.VISIBLE);
            tvNojobSelected.setTypeface(font);
            layout.setVisibility(View.GONE);
            ll_service_Container.addView(view);
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void showBigImage(Uri url)
    {
        dialog=new Dialog(JobAcceptedActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_imageview);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView bigImageView= (ImageView) dialog.findViewById(R.id.ivBigImage);

        Picasso.with(JobAcceptedActivity.this)
                .load(url)
                .into(bigImageView);

        dialog.show();

    }
}
