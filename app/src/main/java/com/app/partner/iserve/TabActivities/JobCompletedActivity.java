package com.app.partner.iserve.TabActivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.partner.iserve.Messaging.MessagingActivityNew;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import com.app.partner.iserve.CancelationDialog;
import com.app.partner.iserve.MainActivity;
import com.app.partner.iserve.Pojo.AbortAppointmentResponse;
import com.app.partner.iserve.Pojo.Appointments;
import com.app.partner.iserve.Pojo.UpdateMasterStatusResponsePojo;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.LocationUtil;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.Scaler;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

public class JobCompletedActivity extends AppCompatActivity implements LocationUtil.GetLocationListener {

    private SessionManager sessionManager;
    private Appointments mAppointmentDetail;
    private String TAG="JobCompletedActivity";
    private ImageView ivBackbtn;
    private TextView mToolbar_title,
            tvCancel,
            tvTitleInvoice,
            tvTitleJobaccepted,
            tvTitleJobStarted,
            tvTitleOnTheWay,
            tvCustomerName,
            tvJobTimerHeader,tvTimer,
            tvTitleHours,
            tvTitleMinutes,
            tvJobDetailsHeader,
            tvHeaderJobPhotos,
            tvJobDetailsDescription,
            tvJobCompletedTitle,tvSelectedServiceHeader;

    //private LinearLayout llCall,llMessage,llLocate;

    private TextView btnPauseTimer;
    private LinearLayout ll_inflater,ll_service_Container;
    private SeekBar myseek;
    private ProgressDialog mProgressDailog;
    private TextView tvCustomerAddrs1;
    private boolean runTimer=false;
    private int timeConsumedSecond=0;
    private Handler handler;
    private Runnable myRunnable;
    private LocationUtil locationUtil;
    private Typeface font,fontBold;
    private Dialog dialog;

    ImageView ivCall,ivMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_completed);

        sessionManager=new SessionManager(this);
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        mAppointmentDetail= (Appointments) bundle.getSerializable("APPOINTMENT");

        initLayoutId();
        setValues();
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    private void initLayoutId()
    {
        font=Utility.getFontRegular(JobCompletedActivity.this);
        fontBold=Utility.getFontBold(JobCompletedActivity.this);

        ivBackbtn= (ImageView) findViewById(R.id.ivBackbtn);

        btnPauseTimer= (TextView) findViewById(R.id.btnPauseTimer);
        btnPauseTimer.setTypeface(fontBold);

        mToolbar_title= (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvCancel= (TextView) findViewById(R.id.tvCancel);
        tvCancel.setTypeface(font);

        tvTitleInvoice= (TextView) findViewById(R.id.tvTitleInvoice);
        tvTitleInvoice.setTypeface(font);

        tvTitleJobaccepted= (TextView) findViewById(R.id.tvTitleJobaccepted);
        tvTitleJobaccepted.setTypeface(font);

        tvTitleJobStarted= (TextView) findViewById(R.id.tvTitleJobStarted);
        tvTitleJobStarted.setTypeface(font);

        tvTitleOnTheWay= (TextView) findViewById(R.id.tvTitleOnTheWay);
        tvTitleOnTheWay.setTypeface(font);

        tvCustomerName= (TextView) findViewById(R.id.tvCustomerName);
        tvCustomerName.setTypeface(fontBold);

        tvJobTimerHeader= (TextView) findViewById(R.id.tvJobTimerHeader);
        tvJobTimerHeader.setTypeface(font);

        tvTimer= (TextView) findViewById(R.id.tvTimer);
        tvTimer.setTypeface(font);

        tvTitleHours= (TextView) findViewById(R.id.tvTitleHours);
        tvTitleHours.setTypeface(font);

        tvTitleMinutes= (TextView) findViewById(R.id.tvTitleMinutes);
        tvTitleMinutes.setTypeface(font);

        tvJobDetailsHeader= (TextView) findViewById(R.id.tvJobDetailsHeader);
        tvJobDetailsHeader.setTypeface(font);

        tvHeaderJobPhotos= (TextView) findViewById(R.id.tvHeaderJobPhotos);
        tvHeaderJobPhotos.setTypeface(font);

        tvJobDetailsDescription= (TextView) findViewById(R.id.tvJobDetailsDescription);
        tvJobDetailsDescription.setTypeface(font);

        tvJobCompletedTitle= (TextView) findViewById(R.id.tvJobCompletedTitle);
        tvJobCompletedTitle.setTypeface(font);

        tvSelectedServiceHeader= (TextView) findViewById(R.id.tvSelectedServiceHeader);
        tvSelectedServiceHeader.setTypeface(font);

        ivCall= (ImageView) findViewById(R.id.ivCall);
        ivMessage= (ImageView) findViewById(R.id.ivMessage);

        ll_inflater= (LinearLayout) findViewById(R.id.ll_inflater);
        ll_service_Container= (LinearLayout) findViewById(R.id.ll_service_Container);

        myseek= (SeekBar) findViewById(R.id.myseek);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    private void setValues()
    {
        mProgressDailog=Utility.getProcessDialog(this);
        if(mAppointmentDetail!=null)
        {
            mToolbar_title.setText(getResources().getString(R.string.jobId)+" "+mAppointmentDetail.getBid());

            tvCustomerName.setText(mAppointmentDetail.getFname());

            ivCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String uri = "tel:"+mAppointmentDetail.getPhone() ;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                }
            });
            ivMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("sms:"+mAppointmentDetail.getPhone()));
                    startActivity(sendIntent);*/
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("APPOINTMENT",mAppointmentDetail);
                    Intent intent=new Intent(JobCompletedActivity.this, MessagingActivityNew.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            });
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mAbortAppointment();
            }
        });
        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setElapsedTime(timeConsumedSecond);
                finish();
            }
        });

        if(mAppointmentDetail.getStatus().equals("21"))
        {
            btnPauseTimer.setEnabled(false);
        }
        else if(mAppointmentDetail.getStatus().equals("6"))
        {
            btnPauseTimer.setEnabled(true);
            tvJobCompletedTitle.setText(getResources().getString(R.string.JOB_COMPLETED));
        }

        btnPauseTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mAppointmentDetail.getStatus().equals("6"))
                {
                    if(runTimer)
                    {
                        btnPauseTimer.setText("START TIMER");
                        mUpdateApptTimer("2");
                        runTimer=false;
                    }
                    else if(!runTimer)
                    {
                        runTimer=true;
                        mUpdateApptTimer("1");
                        btnPauseTimer.setText("PAUSE TIMER");
                        timeConsumedSecond=(int)sessionManager.getElapsedTime();
                        mJobTimer();
                    }
                }
            }
        });

        myseek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75)
                {
                    seekBar.setProgress(100);
                    runTimer = false;

                    if(mAppointmentDetail.getStatus().equals("21"))
                    {
                        mUpdateApptStatus();
                    }
                    else if(mAppointmentDetail.getStatus().equals("6"))
                    {
                        mRaiseInvoice();
                        mUpdateApptStatus();
                    }

                }
                else
                {
                    seekBar.setProgress(0);
                }
            }
        });

        if(mAppointmentDetail.getCustomer_notes().isEmpty())
        {
            tvJobDetailsDescription.setText("No details");
        }
        else
        {
            tvJobDetailsDescription.setText(mAppointmentDetail.getCustomer_notes());
        }

        if(mAppointmentDetail.getServices()!=null)
        {
            mSetSelectedServices();
        }
        mAddJobPhotos();


/*
        if(mAppointmentDetail.getStatus().equals("21"))
        {
            btnPauseTimer.setText("START TIMER");
            tvJobCompletedTitle.setText(getResources().getString(R.string.JOB_STARTED));
        }
        else if(mAppointmentDetail.getStatus().equals("6"))
        {
            tvJobCompletedTitle.setText(getResources().getString(R.string.JOB_COMPLETED));
        }*/

        if(mAppointmentDetail.getStatus().equals("6") )
        {
            if (!sessionManager.isTimerStarted())
            {
                Log.d(TAG, "onResume: "+getDurationString((int) sessionManager.getElapsedTime()));
                tvTimer.setText(getDurationString((int) sessionManager.getElapsedTime()));
                /*mJobTimer();
                mUpdateApptTimer("1");*/
            }
            else
            {
                mSubStractTimePaused();
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mUpdateApptStatus()
    {

        if(Utility.isNetworkAvailable(JobCompletedActivity.this))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }
            RequestBody requestBody = null;
            if(mAppointmentDetail.getStatus().equals("6"))
            {
                requestBody= new FormEncodingBuilder()
                        .add("ent_sess_token",sessionManager.getSessionToken())
                        .add("ent_dev_id",sessionManager.getDeviceId())
                        .add("ent_pat_email",mAppointmentDetail.getEmail())
                        .add("ent_appnt_dt",mAppointmentDetail.getApntDt())
                        .add("ent_response",""+ AppConstants.STATUS_INVOICE_RAISED)
                        .add("ent_doc_remarks","")
                        .add("ent_timer",""+timeConsumedSecond)
                        .add("ent_bid",mAppointmentDetail.getBid())
                        .add("ent_date_time",Utility.getCurrentGmtTime())
                        .build();
            }else if(mAppointmentDetail.getStatus().equals("21"))
            {
                requestBody= new FormEncodingBuilder()
                        .add("ent_sess_token",sessionManager.getSessionToken())
                        .add("ent_dev_id",sessionManager.getDeviceId())
                        .add("ent_pat_email",mAppointmentDetail.getEmail())
                        .add("ent_appnt_dt",mAppointmentDetail.getApntDt())
                        .add("ent_response",""+ AppConstants.STATUS_JOB_STARTED)
                        .add("ent_doc_remarks","")
                        .add("ent_timer","")
                        .add("ent_bid",mAppointmentDetail.getBid())
                        .add("ent_date_time",Utility.getCurrentGmtTime())
                        .build();
            }

            Okhttp_connection.doOkhttpRequest(ServiceUrls.UPDATE_APPT_STATUS, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK response"+result);
                    Gson gson=new Gson();
                    UpdateMasterStatusResponsePojo updateMasterStatusResponsePojo=gson.fromJson(result,UpdateMasterStatusResponsePojo.class);

                    if("0".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        if("60".equals(updateMasterStatusResponsePojo.getErrNum()))
                        {

                            handler.removeCallbacks(myRunnable);
                            sessionManager.setTimeWhilePaused(0);
                            sessionManager.setElapsedTime(0);
                            sessionManager.setTimerStarted(false);

                            MainActivity.acknowledgeCustomer(mAppointmentDetail.getBid(),mAppointmentDetail.getCid(),AppConstants.STATUS_INVOICE_RAISED,sessionManager.getProviderID(),"");
                            mAppointmentDetail.setStatus(""+AppConstants.STATUS_INVOICE_RAISED);

                            Intent intent=new Intent(JobCompletedActivity.this,InvoiceActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("APPOINTMENT",mAppointmentDetail);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                        else if("59".equals(updateMasterStatusResponsePojo.getErrNum()))
                        {
                            MainActivity.acknowledgeCustomer(mAppointmentDetail.getBid(),mAppointmentDetail.getCid(),AppConstants.STATUS_JOB_STARTED,sessionManager.getProviderID(),"");

                            mAppointmentDetail.setStatus(""+AppConstants.STATUS_JOB_STARTED);
                            tvJobCompletedTitle.setText(getResources().getString(R.string.JOB_COMPLETED));
                            myseek.setProgress(0);
                            btnPauseTimer.setEnabled(true);
                            btnPauseTimer.setText("PAUSE TIMER");

                            runTimer=true;
                            mJobTimer();
                            mUpdateApptTimer("1");


                        }
                    }
                    else if("1".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        mShowErrorMessage(updateMasterStatusResponsePojo.getErrMsg(),false);
                    }
                    else
                    {
                        finish();
                    }
                }

                @Override
                public void onError(String error) {
                    Utility.printLog("PK error"+error);
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mUpdateApptStatus();
                        }
                    }).show();
        }


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mAddJobPhotos(){

        double size[]= Scaler.getScalingFactor(JobCompletedActivity.this);
        int no_of_images=0;
        if(!"".equals(mAppointmentDetail.getJob_imgs()))
        {
            no_of_images=Integer.parseInt(mAppointmentDetail.getJob_imgs());

            for(int i=0;i<no_of_images;i++)
            {
                final String url=AppConstants.APPOINTMENT_JOB_IMAGES+mAppointmentDetail.getBid()+"_"+i+".png";

                LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.single_row_job_photos,null);

                ImageView imageView= (ImageView) view.findViewById(R.id.jobPhoto);
                imageView.setBackgroundResource(R.drawable.noimage);

                ProgressBar progressBar= (ProgressBar) view.findViewById(R.id.ProgressBar);
                // Picasso.with(JobAcceptedActivity.this).load().resize(size[])
               /* LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(5,5,5,5);
                imageView.setLayoutParams(layoutParams);*/
                ll_inflater.addView(view);

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showBigImage(Uri.parse(url));
                    }
                });


                Utility.printLog();
                int height=(int)size[0]*50;
                int width=(int)size[1]*50;
                try
                {

                    Picasso.with(this)
                            .load(url)
                            .resize(height,width)
                            .placeholder(R.drawable.noimage)
                            .into(imageView,new ImageLoadedCallback(progressBar));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }else
        {
            LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_job_photos,null);

            ImageView imageView= (ImageView) view.findViewById(R.id.jobPhoto);

            ProgressBar progressBar= (ProgressBar) view.findViewById(R.id.ProgressBar);

            TextView textView= (TextView) view.findViewById(R.id.tvNoImages);
            textView.setTypeface(font);


            textView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

            ll_inflater.addView(view);
        }

    }


////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * <p>Image loading Callback</p>
     * callback class for progress bar for
     * image loading
     * after image load setting progress bar visibility gone
     * @author embed
     */
    private class ImageLoadedCallback implements Callback
    {
        ProgressBar progressBar;

        public  ImageLoadedCallback(ProgressBar progBar)
        {
            progressBar = progBar;
        }

        @Override
        public void onSuccess()
        {
            if(progressBar != null)
            {
                progressBar.setVisibility(View.GONE);
            }

        }

        @Override
        public void onError()
        {

        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mJobTimer()
    {
        sessionManager.setTimerStarted(true);

        handler = new Handler();
        myRunnable=new Runnable() {

            @Override
            public void run()
            {
                if(runTimer)
                {
                    timeConsumedSecond = timeConsumedSecond + 1;
                    tvTimer.setText(""+getDurationString(timeConsumedSecond));
                    handler.postDelayed(this, 1000);
                }
                else
                {
                    sessionManager.setTimeWhilePaused(System.currentTimeMillis());
                    sessionManager.setElapsedTime(timeConsumedSecond);
                    Log.d(TAG, "run: "+sessionManager.getTimeWhilePaused());
                    Log.d(TAG, "run: "+timeConsumedSecond);
                }
            }
        };

        handler.postDelayed(myRunnable , 1000);

    }
    private String getDurationString(int seconds)
    {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        int hours = (int) (TimeUnit.SECONDS.toHours(seconds) - (day *24));
        int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60));
        int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60));
        //Utility.printLog(TAG+"Duration "+hours+" : "+minute);

        String timer=String.format("%02d",hours)+" : "+String.format("%02d",minute)+" : "+String.format("%02d",second);

        return timer;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume()
    {
        super.onResume();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mSubStractTimePaused()
    {
        Utility.printLog(TAG+"run : time while paused "+sessionManager.getTimeWhilePaused());
        Utility.printLog(TAG+"run : time elapsed "+sessionManager.getElapsedTime());
        if (sessionManager.getElapsedTime() >= 0)
        {
            long mTimeWhilePaused = sessionManager.getTimeWhilePaused();
            if(!"0".equals(mTimeWhilePaused))
            {
                long currentTime = System.currentTimeMillis() - mTimeWhilePaused;
                Utility.printLog("TIME ELAPSED EQUALS"+currentTime);
                timeConsumedSecond = (int)sessionManager.getElapsedTime();
                timeConsumedSecond = timeConsumedSecond+(int)Math.round(((float)(currentTime))/1000f);
            }
            else
            {
                timeConsumedSecond = (0);
            }
        }
        else
        {
            timeConsumedSecond = (0);
        }
        runTimer = true;
        btnPauseTimer.setText("PAUSE TIMER");

        mJobTimer();

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg, final boolean finish)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(JobCompletedActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                if(finish)
                {
                    //sessionManager.clearSession();
                    finish();
                }
                dialogInterface.dismiss();

            }
        });

        dialogBuilder.show();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        runTimer = false;
        Utility.printLog(TAG+"run : time while paused onDestroy "+sessionManager.getTimeWhilePaused());
        sessionManager.setTimeWhilePaused(System.currentTimeMillis());
        Log.d(TAG, "onDestroy: called");
    }

    @Override
    public void onBackPressed() {
        sessionManager.setElapsedTime(timeConsumedSecond);
        super.onBackPressed();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mUpdateApptTimer(final String timer_status)
    {

        if(Utility.isNetworkAvailable(JobCompletedActivity.this))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }
            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_timer",""+timeConsumedSecond)
                    .add("ent_bid",mAppointmentDetail.getBid())
                    .add("ent_date_time",Utility.getCurrentGmtTime())
                    .add("ent_timer_status",timer_status)
                    .build();
            Utility.printLog("UpdateApptTimer request ent_sess_token:"+sessionManager.getSessionToken()+" ent_dev_id:"+sessionManager.getDeviceId()+" ent_timer:"+timeConsumedSecond+" ent_bid:"+mAppointmentDetail.getBid()+" ent_date_time:"+Utility.getCurrentGmtTime()+" ent_timer_status:"+timer_status);
            Okhttp_connection.doOkhttpRequest(ServiceUrls.UPDATE_APPT_TIMER, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("UpdateApptTimer response"+result);
                    Gson gson=new Gson();
                    UpdateMasterStatusResponsePojo updateMasterStatusResponsePojo=gson.fromJson(result,UpdateMasterStatusResponsePojo.class);

                    if("0".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        if("1".equals(timer_status))
                        {
                            MainActivity.acknowledgeCustomer(mAppointmentDetail.getBid(),mAppointmentDetail.getCid(),AppConstants.STATUS_TIMER_STARTED,sessionManager.getProviderID(),"");
                        }
                        else if("2".equals(timer_status))
                        {
                            sessionManager.setTimerStarted(false);
                            MainActivity.acknowledgeCustomer(mAppointmentDetail.getBid(),mAppointmentDetail.getCid(),AppConstants.STATUS_TIMER_STOPPED,sessionManager.getProviderID(),"");
                        }

                    }
                    else if("1".equals(updateMasterStatusResponsePojo.getErrFlag()))
                    {
                        mShowErrorMessage(updateMasterStatusResponsePojo.getErrMsg(),false);
                    }
                    else
                    {
                        finish();
                    }
                }

                @Override
                public void onError(String error) {
                    Utility.printLog("PK error"+error);
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mUpdateApptStatus();
                        }
                    }).show();
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mAbortAppointment()
    {
        CancelationDialog dialog=new CancelationDialog(JobCompletedActivity.this, new CancelationDialog.CancelationCallback() {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog(TAG+"PK response"+result);
                if(result!=null && !result.isEmpty())
                {
                    Gson gson=new Gson();
                    AbortAppointmentResponse response=gson.fromJson(result,AbortAppointmentResponse.class);

                    if(response.getErrFlag().equals("0"))
                    {
                        mShowErrorMessage(response.getErrMsg(),true);
                        //finish();
                    }
                    else
                    {
                        mShowErrorMessage(response.getErrMsg(),false);
                    }
                }
            }

            @Override
            public void onError(String error) {
                Utility.printLog(TAG+"PK error response"+error);
            }
        });

        dialog.showCancellationDialog(mAppointmentDetail.getBid(),mAppointmentDetail.getCid());
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mRaiseInvoice()
    {
        sessionManager.setDuration((timeConsumedSecond));

        if(mProgressDailog!=null && !mProgressDailog.isShowing())
        {
            mProgressDailog.show();
        }
        RequestBody requestBody= new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_minutes",""+(timeConsumedSecond)/60)
                .add("ent_bid",mAppointmentDetail.getBid())
                .add("ent_date_time",Utility.getCurrentGmtTime())
                .build();
        Utility.printLog("mRaiseInvoice request ent_sess_token:"+sessionManager.getSessionToken()+" ent_dev_id:"+sessionManager.getDeviceId()+" ent_timer:"+timeConsumedSecond+" ent_bid:"+mAppointmentDetail.getBid()+" ent_date_time:"+Utility.getCurrentGmtTime());
        Okhttp_connection.doOkhttpRequest(ServiceUrls.RAISE_INVOICE, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(mProgressDailog!=null && mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
                Utility.printLog("mRaiseInvoice response"+result);
               /* Gson gson=new Gson();
                UpdateMasterStatusResponsePojo updateMasterStatusResponsePojo=gson.fromJson(result,UpdateMasterStatusResponsePojo.class);*/

               /* if("0".equals(updateMasterStatusResponsePojo.getErrFlag()))
                {

                }
                else if("1".equals(updateMasterStatusResponsePojo.getErrFlag()))
                {
                    mShowErrorMessage(updateMasterStatusResponsePojo.getErrMsg(),false);
                }
                else
                {
                    finish();
                }*/
            }

            @Override
            public void onError(String error) {
                Utility.printLog("PK error"+error);
                if(mProgressDailog!=null && mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
            }
        });

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void getCurrentLocation()
    {
        if (locationUtil == null)
        {
            locationUtil = new LocationUtil(this, this);
        }
        else
        {
            locationUtil.checkLocationSettings();
        }
    }
    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void location_Error(String error) {

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mSetSelectedServices()
    {


        int size=mAppointmentDetail.getServices().size();

        if(size>0)
        {
            for(int i=0;i<size;i++)
            {
                LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.single_row_inflate_services,null);

                TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
                tvNojobSelected.setTypeface(font);

                TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
                tvSelectedServicePrice.setTypeface(font);

                TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
                tvSelectedService.setTypeface(font);

                LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);

                tvSelectedServicePrice.setText(AppConstants.CURRENCY+" "+mAppointmentDetail.getServices().get(i).getSprice());
                tvSelectedService.setText(mAppointmentDetail.getServices().get(i).getSname());
                tvNojobSelected.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);

                ll_service_Container.addView(view);
            }
        }else
        {
            LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_inflate_services,null);

            TextView tvNojobSelected= (TextView) view.findViewById(R.id.tvNoSelectedServices);
            tvNojobSelected.setTypeface(font);

            TextView tvSelectedServicePrice= (TextView) view.findViewById(R.id.tvSelectedServicePrice);
            TextView tvSelectedService= (TextView) view.findViewById(R.id.tvSelectedService);
            LinearLayout layout=(LinearLayout)view.findViewById(R.id.ll_service);
            tvNojobSelected.setVisibility(View.VISIBLE);
            layout.setVisibility(View.GONE);
            ll_service_Container.addView(view);
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void showBigImage(Uri url)
    {
        dialog=new Dialog(JobCompletedActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_imageview);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView bigImageView= (ImageView) dialog.findViewById(R.id.ivBigImage);

        Picasso.with(JobCompletedActivity.this)
                .load(url)
                .into(bigImageView);

        dialog.show();

    }
}
