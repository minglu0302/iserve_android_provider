package com.app.partner.iserve.TabActivities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.partner.iserve.CancelationDialog;
import com.app.partner.iserve.Pojo.AbortAppointmentResponse;
import com.app.partner.iserve.Pojo.GetBookedSlotDetailsResponse;
import com.app.partner.iserve.Pojo.SampleResponse;
import com.app.partner.iserve.Pojo.Slot;
import com.app.partner.iserve.Pojo.SlotDetails;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.ImageLoadedCallback;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.Scaler;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LaterBookingDetails extends AppCompatActivity {

    private ImageView ivBackbtn;
    private TextView mToolbar_title,tvCancel,tvTitleInvoice,tvTitleJobaccepted,tvTitleJobStarted,tvTitleOnTheWay,tvCustomerName,tvCustomerAddrs1,tvCustomerAddrs2,tvJobDetailsHeader,tvJobDetailsDescription,tvHeaderJobPhotos,tvSlotDate,tvSlotTime,tvSlotFreeText;
    private LinearLayout llCall,llMessage,llLocate,llSlotTime,ll_inflater,llNoSlot,ll_Full;
    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;
    private Slot mSlot;
    private String TAG="LaterBookingDetails";
    private Button btnDeleteSlot;
    private Typeface font,fontBold;
    private SimpleDateFormat sdf_time,sdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_later_booking_details);

        mSlot= (Slot) getIntent().getSerializableExtra("SlotDetails");
        sdf_time = new SimpleDateFormat("H:mm a");
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mInitLayoutID();
        if(mSlot!=null && !mSlot.getBooked().equals("1"))
        {
            mGetBookedSlotDetail(mSlot);
        }
        else
        {
            mToolbar_title.setText(getResources().getString(R.string.SLOT_DETAILS));
            tvCancel.setVisibility(View.GONE);
            llNoSlot.setVisibility(View.VISIBLE);
            ll_Full.setVisibility(View.GONE);
            btnDeleteSlot.setVisibility(View.VISIBLE);

            Date date = null;

            try {
                date=sdf.parse(mSlot.getSlotDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            tvSlotDate.setText(sdf.format(date));
            tvSlotTime.setText(mSlot.getStart_dt()+"-"+mSlot.getEnd_dt());

        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mInitLayoutID()
    {
        font=Utility.getFontRegular(LaterBookingDetails.this);
        fontBold=Utility.getFontBold(LaterBookingDetails.this);

        mProgressDailog=Utility.getProcessDialog(this);
        sessionManager=new SessionManager(LaterBookingDetails.this);

        ivBackbtn= (ImageView) findViewById(R.id.ivBackbtn);

        mToolbar_title= (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);

        tvCancel= (TextView) findViewById(R.id.tvCancel);
        tvCancel.setTypeface(font);

       /* tvTitleInvoice= (TextView) findViewById(R.id.tvTitleInvoice);
        tvTitleInvoice.setTypeface(font);

        tvTitleJobaccepted= (TextView) findViewById(R.id.tvTitleJobaccepted);
        tvTitleJobaccepted.setTypeface(font);

        tvTitleJobStarted= (TextView) findViewById(R.id.tvTitleJobStarted);
        tvTitleJobStarted.setTypeface(font);

        tvTitleOnTheWay= (TextView) findViewById(R.id.tvTitleOnTheWay);
        tvTitleOnTheWay.setTypeface(font);*/

        tvCustomerName= (TextView) findViewById(R.id.tvCustomerName);
        tvCustomerName.setTypeface(font);

        tvCustomerAddrs1= (TextView) findViewById(R.id.tvCustomerAddrs1);
        tvCustomerAddrs1.setTypeface(font);

        tvCustomerAddrs2= (TextView) findViewById(R.id.tvCustomerAddrs2);
        tvCustomerAddrs2.setTypeface(font);

        tvJobDetailsHeader= (TextView) findViewById(R.id.tvJobDetailsHeader);
        tvJobDetailsHeader.setTypeface(font);

        tvJobDetailsDescription= (TextView) findViewById(R.id.tvJobDetailsDescription);
        tvJobDetailsDescription.setTypeface(font);

        tvHeaderJobPhotos= (TextView) findViewById(R.id.tvHeaderJobPhotos);
        tvHeaderJobPhotos.setTypeface(font);

        tvSlotDate= (TextView) findViewById(R.id.tvSlotDate);
        tvSlotDate.setTypeface(font);

        tvSlotFreeText= (TextView) findViewById(R.id.tvSlotFreeText);
        tvSlotFreeText.setTypeface(font);

        tvSlotTime= (TextView) findViewById(R.id.tvSlotTime);
        tvSlotTime.setTypeface(font);

        llCall= (LinearLayout) findViewById(R.id.llCall);
        llMessage= (LinearLayout) findViewById(R.id.llMessage);
        llLocate= (LinearLayout) findViewById(R.id.llLocate);
        llSlotTime= (LinearLayout) findViewById(R.id.llSlotTime);
        ll_inflater= (LinearLayout) findViewById(R.id.ll_inflater);
        llNoSlot= (LinearLayout) findViewById(R.id.llNoSlot);
        ll_Full= (LinearLayout) findViewById(R.id.ll_Full);
        ll_Full.setVisibility(View.GONE);

        btnDeleteSlot=(Button)findViewById(R.id.btnDeleteSlot);
        btnDeleteSlot.setTypeface(fontBold);

        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnDeleteSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRemoveSlot();
            }
        });


    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mGetBookedSlotDetail(final Slot mSlot)
    {
        if(Utility.isNetworkAvailable(LaterBookingDetails.this))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }

            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_slot_id",mSlot.getId())
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GetBookedSlotDetail, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if (mProgressDailog != null && mProgressDailog.isShowing()) {
                        mProgressDailog.dismiss();
                    }

                    Utility.printLog("PK GetBookedSlotDetail response" + result);
                    Gson gson = new Gson();
                    GetBookedSlotDetailsResponse details = gson.fromJson(result, GetBookedSlotDetailsResponse.class);

                    if ("0".equals(details.getErrFlag()))
                    {
                        mSetValues(details);
                    }
                    else if("1".equals(details.getErrFlag())&&("132".equals(details.getErrNum())))
                    {
                        mToolbar_title.setText(getResources().getString(R.string.SLOT_DETAILS));
                        tvCancel.setVisibility(View.GONE);
                        llNoSlot.setVisibility(View.VISIBLE);
                        ll_Full.setVisibility(View.GONE);
                        btnDeleteSlot.setVisibility(View.VISIBLE);


                       // mShowErrorMessage(details.getErrMsg(),false);
                    }
                    else
                    {
                        mShowErrorMessage(details.getErrMsg(),false);
                    }
                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                }
            });
        }
        else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mGetBookedSlotDetail(mSlot);
                        }
                    }).show();
        }

    }


////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShowErrorMessage(String errMsg, final boolean finish)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(LaterBookingDetails.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                if(finish)
                {
                    //sessionManager.clearSession();
                    finish();
                }
                dialogInterface.dismiss();

            }
        });

        dialogBuilder.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mSetValues(final GetBookedSlotDetailsResponse details)
    {
        mToolbar_title.setText(getResources().getString(R.string.jobId)+" "+details.getData().getBid());
        tvCancel.setVisibility(View.VISIBLE);
        llNoSlot.setVisibility(View.GONE);
        btnDeleteSlot.setVisibility(View.GONE);
        ll_Full.setVisibility(View.VISIBLE);

        tvCustomerName.setText(details.getData().getFname());
        tvCustomerAddrs1.setText(details.getData().getAddress());

        String apptDate[]=details.getData().getAppt_date().toString().split(" ");
        tvSlotDate.setText(apptDate[0]);
        tvSlotTime.setText(mSlot.getStart_dt()+" - "+mSlot.getEnd_dt());

        llCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String uri = "tel:"+details.getData().getMobile() ;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
            }
        });
        llMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"+details.getData().getMobile()));
                startActivity(sendIntent);
            }
        });
        llLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String apntLat = details.getData().getAppt_lat();
                String apntLong = details.getData().getAppt_long();
                String muri = "google.navigation:q=" + apntLat + "," + apntLong;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(muri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });

        mAddJobPhotos(details.getData());

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAbortAppointment(details.getData());
            }
        });

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mAddJobPhotos(SlotDetails data){

        double size[]= Scaler.getScalingFactor(LaterBookingDetails.this);
        int no_of_images=0;
        if(!"".equals(data.getJob_images()))
        {
            no_of_images=Integer.parseInt(data.getJob_images());

            for(int i=0;i<no_of_images;i++)
            {
                LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.single_row_job_photos,null);

                ImageView imageView= (ImageView) view.findViewById(R.id.jobPhoto);
                imageView.setBackgroundResource(R.drawable.noimage);

                ProgressBar progressBar= (ProgressBar) view.findViewById(R.id.ProgressBar);
                // Picasso.with(JobAcceptedActivity.this).load().resize(size[])
           /* LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(5,5,5,5);
            imageView.setLayoutParams(layoutParams);*/
                ll_inflater.addView(view);

                String url= AppConstants.APPOINTMENT_JOB_IMAGES+data.getBid()+"_"+i+".png";
                Utility.printLog();
                int height=(int)size[0]*50;
                int width=(int)size[1]*50;
                try
                {

                    Picasso.with(this)
                            .load(url)
                            .resize(height,width)
                            .placeholder(R.drawable.noimage)
                            .into(imageView,new ImageLoadedCallback(progressBar));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }else
        {
            LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.single_row_job_photos,null);

            ImageView imageView= (ImageView) view.findViewById(R.id.jobPhoto);

            ProgressBar progressBar= (ProgressBar) view.findViewById(R.id.ProgressBar);

            TextView textView= (TextView) view.findViewById(R.id.tvNoImages);

            textView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

            ll_inflater.addView(view);
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mAbortAppointment(SlotDetails data)
    {
        CancelationDialog dialog=new CancelationDialog(LaterBookingDetails.this, new CancelationDialog.CancelationCallback() {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog(TAG+"PK response"+result);
                if(result!=null && !result.isEmpty())
                {
                    Gson gson=new Gson();
                    AbortAppointmentResponse response=gson.fromJson(result,AbortAppointmentResponse.class);

                    if(response.getErrFlag().equals("0"))
                    {
                        mShowErrorMessage(response.getErrMsg(),true);
                    }
                    else
                    {
                        mShowErrorMessage(response.getErrMsg(),false);
                    }
                }
            }

            @Override
            public void onError(String error) {
                Utility.printLog(TAG+"PK error response"+error);
            }
        });

        dialog.showCancellationDialog(data.getBid(),data.getCid());
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mRemoveSlot()
    {
        if(Utility.isNetworkAvailable(LaterBookingDetails.this))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }

            RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_slot_id",mSlot.getId())
                    .add("ent_pro_id",sessionManager.getProviderID())
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.REMOVE_SLOT, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {

                    Utility.printLog(TAG+" RemoveSlot response "+result);
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Gson gson=new Gson();
                    SampleResponse response=gson.fromJson(result,SampleResponse.class);

                    if(response.getErrFlag().equals("0"))
                    {
                        finish();
                    }
                    else
                    {
                        mShowErrorMessage(response.getErrMsg(),false);
                    }
                }

                @Override
                public void onError(String error) {

                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }

                }
            });
        }else
        {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            mRemoveSlot();
                        }
                    }).show();
        }
    }
}
