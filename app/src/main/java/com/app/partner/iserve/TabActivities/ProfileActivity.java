package com.app.partner.iserve.TabActivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import com.app.partner.iserve.Adapters.MyRecyclerViewAdater;
import com.app.partner.iserve.BuildConfig;
import com.app.partner.iserve.ForeGroundService;
import com.app.partner.iserve.ForgotpassNewPassActivity;
import com.app.partner.iserve.MainActivity;
import com.app.partner.iserve.Pojo.GetMasterProfile;
import com.app.partner.iserve.Pojo.UpdateApptStatusPojo;
import com.app.partner.iserve.R;
import com.app.partner.iserve.SplashActivity;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.CircleTransform;
import com.app.partner.iserve.Utility.InternalStorageContentProvider;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.RnuTimePermissionRequest;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Upload_file_AmazonS3;
import com.app.partner.iserve.Utility.Utility;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ProfileActivity extends AppCompatActivity {

    private ImageView ivBackbtn,ivProfilePic;
    private TextView
            tvProReviews,
            tvHeaderServiceCategories,
            tvHeaderAboutMe,
            tvHeaderExpertise,
            tvHeaderLanguages,
            tvHeaderJobPhotos,
            tvHeaderProReviews,
            tvDone,
            tvAddPhotos,
            tvProviderMail,
            tvVersion,
            tvHeaderEmail;

    private EditText tvProName,tvLanguages,tvExpertise,tvAboutMe;
    private RatingBar rating;
    private Button btnLogout,btnChangePass;
    private ProgressDialog mProgressDailog,mProgressDailog1;
    private SessionManager sessionManager;
    private String TAG="ProfileActivity";
    private LinearLayout ll_container_service_category;
    private RecyclerView sv_horizontal;
    private MyRecyclerViewAdater adater;
    private ArrayList<Uri> imageUriList;
    private final int REQUEST_CODE_GALLERY = 0x1;
    private final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    private final int REQUEST_CODE_CROP_IMAGE = 0x3;
    private File mFileTemp;
    private ImageView imageView;
    private static final int PERMISSION_REQUEST_CODE=103;
    private RnuTimePermissionRequest rnuTimePermissionRequest;
    private String[] permissions;
    private String mobileNumber="";
    private GetMasterProfile getMasterProfile=null;
    private String currentVersion;
    private boolean isPicturetaken=false;
    private boolean fromProfile=false;
    private ViewGroup viewGroup;
    private String profileImage="";
    private LinearLayout tvServiceCat;
    Upload_file_AmazonS3 amazonS3;
    private boolean reqCameraAndStoragePermision;
    private Typeface font;
    private Typeface fontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        sessionManager=new SessionManager(this);
        mProgressDailog= Utility.getProcessDialog(this);
        mProgressDailog1=Utility.getProcessDialog(this);
        amazonS3 = Upload_file_AmazonS3.getInstance(ProfileActivity.this, AppConstants.COGNITO_POOL_ID);
        initLayoutId();

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initLayoutId()
    {

        font=Utility.getFontRegular(ProfileActivity.this);
        fontBold=Utility.getFontBold(ProfileActivity.this);

        rnuTimePermissionRequest =new RnuTimePermissionRequest(ProfileActivity.this);
        permissions= new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};

        ivBackbtn=(ImageView)findViewById(R.id.ivBackbtn);
        ivProfilePic=(ImageView)findViewById(R.id.ivProfilePic);

        rating=(RatingBar)findViewById(R.id.invoice_driver_rating);
        rating.setEnabled(false);

        tvProName=(EditText) findViewById(R.id.tvProName);
        tvProName.setTypeface(fontBold);

        tvProReviews=(TextView)findViewById(R.id.tvProReviews);
        tvProReviews.setTypeface(font);

        tvHeaderServiceCategories=(TextView)findViewById(R.id.tvHeaderServiceCategories);
        tvHeaderServiceCategories.setTypeface(font);

        tvHeaderAboutMe=(TextView)findViewById(R.id.tvHeaderAboutMe);
        tvHeaderAboutMe.setTypeface(font);

        tvAboutMe=(EditText)findViewById(R.id.tvAboutMe);
        tvAboutMe.setTypeface(font);

        tvHeaderExpertise=(TextView)findViewById(R.id.tvHeaderExpertise);
        tvHeaderExpertise.setTypeface(font);

        tvHeaderLanguages=(TextView)findViewById(R.id.tvHeaderLanguages);
        tvHeaderLanguages.setTypeface(font);

        tvLanguages=(EditText)findViewById(R.id.tvLanguages);
        tvLanguages.setTypeface(font);

        tvHeaderJobPhotos=(TextView)findViewById(R.id.tvHeaderJobPhotos);
        tvHeaderJobPhotos.setTypeface(font);

       /* tvHeaderProReviews=(TextView)findViewById(R.id.tvHeaderProReviews);
        tvHeaderProReviews.setTypeface(font);*/

        tvServiceCat=(LinearLayout)findViewById(R.id.tvServiceCat);

        tvExpertise=(EditText)findViewById(R.id.tvExpertise);
        tvExpertise.setTypeface(font);

        tvProviderMail=(TextView)findViewById(R.id.tvProviderMail);
        tvDone=(TextView)findViewById(R.id.tvDone);
        tvVersion=(TextView)findViewById(R.id.tvVersion);

        btnLogout=(Button)findViewById(R.id.btnLogout);
        btnLogout.setTypeface(fontBold);

        btnChangePass=(Button)findViewById(R.id.btnChangePass);
        btnChangePass.setTypeface(fontBold);

        tvAddPhotos=(TextView)findViewById(R.id.tvAddPhotos);
        tvAddPhotos.setTypeface(font);

        tvHeaderEmail=(TextView)findViewById(R.id.tvHeaderEmail);
        tvHeaderEmail.setTypeface(font);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                AlertDialog.Builder builder=Utility.getAlertDialogBuilder(ProfileActivity.this);
                builder.setTitle("Alert:");
                builder.setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mLogoutUser();
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getMasterProfile!=null)
                {
                    Intent intent=new Intent(ProfileActivity.this,ForgotpassNewPassActivity.class);
                    intent.putExtra("MOBILE",mobileNumber);
                    intent.putExtra("FROM","2");
                    startActivity(intent);
                }

            }
        });



        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager)ProfileActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if(tvDone.getText().toString().equals("EDIT"))
                {
                    setFocusForEditTexts(true);
                    tvAddPhotos.setTextColor(getResources().getColor(R.color.shark));
                    tvDone.setText("SAVE");
                    tvProName.requestFocus(View.FOCUS_RIGHT);
                }
                else if(tvDone.getText().toString().equals("SAVE"))
                {
                    if(!tvProName.getText().toString().trim().equals(""))
                    {
                        setFocusForEditTexts(false);
                        tvAddPhotos.setTextColor(getResources().getColor(R.color.silver));
                        adater.setAddOptionEnabled(false);
                        adater.notifyDataSetChanged();
                        tvAddPhotos.setText("add");
                        tvDone.setText("EDIT");


                        updateMasterProfile();
                    }
                    else
                    {

                        tvProName.requestFocus(View.FOCUS_RIGHT);
                        Toast.makeText(ProfileActivity.this,getResources().getString(R.string.Name_should_not_be_empty), Toast.LENGTH_SHORT).show();
                    }

                    // updateMasterProfile(imageUriList.size());
                }
            }
        });

        sv_horizontal = (RecyclerView) findViewById(R.id.sv_horizontal);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        sv_horizontal.setLayoutManager(linearLayoutManager);

        imageUriList=new ArrayList<>();
        adater = new MyRecyclerViewAdater(this, imageUriList);



        setFocusForEditTexts(false);
        try
        {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        tvVersion.setText("Version "+currentVersion);

        viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstants.isTabService) {
            mGetMasterProfile();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mGetMasterProfile()
    {
        if(Utility.isNetworkAvailable(ProfileActivity.this))
        {
            mProgressDailog.show();

            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_user_type",""+1)
                    .add("ent_date_time",Utility.getCurrentGmtTime())
                    .build();
            Utility.printReq(requestBody,"getMasterProfile Request");

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_MASTER_PROFILE, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog(TAG+" getMasterProfile response"+result);
                    Gson gson=new Gson();
                    getMasterProfile=gson.fromJson(result,GetMasterProfile.class);

                    if("0".equals(getMasterProfile.getErrFlag()) && "21".equals(getMasterProfile.getErrNum()))
                    {
                        AppConstants.isTabService = false;
                        //got details
                        setProfileValues(getMasterProfile);
                    }
                    else if("6".equals(getMasterProfile.getErrNum())&&"1".equals(getMasterProfile.getErrFlag()))
                    {
                        //session expired
                        mShowErrorMessage(getMasterProfile.getErrMsg(),true);
                    }
                    else if("83".equals(getMasterProfile.getErrNum())&&"1".equals(getMasterProfile.getErrFlag()))
                    {
                        //logged in from other device
                        mShowErrorMessage(getMasterProfile.getErrMsg(),true);
                    }
                }

                @Override
                public void onError(String error)
                {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog(TAG+" getMasterProfile error"+error);
                }
            });
        }
        else
        {
            Snackbar.make(viewGroup, getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            if (AppConstants.isTabService) {
                                mGetMasterProfile();
                            }
                        }
                    }).show();
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void setProfileValues(GetMasterProfile getMasterProfile)
    {
        int width= 150;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            width = ivProfilePic.getMinimumWidth();
        }
        int hieght= 150 ;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            hieght = ivProfilePic.getMinimumHeight();
        }

        mobileNumber=getMasterProfile.getMobile();

        tvProName.setText(String.format("%s %s", getMasterProfile.getfName(), getMasterProfile.getlName()));
        Log.i("driver rat","is "+getMasterProfile.getAvgRate());

        rating.setRating(Float.parseFloat(getMasterProfile.getAvgRate()));
        tvAboutMe.setText(getMasterProfile.getAbout());

        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.thMain.setCurrentTab(0);
            }
        });

        if(getMasterProfile.getpPic()!=null && !getMasterProfile.getpPic().isEmpty())
        {

            String url=getMasterProfile.getpPic();
            if(url.contains("amazonaws"))
            {
                isPicturetaken=true;
                profileImage=url;
            }

            Picasso.with(ProfileActivity.this)
                    .load(url)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.pro_photo)
                    //.resize(width,hieght)
                    .into(ivProfilePic);
        }

        tvExpertise.setText(getMasterProfile.getExpertise());

        tvLanguages.setText(getMasterProfile.getLanguages());

        mAddServiceCategories(getMasterProfile);

        tvProReviews.setText(getMasterProfile.getTotRats()+" Reviews");

        tvProviderMail.setText(getMasterProfile.getEmail());

        if(getMasterProfile.getJob_images()!=null)
        {
            int size=getMasterProfile.getJob_images().size();
            imageUriList.clear();
            for (int i=0;i<size;i++)
            {
                //imageUriList.add(Uri.parse(AppConstants.PROFILE_JOB_IMAGES+sessionManager.getProviderID()+"_"+i+".png"));
                if(!getMasterProfile.getJob_images().get(i).isEmpty())
                {
                    imageUriList.add(Uri.parse(getMasterProfile.getJob_images().get(i)));
                }
            }
            adater.setImageFile(imageUriList);
        }
        sv_horizontal.setAdapter(adater);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mAddServiceCategories(final GetMasterProfile getMasterProfile)
    {
        tvServiceCat.removeAllViews();
        for(int i=0;i<getMasterProfile.getCatArray().size();i++)
        {
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView tv=new TextView(this);
            tv.setLayoutParams(lparams);
            tv.setPadding(10,10,10,10);
            tv.setTypeface(font);
            tv.setTextColor(getResources().getColor(R.color.shark));
            tv.setAllCaps(true);
            tv.setText(getMasterProfile.getCatArray().get(i).getCat_name());

            final int finalI = i;
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utility.printLog(TAG+"Category "+getMasterProfile.getCatArray().get(finalI).getCat_name()+" ID: "+getMasterProfile.getCatArray().get(finalI).getCat_id());
                    Intent intent=new Intent(ProfileActivity.this,CategoryDetails.class);
                    intent.putExtra("CAT_ID",getMasterProfile.getCatArray().get(finalI).getCat_id());
                    startActivity(intent);
                }
            });

            tvServiceCat.addView(tv);
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mLogoutUser()
    {
        mProgressDailog.setMessage("Logging out...");
        mProgressDailog.show();

        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_user_type",""+1)
                .add("ent_date_time",Utility.getCurrentGmtTime())
                .build();

        Okhttp_connection.doOkhttpRequest(ServiceUrls.LOGOUT, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(mProgressDailog!=null && mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
                Utility.printLog(TAG+" logout response"+result);
                Gson gson=new Gson();
                UpdateApptStatusPojo logoutResponse=gson.fromJson(result,UpdateApptStatusPojo.class);
                if("0".equals(logoutResponse.getErrFlag()))
                {
                    if("29".equals(logoutResponse.getErrNum()))
                    {
                        mStartLogout();
                    }
                }
                else if("1".equals(logoutResponse.getErrFlag()))
                {
                    mShowErrorMessage(logoutResponse.getErrMsg(),false);
                }

            }

            @Override
            public void onError(String error)
            {
                if(mProgressDailog!=null && mProgressDailog.isShowing()){
                    mProgressDailog.dismiss();
                }
                Utility.printLog(TAG+" logout error"+error);
            }
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mStartLogout()
    {
        sessionManager.clearSession();

        //stopService(new Intent(ProfileActivity.this, MyService.class));
        Intent stopIntent = new Intent(ProfileActivity.this, ForeGroundService.class);
        stopIntent.setAction(AppConstants.ACTION.STOPFOREGROUND_ACTION);
        startService(stopIntent);

        Intent intent=new Intent(ProfileActivity.this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void mShowErrorMessage(String errMsg, final boolean logout)
    {
        final AlertDialog.Builder dialogBuilder = Utility.getAlertDialogBuilder(ProfileActivity.this);
        dialogBuilder.setTitle(getResources().getString(R.string.alert));
        dialogBuilder.setMessage(errMsg);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                if(logout)
                {
                    sessionManager.clearSession();
                    Intent intent=new Intent(ProfileActivity.this,SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                dialogInterface.dismiss();

            }
        });

        dialogBuilder.show();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public void chooseImageOperation(int position, ImageView iv_add_image)
    {
        final Dialog mDialog = new Dialog(ProfileActivity.this);
        // mDialog.setTitle("Select photo from :");
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        mDialog.setContentView(R.layout.profile_pic_options);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));


        Button btnCamera = (Button) mDialog.findViewById(R.id.camera);
        Button btnCancel = (Button) mDialog.findViewById(R.id.cancel);
        Button btnGallery = (Button) mDialog.findViewById(R.id.gallery);
        Button btnRemove = (Button) mDialog.findViewById(R.id.removephoto);
        View line=  mDialog.findViewById(R.id.line3);

        String state = Environment.getExternalStorageState();
        String filename;
        if(position==-1)
        {
            if(!isPicturetaken)
            {
                btnRemove.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
            }
            else
            {
                btnRemove.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
            }
            filename=AppConstants.TEMP_PHOTO_FILE_NAME+System.currentTimeMillis()+".png";
        }
        else
        {
            filename=sessionManager.getProviderID()+"_"+position+".png";
            btnRemove.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
            imageView=iv_add_image;
        }


        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                takePicture();
                mDialog.dismiss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                openGallery();
                mDialog.dismiss();
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ivProfilePic.setImageResource(R.drawable.pro_photo);
                isPicturetaken=false;
                profileImage="";
                mDialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mDialog.dismiss();
            }
        });



        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), filename);
        }
        else
        {
            mFileTemp = new File(getFilesDir(),filename);
        }


        mDialog.show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void takePicture()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider",mFileTemp);
            }
            else
            {
                if (Environment.MEDIA_MOUNTED.equals(state))
                {
                    mImageCaptureUri = Uri.fromFile(mFileTemp);
                }
                else
                {
                    mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
                }
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

            Log.d("error", "cannot take picture", e);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {

            return;
        }

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:

                try {
                    InputStream inputStream = getContentResolver().openInputStream(
                            data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(
                            mFileTemp);
                    Log.d("", "inputStream" + inputStream);
                    Log.d("", "fileOutputStream" + fileOutputStream);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                    Log.d("", "Error while creating temp file", e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;

            case REQUEST_CODE_CROP_IMAGE:

                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                Log.d("", "path fileOutputStream " + path);

                if (path == null) {

                    return;
                }

                /*Bitmap bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                imageView.setImageBitmap(bitmap);*/
                mUploadImageToBucket();


                // isPicturetaken=true;
                break;

        }

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void startCropImage()
    {
        Utility.printLog("Strat crop");
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mUploadImageToBucket()
    {
        mProgressDailog1.setMessage("Uploading...");
        mProgressDailog1.show();
        Toast.makeText(ProfileActivity.this, "Uploading", Toast.LENGTH_SHORT).show();

        Upload_file_AmazonS3 amazonS3 = Upload_file_AmazonS3.getInstance(ProfileActivity.this, AppConstants.COGNITO_POOL_ID);

        // final String folderFileName = AppConstants.AMZAON_S3_PROFILE_PICTURE+fileName;
        amazonS3.UploadFile(AppConstants.BUCKET,AppConstants.BUCKET_JOB_IMAGES+mFileTemp.getName(), mFileTemp, new Upload_file_AmazonS3.Upload_CallBack() {
            @Override
            public void sucess(String url)
            {
                String mAmazonImgUrl=AppConstants.AMAZON_BASE_URL+AppConstants.BUCKET+"/"+AppConstants.BUCKET_JOB_IMAGES+mFileTemp.getName();
                Log.d("Amazon response",url);
                mProgressDailog1.dismiss();
                if(!fromProfile)
                {
                    Log.d(TAG, "mUploadImageToBucket: "+mAmazonImgUrl);
                    imageUriList.add(Uri.parse(mAmazonImgUrl));
                    adater.notifyDataSetChanged();
                }
                else
                {
                    fromProfile=false;
                    try {
                        profileImage=mAmazonImgUrl;
                        Picasso.with(ProfileActivity.this)
                                .load(mAmazonImgUrl)
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.pro_photo)
                                .into(ivProfilePic);
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void error(String errormsg)
            {
                Log.d("Amazon response","error");
                mProgressDailog1.dismiss();
            }
        });

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void updateMasterProfile()
    {

        mProgressDailog.setMessage("Loading...");
        mProgressDailog.show();

        String jobImages ="";

        for(int i=0;i<imageUriList.size();i++)
        {
            jobImages=jobImages+imageUriList.get(i).toString()+",";
        }
        if(!jobImages.isEmpty())
        {
            jobImages=jobImages.substring(0,jobImages.length()-1);
        }

        Utility.printLog(TAG+" updatemasterprofile req JobImages= "+jobImages);
                /*if(no_of_imgs==0)
                {*/
        RequestBody requestBody=new FormEncodingBuilder()
                .add("ent_sess_token",sessionManager.getSessionToken())
                .add("ent_dev_id",sessionManager.getDeviceId())
                .add("ent_date_time",Utility.getCurrentGmtTime())
                .add("ent_about",tvAboutMe.getText().toString().toString().trim())
                .add("ent_name",tvProName.getText().toString().toString().trim())
                .add("ent_lang",tvLanguages.getText().toString().toString().trim())
                .add("ent_expertise",tvExpertise.getText().toString().toString().trim())
                .add("ent_profile_img",profileImage)
                .add("ent_job_img",jobImages)
                .build();
               /* }else
                {
                         requestBody=new FormEncodingBuilder()
                        .add("ent_sess_token",sessionManager.getSessionToken())
                        .add("ent_dev_id",sessionManager.getDeviceId())
                        .add("ent_date_time",Utility.getCurrentGmtTime())
                        .add("ent_about",tvAboutMe.getText().toString())
                        .add("ent_name",tvProName.getText().toString())
                        .add("ent_lang",tvLanguages.getText().toString())
                        .add("ent_expertise",tvExpertise.getText().toString())
                        .add("ent_profile_img",profileImage)
                        .add("ent_num_job_img",imageUriList.size()+"")
                        .build();
                }*/


        Utility.printReq(requestBody,"Updatemasterprofile req ");

        Okhttp_connection.doOkhttpRequest(ServiceUrls.UPDATE_MASTER_PROFILE, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                AppConstants.isTabService = true;
                Utility.printLog(TAG+" updatemasterprofile response "+result);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
                JSONObject jsonObject;
                if(result!=null && !result.isEmpty()){
                    try
                    {
                        jsonObject= new JSONObject(result);
                        if(jsonObject.toString().contains("errFlag")){
                            if(jsonObject.get("errFlag").equals("0"))
                            {
                                Toast.makeText(ProfileActivity.this,jsonObject.get("errMsg").toString(),Toast.LENGTH_SHORT).show();
                                // mGetMasterProfile();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String error)
            {
                Utility.printLog(TAG+" updatemasterprofile error "+error);
                if(mProgressDailog!=null && mProgressDailog.isShowing())
                {
                    mProgressDailog.dismiss();
                }
            }
        });


    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode){
            case PERMISSION_REQUEST_CODE :

                if(grantResults.length>0){

                    for(int i=0;i<grantResults.length;i++){
                        Log.d("ORPresult grantResults"," "+grantResults[i]);
                        if(grantResults[i]!= PackageManager.PERMISSION_GRANTED)
                        {
                            mShoudHavePermission(permissions[i]);
                        }
                    }
                }
                else
                {
                    showMessage("",2,"SETTINGS","CANCEL","You need to allow access for some permission.");
                }
                break;
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void mShoudHavePermission(String permission)
    {
        Log.d("showrational","called "+permission);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if(shouldShowRequestPermissionRationale(permission))
            {
                showMessage(permission,1,"OK","CANCEL","You need to allow access for "+permission+" permission.");
            }
            else
            {
                showMessage(permission,2,"SETTINGS","EXIT APP","You need to allow access for "+permission+" permission.");
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private void showMessage(final String permission,final int dialog,String positiveButton,String negativeButton,String message)
    {
        new android.support.v7.app.AlertDialog.Builder((this))
                .setMessage(message)
                .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(dialog==1)
                        {
                            rnuTimePermissionRequest.mRequestPermissions(new String[]{permission},8080);
                        }
                        else if(dialog==2)
                        {
                            if (ProfileActivity.this == null)
                            {
                                return;
                            }
                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + ProfileActivity.this.getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                            finish();
                        }

                    }
                })
                .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create()
                .show();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setFocusForEditTexts(boolean focus)
    {
        tvExpertise.setFocusableInTouchMode(focus);
        tvExpertise.setFocusable(focus);

        tvAboutMe.setFocusableInTouchMode(focus);
        tvAboutMe.setFocusable(focus);

        tvLanguages.setFocusableInTouchMode(focus);
        tvLanguages.setFocusable(focus);

        tvProName.setFocusableInTouchMode(focus);
        tvProName.setFocusable(focus);

        tvAddPhotos.setFocusableInTouchMode(focus);
        tvAddPhotos.setFocusable(focus);

        if(!focus)
        {
            tvAddPhotos.setOnClickListener(null);
        }
        else
        {
            tvAddPhotos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(tvAddPhotos.getText().toString().equalsIgnoreCase("add"))
                    {
                        if(!rnuTimePermissionRequest.mCheckPermission(permissions))
                        {
                            rnuTimePermissionRequest.mRequestPermissions(permissions,PERMISSION_REQUEST_CODE);
                        }
                        else
                        {
                            adater.setAddOptionEnabled(true);
                            adater.notifyDataSetChanged();
                            tvAddPhotos.setText("done");
                        }

                    }else
                    {
                        adater.setAddOptionEnabled(false);
                        adater.notifyDataSetChanged();
                        tvAddPhotos.setText("add");
                        //  updateMasterProfile();
                    }
                }
            });
        }

        if(!focus)
        {
            ivProfilePic.setOnClickListener(null);
        }
        else
        {
            ivProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fromProfile=true;
                    chooseImageOperation(-1,ivProfilePic);
                }
            });
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void removeImageUri(int pos)
    {
        //amazonS3.delete_Item(AppConstants.BUCKET_NAME, imageUriList.get(pos).toString());
        imageUriList.remove(pos);
        updateMasterProfile();
    }


   /* private void myCameraAndStorageReq() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !(checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            Log.d("App_is_in_fb12",
                    shouldShowRequestPermissionRationale(CAMERA) + "\t" + checkSelfPermission(CAMERA) + "\t" + PackageManager.PERMISSION_GRANTED
                            + "\n" + shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE) + "\t" + checkSelfPermission(READ_EXTERNAL_STORAGE) + "\t" + PackageManager.PERMISSION_GRANTED
                            + "\n" + shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE) + "\t" + checkSelfPermission(WRITE_EXTERNAL_STORAGE) + "\t" + PackageManager.PERMISSION_GRANTED
            );

            if (shouldShowRequestPermissionRationale(CAMERA)
                    || shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)
                    || shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {

                requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

            } else {

                if (!reqCameraAndStoragePermision) {
                    reqCameraAndStoragePermision = true;
                    requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                } else {
                    Toast.makeText(ProfileActivity.this, "Permission sikkil pa", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        Utility.printLog(TAG+" onPause called");
        InputMethodManager imm = (InputMethodManager)ProfileActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tvProName.getWindowToken(), 0);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
