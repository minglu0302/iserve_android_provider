package com.app.partner.iserve.TabActivities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.partner.iserve.Adapters.ScheduleAdapter;
import com.app.partner.iserve.Address.AddSlotsActivity;
import com.app.partner.iserve.Pojo.GetAllSlotResponse;
import com.app.partner.iserve.Pojo.Slot;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.example.calenderpk.compactcalendarview.CompactCalendarView;
import com.example.calenderpk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ScheduleActivity extends AppCompatActivity
{

    private TextView tvAddSlot,mMonthTitle,tvTitle;
    private CompactCalendarView compactCalendarView;
    private SessionManager sessionManager;
    private Calendar calendar;
    private SimpleDateFormat format,sdfDateMonth;
    private ImageView ivPrevBtn,ivnextBtn;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private ProgressDialog mProgressDailog;
    private GetAllSlotResponse mGetAllSlotResponse;
    private ArrayList<Slot> mDataset=new ArrayList<>();
    private List<Event> mSlotEventList;
    private ArrayList<Slot> mutableSlots;
    private RecyclerView slots_listview;
    private RecyclerView.Adapter scheduleAdapter;
    private LinearLayout ll_root;
    private GestureDetector gestureDetector;
    private ViewGroup viewGroup;
    private String TAG="ScheduleActivity";
    private CompactCalendarView.CompactCalendarViewListener myCompactCalendarViewListener;
    private Typeface font,fontBold;
    private boolean isCalenderHidden=false;

    String lastDate;

    private BroadcastReceiver receiver;
    private IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        sessionManager=new SessionManager(this);
        calendar= Calendar.getInstance();
        format = new SimpleDateFormat("yyyy-MM-dd");
        sdfDateMonth = new SimpleDateFormat("yyyy-MM");
        initLayoutId();

        filter = new IntentFilter();
        filter.addAction("com.phix.provider.refreshBooking");

        receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                if(intent.getStringExtra("action").equals("1"))
                {
                    Log.d("Schdule", "onReceive: called");
                    mGetSlots(lastDate);
                }
            }
        };


    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    private void initLayoutId()
    {

        font= Utility.getFontRegular(ScheduleActivity.this);
        fontBold=Utility.getFontBold(ScheduleActivity.this);

        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        ivPrevBtn=(ImageView)findViewById(R.id.ivPrevBtn);
        ivnextBtn=(ImageView)findViewById(R.id.ivnextBtn);

        tvAddSlot=(TextView)findViewById(R.id.tvAddSlot);
        tvAddSlot.setTypeface(fontBold);

        mMonthTitle=(TextView)findViewById(R.id.mMonthTitle);
        mMonthTitle.setTypeface(font);

        tvTitle=(TextView)findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fontBold);

        mMonthTitle.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        slots_listview=(RecyclerView)findViewById(R.id.slots_listview);
        mutableSlots=new ArrayList<>();
        scheduleAdapter=new ScheduleAdapter(mutableSlots,ScheduleActivity.this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        slots_listview.setLayoutManager(llm);
        slots_listview.setAdapter(scheduleAdapter);

        tvAddSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(ScheduleActivity.this,AddSlotsActivity.class);
                startActivity(intent);
            }
        });

        ivPrevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compactCalendarView.showPreviousMonth();
            }
        });

        ivnextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compactCalendarView.showNextMonth();
            }
        });



        myCompactCalendarViewListener=new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> eventList = compactCalendarView.getEvents(dateClicked);
                mutableSlots.clear();

                for(Event slot : eventList){
                    mutableSlots.add((Slot) slot.getData());
                }
				
				Collections.sort(mutableSlots, new Comparator<Slot>() {
                    @Override
                    public int compare(Slot o1, Slot o2) {
                        return o1.getStart_dt().compareTo(o2.getStart_dt());
                    }
                });
                scheduleAdapter.notifyDataSetChanged();
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth)
            {
                mMonthTitle.setText(dateFormatForMonth.format(firstDayOfNewMonth));

                String date=sdfDateMonth.format(firstDayOfNewMonth);

                lastDate = date;

                mGetSlots(date);
            }
        };

        compactCalendarView.setListener(myCompactCalendarViewListener);
        GestureDetector.SimpleOnGestureListener simpleOnGestureListener
                = new GestureDetector.SimpleOnGestureListener(){


            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                   float velocityY) {
                try
                {
                    String swipe = "";
                    float sensitvity = 50;

                    // TODO Auto-generated method stub
                    if((e1.getX() - e2.getX()) > sensitvity){
                        swipe += "Swipe Left\n";
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();

                    }else if((e2.getX() - e1.getX()) > sensitvity){
                        swipe += "Swipe Right\n";
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }else{
                        swipe += "\n";
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }

                    if((e1.getY() - e2.getY()) > sensitvity){
                        swipe += "Swipe Up\n";
                        if(!isCalenderHidden)
                        {
                            compactCalendarView.hideCalendarWithAnimation();
                            isCalenderHidden=true;
                        }


                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }else if((e2.getY() - e1.getY()) > sensitvity){
                        swipe += "Swipe Down\n";
                        if(isCalenderHidden)
                        {
                            compactCalendarView.showCalendarWithAnimation();
                            isCalenderHidden=false;
                        }
                        //Toast.makeText(HistoryActivity.this, swipe, Toast.LENGTH_SHORT).show();
                    }else{
                        swipe += "\n";
                    }

                    //gestureEvent.setText(swipe);


                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return super.onFling(e1, e2, velocityX, velocityY);
            }
        };

        gestureDetector = new GestureDetector(simpleOnGestureListener);

        slots_listview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onResume() {
        super.onResume();

        AppConstants.isTabService = true;
        InputMethodManager imm = (InputMethodManager)ScheduleActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);

        String date=sdfDateMonth.format(calendar.getTime());
        Utility.printLog(TAG+" Getting slots for "+date);
        lastDate = date;

        mGetSlots(date);

        if(receiver != null)
        {
            registerReceiver(receiver,filter);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        if(receiver!=null)
        {
            unregisterReceiver(receiver);
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mGetSlots(String month)
    {
        if(Utility.isNetworkAvailable(ScheduleActivity.this))
        {
            mProgressDailog= Utility.getProcessDialog(ScheduleActivity.this);
            if(mProgressDailog!=null && !mProgressDailog.isShowing())
            {
                mProgressDailog.show();
            }

            RequestBody requestBody=new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_pro_id",sessionManager.getProviderID())
                    .add("ent_month",month)
                    .build();
            Utility.printReq(requestBody,"GET_ALL_SLOT req ");

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_ALL_SLOT, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    Utility.printLog("GET_ALL_SLOT resonse "+result);
                    if(mProgressDailog!=null && mProgressDailog.isShowing())
                    {
                        mProgressDailog.dismiss();
                    }
                    Gson gson=new Gson();
                    mGetAllSlotResponse=gson.fromJson(result,GetAllSlotResponse.class);
                    setmGetAllSlotResponseHandler(mGetAllSlotResponse);


                }

                @Override
                public void onError(String error) {
                    Utility.printLog("GET_ALL_SLOT error "+error);
                    mProgressDailog.dismiss();
                }
            });
        }else
        {
            Snackbar.make(viewGroup, getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            String date=sdfDateMonth.format(calendar.getTime());
                            Utility.printLog(TAG+" Getting slots for "+date);
                            mGetSlots(date);
                        }
                    })
                    .show();
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setmGetAllSlotResponseHandler(GetAllSlotResponse mGetAllSlotResponse)
    {
        int slots=mGetAllSlotResponse.getSlots().size();
        if(slots>0)
        {
            mDataset.clear();
            for(int i=0;i<slots;i++)
            {
                int slotSize=mGetAllSlotResponse.getSlots().get(i).getSlot().size();

                for(int j=0;j<slotSize;j++)
                {
                    Slot slot=mGetAllSlotResponse.getSlots().get(i).getSlot().get(j);
                    slot.setSlotDate(mGetAllSlotResponse.getSlots().get(i).getDate());
                    mDataset.add(slot);
                }
            }

            setCalenderView(mDataset);
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setCalenderView(ArrayList<Slot> mDataset)
    {
        compactCalendarView.removeAllEvents();

        if(mDataset.size()>0)
        {
            mSlotEventList=new ArrayList<>();
            for(int i=0;i<mDataset.size();i++)
            {
                Date date;
                long timeInMills = 0;

                try
                {
                    date=format.parse(mDataset.get(i).getSlotDate());
                    timeInMills=date.getTime();
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }

                mSlotEventList.add(new Event(Color.argb(255, 169, 68, 65), timeInMills,mDataset.get(i)));

                Utility.printLog("PK EVENT "+timeInMills);


            }

            compactCalendarView.addEvents(mSlotEventList);
            scheduleAdapter.notifyDataSetChanged();
            myCompactCalendarViewListener.onDayClick(new Date());

        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

   /* @Override
    public boolean onTouchEvent(MotionEvent event) {
        Utility.printLog("GestureDetector onTouchEvent called");
        return gestureDetector.onTouchEvent(event);
    }*/

////////////////////////////////////////////////////////////////////////////////////////////////////


}
