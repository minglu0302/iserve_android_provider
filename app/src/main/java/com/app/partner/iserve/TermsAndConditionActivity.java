package com.app.partner.iserve;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Utility;

public class TermsAndConditionActivity extends AppCompatActivity {

    private LinearLayout ll_terms,ll_Privacy;
    private ImageView ivBackbtn;
    private TextView tvTerms,tvPolicy,mToolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);

        ll_terms=(LinearLayout)findViewById(R.id.ll_terms);
        ll_Privacy=(LinearLayout)findViewById(R.id.ll_Privacy);
        ivBackbtn=(ImageView)findViewById(R.id.ivBackbtn);
        tvTerms=(TextView)findViewById(R.id.tvTerms);
        tvPolicy=(TextView)findViewById(R.id.tvPolicy);
        mToolbar_title=(TextView)findViewById(R.id.mToolbar_title);
        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.os.Bundle bundle=new android.os.Bundle();
                bundle.putString("TEXTSTRING", "Terms");
                bundle.putString("LINK", AppConstants.TERMS_AND_CONDITION);
                Intent intent=new Intent(TermsAndConditionActivity.this,WebViewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        ll_Privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.os.Bundle bundle=new android.os.Bundle();
                bundle.putString("TEXTSTRING", "Policy");
                bundle.putString("LINK", AppConstants.PRIVACY_POLICY);
                Intent intent=new Intent(TermsAndConditionActivity.this,WebViewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        Typeface font= Utility.getFontRegular(TermsAndConditionActivity.this);
        Typeface fontBold=Utility.getFontBold(TermsAndConditionActivity.this);

        tvTerms.setTypeface(font);
        tvPolicy.setTypeface(font);
        mToolbar_title.setTypeface(fontBold);
    }
}
