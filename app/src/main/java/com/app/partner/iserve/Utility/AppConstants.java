package com.app.partner.iserve.Utility;

/**
 * Created by embed on 1/3/16.
 */
public class AppConstants
{
 // public static final String TEMP_PHOTO_FILE_NAME = "povider_profile";
 public static final String TEMP_PHOTO_FILE_NAME = "povider_profile";

 public static final String TEMP_SIGNATURE_FILE_NAME = "Signature";

 public static final String TERMS_AND_CONDITION = "";

 public static final String PRIVACY_POLICY = "";

 public static final String COGNITO_POOL_ID="";

 public static final String BUCKET="";
 public static final String BUCKET_INVOICE_SIGNATURE="Invoice/";
 public static final String BUCKET_JOB_IMAGES="jobImages/";
 public static final String BUCKET_PROFILE_PICTURE="ProfileImages/";
 public static final String BUCKET_ID_PROOF_PICTURE="id_proof/";
 public static final String AMAZON_BASE_URL = "https://s3.amazonaws.com/";

 public static final String APPOINTMENT_JOB_IMAGES="";

 public static final String CURRENCY="$";

 public static int STATUS_ON_THE_WAY=5;

 public static int STATUS_ACCEPTED=2;

 public static int STATUS_REJECTED=3;

 public static int STATUS_I_HAVE_ARRIVED=21;

 public static int STATUS_INVOICE_RAISED=22;

 public static int STATUS_JOB_STARTED=6;

 public static int STATUS_JOB_COMPLETED=7;

 public static int STATUS_TIMER_STARTED=15;

 public static int STATUS_TIMER_STOPPED=16;

 public static int STATUS_CANCEL=10;

 public static boolean isAddressSelectable=false;

 public static boolean isAddressEditable=false;

 public static boolean isTabService=true;

 public static boolean isFromLogin=false;

 public interface ACTION
 {
  String MAIN_ACTION = "com.iserve.foregroundservice.action.main";

  String STARTFOREGROUND_ACTION = "com.iserve.foregroundservice.action.startforeground";

  String STOPFOREGROUND_ACTION = "com.iserve.foregroundservice.action.stopforeground";
 }

 public interface NOTIFICATION_ID
 {
  int FOREGROUND_SERVICE = 108;
 }
}
