package com.app.partner.iserve.Utility;

import android.os.AsyncTask;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

/**
 * <h1>Okhttp_connection</h1>
 * <p>
 *     This class simple perform http connection call
 *     by the help of the OkHttpRequestData object.
 *     {@code  OkHttpRequestData data = new OkHttpRequestData()}.
 *     call OkHttp-connection and provide the internet service to this App.
 * </p>
 * @author  3Embed
 * @since  23/11/15.
 * @version 1.0.
 */
public class Okhttp_connection
{

    /**
     * <h1>OkHttpRequestData</h1>
     * <p>
     *     Class is use to hold three parameter i.e String object,RequestBody object and OkHttpRequestCallback
     *     in a single place.
     *     Because async Task takes only single parameter nad i have to send three parameter so.
     *     Wrapping three things into a single object and sending one object to async task.
     * </p>
     * @see RequestBody*/
    private static  class OkHttpRequestData
    {
        String request_Url;
        RequestBody requestBody;
        OkHttpRequestCallback callbacks;
    }
    /**
     * <h2>doOkhttpRequest</h2>
     * <p>
     * This method receive all the data and Store then into to single
     * array of class
     * Service Call using okHttp Request.
     * </p>
     * <p>
     *     this Method Take a Request Body and a url,and OkHttpRequestCallback and does a Asyntask,
     *     and does a request to the given Url
     * </p>
     * @param request_Url contains the url of the given Service link to do performance.
     * @param requestBody contains the require data to send the given Url link.
     * @param callbacks contains the reference to set the call back response to the calling class.*/
    public static void doOkhttpRequest(String request_Url,RequestBody requestBody,OkHttpRequestCallback callbacks)
    {
        OkHttpRequestData data = new OkHttpRequestData();
        data.request_Url = request_Url;
        data.requestBody = requestBody;
        data.callbacks = callbacks;
/**
 * Calling the Async task to perform the Service call.*/
        new OkHttpRequest().execute(data);
    }

    /**
     * <h1>OkHttpRequest</h1>
     * OkHttpRequest extends async task to perform the function indecently .
     * Does a service call using OkHttp client.
     * <P>
     *     This class extends async task and override the method of async task .
     *     on doInBackground method of async task.
     *     performing a service call to th given url and sending data given to the class.
     *     By the help of the OkHttpClient and sending the call back method to the calling Activity by setting
     *     data to the given reference of call-Back Interface object.
     * </P>
     * If Any thing Happened to the service call like Connection Failed or any thin else.
     * Telling to the User that connection is too slow when handling Exception.
     *@see Response
     * @see OkHttpClient
     * */
    private  static class OkHttpRequest extends AsyncTask<OkHttpRequestData, Void, String>
    {

        OkHttpRequestCallback callbacks;
        boolean error =false;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(OkHttpRequestData... params)
        {
            callbacks = params[0].callbacks;
            String result="";
            try
            {
                OkHttpClient okHttpClient=new OkHttpClient();
                okHttpClient.setConnectTimeout(20, TimeUnit.SECONDS);
                okHttpClient.setReadTimeout(20,TimeUnit.SECONDS);
                Request request = new Request.Builder()
                        .url(params[0].request_Url)
                        .post(params[0].requestBody)
                        .build();
                Response response=okHttpClient.newCall(request).execute();
                result=response.body().string();
                response.body().close();
            }
            catch (UnsupportedEncodingException e)
            {

                error= true;
                Utility.printLog("UnsupportedEncodingException"+e.toString());
                result ="Connection Failed..";
                e.printStackTrace();
            }
            catch (IOException e)
            {

                error= true;
                Utility.printLog("Read IO exception"+e.toString());
                result ="Connection is too slow..";
                e.printStackTrace();
            }
            catch (Exception e)
            {

                error= true;
                Utility.printLog("Read Exception"+e.toString());
                result ="Connection is too slow..";
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            if(!error)
            {
                callbacks.onSuccess(result);
            }
            else
            {
                callbacks.onError(result);
            }
        }
    }
    /**
     * interface for Session Call back request
     * */
    public interface OkHttpRequestCallback
    {
        /**
         * Called When Success result of JSON request
         *
         * @param result
         */
        public void onSuccess(String result);


        /**
         * Called When Error result of JSON request
         *
         * @param error
         */
        public void onError(String error);

    }

}
