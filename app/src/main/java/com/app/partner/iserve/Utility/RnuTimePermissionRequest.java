package com.app.partner.iserve.Utility;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by embed on 29/10/16.
 */
public class RnuTimePermissionRequest
{
    private Activity activity;

    public RnuTimePermissionRequest(Activity activity)
    {
        this.activity=activity;
    }

    public boolean mCheckPermission(String[] args)
    {
        int result;
        for(int index=0;index<args.length;index++)
        {
            result= ContextCompat.checkSelfPermission(activity,args[index]);
            if(result!= PackageManager.PERMISSION_GRANTED)
            {
                return false;
            }
        }

        return true;
    }

    public void mRequestPermissions(String[] args,int PERMISSION_REQUEST_CODE)
    {
        ActivityCompat.requestPermissions(activity,args,PERMISSION_REQUEST_CODE);
    }
}
