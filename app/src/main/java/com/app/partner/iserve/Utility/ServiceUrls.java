package com.app.partner.iserve.Utility;

/**
 * Created by embed on 8/1/16.
 */
public class ServiceUrls
{
    public static final String SERVICE_URL="";

    public static final String SOCKET_PATH="";

    public static final String MASTER_LOGIN=SERVICE_URL+"masterLogin";

    public static final String MASTER_SIGNUP1=SERVICE_URL+"masterSignup";

    public static final String GET_CITY_LIST=SERVICE_URL+"getCityList";

    public static final String GET_TYPE_BY_CITY_ID=SERVICE_URL+"getTypeBYCityId";

    public static final String GET_MASTER_APPOINTMENTS_HOME=SERVICE_URL+"getMasterAppointmentsHome";

    public static final String GET_MASTER_APPOINTMENTS_HISTORY=SERVICE_URL+"getMasterAppointmentsHistory";

    public static final String UPDATE_MASTER_STATUS=SERVICE_URL+"updateMasterStatus";

    public static final String UPDATE_APPT_STATUS=SERVICE_URL+"updateApptStatus";

    public static final String RESPOND_TO_APPOINTMENT=SERVICE_URL+"respondToAppointment";

    public static final String LOGOUT=SERVICE_URL+"logout";

    public static final String GET_MASTER_PROFILE=SERVICE_URL+"getMasterProfile";

    public static final String UPDATE_APPT_TIMER=SERVICE_URL+"UpdateApptTimer";

    public static final String ABORT_APPOINTMENT=SERVICE_URL+"abortAppointment";

    public static final String UPDATE_MASTER_PROFILE=SERVICE_URL+"updateMasterProfile";

    public static final String RAISE_INVOICE=SERVICE_URL+"raiseInvoice";

    public static final String ADD_SLOT=SERVICE_URL+"addslot";

    public static final String ADD_PROVIDER_ADDRESS=SERVICE_URL+"AddProviderAddress";

    public static final String GET_ALL_SLOT=SERVICE_URL+"GetAllSlot";

    public static final String GET_PROVIDER_ADDRESS=SERVICE_URL+"GetProviderAddress";

    public static final String DELETE_PROVIDER_ADDRESS=SERVICE_URL+"DeleteProviderAddress";

    public static final String CHECK_MOBILE=SERVICE_URL+"ProviderCheckMobile";

    public static final String VALIDATE_EMAIL_ZIP=SERVICE_URL+"validateEmailZip";

    public static final String GET_CANCELLATION_REASON=SERVICE_URL+"getCancellationReson";

    public static final String FORGOT_PASSWORD=SERVICE_URL+"forgotPassword";

    public static final String GET_FINANCIAL_DATA=SERVICE_URL+"GetFinancialData";

    public static final String GetCategoryDetail=SERVICE_URL+"GetCategoryDetail";

    public static final String UpdateCategoryDetail=SERVICE_URL+"UpdateCategoryDetail";

    public static final String GetBookedSlotDetail=SERVICE_URL+"GetBookedSlotDetail";

    public static final String REMOVE_SLOT=SERVICE_URL+"removeSlot";

    public static final String GET_ALL_MESSAGES=SERVICE_URL+"getallMessages";

    public static final String ADD_BANK_DETAILS=SERVICE_URL+"AddBankDetails";

    public static final String GET_MASTER_BANK_DATA=SERVICE_URL+"getMasterBankData";

    public static final String DELETE_MASTER_BANK=SERVICE_URL+"deleteMasterBank";

    public static final String BANK_ACCOUNT_DEFAULT=SERVICE_URL+"BankAccountDefault";

}
