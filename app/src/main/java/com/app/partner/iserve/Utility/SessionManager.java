package com.app.partner.iserve.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by embed on 28/1/16.
 */
public class SessionManager
{
    private Context context;
    public static final String PREF_NAME = "ISERVE_PRO";
    public static int PRIVATE_MODE = 0;
    SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public static String PUSH_TOKEN = "push_token";
    public static String DEVICE_ID = "device_id";
    public static String SESSIONTOKEN= "session_token";
    public static String CURRENT_LAT= "current_lat";
    public static String CURRENT_LNG= "current_long";
    public static String SERVER_CHN= "server_chn";
    public static String PROVIDER_CHN= "provider_chn";
    public static String MY_LISTNER_CHN= "provider_lstn_chn";
    public static String MY_EMAIL= "provider_email";
    public static String MY_ID= "provider_id";
    public static String TIME_WHILE_PAUSED= "time_while_paused";
    public static String TIME_ELAPSED= "time_elapsed";
    public static String IS_TIMER_STARTED= "timer_started";
    public static String DURATION= "duration";
    public static String PASSWORD= "password";
    public static String BOOKING_DATA= "booking_data";
    public static String BOOKING_REQUSTED= "booking_requested";

    private String IS_USER_LOGGED_IN ="isuserloggedin";

    public SessionManager(Context context)
    {
        this.context=context;
        sharedPreferences=context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = sharedPreferences.edit();
        editor.commit();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *
     * @return pushToken
     */
    public String getPushToken() {
        return sharedPreferences.getString(PUSH_TOKEN, null);
    }

    public void setPushToken(String pushToken)
    {
        editor.putString(PUSH_TOKEN, pushToken);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *
     * @return deviceID
     */
    public String getDeviceId() {
        return sharedPreferences.getString(DEVICE_ID, null);
    }

    public void setDeviceId(String pushToken)
    {
        editor.putString(DEVICE_ID, pushToken);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *<p>creates session after user logged in<p/>
     */
    public void createSession()
    {
        editor.putBoolean(IS_USER_LOGGED_IN, true);
        editor.commit();
    }

    public boolean isUserLoggedIn()
    {
        return sharedPreferences.getBoolean(IS_USER_LOGGED_IN, false);
    }

    public void clearSession()
    {
        editor.putBoolean(IS_USER_LOGGED_IN, false);
        editor.clear();
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @return session token
     */
    public String getSessionToken()
    {
        return sharedPreferences.getString(SESSIONTOKEN,null);
    }
    public void setSessionToken(String session)
    {
        editor.putString(SESSIONTOKEN,session);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @return current lat
     */
    public String getProviderCurrentlat()
    {
        return sharedPreferences.getString(CURRENT_LAT,"0");
    }
    public void setProviderCurrentlat(String session)
    {
        editor.putString(CURRENT_LAT,session);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @return current long
     */
    public String getProviderCurrentlong()
    {
        return sharedPreferences.getString(CURRENT_LNG,"0");
    }

    public void setProviderCurrentlong(String session)
    {
        editor.putString(CURRENT_LNG,session);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    /**server channel will be set from the login response
     *
     * @return Server Channel
     */
    public String getServerChannel()
    {
        return sharedPreferences.getString(SERVER_CHN,null);
    }
    public void setServerChannel(String session)
    {
        editor.putString(SERVER_CHN,session);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**Provider channel will be set from the login response
     *
     * @return Provider Channel
     */
    public String getMyChannel()
    {
        return sharedPreferences.getString(PROVIDER_CHN,null);
    }
        public void setMyChannel(String session)
        {
            editor.putString(PROVIDER_CHN,session);
            editor.commit();
        }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**Provider listner channel will be set from the login response
     *
     * @return Provider listner Channel
     */
    public String getMyListnerChannel()
    {
        return sharedPreferences.getString(MY_LISTNER_CHN,null);
    }
    public void setMyListnerChannel(String session)
    {
        editor.putString(MY_LISTNER_CHN,session);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *
     * @return Provider provider mail id
     */
    public String getProviderEmail()
    {
        return sharedPreferences.getString(MY_EMAIL,null);
    }
    public void setProviderEmail(String email)
    {
        editor.putString(MY_EMAIL,email);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *provider id will be set in the login response
     * @return Provider provider  id
     */
    public String getProviderID()
    {
        return sharedPreferences.getString(MY_ID,null);
    }
    public void setProviderID(String email)
    {
        editor.putString(MY_ID,email);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *time while paused  will be set in the jobCompleted activity
     * @return *time while paused
     */
    public long getTimeWhilePaused()
    {
        return sharedPreferences.getLong(TIME_WHILE_PAUSED,0);
    }
    public void setTimeWhilePaused(long time)
    {
        editor.putLong(TIME_WHILE_PAUSED,time);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *TIME_ELAPSED will be set in the jobCompleted activity
     * @return *TIME_ELAPSED
     */
    public long getElapsedTime()
    {
        return sharedPreferences.getLong(TIME_ELAPSED,0);
    }
    public void setElapsedTime(long time)
    {
        editor.putLong(TIME_ELAPSED,time);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *IS_TIMER_STARTED will be set in the jobCompleted activity
     * @return *IS_TIMER_STARTED
     */
    public void setTimerStarted(boolean started)
    {
        editor.putBoolean(IS_TIMER_STARTED, started);
        editor.commit();
    }

    public boolean isTimerStarted()
    {

        return  sharedPreferences.getBoolean(IS_TIMER_STARTED, false);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *DURATION will be set in the jobCompleted activity
     * @return *DURATION
     */
    public long getDuration()
    {
        return sharedPreferences.getLong(DURATION,0);
    }
    public void setDuration(long time)
    {
        editor.putLong(DURATION,time);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *PASSWORD will be set in the Login Response
     * @return *DURATION
     */
    public String getPassword()
    {
        return sharedPreferences.getString(PASSWORD,"");
    }
    public void setPassword(String pass)
    {
        editor.putString(PASSWORD,pass);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  set in ForgroundService
     * @return Booking Details
     */
    public String getBookingData() {
        return sharedPreferences.getString(BOOKING_DATA, "");
    }

    public void setBookingData(String data)
    {
        editor.putString(BOOKING_DATA, data);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public int getLastBookingRequested()
    {
        return sharedPreferences.getInt(BOOKING_REQUSTED, -1);
    }

    public void setLastBookingRequested(int bid)
    {
        editor.putInt(BOOKING_REQUSTED, bid);
        editor.commit();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
}
