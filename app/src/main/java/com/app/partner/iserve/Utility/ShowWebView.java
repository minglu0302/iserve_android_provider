package com.app.partner.iserve.Utility;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.app.partner.iserve.R;

@SuppressLint("SetJavaScriptEnabled")
public class ShowWebView extends AppCompatActivity
{

	//private Button button;
	private WebView webView;
	private ProgressDialog progressDialog;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_web_view);

		Bundle extas = getIntent().getExtras();

		String url=extas.getString("LINK");
		if (progressDialog == null)
		{
			// in standard case YourActivity.this
			progressDialog = new ProgressDialog(ShowWebView.this,5);
			progressDialog.setCancelable(true);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		webView = (WebView) findViewById(R.id.webView1);
		startWebView(url);

	}

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) 
	{
		switch (item.getItemId()) 
		{
		case android.R.id.home:

			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private void startWebView(String url)
	{
		webView.setWebViewClient(new WebViewClient()
		{     
			//If you will not use this method url links are open in new browser not in web view
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{             
				view.loadUrl(url);
				return true;
			}

			//Show loader on url load
			public void onLoadResource (WebView view, String url)
			{

			}
			public void onPageFinished(WebView view, String url) {
				try
				{
					if (progressDialog.isShowing())
					{
						progressDialog.dismiss();
						progressDialog = null;
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}

		});

		// Javascript inabled on webview 
		webView.getSettings().setJavaScriptEnabled(true);


		//Load url in webview
		webView.loadUrl(url);
	}

	// Open previous opened link from history on webview when back button pressed

	@Override
	// Detect when the back button is pressed
	public void onBackPressed()
	{
		if(webView.canGoBack()) 
		{
			webView.goBack();
		}
		else
		{
			// Let the system handle the back button
			super.onBackPressed();
		}
	}
	@Override
	protected void onStop() 
	{
		super.onStop();

	}
	@Override
	protected void onStart()
	{
		super.onStart();
	}
}