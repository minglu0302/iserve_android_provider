package com.app.partner.iserve.Utility;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.app.partner.iserve.R;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okio.Buffer;


/**
 * Created by embed on 11/1/16.
 */
public class Utility implements LocationListener {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static long MIN_TIME_FOR_UPDATE = 1000 * 60 * 2;
    private static long MIN_DISTANCE_FOR_UPDATE = 10;
    private static Location location;
    private static LocationManager locationManager;
    private static String PREFS_NAME="preference";
    /**
     *
     * @param msg
     * <p> used to print the logs <p/>
     */
    public static void printLog(String... msg) {
        String str = "";
        for (String i : msg) {
            str = str + "\n" + i;
        }
        if (true) {
            Log.d("IServePro", str);
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param context
     * @return string
     * <p>
     * Uses Telephony services and returns the device id
     * <p/>
     */
    public static String getDeviceId(Context context)
    {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param emailStr
     * @return boolean
     * <p>Checks the mail id by comparing with regular expression <p/>
     *
     */

    public static boolean validateEmail(String emailStr)
    {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

///////////////////////////////////////////////////////////////////////////////////////////////////

   /* public static AlertDialog.Builder getAlertDialogBuilder(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("title");
        builder.setMessage("message");
        builder.setPositiveButton(context.getResources().getString(R.string.ok), null);
        builder.setNegativeButton(context.getResources().getString(R.string.cancel), null);
        return builder;
    }*/

    public static AlertDialog.Builder getAlertDialogBuilder(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context,5);
        builder.setTitle("title");
        builder.setMessage("message");
        return builder;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static String getCurrentGmtTime() {
        String curentdateTime = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        curentdateTime = sdf.format(new Date());

        return curentdateTime;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static String getGmtTime() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formater.format(date);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ProgressDialog getProcessDialog(Activity activity) {
        // prepare the dialog box
        ProgressDialog dialog = new ProgressDialog(activity);
        // make the progress bar cancelable
        dialog.setCancelable(true);
        // set a message text
        dialog.setMessage("Loading...");
        // show it
        return dialog;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * <h>IsNetworkAvailable</h>
     * @param context
     * @return boolean
     */
   /* public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }*/

////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * <p>
     * This method is used to get current lat & lng
     * </p>
     *
     * @param cntx
     * @return
     */
    public static double[] getLocation(Context cntx) {
        double[] location = new double[2];

        Location gpsLocation = findLocation(cntx, LocationManager.GPS_PROVIDER);

        Location nwLocation = findLocation(cntx, LocationManager.NETWORK_PROVIDER);

        if (nwLocation != null) {
            location[0] = nwLocation.getLatitude();
            location[1] = nwLocation.getLongitude();
            printLog("data network" + location[0] + "<---->" + location[1]);
        } else {
            if (gpsLocation != null) {
                location[0] = gpsLocation.getLatitude();
                location[1] = gpsLocation.getLongitude();
                printLog("data gps" + location[0] + "<---->" + location[1]);
            } else {
                showSettingsAlert(cntx);
            }
        }
        return location;
    }

    private static Location findLocation(Context cntx, String provider) {
        Utility obj = new Utility();

        locationManager = (LocationManager) cntx.getSystemService(Service.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(provider)) {
            if (ActivityCompat.checkSelfPermission(cntx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(cntx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            locationManager.requestLocationUpdates(provider,
                    MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, obj);
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(provider);
                return location;
            }
        }
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public static void showSettingsAlert(final Context mContext) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS or Netwrok is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog


        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
///////////////////////////////////////////////////////////////////////////////////////////////////
    public static void printReq(RequestBody requestBody, String forr){
        Buffer sink = new Buffer();
        try {
            requestBody.writeTo(sink);
            byte[] bArr=sink.readByteArray();
            String req = new String(bArr);
            printLog("Printing Okhttp Req "+forr+ " " + req);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public static boolean isNetworkAvailable(Context context)
    {

        ConnectivityManager connectivity  = null;
        boolean isNetworkAvail = false;

        try
        {
            connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connectivity != null)
            {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();

                if (info != null)
                {
                    for (int i = 0; i < info.length; i++)
                    {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if(connectivity !=null)
            {
                connectivity = null;
            }
        }
        return isNetworkAvail;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void ShowAlert(String msg, Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Note");
        alertDialogBuilder.setMessage(msg).setCancelable(false)
                .setNegativeButton("ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    static public boolean setPreference(Context c, String value, String key) {
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        settings = c.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    static public String getPreference(Context c, String key) {
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        settings = c.getSharedPreferences(PREFS_NAME, 0);
        String value = settings.getString(key, "");
        return value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public static boolean isMyServiceRunning(Context context,Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////
    public static boolean isApplicationSentToBackground(final Context context)
    {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty())
        {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName()))
            {
                return true;
            }
        }
        return false;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////

    public static Typeface getFontRegular(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(),"fonts/Muli.ttf");
    }
    public static Typeface getFontBold(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(),"fonts/Muli-Bold.ttf");
    }

///////////////////////////////////////////////////////////////////////////////////////////////////
    public static void mShowMessage(String title ,String msg, Activity activity)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,5);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(activity.getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();

                dialogInterface.dismiss();

            }
        });

        builder.show();
    }

    public static String sentingDateFormat(int year, int monthOfYear, int dayOfMonth)
    {
        monthOfYear=monthOfYear+1;
        String month=""+monthOfYear;
        String day=""+dayOfMonth;
        if(monthOfYear<=9)
        {
            month=0+month;
        }
        if(dayOfMonth<=9)
        {
            day=0+day;
        }
        String dateFormat=month+"/"+day+"/"+year;

        return dateFormat;
    }

    public static  String displayDateFormat(int year, int monthOfYear, int dayOfMonth)
    {
        monthOfYear=monthOfYear+1;
        String month=""+monthOfYear;
        String day=""+dayOfMonth;
        /*if(monthOfYear<=9)
        {
            month=0+month;
        }*/
        if(dayOfMonth<=9)
        {
            day=0+day;
        }
        switch (monthOfYear) {
            case 1:
                month="Jan";
                break;

            case 2:
                month="Feb";
                break;

            case 3:
                month="Mar";
                break;

            case 4:
                month="Apr";
                break;

            case 5:
                month="May";
                break;
            case 6:
                month="Jun";
                break;

            case 7:
                month="Jul";
                break;

            case 8:
                month="Aug";
                break;

            case 9:
                month="Sep";
                break;

            case 10:
                month="Oct";
                break;

            case 11:
                month="Nov";
                break;

            case 12:
                month="Dec";
                break;

            default:
                break;
        }
        String dateFormat=day+"-"+month+"-"+year;
        return dateFormat;
    }

}
