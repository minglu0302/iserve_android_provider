package com.app.partner.iserve.ViewPagerFrag;


import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import com.app.partner.iserve.Pojo.CurrentCycle;
import com.app.partner.iserve.Pojo.GetFinancialDataResponse;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.AppConstants;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentCycleFrag extends Fragment {


    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;
    private View view;
    private TextView tvStartTime,
            tvEndTime,
            tvAvgRating,
            tvAccetanceRate,
            tvTotalBookings,
            tvEarnings,
            tvOpenningBal,
            tvClosedBal,
            tvCompletedBookings,
            tvRejectedBookings,
            tvIgnoredBookings,
            tvCancelledBookings,
            tvStartTimeHeader,
            tvEndTimeHeader,
            tvAvgRatingHeader,
            tvAcceptanceRateHeader,
            tvTotalBookingsHeader,
            tvEarningHeader,
            tvOpenningBalHeader,
            tvClosedBalHeader,
            tvCompltdBookingsHeader,
            tvRejectedBookingsHeader,
            tvIgnoredBookingsHeader,
            tvCancelledBookingsHeader;
    private String Value;
    private Typeface fontBold,font;

    public CurrentCycleFrag()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        sessionManager=new SessionManager(getActivity());
        mProgressDailog=Utility.getProcessDialog(getActivity());
        view = inflater.inflate(R.layout.fragment_payments_logs, container, false);
        mInitLayoutId(view);
        mProgressDailog=Utility.getProcessDialog(getActivity());
        mGetFinancialData();
        return view;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mInitLayoutId(View view)
    {
        font=Utility.getFontRegular(getActivity());
        fontBold=Utility.getFontBold(getActivity());

        tvStartTime= (TextView) view.findViewById(R.id.tvStartTimeValue);
        tvStartTime.setTypeface(font);

        tvEndTime= (TextView) view.findViewById(R.id.tvEndTimeValue);
        tvEndTime.setTypeface(font);

        tvAvgRating= (TextView) view.findViewById(R.id.tvAvgRatingValue);
        tvAvgRating.setTypeface(font);

        tvAccetanceRate= (TextView) view.findViewById(R.id.tvAcceptanceRateValue);
        tvAccetanceRate.setTypeface(font);

        tvTotalBookings= (TextView) view.findViewById(R.id.tvTotalBookingsValue);
        tvTotalBookings.setTypeface(font);

        tvEarnings= (TextView) view.findViewById(R.id.tvEarningValue);
        tvEarnings.setTypeface(font);

        tvOpenningBal= (TextView) view.findViewById(R.id.tvOpenningBalValue);
        tvOpenningBal.setTypeface(font);

        tvClosedBal= (TextView) view.findViewById(R.id.tvClosedBalValue);
        tvClosedBal.setTypeface(font);

        tvCompletedBookings= (TextView) view.findViewById(R.id.tvCompltdBookingsValue);
        tvCompletedBookings.setTypeface(font);

        tvRejectedBookings= (TextView) view.findViewById(R.id.tvRejectedBookingsValue);
        tvRejectedBookings.setTypeface(font);

        tvIgnoredBookings= (TextView) view.findViewById(R.id.tvIgnoredBookingsValue);
        tvIgnoredBookings.setTypeface(font);

        tvCancelledBookings= (TextView) view.findViewById(R.id.tvCancelledBookingsValue);
        tvCancelledBookings.setTypeface(font);

        tvStartTimeHeader= (TextView) view.findViewById(R.id.tvStartTimeHeader);
        tvStartTimeHeader.setTypeface(font);

        tvEndTimeHeader= (TextView) view.findViewById(R.id.tvEndTimeHeader);
        tvEndTimeHeader.setTypeface(font);

        tvAvgRatingHeader= (TextView) view.findViewById(R.id.tvAvgRatingHeader);
        tvAvgRatingHeader.setTypeface(font);

        tvAcceptanceRateHeader= (TextView) view.findViewById(R.id.tvAcceptanceRateHeader);
        tvAcceptanceRateHeader.setTypeface(font);

        tvTotalBookingsHeader= (TextView) view.findViewById(R.id.tvTotalBookingsHeader);
        tvTotalBookingsHeader.setTypeface(font);

        tvEarningHeader= (TextView) view.findViewById(R.id.tvEarningHeader);
        tvEarningHeader.setTypeface(font);

        tvOpenningBalHeader= (TextView) view.findViewById(R.id.tvOpenningBalHeader);
        tvOpenningBalHeader.setTypeface(font);

        tvClosedBalHeader= (TextView) view.findViewById(R.id.tvClosedBalHeader);
        tvClosedBalHeader.setTypeface(font);

        tvCompltdBookingsHeader= (TextView) view.findViewById(R.id.tvCompltdBookingsHeader);
        tvCompltdBookingsHeader.setTypeface(font);

        tvRejectedBookingsHeader= (TextView) view.findViewById(R.id.tvRejectedBookingsHeader);
        tvRejectedBookingsHeader.setTypeface(font);

        tvIgnoredBookingsHeader= (TextView) view.findViewById(R.id.tvIgnoredBookingsHeader);
        tvIgnoredBookingsHeader.setTypeface(font);

        tvCancelledBookingsHeader= (TextView) view.findViewById(R.id.tvCancelledBookingsHeader);
        tvCancelledBookingsHeader.setTypeface(font);

    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mGetFinancialData()
    {
        if(Utility.isNetworkAvailable(getActivity()))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing()){
                mProgressDailog.show();
            }
            final RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_pro_id",sessionManager.getProviderID())
                    .add("ent_date_time", Utility.getCurrentGmtTime())
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_FINANCIAL_DATA, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {

                    if(mProgressDailog!=null && mProgressDailog.isShowing())
                    {
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK getfinancialdata response"+result);
                    Gson gson=new Gson();
                    GetFinancialDataResponse response=gson.fromJson(result,GetFinancialDataResponse.class);
                    if(response.getErrFlag().equals("0")&& response.getErrNum().equals("21"))
                    {
                        mSetValues(response.getCurrentCycle());
                    }
                }

                @Override
                public void onError(String error) {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK error"+error);
                }
            });
        }
        else
        {
            Snackbar.make(getActivity().getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                           mGetFinancialData();
                        }
                    }).show();
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    public void mSetValues(CurrentCycle currentCycle)
    {
        String date[]=currentCycle.getStart_date().split(" ");
        tvStartTime.setText(date[1]);

        tvAvgRating.setText(currentCycle.getRev_rate());

        tvAccetanceRate.setText(currentCycle.getAcpt_rate()+"%");

        tvTotalBookings.setText(currentCycle.getTotal_bookings());

        if(!"".equals(currentCycle.getBooking_earn()))
        {
            Value = String.format("%.2f", Double.parseDouble(currentCycle.getBooking_earn()));
            tvEarnings.setText( AppConstants.CURRENCY+" "+Value);
        }
        if(!"".equals(currentCycle.getOpening_balance()))
        {
            Value = String.format("%.2f", Double.parseDouble(currentCycle.getOpening_balance()));
            tvOpenningBal.setText( AppConstants.CURRENCY+" "+Value);
        }
        if(!"".equals(currentCycle.getClosing_balance()))
        {
            Value = String.format("%.2f", Double.parseDouble(currentCycle.getClosing_balance()));
            tvClosedBal.setText( AppConstants.CURRENCY+" "+Value);
        }

        tvCompletedBookings.setText(currentCycle.getTotal_accepted());

        tvRejectedBookings.setText(currentCycle.getTotal_reject());

        tvIgnoredBookings.setText(currentCycle.getTotal_ignore());

        tvCancelledBookings.setText(currentCycle.getTot_cancel());
    }
}
