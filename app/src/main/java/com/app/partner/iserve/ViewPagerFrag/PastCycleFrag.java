package com.app.partner.iserve.ViewPagerFrag;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.partner.iserve.Adapters.PastCycleAdapter;
import com.app.partner.iserve.Pojo.GetFinancialDataResponse;
import com.app.partner.iserve.Pojo.PastCycle;
import com.app.partner.iserve.R;
import com.app.partner.iserve.Utility.Okhttp_connection;
import com.app.partner.iserve.Utility.ServiceUrls;
import com.app.partner.iserve.Utility.SessionManager;
import com.app.partner.iserve.Utility.Utility;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;


public class PastCycleFrag extends Fragment {


    private View view;

    private ProgressDialog mProgressDailog;
    private SessionManager sessionManager;
    private RecyclerView recyclerView;
    private PastCycleAdapter pastCycleAdapter;
    private ArrayList<PastCycle> list=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_past_cycle, container, false);
        mInitLayoutId(view);
        mProgressDailog=Utility.getProcessDialog(getActivity());
        sessionManager=new SessionManager(getActivity());
        mGetFinancialData();
        return view;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
   private void mInitLayoutId(View view)
   {
       recyclerView= (RecyclerView) view.findViewById(R.id.pastCycleRecycler);
       pastCycleAdapter=new PastCycleAdapter(getActivity(),list);

       LinearLayoutManager llm = new LinearLayoutManager(getActivity());
       llm.setOrientation(LinearLayoutManager.VERTICAL);
       recyclerView.setLayoutManager(llm);

       recyclerView.setAdapter(pastCycleAdapter);
   }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mGetFinancialData()
    {
        if(Utility.isNetworkAvailable(getActivity()))
        {
            if(mProgressDailog!=null && !mProgressDailog.isShowing()){
                mProgressDailog.show();
            }
            final RequestBody requestBody= new FormEncodingBuilder()
                    .add("ent_sess_token",sessionManager.getSessionToken())
                    .add("ent_dev_id",sessionManager.getDeviceId())
                    .add("ent_pro_id",sessionManager.getProviderID())
                    .add("ent_date_time", Utility.getCurrentGmtTime())
                    .build();

            Okhttp_connection.doOkhttpRequest(ServiceUrls.GET_FINANCIAL_DATA, requestBody, new Okhttp_connection.OkHttpRequestCallback() {
                @Override
                public void onSuccess(String result)
                {

                    if(mProgressDailog!=null && mProgressDailog.isShowing())
                    {
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK getfinancialdata response"+result);
                    Gson gson=new Gson();
                    GetFinancialDataResponse response=gson.fromJson(result,GetFinancialDataResponse.class);
                    if(response.getErrFlag().equals("0")&& response.getErrNum().equals("21"))
                    {
                        mSetValues(response.getPastCycle());
                    }
                }

                @Override
                public void onError(String error) {
                    if(mProgressDailog!=null && mProgressDailog.isShowing()){
                        mProgressDailog.dismiss();
                    }
                    Utility.printLog("PK error"+error);
                }
            });
        }
        else
        {
            Snackbar.make(getActivity().getCurrentFocus(), getResources().getString(R.string.network_check), Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mGetFinancialData();
                        }
                    }).show();
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    public void mSetValues(ArrayList<PastCycle> pastCycle)
    {
        if(pastCycle.size()>0)
        {
            list.clear();
            list.addAll(pastCycle);
            pastCycleAdapter.notifyDataSetChanged();
        }
    }
}
