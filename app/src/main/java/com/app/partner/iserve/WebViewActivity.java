package com.app.partner.iserve;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.partner.iserve.Utility.Utility;

public class WebViewActivity extends AppCompatActivity {

    private  ProgressDialog progressDialog;
    private WebView webView;
    private ImageView ivBackbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        Bundle extas = getIntent().getExtras();
        String url=extas.getString("LINK");
        String title=extas.getString("TEXTSTRING");

        Typeface font=Utility.getFontRegular(WebViewActivity.this);
        Typeface fontBold=Utility.getFontBold(WebViewActivity.this);

        ivBackbtn=(ImageView)findViewById(R.id.ivBackbtn);
        ivBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView mToolbar_title = (TextView) findViewById(R.id.mToolbar_title);
        mToolbar_title.setTypeface(fontBold);
        mToolbar_title.setText(title);

        if (progressDialog == null)
        {
            // in standard case YourActivity.this
            progressDialog = Utility.getProcessDialog(WebViewActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }
        webView = (WebView) findViewById(R.id.webView1);
        startWebView(url);
    }


    private void startWebView(String url)
    {
        webView.setWebViewClient(new WebViewClient()
        {
            //If you will not use this method url links are open in new browser not in web view
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource (WebView view, String url)
            {

            }
            public void onPageFinished(WebView view, String url) {
                try
                {
                    if (progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }

        });

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);


        //Load url in webview
        webView.loadUrl(url);
    }
}
